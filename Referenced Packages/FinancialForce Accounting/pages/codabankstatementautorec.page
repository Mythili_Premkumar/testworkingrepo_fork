<apex:page tabStyle="c2g__codaBankStatement__c" sidebar="false" standardcontroller="c2g__codaBankStatement__c" extensions="c2g.CODABankStatementController" action="{!setAndCheckCanReconcile}">
	<link type="text/css" href="{!URLFOR($Resource.front_bankrec,'front_bankrec/pages/autorec/autorec.css')}" rel="stylesheet"/>
	<link type="text/css" href="{!URLFOR($Resource.front_bankrec,'front_bankrec/components/UberPopup/uberpopup.css')}" rel="stylesheet"/>
	<link type="text/css" href="{!URLFOR($Resource.front_bankrec,'front_bankrec/components/UberFilter/uberfilter.css')}" rel="stylesheet"/>
	<script type="text/javascript" src="{!URLFOR($Resource.front_bankrec,'front_bankrec/base/frameworks/jquery-1.5.min.js')}"></script>
	<script type="text/javascript">
		var $FFDC = jQuery.noConflict();
	</script>
	<script type="text/javascript" src="{!URLFOR($Resource.front_bankrec,'front_bankrec/pages/autorec/autorec.js')}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.front_bankrec,'front_bankrec/components/UberPopup/uberpopup.js')}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.front_bankrec,'front_bankrec/components/UberFilter/uberfilter.js')}"></script>
	<apex:stylesheet value="{!URLFOR($Resource.c2g__coda2go,'coda2go/web/css/style.css')}"/>
	<style>
		.pbError{
	    	background-image:url({!URLFOR($Resource.coda2go, 'coda2go/web/images/pbErrorIcon.gif')});
	    }
	</style>
	<apex:form >
		
		<apex:sectionHeader title="{!$Label.codaAutoRecFormTitle}" subtitle="{!c2g__codaBankStatement__c.Name}" help="{!URLFOR('/apex/c2g__codahelploader?topicid=1083')}"/>
		<apex:pageblock id="reconcillingNotAllowedBlock" mode="edit" rendered="{!!canReconcile}">
			<apex:pageBlockButtons >
				<apex:commandButton action="{!cancel}" value="{!$Label.codaAutoRecCancelBtn}"/>
			</apex:pageBlockButtons>
			<apex:outputPanel id="messagePanel" layout="block">
                <apex:outputPanel id="errorPanel" layout="block" styleClass="pbError">
                    <apex:repeat value="{!viewstate.errorMessages}" var="errorMessage">
                       	<apex:outputText value="{!errorMessage}"/><br/>
                   	</apex:repeat>
                </apex:outputPanel>
            </apex:outputPanel>
		</apex:pageblock>
		
		<div id="autoRecWrapper" style="position:relative">
		<apex:pageBlock title="{!$Label.codaAutoRecBlockTitle}" mode="edit" rendered="{!canReconcile}">
			<apex:pageBlockButtons >
				<apex:commandButton action="{!cancel}" value="{!$Label.codaAutoRecCancelBtn}"/>
			</apex:pageBlockButtons>
			
			<apex:outputPanel id="reconcileMessagePanel" layout="block">
                <apex:outputPanel id="reconcileErrorPanel" layout="block" styleClass="pbError" rendered="{!viewstate.showErrorMessages}">
                    <apex:repeat value="{!viewstate.errorMessages}" var="reconcileErrorMessage" rendered="{!viewstate.showErrorMessages}">
                        <apex:outputText value="{!reconcileErrorMessage}"/><br/>
                    </apex:repeat>
                </apex:outputPanel>
            </apex:outputPanel> 
			
			<apex:pageBlockSection columns="1">
				<apex:outputPanel layout="block" styleClass="bankRecWrapper">
					<table cellPadding="0" cellSpacing="0" border="0" width="100%">
						<tr>
							<td>
								<div class="matchingColumns">
									<div class="matchingColumnsOverlay">
										<div class="matchingColumnsOverlayBg"></div>
										<div class="matchingColumnsOverlayWaiting"></div>
									</div>
									<div class="leftColumn column">
										<div class="header">
											{!$Label.codaAutoRecBankStatementLines}
										</div>
										<div class="filterCell">
											<div class="filterInput">
												<input type="text" id="filterBankLinesInput"/>
												<div class="clear"></div>
											</div>
										</div>
										<div class="createTransaction" title="{!$Label.codaCreateTransactionFromBankStatement}"></div>
										<div class="scroller">
											<div class="lineItems lineItemsLeft" id="bankLines">
												<apex:variable var="linesChecked" value="{!0}"/>
												
												<apex:repeat var="sl" value="{!viewState.BankStatement.BankStatementLineItems}">
													<div class="lineItem {!IF(sl.isChecked,'selected','')}">
														<div class="id" style="display:none">{!sl.Dto.id}</div>
														<div class="date"><apex:outputField value="{!sl.Dto.c2g__Date__c}"/></div>
														<div class="ref"><apex:outputField value="{!sl.Dto.c2g__Reference__c}"/></div>
														<div class="desc"><apex:outputField value="{!sl.Dto.c2g__Description__c}"/></div>
														<div class="amount {!IF(sl.Dto.Amount__c<0,'debit','credit')}">
														<apex:outputText value="{0,number,###############0.00}">
						                                        <apex:param value="{!sl.Dto.c2g__Amount__c}"/>
						                                    </apex:outputText>
														</div>
														<div class="status">
															<apex:inputCheckBox value="{!sl.isChecked}" styleClass="statusInput"/>
															<apex:outputPanel layout="none" rendered="{!sl.isChecked}">
																<apex:variable var="linesChecked" value="{!linesChecked+1}"/>
															</apex:outputPanel>
														</div>
													</div>
												</apex:repeat>
											</div>
										</div>
										<div class="footer">
										<span class="noChecked">
											<apex:outputText value="{0,number,#,###,###,###,###,##0}">
		                                        <apex:param value="{!linesChecked}"/>
		                                    </apex:outputText>
	                                    </span>&nbsp;{!$Label.codaAutoRecLinesSelected}<span class="balance">
	                                    <apex:outputText value="{0,number,###############0.00}">
	                                    	<apex:param value="{!viewState.BankBalance}"/>
	                                    </apex:outputText>
	                                    </span></div>
									</div>
									<div Class="rightColumn column">
										<div class="header">
											{!$Label.codaAutoRecTransactions}
										</div>
										<div class="filterCell">
											<div class="filterInput">
												<input type="text" id="filterTransactionLinesInput"/>
												<div class="clear"></div>
											</div>
										</div>
										<apex:variable var="transactionsChecked" value="{!0}"/>
										<div class="transactionsLoader">
											<div class="background"></div>
											<div class="icon"></div>
										</div>
										<apex:outputPanel id="transactionsColumn" styleClass="scroller">
											<div class="lineItems lineItemsRight" id="transactionLines">
												
												<apex:repeat var="tl" value="{!viewState.BankStatement.TransactionLineItems}">
													<div class="lineItem {!IF(tl.isChecked,'selected','')}">
														<div class="date"><apex:outputField value="{!tl.Dto.c2g__DueDate__c}"/></div>
														<div class="ref"><apex:outputField value="{!tl.Dto.c2g__LineReference__c}"/></div>
														<div class="amount {!IF(tl.Dto.BankAccountValue__c<0,'debit','credit')}">
															<apex:outputText value="{0,number,###############0.00}">
						                                        <apex:param value="{!tl.Dto.c2g__BankAccountValue__c}"/>
						                                    </apex:outputText>
														</div>
														<div class="transaction"><apex:outputText value="{!tl.TransactionName}" /></div>
														<div class="status">
															<apex:inputCheckBox value="{!tl.isChecked}" styleClass="statusInput"/>
															<apex:outputPanel layout="none" rendered="{!tl.isChecked}">
																<apex:variable var="transactionsChecked" value="{!transactionsChecked+1}"/>
															</apex:outputPanel>
														</div>
													</div>
												</apex:repeat>
											</div>
										</apex:outputPanel>
										<div class="footer">
										<span class="noChecked">
											<apex:outputText value="{0,number,#,###,###,###,###,##0}">
		                                        <apex:param value="{!transactionsChecked}"/>
		                                    </apex:outputText>
										</span>&nbsp;{!$Label.codaAutoRecLinesSelected}<span class="balance">
										<apex:outputText value="{0,number,###############0.00}">
	                                    	<apex:param value="{!viewState.TranBalance}"/>
	                                    </apex:outputText>
										</span></div>
									</div>
								</div>
							</td>
							<td class="actionsColumn">
								<div class="actionsWrapper">
									<span class="balanceTitle">{!$Label.codaAutoRecBalance}</span>
									<div class="reconcileBalanceWrapper">
										<div class="reconcileBalance doesNotBalance">0.00</div>
									</div>
									<apex:commandButton id="commitButton_h" action="{!commitSelectedLines}" styleClass="hiddensubmit"/>
									<div class="button buttonBig btn" id="commitButton" onclick="document.getElementById('{!$Component.commitButton_h}').click();" style="font-size:13px !important;padding:4px;margin-top:15px;width:220px;margin-left:0;margin-right:0;font-weight:400;">
										{!$Label.codaAutoRecCommitSelectedLines}
									</div>
									
									<apex:outputPanel style="position:absolute;bottom:8px;" layout="block" rendered="{!viewState.showCompleteReconciliationButton}">
										<apex:commandButton action="{!completeReconciliation}" value="{!$Label.codaBankStatementCompleteReconciliation}" style="font-size:13px !important;padding:4px;margin-top:15px;width:220px;margin-left:0;margin-right:0;font-weight:400;width:230px;"/>
									</apex:outputPanel>
									
								</div>
							</td>
						</tr>
					</table>
				</apex:outputPanel>
				<!-- Dialog box -->
                <apex:outputPanel layout="none">
                    <apex:outputPanel id="asyncronousPanel" layout="block" rendered="{!!viewstate.CommitFinished}">
                        <style type="text/css"> body .dialogWrapper{ position:absolute;}</style>
                        <script type="text/javascript">
                            
                            $FFDC(document).ready(function () {
                                var waitContent = $FFDC(document.createElement('div')).css({'text-align':'center'});
                                var waitContentDiv = $FFDC(document.createElement('div')).appendTo(waitContent);
                                waitContentDiv.html("<h3 style='display:block;margin:20px;'>{!$Label.codaBankStatementAutoRecProcessing}</h3><img style='margin-bottom:20px;' src='/img/loading.gif'/>");
								
								$FFDC('#autoRecWrapper').UberPopup({
									popupTitle: '{!$Label.codaProcessing}',
									popupContent: waitContent,
									popupWidth: 360,
									popupShowClose: false
								});
							});
                        </script>
                        <apex:actionPoller action="{!viewstate.checkCommitFinished}" oncomplete="if({!viewstate.CommitFinished}){closeDialogBox()};" enabled="{!!viewstate.CommitFinished}" interval="5"/>
                    </apex:outputPanel>
                    <script type="text/javascript">
	                    function closeDialogBox(){
	                        window.location = window.location;
	                    }
                    </script>
                </apex:outputPanel>
			</apex:pageBlockSection>
		</apex:pageBlock>
		</div>
		
		<apex:actionFunction name="refreshApexTransactions" action="{!reloadTransactionLines}" rerender="transactionsColumn" oncomplete="scrollToTransaction();"/>
		
		<script type="text/javascript">
			
			
			function scrollToTransaction(){
				var scrollTop = $FFDC('.rightColumn .scroller').scrollTop();
				var targetOffset = ($FFDC('.rightColumn .scroller .lineItem:last-child').offset().top + scrollTop);
				$FFDC('.rightColumn .scroller').animate({scrollTop: targetOffset}, 500,function(){
					$FFDC('.transactionsLoader').removeClass('transactionsLoaderOn');
				});
			}
			
			$FFDC(document).ready(function () {
			
				$FFDC('#bankLines').UberFilter({
					filterInput: '#filterBankLinesInput',
					filterInputDefault: '{!$Label.codaFilterBankStatementLines}',
					filterItemWrapper: '#bankLines',
					filterItem: '.lineItem'
				});
			
				$FFDC('#transactionLines').UberFilter({
					filterInput: '#filterTransactionLinesInput',
					filterInputDefault: '{!$Label.codaFilterTransactions}',
					filterItemWrapper: '#transactionLines',
					filterItem: '.lineItem'
				});
			
				$FFDC('.createTransaction').bind('click', function () {
			
					var id = escape(lastBankLine.find('.id').text());
					
					var content = $FFDC(document.createElement('div')).css({
						'height': '275px'
					});
			
						var iFrame = $FFDC(document.createElement('iframe')).css({
							'border': 'none',
							'width': '100%',
							'height': '100%'
						}).attr({
							'src': '{!$Page.codabankstatementcreatetransaction}?id='+ id,
							'frameborder':'0',
							'scrolling': 'no'
						}).appendTo(content);
			
						var loadingOverlayWrapper = $FFDC(document.createElement('div')).addClass('loadingOverlayWrapper').css({
							'width': '100%',
							'height': '100%'
						}).attr({
							'id': 'loadingOverlayWrapper'
						}).appendTo(content);
				
							var loadingOverlay = $FFDC(document.createElement('div')).addClass('loadingOverlay').appendTo(loadingOverlayWrapper);
							loadingOverlay.css({opacity:0.8});
							
							var loadingIconWrapper = $FFDC(document.createElement('div')).addClass('loadingIconWrapper').appendTo(loadingOverlayWrapper);
							
								var loadingIcon = $FFDC(document.createElement('img')).attr({
									'src': '/img/loading.gif'
								}).appendTo(loadingIconWrapper);
			
					$FFDC('body').UberPopup({
						popupTitle: '{!$Label.codaCreateTransactions}',
						popupContent: content,
						popupWidth: 600,
						popupShowClose: true
					});
				});
			});
		</script>
		
		
	</apex:form>
</apex:page>