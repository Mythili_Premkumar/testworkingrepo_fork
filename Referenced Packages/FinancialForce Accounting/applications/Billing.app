<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>FinancialForce Billing</description>
    <label>Billing</label>
    <logo>FinancialForceDocuments/FinancialForce.png</logo>
    <tab>standard-Account</tab>
    <tab>Billing_Reconciliation</tab>
    <tab>Digital_Telco_Scheduler__c</tab>
    <tab>standard-Case</tab>
    <tab>Directory_Edition__c</tab>
    <tab>codaInvoice__c</tab>
    <tab>codaCreditNote__c</tab>
    <tab>codaCashEntry__c</tab>
    <tab>CashMatching</tab>
    <tab>CORE_Hist_Cust_Service_Log_AR_Notes__c</tab>
    <tab>List_of_Business__c</tab>
    <tab>ffps_berry__Cash_Entry_Staging__c</tab>
    <tab>Radio_Button_Background_Matching</tab>
    <tab>Custom_Cash_Matching</tab>
    <tab>ffps_ct__FlatTransaction__c</tab>
    <tab>Galley_Parameter_N</tab>
    <tab>ffps_ct__MatchingWriteOff__c</tab>
    <tab>Tabular_Custom_Data_Table_Tax__c</tab>
    <tab>ffps_ct__StagingTax__c</tab>
</CustomApplication>
