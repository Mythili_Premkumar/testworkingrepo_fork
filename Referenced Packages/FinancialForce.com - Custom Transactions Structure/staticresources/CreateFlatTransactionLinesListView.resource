/**
 * Javascript Library or SupercashMatching.page
 *
 * All functions are namespaced with the FF. Make sure you understand the DOM before making changes. In particular pay
 * attention to the way the table rows are constructed.
 */

var FF = {
	/**
	 * Variable to hold the Static resource Path
	 */
	resourcePath: String(),

	/**
	 * Setup function called on page load
	 */
	setup: function(resourcePath)
	{
		// Assign the static resource path
		FF.resourcePath = resourcePath;

		// create the error dialog
		jQuery('#errordialog').dialog({
			autoOpen: false,
			modal: true
		});

		// Disable the Retrieve Button
		jQuery('button[id^="btn-createlines-"]').button().hide();

		// // Wire up UI events
		FF.wireUpUIEvents();

		FF.showInputKeyDialog();
		// // wireup window resize
		jQuery(window).on('resize', function(event) {
			FF.resizeTransactions();
		});
	},
	showInputKeyDialog: function()
	{   	

		FF.createInputDialog('Please Enter Key.', function() {
			jQuery('button[id^="btn-createlines-1"]').click();
    	});
		return;  
	},
	/**
	 * Wire up all the Core UI events, such as button clicks and form changes
	 */
	wireUpUIEvents: function()
	{
		FF.wireUpBtnCreateLinesClick();
	},
	/**
	 * Wire up create flat transactions lines
	 */
	wireUpBtnCreateLinesClick: function()
	{
        jQuery('button[id^="btn-createlines-"]').on('click', function(event) {

			FF.createLines();
		});
    },
	/**
	 * create lines
	 */
	createLines: function()
	{
		var listOfIds = jQuery.parseJSON(FF.getTransactionIds());
		var transIds = new Array();
		var key = FF.getKey();
		for (var i=0; i < listOfIds.length; i++)
		{
			transIds.push(listOfIds[i]);
		}
		if(transIds.length == 0)
		{
			FF.errorDialog('Please Select atleast one Transacion');
			return;
		}
		// Call the service
		FF_INPROGRESS.show('Processing');

		Visualforce.remoting.timeout = 120000;		
		Visualforce.remoting.Manager.invokeAction(
            'ffps_ct.RecalculateFlatTransactionLines.listPageButton',
            transIds,
            key,
            function(result, event)
            {
				FF_INPROGRESS.hide();
                if (event.status)	// success
                {
					location.reload();
                }
                else				// error
                {
                    FF.reportRemotingError(event);
                }
            },
            {escape: true}
        );
    },
	/**
	 * Get To allow modify value
	 */
	getTransactionIds: function()
	{
		var listOfIds = jQuery('#transaction-ids').val();
		return listOfIds;
	},
	/**
	 * Get To allow modify value
	 */
	getKey: function()
	{
		var key = jQuery('#key').val();
		return key;
	},
	/**
	 * Get Transaction prefix
	 */
	getPrefix: function()
	{
		var prefix = jQuery('#transaction-prefix').val();
		return prefix;
	},
	/**
	 * Resizes the Transactions to make the most of available screen real estate
	 */
	resizeTransactions: function()
	{
		var accordion = jQuery('#container .ff-form');

		// if its not rendered, quit
		if (accordion.length == 0)
		{
			return;
		}

		// get the window height
		var windowHeight = jQuery(window).height();

		// get the offset of the content
		var containerTop = jQuery('#container .ff-form .ui-accordion-content').offset().top;

		var containerHeight = windowHeight - containerTop - 30;

		jQuery('#container .ff-form .ui-accordion-content').height(containerHeight);
	},
	/**
	 * Report an arbitary message
	 *
	 * Arguments: 	String		Message (can include markup)
	 */
	createInputDialog: function(message, onClose)
	{
		var html = '<p>' + message + '</p>';

		jQuery('#enterKey-container').html(html);

		jQuery('#enterKey').dialog({
			modal: true,
			buttons: {
        		Exit: function() {
        			window.open('/'+ FF.getPrefix(),'_parent');
        		},
				Create: function() {
          			onClose();
        		},
      		},
      		close: function(event, ui) {
        		window.open('/'+ FF.getPrefix(),'_parent');
      		}
    	});		
	},
	/**
	 * Report an arbitary message
	 *
	 * Arguments: 	String		Message (can include markup)
	 */
	errorDialog: function(message, onClose)
	{
		var html = '<p>' + message + '</p>';

		jQuery('#errorlog-container').html(html);

		jQuery('#errorlog').dialog({
			modal: true,
			buttons: {
        		Exit: function() {
        			window.open('/'+ FF.getPrefix(),'_parent');
        		},
      		},
      		close: function(event, ui) {
        		window.open('/'+ FF.getPrefix(),'_parent');
      		}
    	});		
	},
	/**
	 * Generic confirm dialog box
	 *
	 * Arguments: 	message		The prompt to present to the user
	 *				onConfirm	The function to call on a positive response
	 */
	confirmDialog: function(message, onConfirm)
	{
		var html = '<p>' + message + '</p>';

		jQuery('#conf-dialog-container').html(html);

		jQuery('#confirmdialog').dialog({
			modal: true,
			buttons: {
				Cancel: function() {
					jQuery(this).dialog( "close" );
        		},
        		Confirm: function() {
          			jQuery( this ).dialog( "close" );
          			onConfirm();
        		}
      		}
    	});
	},

	/**
	 * Helper function to sanitise a value to avoid unsightly NULL or UNDEFINED
	 */
	getText: function(value)
	{
		if (typeof value == 'undefined')
		{
			return '';
		}
		else
		{
			return value;
		}
	},

	/**
	 * Helper function to return a String representation of a value
	 */
	getValueAsString: function(value)
	{
		if (typeof value == 'undefined')
		{
			return '0.00';
		}
		else
		{
			return value.toFixed(2);
		}
	},

	/**
	 * Helper function to return a value as a Float
	 */
	getValueAsFloat: function(value)
	{
		var flt = parseFloat(value);

		if (isNaN(flt))
		{
			return 0;
		}
		else
		{
			return flt;
		}
	},

	/**
	 * Helper function to return a Date String from an SObject Date
	 */
	getDateString: function(sObjectDate)
	{
		var myDate = new Date(sObjectDate);

		var dateString = myDate.getDate() > 9 ? '' + myDate.getDate() : '0' + myDate.getDate();
		var monthString = (myDate.getMonth() + 1) > 9 ? '' + (myDate.getMonth() + 1) : '0' + (myDate.getMonth() + 1);

		return monthString + '/' + dateString + '/' + myDate.getFullYear();
	},

	/**
	 * Helper function to return a date from a date string
	 */
	getDate: function(dateString)
	{
		var elms = dateString.split('/');

		// Check the number of elements
		if (elms.length != 3)
		{
			FF.reportError('Invalid Date: ' + dateString + ', expect mm/dd/yy');
			return Number.NaN;
		}

		var rtnDate = new Date(elms[2], elms[0] -1, elms[1], 1, 0, 0, 0);

		if (isNaN(rtnDate))
		{
			FF.reportError('Invalid Date: ' + dateString + ', expect mm/dd/yy');
			return Number.NaN;
		}

		return rtnDate;
	},

	/**
	 * Helper function to resolve the Id of a Parent element
	 */
	resolveParentId: function(elmId)
	{
		var lastDelimiter = elmId.lastIndexOf('-');

		return elmId.substring(0, lastDelimiter);
	},
	/**
	 * Report an arbitary message
	 *
	 * Arguments: 	String		Message (can include markup)
	 */
	info: function(message, onClose)
	{
		var html = '<p>' + message + '</p>';

		jQuery('#infodialog-container').html(html);

		jQuery('#infodialog').dialog({
			modal: true,
			buttons: {
				Ok: function() {
          			jQuery( this ).dialog( "close" );
        		}
      		},
      		close: function(event, ui) {
          			onClose();
      		}
    	});		
	},

	/**
	 * Report an arbitary error
	 *
	 * Arguments: 	String		Error message (can include markup)
	 */
	reportError: function(error)
	{
		var html = '<p>' + error + '</p>';

		jQuery('#dialog-container').html(html);

		jQuery('#errordialog').dialog( 'open' );
	},

	/**
	 * Report a remoting event error
	 *
	 * Arguments: 	Object		Remoting event
	 */
	reportRemotingError: function(event)
	{
		var html = '<p>The following error occurred:</p>';

		html += '<p>' + event.message + '</p>';

		jQuery('#dialog-container').html(html);

		jQuery('#errordialog').dialog( 'open' );
	},
}