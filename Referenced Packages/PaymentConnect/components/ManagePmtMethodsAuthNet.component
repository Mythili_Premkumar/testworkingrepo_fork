<apex:component id="ManagePaymentMethodsAuthNetComponent"  controller="pymt.ManagePmtMethodsAuthNetController"  allowDML="true">
<apex:attribute name="processorConnectionId" assignTo="{!processorConnectionRecordId}" type="ID" description="Salesforce record Id for the PaymentConnect Processor Connection ."/>
<apex:attribute name="contactId" assignTo="{!contactRecordId}" type="ID" description="Salesforce record Id for the Contact."/>
<style>
.pc_edit_pmt_method_form_box {
	border:1px solid #dddddd; 
	padding:4px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
#pc_add_new_payment_method_panel {
	margin-top:10px;
	border:1px solid #dddddd;
	padding:4px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}

</style>
<apex:form id="paymentMethodPortletForm"> 

<table class="pc_payment_methods_table">
<thead>
<tr>
<th>Name</th> 
<th>Billing Name</th>
<th>Type</th>
<th>Last 4</th>
<th>Expiration</th>
<th>Default</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</tr>
</thead>
<apex:repeat value="{!paymentMethods}" var="pmtMethod">
<tr>
<td>{!pmtMethod.name}</td> 
<td>{!pmtMethod.billingName}</td>
<td>{!IF(pmtMethod.methodType == 'Credit Card',pmtMethod.cardType,'Bank Account')}</td>
<td>{!pmtMethod.last4}</td>
<td>{!pmtMethod.expDate}</td>
<td style="width:10%;"><apex:inputCheckbox value="{!pmtMethod.isDefault}" disabled="true"/></td>
<td style="text-align:right;white-space: nowrap;"><apex:commandLink action="{!editSelectedMethod}" value="Edit"><apex:param name="selectedMethodToEdit" assignTo="{!selectedMethodId}" value="{!pmtMethod.recordId}"/></apex:commandLink> |
&nbsp;<apex:commandLink action="{!makeDefault}" value="Make Default"><apex:param name="selectedMethodToDefault" assignTo="{!selectedMethodId}" value="{!pmtMethod.recordId}"/></apex:commandLink> |
&nbsp;<apex:commandLink action="{!deleteSelectedMethod}" onclick="return confirm('Are you sure you want to delete this payment method?');" value="Delete"><apex:param name="selectedMethodToDel" assignTo="{!selectedMethodId}" value="{!pmtMethod.recordId}"/></apex:commandLink>
</td>
<td>
<apex:outputPanel rendered="{!(pmtMethod.showWarning == TRUE)}">
&nbsp;<img src="/s.gif" class="warning_icon_small" title="{!pmtMethod.warningText}"/>
</apex:outputPanel>
</td>
</tr> 
<apex:outputPanel layout="none" rendered="{!pmtMethod.edit}">
<tr>
<td colspan="8">
<div class="pc_edit_pmt_method_form_box">

<apex:panelGrid columns="2">
<apex:outputText value="Payment Method Name"/>
<apex:inputText value="{!pmtMethod.name}"/>

<apex:outputText value="Bank Name" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>
<apex:inputText value="{!pmtMethod.bankName}" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>

<apex:outputText value="Name On Account" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>
<apex:inputText value="{!pmtMethod.bankNameOnAccount}" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>

<apex:outputText value="Account Number" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>
<apex:inputText value="{!pmtMethod.bankAccountNumber}" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>

<apex:outputText value="Routing Number" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>
<apex:inputText value="{!pmtMethod.bankRoutingNumber}" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>

<apex:outputText value="Account Type" rendered="{!pmtMethod.methodType == 'Bank Account'}"/>
<apex:selectList value="{!pmtMethod.bankAccountType}" size="1" rendered="{!pmtMethod.methodType == 'Bank Account'}">
<apex:selectOptions value="{!bankAccountTypeOptions}"/>
</apex:selectList>

<apex:outputText value="Cardholder First Name" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:inputText value="{!pmtMethod.billingFirstName}" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>

<apex:outputText value="Cardholder Last Name" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:inputText value="{!pmtMethod.billingLastName}" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>

<apex:outputText value="Card Number" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:inputText value="{!pmtMethod.creditCardNumber}" id="cardField" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>

<apex:outputText value="CVV Code" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:inputText value="{!pmtMethod.cardCode}" id="cardCode" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>

<apex:outputText value="Card Type" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:selectList value="{!pmtMethod.cardType}" size="1" rendered="{!pmtMethod.methodType == 'Credit Card'}">
<apex:selectOptions value="{!cardTypeOptions}"/>
</apex:selectList>

<apex:outputText value="Expiration Date" rendered="{!pmtMethod.methodType == 'Credit Card'}"/>
<apex:outputPanel layout="inline" rendered="{!pmtMethod.methodType == 'Credit Card'}">
<apex:selectList value="{!pmtMethod.expMonth}"  size="1">
<apex:selectOption itemValue="01"/>
<apex:selectOption itemValue="02"/>
<apex:selectOption itemValue="03"/>
<apex:selectOption itemValue="04"/>
<apex:selectOption itemValue="05"/>
<apex:selectOption itemValue="06"/>
<apex:selectOption itemValue="07"/>
<apex:selectOption itemValue="08"/>
<apex:selectOption itemValue="09"/>
<apex:selectOption itemValue="10"/>
<apex:selectOption itemValue="11"/>
<apex:selectOption itemValue="12"/>
</apex:selectList>&nbsp;<apex:selectList value="{!pmtMethod.expYear}" size="1">
<apex:selectOptions value="{!expYearOptions}"/> 
</apex:selectList>
</apex:outputPanel>

<apex:outputText value="Billing Street"/>
<apex:inputText value="{!pmtMethod.billingStreet}"/>

<apex:outputText value="Billing City"/>
<apex:inputText value="{!pmtMethod.billingCity}"/>

<apex:outputText value="Billing State"/>
<apex:inputText value="{!pmtMethod.billingState}"/>

<apex:outputText value="Billing Postal Code"/>
<apex:inputText value="{!pmtMethod.billingPostalCode}"/>

<apex:outputText value="Billing Country"/>
<apex:inputText value="{!pmtMethod.billingCountry}"/>

<apex:outputText value="Billing Phone"/>
<apex:input type="tel" value="{!pmtMethod.billingPhone}"/>

<!-- Billing email on the profile must match the email on the contact record - so we don't allow the user to specify a different value here -->

<apex:outputText value=""/>
<apex:outputPanel layout="inline">
<apex:inputCheckbox value="{!pmtMethod.isDefault}" disabled="{!pmtMethod.isDefault}"/><apex:outputText value=" Make this my default payment method." />
</apex:outputPanel>

<apex:outputText value=""/>
<apex:commandButton value="Save Changes" action="{!saveChanges}">
<apex:param value="{!pmtMethod.recordId}" id="saveChangeRecId" assignTo="{!selectedMethodId}"/>
</apex:commandButton>

</apex:panelGrid>
<script>
// Turn off autocomplete for the card number field above
var el = document.getElementById("{!$Component.cardField}");
if (el !== null) {
	el.setAttribute('autocomplete','off'); 
}
</script>
</div>

</td>
</tr>
</apex:outputPanel>

</apex:repeat>
</table>
<br/>
<div class="pc_manage_pmt_method_buttons">
<apex:commandButton value="Refresh List" action="{!refreshList}"  />&nbsp;<apex:commandButton value="Add New Payment Method" oncomplete="var el =document.getElementById('pc_add_new_payment_method_panel');if (el.style.display == 'none') {el.style.display = 'block';} else {el.style.display = 'none';}" />
</div>

<div id="pc_add_new_payment_method_panel" style="display:none;">
<h2>New Payment Method</h2>
<apex:panelGrid columns="2" id="newMethodPanel">

<apex:outputText value="Payment Method Name"/>
<apex:inputText value="{!pmtMethodName}"/>

<apex:outputText value="Payment Method Type"/>
<apex:outputPanel layout="inline">
<apex:selectRadio value="{!pmtMethodType}" >
<apex:selectOption itemValue="Credit Card" itemLabel="Credit Card"/>
<apex:selectOption itemValue="Bank Account" itemLabel="Bank Account"/>
<apex:actionSupport event="onchange" rerender="newMethodPanel"/>
</apex:selectRadio>
</apex:outputPanel>

<apex:outputText value="Cardholder First Name" rendered="{!pmtMethodType == 'Credit Card'}"/>
<apex:inputText value="{!billingFirstName}" rendered="{!pmtMethodType == 'Credit Card'}"/>

<apex:outputText value="Cardholder Last Name" rendered="{!pmtMethodType == 'Credit Card'}"/>
<apex:inputText value="{!billingLastName}" rendered="{!pmtMethodType == 'Credit Card'}"/>

<apex:outputText value="Card Number" rendered="{!pmtMethodType == 'Credit Card'}"/>
<apex:input type="text" html-autocomplete="off" value="{!creditCardNumber}" id="newCardNumberField" rendered="{!pmtMethodType == 'Credit Card'}"/>

<apex:outputText value="Bank Name" rendered="{!pmtMethodType == 'Bank Account'}"/>
<apex:inputText value="{!bankName}" id="newBankNameField" rendered="{!pmtMethodType == 'Bank Account'}"/>

<apex:outputText value="Name On Account" rendered="{!pmtMethodType == 'Bank Account'}"/>
<apex:inputText value="{!bankNameOnAccount}" id="newBankNameOnAccountField" rendered="{!pmtMethodType == 'Bank Account'}"/>


<apex:outputText value="Account Number" rendered="{!pmtMethodType == 'Bank Account'}"/>
<apex:input type="text" html-autocomplete="off"  value="{!bankAccountNumber}" id="newAccountNumberField" rendered="{!pmtMethodType == 'Bank Account'}"/>

<apex:outputText value="Card Type" rendered="{!pmtMethodType == 'Credit Card'}"/>
<apex:selectList value="{!cardType}" size="1" rendered="{!pmtMethodType == 'Credit Card'}">
<apex:selectOptions value="{!cardTypeOptions}"/>
</apex:selectList>

<apex:outputText value="Routing Number" rendered="{!pmtMethodType == 'Bank Account'}"/>
<apex:input type="text" html-autocomplete="off"  value="{!bankRoutingNumber}" id="newRoutingNumberField" rendered="{!pmtMethodType == 'Bank Account'}"/>


<apex:outputText value="Account Type" rendered="{!pmtMethodType == 'Bank Account'}"/>
<apex:selectList value="{!bankAccountType}" size="1" rendered="{!pmtMethodType == 'Bank Account'}">
<apex:selectOptions value="{!bankAccountTypeOptions}"/>
</apex:selectList>

<apex:outputText value="Expiration Date" rendered="{!pmtMethodType == 'Credit Card'}"/>
<apex:outputPanel layout="inline" rendered="{!pmtMethodType == 'Credit Card'}">
<apex:selectList value="{!expMonth}"  size="1">
<apex:selectOption itemValue="01"/>
<apex:selectOption itemValue="02"/>
<apex:selectOption itemValue="03"/>
<apex:selectOption itemValue="04"/>
<apex:selectOption itemValue="05"/>
<apex:selectOption itemValue="06"/>
<apex:selectOption itemValue="07"/>
<apex:selectOption itemValue="08"/>
<apex:selectOption itemValue="09"/>
<apex:selectOption itemValue="10"/>
<apex:selectOption itemValue="11"/>
<apex:selectOption itemValue="12"/>
</apex:selectList>&nbsp;<apex:selectList value="{!expYear}" size="1">
<apex:selectOptions value="{!expYearOptions}"/> 
</apex:selectList>
</apex:outputPanel>

<apex:outputText value="Billing Street"/>
<apex:inputText value="{!billingStreet}"/>

<apex:outputText value="Billing City"/>
<apex:inputText value="{!billingCity}"/>

<apex:outputText value="Billing State"/>
<apex:inputText value="{!billingState}"/>

<apex:outputText value="Billing Postal Code"/>
<apex:inputText value="{!billingPostalCode}"/>

<apex:outputText value="Billing Country"/>
<apex:inputText value="{!billingCountry}"/>

<apex:outputText value="Billing Phone"/>
<apex:input type="tel" value="{!billingPhone}"/>

<apex:outputText value="Billing Email" rendered="{!ISBLANK(contactEmail)}"/>
<apex:input type="email" value="{!contactEmail}"  rendered="{!ISBLANK(contactEmail)}"/>

<apex:outputText value=""/>
<apex:outputPanel layout="inline">
<apex:inputCheckbox value="{!isDefault}"/><apex:outputText value=" Make this my default payment method."/>
</apex:outputPanel>


<apex:outputText value=""/>
<apex:outputPanel >
<apex:commandButton value="Cancel" oncomplete="var el =document.getElementById('pc_add_new_payment_method_panel');if (el.style.display == 'none') {el.style.display = 'block';} else {el.style.display = 'none';}" />&nbsp;<apex:commandButton value="Submit" action="{!addNewPaymentMethod}"/>

</apex:outputPanel>
</apex:panelGrid>
<script>
// Turn off autocomplete for the card number field above
var el = document.getElementById("{!$Component.newCardNumberField}");
if (el !== null) {
	el.setAttribute('autocomplete','off'); 
}
</script>
</div>
</apex:form>


</apex:component>