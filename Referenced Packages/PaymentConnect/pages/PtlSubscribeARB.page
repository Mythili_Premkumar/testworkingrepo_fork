<apex:page id="ptlSubscribeARBPage" controller="pymt.PtlSubscribeARBController"  cache="false"  sidebar="false" tabStyle="pymt__PaymentX__c" language="{!$CurrentPage.Parameters.lang}">
<link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.pymt__PaymentConnect,'styles/ptl_basic.css')}" />
<head>
<title>Subscription Checkout</title></head>

 <script src="{!URLFOR($Resource.PaymentConnect, 'includes/tooltip/wz_tooltip.js')}"></script>
<div id="pc_context_{!HTMLENCODE($CurrentPage.parameters.context)}">
<div id="pc_lang_{!HTMLENCODE($CurrentPage.parameters.lang)}">
<div class="pc_page" id="pc_ptlsubscribearb_page">
  <div class="pc_page_content_top"></div>
  <div class="pc_page_content" >
    <div class="pc_page_content_inner"> 
      

<apex:outputPanel layout="none">
<script language="javascript">

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      if (oldonload) {
        oldonload();
      }
      func();
    }
  }
} 

// Disable browser autocomplete for fields accepting sensitive data
function disableAutoComplete() {
    var el;
    el = document.getElementById("{!$Component.checkoutForm.ccNumber}");
    if (el) el.setAttribute("autocomplete","off");
    el = document.getElementById("{!$Component.checkoutForm.cvvNumber}");
    if (el) el.setAttribute("autocomplete","off");
}

addLoadEvent(disableAutoComplete);
        
        var enableValidation=false;
        function validateForm() {

            if (!enableValidation) return true;
            enableValidation=false;
            
              if (document.getElementById('{!$Component.checkoutForm.createSubscription}').disabled==true) {
                 // validation done only on createSubscription action.  Skip if createSubscription button is disabled.
                 return true;
              }
              
              var fname = document.getElementById("{!$Component.checkoutForm.billingfirstname}");
              var lname = document.getElementById("{!$Component.checkoutForm.billinglastname}");
              if ((fname != null && lname !== null) && (fname.value == "" || lname.value == "")) {
                  alert("Please provide the billing contact name.");
                  return false;
              }
          
              var address = document.getElementById("{!$Component.checkoutForm.billingstreet}");
              if (address != null && address.value == "") {
                  alert("Please provide the billing address.");
                  return false;
              }
          
              var city = document.getElementById("{!$Component.checkoutForm.billingcity}");
              if (city != null && city.value == "") {
                  alert("Please provide the billing city.");
                  return false;
              }
          
              var state = document.getElementById("{!$Component.checkoutForm.billingstate}");
              if (state != null && state.value == "") {
                  alert("Please provide the billing state/province.");
                  return false;
              }
          
              var zip = document.getElementById("{!$Component.checkoutForm.billingpostalcode}");
              if (zip != null && zip.value == "") {
                  alert("Please provide the billing postal code.");
                  return false;
              }
          
              var country = document.getElementById("{!$Component.checkoutForm.billingcountry}");
              if (country != null && country.value == "") {
                  alert("Please select a billing country.");
                  return false;
              }
          
              var cardNumber = document.getElementById("{!$Component.checkoutForm.ccNumber}");
              if (cardNumber != null) {
                if (cardNumber.value == "") {
                  alert("Please enter your credit card number.");
                  return false;
                }
                if (cardNumber.value.length > 16) {
                  alert("Please enter a valid credit card number.");
                  return false;
                }

              }
              
              var cvvNumber = document.getElementById("{!$Component.checkoutForm.cvvNumber}");
              if (cvvNumber != null && cvvNumber.value == "") {
                  alert("Please enter your CVV number.");
                  return false;
              }


              var bankName = document.getElementById("{!$Component.checkoutForm.bankname}");
              if (bankName != null && bankName.value == "") {
                  alert("Please enter the name of your bank.");
                  return false;
              }    
              
              var bankAcctName = document.getElementById("{!$Component.checkoutForm.bankaccountname}");
              if (bankAcctName != null && bankAcctName.value == "") {
                  alert("Please enter the name on your bank account.");
                  return false;
              }    
                            
              var bankAcctNumber = document.getElementById("{!$Component.checkoutForm.bankaccountnumber}");
              if (bankAcctNumber != null && bankAcctNumber.value == "") {
                  alert("Please enter your bank account number.");
                  return false;
              }              

              var routingNumber = document.getElementById("{!$Component.checkoutForm.bankroutingnumber}");
              if (routingNumber != null && routingNumber.value == "") {
                  alert("Please enter your bank routing number.");
                  return false;
              }              

              return true;
          }
          </script>
      </apex:outputPanel>
      <apex:pageMessages id="messages"/>
      <apex:form id="checkoutForm" styleClass="pc_subscription_checkout_form"  onsubmit="return validateForm();" >
        <apex:outputPanel id="cardEntryPanel" rendered="{!pageMode=='ProcessPayment'}">
          <apex:outputPanel rendered="{!showPageContents}">
            <apex:sectionHeader title="{!$Label.SiteSubscribeARB_CheckoutHeading}" />
              
               <h2>{!$Label.SiteSubscribeARB_SubscriptionSummaryHeading}</h2>
                <apex:panelGrid columns="2" columnClasses="labelColumn,term_column" id="subscription_terms">
                
                    <apex:outputText value="{!$Label.Label_SubscriptionFor}:" />
                    <apex:outputText value="{!profileName}"/>
    
                    <apex:outputText value="{!$Label.Label_Description}:" rendered="{!NOT(ISNULL(profileDescription))}" />
                    <apex:outputText value="{!profileDescription}" rendered="{!NOT(ISNULL(profileDescription))}"/>
                                
                    <apex:outputText value="{!$Label.Label_StartDate}:" />
                    <apex:outputText value="{!startDateString}"/>
              
              
                    <apex:outputText value="{!$Label.Label_TrialAmount}:"  rendered="{!NOT(ISNULL(trialTerms))}"/> 
                    <apex:outputPanel layout="inline" rendered="{!NOT(ISNULL(trialTerms))}">
                      <apex:outputText value="{!currencyFormatExpression}" >                  
                      <apex:param value="{!trialAmount}"/>
                      </apex:outputText>&nbsp;<apex:outputText value="{!trialTerms}"/>
                      
                    </apex:outputPanel>
        
                    <apex:outputText value="{!$Label.Label_SubscriptionAmount}:"/> 
                    <apex:outputPanel layout="inline">
                      <apex:outputText value="{!currencyFormatExpression}" >
                      <apex:param value="{!recurringAmount}"/>
                      </apex:outputText>&nbsp;<apex:outputText value="{!subscriptionTerms}"/>
                    </apex:outputPanel>
     
                </apex:panelGrid>
                         
    
              <apex:outputPanel id="nativeCheckoutPanel" >
                <h2>{!$Label.SiteSubscribeARB_BillingInfoHeading}</h2>
                <apex:outputPanel rendered="{!NOT($Label.SiteSubscribeARB_BillingInfoInstructions == '-')}">
                <p class="pc_billing_info_instructions">{!$Label.SiteSubscribeARB_BillingInfoInstructions}</p>
                </apex:outputPanel>
                <apex:outputPanel rendered="{!(paymentTypeOptionsSize > 1)}">
                    <apex:panelGrid columns="3" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn">
                        <apex:outputText value="{!$Label.Label_PaymentMethod}"/>
                        <apex:image url="/s.gif" />
                        <apex:selectRadio value="{!selectedPaymentType}" layout="lineDirection" style="white-space:nowrap;">
                            <apex:selectOptions value="{!paymentTypeOptions}" />
                            <apex:actionSupport event="onchange" action="{!nullAction}" rerender="nativeCheckoutPanel" status="formUpdateStatus" />
                        </apex:selectRadio>             
                    </apex:panelGrid>
                </apex:outputPanel>
                            
                <apex:outputPanel id="billingContactPanel" rendered="{!NOT(selectedPaymentType == 'storedpaymentmethod')}" >                            
                    <apex:panelGrid columns="3" styleClass="pc_billing_info_grid" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn" >
                        <apex:outputText value="{!$Label.Label_FirstName}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billingfirstname" value="{!fnameOnCard}" />
                        
                        <apex:outputText value="{!$Label.Label_LastName}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billinglastname" value="{!lnameOnCard}" />
                        
                        <apex:outputText value="{!$Label.Label_StreetAddress}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billingstreet" value="{!billingStreet}" styleClass="pc_long_input_field"/>
        
                        <apex:outputText value="{!$Label.Label_City}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billingcity" value="{!billingCity}" />
                        
                        <apex:outputText value="{!$Label.Label_StateProvince}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billingState" value="{!billingstate}" styleClass="pc_short_input_field"/>
        
                        <apex:outputText value="{!$Label.Label_PostalCode}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText id="billingpostalcode" value="{!billingPostalCode}" styleClass="pc_short_input_field"/>
        
                        <apex:outputText value="{!$Label.Label_Country}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:selectList id="billingcountry"  value="{!billingCountry}" size="1" multiselect="false">
                        <apex:selectOptions value="{!countryPicklistOptions}" />
                        </apex:selectList>            
                    </apex:panelGrid>
                </apex:outputPanel>

                <apex:outputPanel id="cardCheckoutPanel" rendered="{!selectedPaymentType == 'creditcard'}" >
                    <script>paymentType = "creditcard";</script>            
                    <apex:panelGrid columns="3" styleClass="pc_billing_info_grid" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn" >
                        <apex:outputText value="{!$Label.Label_CardType}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:outputPanel layout="inline">
                            <apex:selectList size="1" id="cardTypeSelectList" multiselect="false" value="{!cardType}">
                                <apex:selectOptions value="{!cardTypeOptions}" />
                          </apex:selectList><img id="pc_accepted_card_logos" src="/s.gif"/><apex:repeat value="{!cardTypeOptions}" var="cardTypeName"><img id="pc_accepted_card_{!SUBSTITUTE(cardTypeName.value,' ','_')}" src="/s.gif"/></apex:repeat>
                        </apex:outputPanel>
                        
                        <apex:outputText value="{!$Label.Label_CardNumber}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText styleclass="pc_long_input_field" id="ccNumber" value="{!creditCardNumber}" />
                                         
                        <apex:outputText value="{!$Label.Label_SecurityCode}"/>
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText styleclass="pc_short_input_field" id="cvvNumber" value="{!cardCode}" />
                        
                        <apex:outputText value="{!$Label.Label_CardExpiration}"/>               
                        <apex:image url="\s.gif" styleClass="requiredFieldMarker"/>
                        <apex:outputPanel layout="inline">
                        <apex:selectList id="expMonth" value="{!expirationMonth}" size="1">
                            <apex:selectOptions value="{!expMonthOptions}" />
                            </apex:selectList>
                            &nbsp;
                            <apex:selectList id="expYear" value="{!expirationYear}" size="1">
                                <apex:selectOptions value="{!expYearOptions}" />
                            </apex:selectList>
                        </apex:outputPanel>  
                        
                        <apex:outputText value="" rendered="{!savePmtDetailsEnabled}"/>
                        <apex:image url="/s.gif" styleClass="optionalFieldMarker"  rendered="{!savePmtDetailsEnabled}"/>
                        <apex:outputPanel layout="none" rendered="{!savePmtDetailsEnabled}">
                        <apex:inputCheckbox value="{!storePaymentDetails}" />&nbsp;<apex:outputText value="{!$Label.Label_SavePaymentDetails}" />
                        </apex:outputPanel>
                                                             
                    </apex:panelGrid>               
                  </apex:outputPanel>
              
                <apex:outputPanel id="paymentMethodCheckoutPanel" rendered="{!selectedPaymentType == 'storedpaymentmethod'}" >
    
                    <script>paymentType = "storedpaymentmethod";</script>
                    <apex:panelGrid columns="3" styleClass="pc_stored_pmt_method_table" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn">
    
                        <apex:outputText value=""/>
                        <apex:image url="/s.gif" styleClass="optionalFieldMarker"/>
                        <apex:selectList size="1" id="paymentMethodSelectList"
                                multiselect="false" value="{!selectedPaymentMethodId}">
                                <apex:selectOptions value="{!paymentMethodOptions}" />
                        </apex:selectList>
    
                    </apex:panelGrid>        
                </apex:outputPanel>
             
            
                <apex:outputPanel id="echeckCheckoutPanel" rendered="{!selectedPaymentType == 'echeck'}" >
    
                    <script>paymentType = "echeck";</script>
                    <apex:panelGrid columns="3" styleClass="pc_echeck_information_table" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn">
    
                        <apex:outputText value="Bank Name"/>
                        <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText styleClass="pc_medium_input_field"  id="bankname"
                                value="{!bankName}" />
    
                        <apex:outputText >Account Type</apex:outputText>
                        <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                        <apex:selectList size="1" id="accountTypeSelectList"
                                multiselect="false" value="{!selectedBankAccountType}">
                                <apex:selectOption itemValue="checking" itemLabel="Checking" />
                                <apex:selectOption itemValue="businesschecking"
                                    itemLabel="Business Checking" />
                                <apex:selectOption itemValue="savings" itemLabel="Savings" />
                        </apex:selectList>
                            
                        <apex:outputText value="Name on Account"/>
                        <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                        <apex:inputText styleClass="pc_medium_input_field"  id="bankaccountname"
                                value="{!bankAccountName}" />
                                    
                        <apex:outputText value="Account Number"/>
                        <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                        <apex:outputPanel layout="none">
                        <apex:inputText styleClass="pc_medium_input_field"  id="bankaccountnumber"
                                value="{!bankAccountNumber}" /><span id="pc_routing_number_img_wrapper" onmouseover="Tip('<img src=\'\\s.gif\' id=\'pc_routing_number_tip\'/>', FOLLOWMOUSE, false, SHADOW, false, OFFSETX, 10 , OFFSETY, 10,  BGCOLOR, 'transparent' , BORDERCOLOR, 'transparent');" onmouseout="UnTip();"><img id="pc_routing_number_img" src="/s.gif"/></span>
                        </apex:outputPanel>                                
                        <apex:outputText value="Routing Number"/>
                        <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                        <apex:outputPanel layout="none">
                        <apex:inputText styleClass="pc_medium_input_field"  id="bankroutingnumber"
                                value="{!bankRoutingNumber}" />
                        </apex:outputPanel>
                                
                                    
                        
                    </apex:panelGrid>               
                </apex:outputPanel>
                          
                
              </apex:outputPanel>
    
              
              <apex:outputPanel id="buttons">
                <apex:panelGrid columns="3" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn" id="button_panel_grid" styleClass="pc_button_panel_grid">
                    <apex:outputText value=""/>
                    <apex:image url="/s.gif"/>
                    <apex:outputPanel layout="inline">
                        <apex:commandButton id="cancel" value="{!$Label.SiteSubscribeARB_CancelButton}"
                                action="{!cancelTransaction}" onclick="document.getElementById('{!$Component.checkoutForm.createSubscription}').disabled=true;" rendered="{!(cancelURL<>null && NOT(ISNULL(cancelURL)))}"/>
                            <apex:commandButton id="createSubscription" value="{!$Label.SiteSubscribePRB_SubscribeButton}" status="formActionStatus"
                                action="{!createSubscription}"  onclick="enableValidation = true;" rerender="checkoutForm, formActionStatus, savePaymentMethodForm, messages" />
                              <apex:actionStatus id="formActionStatus" onstart="document.getElementById('{!$Component.checkoutForm.createSubscription}').disabled=true;" onstop="document.getElementById('{!$Component.checkoutForm.createSubscription}').disabled=false;">
                              <apex:facet name="start">
                                <apex:outputPanel >&nbsp;
                                  <apex:image value="{!URLFOR($Resource.pymt__PaymentConnect, 'images/icon-spinner.gif')}"
                                            style="vertical-align:middle;" alt="" />
                                  &nbsp;Processing... </apex:outputPanel>
                              </apex:facet>
                              <apex:facet name="stop">
                                <apex:image value="{!URLFOR('/s.gif')}" alt="" style="height:17px;" />
                              </apex:facet>
                        </apex:actionStatus>
                    </apex:outputPanel>
                </apex:panelGrid>            
              </apex:outputPanel>
              
            </apex:outputPanel>           
        </apex:outputPanel>
          
      </apex:form>
      
    <apex:form id="savePaymentMethodForm"  >
        <apex:outputPanel id="checkoutPmtMethodFormPanel" rendered="{!pageMode=='SavePmtMethod'}">

            <apex:outputPanel > 
            <h1>Save Payment Method</h1>
            
            <apex:outputPanel id="paymentMethodSummaryPanel" >

                <apex:panelGrid columns="3" styleClass="pc_payment_method_summary_table" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn">

                    <apex:outputText value="Payment Method Name"/>
                    <apex:image url="/s.gif" styleClass="requiredFieldMarker"/>
                    <apex:inputText value="{!newPaymentMethodName}" />
                        
                            
                    <apex:outputText value="Account Number"/>
                    <apex:image url="/s.gif" styleClass="optionalFieldMarker"/>
                    <apex:outputText value="************{!last4}" />

                    <apex:outputText value="Make Default"/>
                    <apex:image url="/s.gif" styleClass="optionalFieldMarker"/>
                    <apex:inputCheckbox value="{!makeNewPaymentMethodDefault}" /> 
                                
                               
                </apex:panelGrid>              
                
       <apex:panelGrid columns="3" columnClasses="labelColumn,requiredFieldMarkerColumn,fieldColumn" id="button_panel_grid" styleClass="pc_button_panel_grid">
            <apex:outputText value=""/>
            <apex:image url="/s.gif" styleClass="optionalFieldMarker"/>
            <apex:outputPanel layout="inline">                
                <apex:commandButton id="skipPmtMethod" value="Skip" onclick="" action="{!finishTransaction}" /> 
                <apex:commandButton id="storePmtMethod" value="Continue" onclick="" action="{!storePaymentMethod}" />
                </apex:outputPanel>
                </apex:panelGrid> 
            </apex:outputPanel>
                        
 
            </apex:outputPanel>
        </apex:outputPanel>
    </apex:form>         
    </div>
    </div>
    </div>
    </div>
    </div>
</apex:page>