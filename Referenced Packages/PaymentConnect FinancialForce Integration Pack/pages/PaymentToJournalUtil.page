<apex:page controller="p2f.FFConnectorPayment2JournalController" tabStyle="pymt__PaymentX__c">
<style>
.bPageBlock .message {
    margin: 4px 4px 4px 4px;
    }
</style>
<apex:sectionHeader title="PaymentConnect FinancialForce Integration Pack" subtitle="Payment-to-Journal Utility" help="{!$Page.p2f__FFConnectorHelp}" />
<apex:pageMessage summary="This user has not been given access to FinancialForce. " rendered="{!NOT($ObjectType.c2g__codaCompany__c.accessible)}"  severity="ERROR"/>
<apex:form id="ffconnectorform"  rendered="{!$ObjectType.c2g__codaCompany__c.accessible}">
<apex:pageBlock title="{!currentCompany}" mode="edit" >
<apex:pageMessages id="messages" />
<apex:pageBlockButtons >
<apex:commandButton value="Create Journal Entry" action="{!postSelected}" disabled="{!formDisabled}"  status="pageStatus" />&nbsp;<apex:actionStatus startText="Updating..." stopText="" id="pageStatus" />
</apex:pageBlockButtons>
<p style="padding:0px 10px 15px 10px;">This page allows you to create journal entries in FinancialForce from completed PaymentConnect payments.  Use the filter criteria below to find the transactions you would like to process, choose the general ledger accounts and options you want to use to create your journal entries, 
and click the Create Journal Entry button.
</p>

<apex:pageBlockSection columns="2" title="Filter Criteria">

<apex:pageblockSectionItem >
<apex:outputLabel value="Company: "/>
<apex:selectList value="{!currentCompany}" multiselect="false" size="1">
<apex:actionSupport event="onchange" action="{!setCompany}" status="pageStatus" rerender="ffconnectorform"/>
<apex:selectOptions value="{!companyOptions}"/>
</apex:selectList>
</apex:pageblockSectionItem>


<apex:pageblockSectionItem helpText="PaymentConnect Processor Connection." >
<apex:outputLabel value="Processor Connection: "/>
<apex:selectList value="{!selectedConnection}" multiselect="false" size="1">
<apex:actionSupport event="onchange" action="{!setProcessorConnection}" status="pageStatus" rerender="ffconnectorform"/>
<apex:selectOptions value="{!connectionOptions}"/>
</apex:selectList>
</apex:pageblockSectionItem>

<apex:pageblockSectionItem helpText="FinancialForce accounting period.  The payments list will be filtered to show transactions occuring within the selected accounting period." >
<apex:outputLabel value="Accounting Period: " />
<apex:selectList value="{!selectedPeriod}" multiselect="false" size="1">
<apex:actionSupport event="onchange" action="{!setPeriod}" status="pageStatus" rerender="ffconnectorform"/>
<apex:selectOptions value="{!periodOptions}"/>
</apex:selectList> 
</apex:pageblockSectionItem>

<apex:pageblockSectionItem helpText="Currency ISO Code to use creating the new Journal.  Selecting a currency will also filter all other currencies out of the list of Payment records below.">
<apex:outputLabel value="Currency: "/>
<apex:selectList value="{!selectedCurrency}" multiselect="false" size="1">
<apex:actionSupport event="onchange" action="{!setCurrency}" status="pageStatus" rerender="ffconnectorform"/>
<apex:selectOptions value="{!currencyOptions}"/>
</apex:selectList> 
</apex:pageblockSectionItem>

</apex:pageblockSection>
<apex:pageBlockSection columns="2" title="Journal Entry Settings">

<apex:pageblockSectionItem helptext="General ledger account to debit with bank deposits.">
<apex:outputLabel value="Bank GLA: "/>
<apex:selectList value="{!selectedBankGLA}" multiselect="false" size="1">
<apex:selectOptions value="{!glaOptions}"/>
</apex:selectList>
</apex:pageblockSectionItem>


<apex:pageblockSectionItem helptext="General ledger account to credit with revenue amounts.">
<apex:outputLabel value="Revenue GLA: " for="revenueGLA" />
<apex:selectList id="revenueGLA" value="{!selectedRevenueGLA}" multiselect="false" size="1">
<apex:selectOptions value="{!glaOptions}"/>
</apex:selectList>
</apex:pageblockSectionItem>

<apex:pageblockSectionItem >
<apex:outputText value=""/>
<apex:outputText value=""/>
</apex:pageblockSectionItem>

<apex:pageblockSectionItem helptext="Default tax code to use for tax liability credits.  Used if a payment record doesn't have a FF Tax Code value assigned.">
<apex:outputLabel value="Default Tax Code: "/>
<apex:selectList value="{!selectedDefaultTaxCode}" multiselect="false" size="1">
<apex:selectOptions value="{!taxCodeOptions}"/>
</apex:selectList>
</apex:pageblockSectionItem>


<apex:pageblockSectionItem helptext="Enables the journal post action after creating a journal record." >
<apex:outputLabel value="Automatically Post Journal" />
<apex:inputCheckbox value="{!postJournal}" />
</apex:pageblockSectionItem>

<apex:pageblockSectionItem helptext="Include account sales and cash line entries in journal.">
<apex:outputLabel value="Include account entry lines." />
<apex:inputCheckbox value="{!addAccountLineEntries}" />
</apex:pageblockSectionItem>

</apex:pageBlockSection> 
<p>&nbsp;</p>
<apex:outputPanel rendered="{!setCon.resultSize =0}">
<div style="border:2px #c0c0c0 solid;padding:25px;margin-left:auto;margin-right:auto;width:80%;text-align:center;">
No payments found. Try modifying your filter criteria. 
</div>
</apex:outputPanel>
<apex:outputPanel id="paymentsTable" rendered="{!setCon.resultSize >0}">
<div style="margin-bottom:5px;">
<apex:outputPanel layout="inline" rendered="{!setCon.resultSize >0}">
<apex:commandLink value="Select All" action="{!selectAll}"  status="pageStatus" rerender="paymentsTable"/>
&nbsp;|&nbsp;
<apex:commandLink value="None" action="{!unselectAll}"  status="pageStatus" rerender="paymentsTable"/>
&nbsp;&nbsp;&nbsp; 
</apex:outputPanel>
<apex:outputPanel layout="inline" rendered="{!setCon.hasPrevious}"> &lt;&lt;&nbsp; <apex:commandLink value="Prev" action="{!previous}" status="pageStatus" rerender="paymentsTable, pagination" />
</apex:outputPanel>
<apex:outputPanel layout="inline" rendered="{!AND(setCon.hasPrevious,setCon.hasNext)}">&nbsp;|&nbsp;
</apex:outputPanel>
<apex:outputPanel layout="inline" rendered="{!setCon.hasNext}"><apex:commandLink value="Next" action="{!next}" status="pageStatus"  rerender="paymentsTable, pagination" /> &gt;&gt;
</apex:outputPanel>
</div>
<apex:pageBlockTable id="paymentsPageBlockTable" value="{!unprocessedPayments}" var="item">
<apex:column ><apex:inputCheckbox value="{!item.selected}"/></apex:column>
<apex:column value="{!item.payment.pymt__Date__c}"/>
<apex:column value="{!item.payment.Name}"/>
<apex:column value="{!item.payment.pymt__Amount__c}"/>
<apex:column value="{!item.payment.pymt__Currency_ISO_Code__c}">
<apex:facet name="header">Currency</apex:facet>
</apex:column>
<apex:column value="{!item.payment.pymt__Tax__c}" />
<apex:column value="{!item.payment.p2f__FF_Tax_Code__c}" />
<apex:column value="{!item.payment.pymt__Transaction_Fee__c}"/>
<apex:column value="{!item.payment.pymt__Status__c}"/>
<apex:column value="{!item.payment.pymt__Contact__c}"/>
<apex:column value="{!item.payment.pymt__Account__c}"/>
<apex:column value="{!item.payment.pymt__Processor_Connection__c}"/>
</apex:pageBlockTable>
<div style="margin-top:5px;">
Records per page:&nbsp;
<apex:selectList value="{!itemsPerPage}" multiselect="false" size="1">
<apex:actionSupport event="onchange"  action="{!setItemCount}" status="pageStatus"  rerender="paymentsTable, pagination" />
<apex:selectOption itemValue="2" itemLabel="2"/>
<apex:selectOption itemValue="25" itemLabel="25"/>
<apex:selectOption itemValue="50" itemLabel="50"/>
<apex:selectOption itemValue="75" itemLabel="75"/>
<apex:selectOption itemValue="100" itemLabel="100"/>
</apex:selectList>
</div>
</apex:outputPanel>
</apex:pageBlock> 
<apex:outputText value="Completed Payments in {!selectedCurrencyCode} from {!startDate} to {!endDate}"  rendered="{!NOT(formDisabled)}"/>
</apex:form>
</apex:page>