<apex:page controller="p2f.FFConnectorConfigController" tabStyle="pymt__PaymentConnect_Admin__tab">
<apex:form >
<apex:sectionHeader title="Linvio PaymentConnect" subtitle="FinancialForce Integration Pack Settings" />
<apex:pageMessages />
<apex:pageBlock title="Integration Pack Settings" helpURL="{!URLFOR($Page.p2f__FFConnectorHelp)}" helpTitle="FinancialForce Integration Pack Help" mode="edit">
<apex:pageBlockButtons >
<apex:commandButton value="Save Settings" action="{!saveSettings}"/>
</apex:pageBlockButtons>

<apex:pageBlockSection columns="1" title="Account Record Setup" collapsible="false">
<apex:pageBlockSectionItem helpText="">
<p>Use the fields below to enable and configure the integration pack trigger that auto-fills Accounts Payable Control and Accounts Receivable Control lookup fields on Accounts.<br/><br/></p>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem helpText="Enables a trigger to auto-fill the FinancialForce Accounts Receivable and Payable Control lookup fields on Accounts when inserted/updated.">
<apex:outputLabel value="Auto-Populate Account GLA Lookups" />
<apex:inputField value="{!batchSettings.p2f__Autofill_Account_GLA_Lookups__c}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem helpText="">
<apex:outputLabel value="Accounts Payable Control Lookup" />
<apex:inputField value="{!accountFields.c2g__CODAAccountsPayableControl__c}"/>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem helpText="">
<apex:outputLabel value="Accounts Receivable Control Lookup" />
<apex:inputField value="{!accountFields.c2g__CODAAccountsReceivableControl__c}"/>
</apex:pageBlockSectionItem>

</apex:pageBlockSection>

<apex:pageBlockSection columns="2" title="Create Cash Entry Button" collapsible="false">
<apex:pageBlockSectionItem helpText="Automatically Post FinancialForce Cash Entries after they have been created from PaymentConnect Payments using the Create Cash Entry Button.">
<apex:outputLabel value="Auto-Post Cash Entries" />
<apex:inputCheckbox value="{!batchSettings.p2f__Post_Cash_Entries__c}"/>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem rendered="false" helpText="Include transaction fees from each Payment record when posting payments to Cash Entries in FinancialForce.">
<apex:outputLabel value="Include Transaction Fees" />
<apex:inputCheckbox value="{!batchSettings.p2f__Include_Transaction_Fees__c}"/>
</apex:pageBlockSectionItem>


</apex:pageBlockSection>
<apex:pageBlockSection columns="1" title="Create Invoice Button">
<apex:pageBlockSectionItem helpText="Automatically Post FinancialForce Invoices after they have been created from PaymentConnect Payments using the Create Invoice Button.">
<apex:outputLabel value="Auto-Post Invoices" />
<apex:inputCheckbox value="{!batchSettings.p2f__Post_Invoices__c}"/>
</apex:pageBlockSectionItem>

</apex:pageBlockSection>

<apex:pageBlockSection columns="1" title="Create Cash Entries Batch Script">
<apex:pageBlockSectionItem helpText="Set of completed payments for which cash entries should be batch created. Processing only payments with invoices allows you to exclude other payments so they can be processed manually or posted to the journal directly." >
<apex:outputLabel value="Create Cash Entries For"/>
<apex:selectList multiselect="false" size="1">
<apex:selectOption itemValue="All Completed Payments" itemLabel="All Completed Payments"/>
<apex:selectOption itemValue="Completed Payments With Invoices" itemLabel="Completed Payments With Invoices"/>
</apex:selectList>
</apex:pageBlockSectionItem>



<apex:pageBlockSectionItem helpText="Batch payment processing timeframe (in days).  Represents 'n' in the last n days that the batch script should search for completed Payments that have not already been processed as a Cash Entry.">
<apex:outputLabel value="Batch Create Cash Entry Timeframe" />
<apex:inputText value="{!batchSettings.p2f__Batch_Script_LastNDays__c}"/>
</apex:pageBlockSectionItem>

</apex:pageBlockSection>
</apex:pageBlock>

<apex:pageBlock title="Create Cash Entries Batch Script Scheduler">
<apex:pageBlockSection >

<apex:pageBlockSectionItem rendered="{!(isScheduled)}">
<apex:outputLabel value="Scheduled Batch Job Status:" />
<apex:outputText value="Scheduled"/>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem rendered="{!(isScheduled)}">
<apex:outputLabel value="Next Scheduled Run:" />
<apex:outputText value="{!nextFireTime}"/>
</apex:pageBlockSectionItem>



<apex:pageBlockSectionItem rendered="{!NOT(isScheduled)}">
<apex:outputLabel value="Schedule batch job to run:"/>
<apex:selectList size="1" multiselect="false" value="{!selectedFreqHour}">
<apex:selectOption itemValue="*" itemLabel="Hourly"/>
<apex:selectOption itemValue="0/2" itemLabel="Every 2 Hours"/>
<apex:selectOption itemValue="0/4" itemLabel="Every 4 Hours"/>
<apex:selectOption itemValue="0/6" itemLabel="Every 6 Hours"/>
<apex:selectOption itemValue="0/12" itemLabel="Every 12 Hours"/>
<apex:selectOption itemValue="0" itemLabel="0:00"/>
<apex:selectOption itemValue="1" itemLabel="1:00"/>
<apex:selectOption itemValue="2" itemLabel="2:00"/> 
<apex:selectOption itemValue="3" itemLabel="3:00"/>
<apex:selectOption itemValue="4" itemLabel="4:00"/>
<apex:selectOption itemValue="5" itemLabel="5:00"/>
<apex:selectOption itemValue="6" itemLabel="6:00"/>
<apex:selectOption itemValue="7" itemLabel="7:00"/>
<apex:selectOption itemValue="8" itemLabel="8:00"/>
<apex:selectOption itemValue="9" itemLabel="9:00"/>
<apex:selectOption itemValue="10" itemLabel="10:00"/>
<apex:selectOption itemValue="11" itemLabel="11:00"/>
<apex:selectOption itemValue="12" itemLabel="12:00"/>
<apex:selectOption itemValue="13" itemLabel="13:00"/>
<apex:selectOption itemValue="14" itemLabel="14:00"/>
<apex:selectOption itemValue="14" itemLabel="14:00"/>
<apex:selectOption itemValue="15" itemLabel="15:00"/>
<apex:selectOption itemValue="16" itemLabel="16:00"/>
<apex:selectOption itemValue="17" itemLabel="17:00"/>
<apex:selectOption itemValue="18" itemLabel="18:00"/>
<apex:selectOption itemValue="19" itemLabel="19:00"/>
<apex:selectOption itemValue="20" itemLabel="20:00"/>
<apex:selectOption itemValue="21" itemLabel="21:00"/>
<apex:selectOption itemValue="22" itemLabel="22:00"/>
<apex:selectOption itemValue="23" itemLabel="23:00"/>
</apex:selectList>
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem rendered="{!NOT(isScheduled)}">
<apex:outputLabel value="Day of the week:"/>
<apex:selectList size="1" multiselect="false" value="{!selectedFreqDay}">
<apex:selectOption itemValue="E" itemLabel="Every Day"/>
<apex:selectOption itemValue="1" itemLabel="Sun"/>
<apex:selectOption itemValue="2" itemLabel="Mon"/>
<apex:selectOption itemValue="3" itemLabel="Tue"/>
<apex:selectOption itemValue="4" itemLabel="Wed"/>
<apex:selectOption itemValue="5" itemLabel="Thu"/>
<apex:selectOption itemValue="6" itemLabel="Fri"/>
<apex:selectOption itemValue="7" itemLabel="Sat"/>
</apex:selectList>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
<apex:outputLabel value=""/>
<apex:outputPanel >
<apex:commandButton value="Schedule Batch Job" action="{!scheduleBatchJob}" rendered="{!NOT(isScheduled)}"/>
<apex:commandButton value="Cancel Scheduled Batch Job" action="{!cancelBatchJob}" rendered="{!(isScheduled)}"/>
<apex:commandButton value="Run Batch Job Once" action="{!runBatchJob}" />
</apex:outputPanel>
</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
<apex:outputLabel value=""/>

</apex:pageBlockSectionItem>
</apex:pageBlockSection>
<br/>

</apex:pageBlock>

</apex:form>
</apex:page>