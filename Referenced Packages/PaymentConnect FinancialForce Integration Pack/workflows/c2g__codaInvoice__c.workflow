<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Customer_Reference_Field</fullName>
        <description>Assigned the Sales Invoice record id to the Customer Reference field.</description>
        <field>c2g__CustomerReference__c</field>
        <formula>Id</formula>
        <name>Set Customer Reference Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Auto-Populate Customer Reference</fullName>
        <actions>
            <name>Set_Customer_Reference_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>c2g__codaInvoice__c.c2g__InvoiceStatus__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>c2g__codaInvoice__c.c2g__CustomerReference__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Auto-populates the customer reference field on each new Sales Invoice with the record Id of the Sales Invoice (to facilitate Background Matching of payments posted as Cash Entries later).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
