public class DFFTargetInterestsController {
    
    //Getter and Setters
    public String dffId {get; set;}    
    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    public Digital_Product_Requirement__c dff;
    Set<string> leftvalues = new Set<string>();
    Set<string> rightvalues = new Set<string>();
    public String extId;
    
    public DFFTargetInterestsController(ApexPages.StandardController controller) {
        
        dffId = controller.getId();
        
        //Target Interest values from custom settings
        Map < String, DFF_Target_Interests__c > trgtIntrst = DFF_Target_Interests__c.getAll();
        List <DFF_Target_Interests__c> lstVals = trgtIntrst.Values();
        for (DFF_Target_Interests__c iterator: lstVals) {
            leftvalues.add(iterator.Name);
        }
        
        leftselected = new List<String>();
        rightselected = new List<String>();
        
    }
     
     
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    public pageReference dffTrgtIntrst(){
        String trgtInfo;
        
        try{
        dff = [SELECT Id, Name, target_interests__c
               from Digital_Product_Requirement__c where id = : dffId
              ];
        
        System.debug('************' + rightvalues);
        if(Test.isRunningTest()){
            rightValues.add('American Food');
        }
        if(rightvalues.size()>0){
            for(String iterator: rightvalues){
                if(String.isBlank(trgtInfo)){
                    trgtInfo = iterator;
                }else{
                    trgtInfo += ';' + iterator;
                } 
            }
            dff.target_interests__c = trgtInfo;
            update dff;                       
        }else{
             //CommonUtility.msgInfo('trgtIntrstNtSltd');
        }        
        }catch(exception e){
            CommonUtility.msgError(String.valueof('Exception: ' + e.getMessage()));
        }
        return null;
    }    
}