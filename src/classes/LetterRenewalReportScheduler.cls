global class LetterRenewalReportScheduler implements Schedulable {
   global void execute(SchedulableContext sc) {
      LetterRenewalBatchController  lrBatch = new LetterRenewalBatchController (); 
      database.executebatch(lrBatch);
   }
}