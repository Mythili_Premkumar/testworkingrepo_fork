public class BillingTransferCommonMethods
{
  public void loadCanvassPickList(list < SelectOption > CanvassPickList,Map < Id, String > mapforEditionName,Id accountId) {
        Set < Id > canvassIdSet = new Set < Id > ();
        for (opportunity iterator: [select id, Account_Primary_Canvass__r.name, Account_Primary_Canvass__r.id from opportunity where AccountId = : accountId]) {
            if (iterator.Account_Primary_Canvass__r.name != null && !canvassIdSet.contains(iterator.Account_Primary_Canvass__r.Id)) {
                CanvassPickList.add(new SelectOption(iterator.Account_Primary_Canvass__r.Id, iterator.Account_Primary_Canvass__r.name));
                canvassIdSet.add(iterator.Account_Primary_Canvass__r.Id);
                mapforEditionName.put(iterator.Account_Primary_Canvass__r.Id, iterator.Account_Primary_Canvass__r.name);

            }

        }
    }
    public boolean fetchOrderSet(Set < Id > directoryIdSet, Set < Id > directoryeditonIdSet,  Id canvassId, Id accountId,boolean editionCheck) {
        editionCheck = false;
        List < Order_Group__c > orderSetList = new List < Order_Group__c > ();
        orderSetList = OrderGroupSOQLMethods.getOrderGroupByAcctIdOpptyCanvassIdForBillingTransfer(accountId, canvassId);
        if (orderSetList.size() > 0) {
            editionCheck = true;
            for (Order_Group__c OG: orderSetList) {
                list < Order_Line_Items__c > tempOLIList = new list < Order_Line_Items__c > ();
                tempOLIList = OG.Order_Line_Items__r;
                if (tempOLIList.size() > 0) {
                    set < String > editionOrCanvassCodes = new set < String > ();
                    for (Order_Line_Items__c OLI: tempOLIList) {
                        if (OLI.media_type__c == 'Print') {
                            if (OLI.Directory_Edition__c != null) {
                                directoryIdSet.add(OLI.Directory_Edition__r.directory__c);
                                directoryeditonIdSet.add(OLI.Directory_Edition__c);
                            }
                        }
                    }
                }
            }
        }
        return editionCheck;
    }
    public  string loadAccountPickList(list < SelectOption > accountPickList,Id accountId,string selectedAccountID) {
    System.debug('#####selectedAccountID  before'+selectedAccountID );
        for (Account iterator: fetchAccount(accountId)) {
            if (String.isEmpty(selectedAccountID)) {
                if (iterator.id == accountId) {
                    selectedAccountID = iterator.id;
                }
            }
            accountPickList.add(new SelectOption(iterator.Id, iterator.Name));
        }
        System.debug('#####selectedAccountID  in c'+selectedAccountID );
        return selectedAccountID;
    }
    public void loadPaymentMethodPickList(string partner,list < SelectOption > paymentMethodPickList) {
        Schema.DescribeFieldResult F = Opportunity.Payment_Method__c.getDescribe();
        List < Schema.PicklistEntry > pickList = F.getPicklistValues();
        paymentMethodPickList.add(new SelectOption('None', '--None--'));
        for (Schema.PicklistEntry iteratorP: pickList) {
            if (partner == 'berry') {
                if (iteratorP.getValue() != 'Telco Billing') paymentMethodPickList.add(new SelectOption(iteratorP.getValue(), iteratorP.getValue()));
            } else if (partner == 'telco') {
                if (iteratorP.getValue() == 'Telco Billing') paymentMethodPickList.add(new SelectOption(iteratorP.getValue(), iteratorP.getValue()));
            }
        }
    }
    public void loadBillingPartner(list < SelectOption > billingPartnerPickList,Map < String, String > mpForPartner,string selectedPartner,string selectedCanvass) {
        billingPartnerPickList.add(new SelectOption('None', '--None--'));
        if (String.isBlank(selectedPartner)) {
            selectedPartner = 'None';
        }
        List < Account > berryAccount = [select id, Name from Account where name = : CommonMessages.berryTelcoName];
        mpForPartner = fetchTelcoName(selectedCanvass);
        if (berryAccount.size() > 0)
            if (!mpForPartner.containsKey(berryAccount[0].id)) mpForPartner.put(berryAccount[0].id, berryAccount[0].Name);
        for (String strPartner: mpForPartner.KeySet()) {
            billingPartnerPickList.add(new SelectOption(strPartner, mpForPartner.get(strPartner)));
        }
    }       
    public list < Contact > fetchContact(string strSelectedAccountID) {
        return ContactSOQLMethods.getContactByAccountIDForBT(new set < Id > {
            strSelectedAccountID
        });
    } 
    public void loadBillingFrequencyPickList(list < SelectOption > billingFrequencyPickList,string selectedFrequency) {
        Schema.DescribeFieldResult F = Opportunity.Billing_Frequency__c.getDescribe();
        List < Schema.PicklistEntry > pickList = F.getPicklistValues();
        billingFrequencyPickList.add(new SelectOption('None', '--None--'));
        if (String.isBlank(selectedFrequency)) {
            selectedFrequency = 'None';
        }
        for (Schema.PicklistEntry iteratorP: pickList) {
            billingFrequencyPickList.add(new SelectOption(iteratorP.getValue(), iteratorP.getValue()));
        }
    }
    public void loadReasonForTransferPickList(list < SelectOption > reasonForTransferList,string selectedReasonForTransfer) {
        Schema.DescribeFieldResult F = Order_Line_Items__c.Reason_for_Transfer__c.getDescribe();
        List < Schema.PicklistEntry > pickList = F.getPicklistValues();
        reasonForTransferList.add(new SelectOption('None', '--None--'));
        if (String.isBlank(selectedReasonForTransfer)) {
            selectedReasonForTransfer = 'None';
        }
        for (Schema.PicklistEntry iteratorP: pickList) {
            reasonForTransferList.add(new SelectOption(iteratorP.getValue(), iteratorP.getValue()));
        }
    }               
    private list < Account > fetchAccount(Id accountId) {
        return AccountSOQLMethods.getAccountByIDandParentID(new set < Id > {
            accountId
        });
    }
    private Map < String, String > fetchTelcoName(string selectedCanvass) {
        Map < String, String > mapOfAccountwithBillingPartner = new Map < String, String > ();
        set < String > partnerName = new set < String > ();
        list < Directory_Mapping__c > lstDL = [select Id, Name, Canvass__c, Directory__c, Telco__c, Default_Directory_Telco__c, Default_Canvass_Telco__c, Telco__r.Account__c, Telco__r.Telco_Account_Name__c FROM Directory_Mapping__c where Canvass__c = : selectedCanvass AND DM_Use_As_Billing_Partner__c=true];
        for (Directory_Mapping__c iteratorDL: lstDL) {
            if (String.isNotBlank(iteratorDL.Telco__r.Telco_Account_Name__c)) {
                mapOfAccountwithBillingPartner.put(string.ValueOf(iteratorDL.Telco__r.Account__c), iteratorDL.Telco__r.Telco_Account_Name__c);
            }
        }
        return mapOfAccountwithBillingPartner;
    }              
}