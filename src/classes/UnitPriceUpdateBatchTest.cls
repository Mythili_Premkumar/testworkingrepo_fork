@isTest
public with sharing class UnitPriceUpdateBatchTest {
	static testMethod void unitPriceTest() {
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Family = 'Print';
        insert product2;  
        Directory__c dir = TestMethodsUtility.createDirectory();          
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.National_Rates_Due__c = System.today();
        insert DE;
        Digital_Product_Requirement__c DFF = TestMethodsUtility.generateDataFulfillmentForm();
        insert DFF;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og); 
        oln.Directory_Edition__c = DE.Id;
        oln.Digital_Product_Requirement__c = DFF.Id;
        oln.Directory__c = dir.Id;
        oln.Product2__c = product2.Id;
        oln.Billing_Partner__c = CommonMessages.berryTelcoName;
        oln.Billing_Frequency__c = CommonMessages.monthlyPayment;
        oln.isCanceled__c = false;
        oln.RecordTypeId = 	system.Label.TestOLIRTNational;
        oln.UnitPrice__c = 90;
        insert oln;
        Test.startTest();
		UnitPriceUpdateBatch obj = new UnitPriceUpdateBatch(system.today());
		Database.executeBatch(obj);
        Test.stopTest();		
	}
}