public class LeadfromCaption_v1{
  
    public static void listingLeadpopulate(list<Listing__c> mainLst){ 
     //Set Varibales to store the values 
     set<String> setStreet = new set<String>();
     set<String> setState = new set<String>();
     set<String> setCity = new set<String>();
     set<String> setCountry = new set<String>();
     set<String> setPostalCode = new set<String>();
     set<String> setPhone = new set<String>();
     set<String> setListingName = new set<String>();
     set<String> setFirstName = new set<String>();
     //set<String> setCompany = new set<String>();
     //Lead record type 
     Schema.DescribeSObjectResult RLead = Lead.SObjectType.getDescribe(); 
         List<Schema.RecordTypeInfo> RTLead = RLead.getRecordTypeInfos(); 
         map<String,String> mapLeadRecordTypes = new map<String,String>(); 
             for(Schema.RecordTypeInfo R : RTLead){ 
                 mapLeadRecordTypes.put(R.getName(),R.getRecordTypeId()); 
             } 
    map<String,Listing__c> mapactldLst = new map<String,Listing__c>();
    system.debug('********LISTING LIST*********'+mainLst);
    if(mainLst.size() >0){
        for(Listing__c objLstng : mainLst){
            system.debug('**********After Insert Start ******************');
                if(string.isBlank(objLstng.Lead__c) && objLstng.Bus_Res_Gov_Indicator__c == 'Business'){
                system.debug('**********After Insert Start  entere here if lead is null ******************');
                 system.debug('********LISTING LIST ENTER HERE*********');
                        if(objLstng.Name != null){
                            setListingName.add(objLstng.Name);
                        }
                        if(objLstng.Listing_Street__c != null){
                            setStreet.add(objLstng.Listing_Street__c);
                        }
                        if(objLstng.Listing_State__c != null){
                            setState.add(objLstng.Listing_State__c);
                        }
                        if(objLstng.Listing_City__c != null){
                            setCity.add(objLstng.Listing_City__c);
                        }
                        if(objLstng.Listing_Country__c != null){
                            setCountry.add(objLstng.Listing_Country__c);
                        }
                        if(objLstng.Listing_Postal_Code__c != null){
                            setPostalCode.add(objLstng.Listing_Postal_Code__c);
                        }
                        if(objLstng.Phone__c != null){
                            setPhone.add(objLstng.Phone__c);
                        }
                        if(objLstng.First_Name__c != null){
                            setFirstName.add(objLstng.First_Name__c);
                        }
                        //setCompany.add(objLstng.Company__c);
                        
                        String strgActLdLst = objLstng.Name+''+objLstng.Phone__c+''+objLstng.Listing_Street__c+''+objLstng.Listing_City__c+''+objLstng.Listing_State__c+''+objLstng.Listing_Postal_Code__c+''+objLstng.Listing_Country__c;
                        mapactldLst.put(strgActLdLst, objLstng);
                        
                        //String strgLeadLst = objLstng.Last_Name__c+''+objLstng.First_Name__c+''+strgActLst;
                        //mapLeadLst.put(strgLeadLst,objLstng);
                        
                
            }
        }
    }   
        List<Listing__c> lstListingsAccToUpdate = new List<Listing__c>();
        List<Listing__c> lstListingsLeadToUpdate = new List<Listing__c>();
        
        //Existing Accounts     

        for(Account acct : [select Id, Name,Phone,Telco_Partner__c,BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingCountry  from Account 
                                where BillingStreet IN :setStreet 
                                AND BillingState IN :setState 
                                AND BillingCountry IN :setCountry 
                                AND BillingPostalCode IN :setPostalCode 
                                AND Phone IN :setPhone 
                                AND name IN :setListingName]){
            String strCheckact = acct.Name+''+acct.Phone+''+acct.BillingStreet+''+acct.BillingCity+''+acct.BillingState+''+acct.BillingPostalCode+''+acct.BillingCountry;
            system.debug('*********Account String Check ********* '+strCheckact);   
            if(mapactldLst.get(strCheckact) != null){
                Listing__c objL = new Listing__c(Id=mapactldLst.get(strCheckact).Id); 
                    objL.Account__c = acct.Id;
                    lstListingsAccToUpdate.add(objL);
                    mapactldLst.remove(strCheckact);
            }
            
       }
       system.debug('*********Listing to update with Account ***********'+lstListingsAccToUpdate);
       if(lstListingsAccToUpdate.size()>0){
            database.update(lstListingsAccToUpdate,false);
       }
        //Existing Leads 
       if(mapactldLst.size()>0){
          
        for(Lead ld : [select Id, LastName, FirstName, Name,Telco_Partner_Account__c,Company, Street, City, State, PostalCode, Country, Phone  from Lead 
                                where (Street != null AND Street IN :setStreet) 
                                AND (State != null AND State IN :setState)
                                AND (City != null AND City IN :setCity) 
                                AND (Country != null AND Country IN :setCountry) 
                                AND (PostalCode != null AND PostalCode IN :setPostalCode)
                                AND (Phone != null AND Phone IN :setPhone)
                                AND (Name != null AND Name IN :setListingName)]){
            
            String strChecklead = ld.LastName+''+ld.Phone+''+ld.Street+''+ld.City+''+ld.State+''+ld.PostalCode+''+ld.Country;
            system.debug('*********Lead String Check ********* '+strChecklead);
            if(mapactldLst.get(strChecklead) != null){
                Listing__c objL = new Listing__c(Id=mapactldLst.get(strChecklead).Id); 
                    objL.Lead__c = ld.Id;
                    objL.Is_Lead__c = True;
                    lstListingsLeadToUpdate.add(objL); 
                    mapactldLst.remove(strChecklead); 
                }
            }
       }
        list<Lead> leadLstInsert = new list<Lead>();
            if(mapactldLst.size()>0){
            system.debug('********Insert lead*************');
                for(Listing__c objLstng :mapactldLst.values()){
                    Lead objLead = new Lead();
                    objLead.LastName = objLstng.Name;
                    objLead.FirstName = objLstng.First_Name__c;
                    objLead.Company =  objLstng.Name;
                    objLead.Phone = objLstng.Phone__c;
                    objLead.Street = objLstng.Listing_Street__c;
                    objLead.City = objLstng.Listing_City__c;
                    objLead.State = objLstng.Listing_State__c;
                    objLead.PostalCode = objLstng.Listing_Postal_Code__c;
                    objLead.Country = objLstng.Listing_Country__c;
                    objLead.Lead_Account_Source__c = objLstng.Lead_Account_Source__c;
                    objLead.Status = 'Unqualified';
                    objLead.Create_Listing__c = 'No';
                    objLead.Primary_Canvass__c = objLstng.Primary_Canvass__c;
                    if(objLstng.Telco_Provider_Account_ID__c != null){
                        objLead.Telco_Partner_Account__c = objLstng.Telco_Provider_Account_ID__c;
                    }
                    objLead.RecordtypeId = mapLeadRecordTypes.get('Service Order Lead');
                    leadLstInsert.add(objLead);
                    
                }
            
            system.debug('*********Lead insertion List ************'+leadLstInsert);
             if(leadLstInsert.size()>0){
                insert leadLstInsert;
             }
                for(Lead objLd :leadLstInsert ){
                    String strCheckld = objLd.LastName+''+objLd.Phone+''+objLd.Street+''+objLd.City+''+objLd.State+''+objLd.PostalCode+''+objLd.Country;
                    if(mapactldLst.get(strCheckld) != null){
                        Listing__c objLst = new Listing__c(Id=mapactldLst.get(strCheckld).Id); 
                        objLst.Lead__c = objLd.Id;
                        objLst.Is_Lead__c = True;
                        lstListingsLeadToUpdate.add(objLst);
                        system.debug('*********Listing Updated with the recent Lead ************'+lstListingsLeadToUpdate); 
                        mapactldLst.remove(strCheckld);
                        system.debug('*********Listing Updated with the recent Lead ************'+lstListingsLeadToUpdate);
                    }
                }
            }
            system.debug('*********Listing Updated with the recent Lead ************'+lstListingsLeadToUpdate);
            if(lstListingsLeadToUpdate != null && lstListingsLeadToUpdate.size()>0 && CommonVariables.LeadListingAdrRecursive==false){
               CommonVariables.LeadListingAdrRecursive=true;
                database.update (lstListingsLeadToUpdate,false);
            
            }
     
    }
    
    public static void addnLstLeadpopulate(map<Id,list<listing__c>> mapListing){
        list<listing__c> objmainLst = new list<listing__c>();
        list<listing__c> addlistUpdate = new list<listing__c>();
        system.debug('********Main Listing map ******'+mapListing);
        objmainLst = [Select Id,Name,Lead__c,Account__c,Main_Listing__c from Listing__c where Id IN : mapListing.keyset()];
        system.debug('********Main Listing******'+objmainLst);
        if(objmainLst.size()>0){
            for(listing__c objMainLstng : objmainLst){
                if(mapListing.get(objMainLstng.Id) != null){
                    for(listing__c objAddnLst : mapListing.get(objMainLstng.Id)){
                        Listing__c addLstng = new Listing__c(Id=objAddnLst.Id);
                        if(objMainLstng.Account__c != null){
                            addLstng.Account__c = objMainLstng.Account__c;
                        }
                        else if(objMainLstng.Lead__c != null){
                        system.debug('********Main Listing Lead enter here******'+objMainLstng.Lead__c);
                            addLstng.Lead__c = objMainLstng.Lead__c;
                        }
                        addlistUpdate.add(addLstng );
                    }
                }
            
            }
        }
        system.debug('** Additional listings update #####'+addlistUpdate);
        if(addlistUpdate.size()>0){
            //CommonVariables.LeadListingAdrRecursive=true;
            database.update (addlistUpdate,false);
            
        }
    }
    
}