global class BillingFirstMonthPrintScheduler implements Schedulable {
    global void execute(SchedulableContext SC) {
        list <Order_Line_Items__c> lstOLITelcoPrint = [SELECT Id, Name, Order__c, Order__r.Account__c, Canvass__c, Description__c , Directory_Code__c, Directory__c, Directory_Edition__c,
                Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.Family, Product2__r.RGU__c,
                ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, UnitPrice__c, Billing_Contact__c,  Edition_Code__c,
                Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, Billing_Start_Date__c, Telco_Invoice_Date__c, Print_First_Bill_Date__c,
                Digital_Product_Requirement__c,Payment_Method__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c, 
                Service_Start_Date__c, Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c,Talus_Fulfillment_Date__c,  Current_Daily_Prorate__c, 
                Quote_Signed_Date__c, Quote_signing_method__c, Total_Prorated_Days_for_contract__c, Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, Order_Group__c, 
                Billing_Close_Date__c, Order_Anniversary_Start_Date__c,Last_Billing_Date__c, Next_Billing_Date__c,  Line_Status__c, 
                Continious_Billing__c, Prorate_Stored_Value__c, Cancellation__c,Total_Prorate__c, Product2__r.Name, Product2__r.Product_Type__c, 
                Prorate_Credit__c, Prorate_Credit_Days__c, Current_Billing_Period_Days__c, Order_Line_Total__c, BillingChangeNoofDays__c, CMR_Name__c,
                Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c, BillingChangesPayment__c, Order_Billing_Date_Changed__c, OriginalBillingDate__c, 
                P4P_Price_Per_Click_Lead__c, P4P_Current_Billing_Clicks_Leads__c,  P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c,Billing_Partner_Account__c, Order_Line_Items__c.Account__c
                FROM Order_Line_Items__c 
                WHERE IsCanceled__c = False 
                AND Telco_Invoice_Date__c = :system.today()
                AND Talus_Go_Live_Date__c = Null
                AND Product2__r.Media_Type__c IN :new set<String>{'Print'}];
                
        if(!lstOLITelcoPrint.isEmpty()) {
            BillingFirstMonthPrintHandlerCtl.processTelcoInvoiceAndFirstPrintBillDateBilling(lstOLITelcoPrint);
        }
    }
}