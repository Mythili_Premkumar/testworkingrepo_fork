@IsTest(seeAllData=True)

private class DirectoryGuide_Test
{
     static testMethod void listingSecHeadLst() {
        Test.StartTest();
        Canvass__c objCanvss = new Canvass__c();
        objCanvss.Name = 'Test Canvass';
        objCanvss.co_brand_name__c = 'Test';
        objCanvss.IsActive__c = true;
            
        insert objCanvss;
         
        List<Directory__c> ObjdirLst = new List<Directory__c>();
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.EBD__c = System.Today().addDays(1);
        objDir.DCR_Close__c = System.Today().addDays(5);
        objDir.BOC__c = System.Today().addDays(2);
        objDir.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDir.Sales_Lockout__c = System.Today();
        objDir.Book_Extract_YP__c = System.Today().addDays(3);
        objDir.Ship_Date__c = System.Today().addDays(-2);
        objDir.Pub_Date__c = System.Today().addDays(4);
        objDir.Canvass__c = objCanvss.id;
        objDir.Directory_Code__c = '123472';
        
        ObjdirLst.add(objDir);
        
        insert objdirLst;
        
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = objdirLst[0].id;
        objDirSec.Name = 'Test Directory Section';
        objDirSec.Section_Page_Type__c = 'YP';
        objDirSec.Section_Code__c = '123472';
        
        insert objDirSec;
        
        Directory_Heading__c objDirHeading = new Directory_Heading__c();
        objDirHeading.Name = 'Test Dir Heading';
        
        insert objDirHeading;
        
        Directory_Guide__c objDirGuide = new Directory_Guide__c();
        objDirGuide.Name = 'test guide';
        
        insert objDirGuide;
        
        Heading_Disclaimer__c objHeadDisc = new Heading_Disclaimer__c();
        objHeadDisc.Name = 'Test Disc';
        objHeadDisc.Disclaimer_Text__c = 'Test Heading Disclaimer Text';
        
        insert objHeadDisc;
        
        
        Section_Heading_Mapping__c objSecHeadMap = new Section_Heading_Mapping__c();
        objSecHeadMap.Name = 'test heading mapping';
        objSecHeadMap.Directory_Heading__c = objDirHeading.id;
        objSecHeadMap.Directory_Section__c = objDirSec.id;
        objSecHeadMap.Directory_Guide__c = objDirGuide.id;
        objSecHeadMap.Heading_Disclaimer__c = objHeadDisc.id;
        objSecHeadMap.Guide_Telltale__c = '1234';
        
        insert objSecHeadMap;
        
        ApexPages.StandardController stdcontroller = new ApexPages.standardController(new Directory_Section__c());    
        ApexPages.currentPage().getParameters().put('Id',objDirSec.Id);
        
        List<DirectoryGuide.WrapobjSecHead> lstDirGuideSecHead = new List<DirectoryGuide.WrapobjSecHead>();
        DirectoryGuide LstDirGuide = new DirectoryGuide(stdcontroller);
        
        
        ApexPages.StandardSetController con = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT Name,Id FROM Section_Heading_Mapping__c limit 10]));
        
        
        LstDirGuide.con=con;
       
        
        LstDirGuide.TxtHeadDisclmID = objHeadDisc.Id;
        DirectoryGuide.WrapobjSecHead WrapSecHeadMap = new DirectoryGuide.WrapobjSecHead();
        WrapSecHeadMap.ischecked = true;
        WrapSecHeadMap.objSecHMap=objSecHeadMap;
        lstDirGuideSecHead.add(WrapSecHeadMap);
        LstDirGuide.WrapobjSecHeadLst = new List<DirectoryGuide.WrapobjSecHead>();
        LstDirGuide.WrapobjSecHeadLst.addall(lstDirGuideSecHead);
       // LstDirGuide.lstDir = new Directory_Section__c ();
       // LstDirGuide.lstDir = objDirSec;
       // LstDirGuide.listingSecHeadLst(objDirSec.Id);
        
        try {
            LstDirGuide.UpdateDirGuides();           
               List<DirectoryGuide.WrapobjSecHead> lstDirGuideSecHead1 = new List<DirectoryGuide.WrapobjSecHead>();
                DirectoryGuide LstDirGuide1 = new DirectoryGuide(stdcontroller);
                DirectoryGuide.WrapobjSecHead WrapSecHeadMap1 = new DirectoryGuide.WrapobjSecHead();
                WrapSecHeadMap1.ischecked = true;
                WrapSecHeadMap1.objSecHMap=objSecHeadMap;
                lstDirGuideSecHead1.add(WrapSecHeadMap1);
                LstDirGuide1.WrapobjSecHeadLst = new List<DirectoryGuide.WrapobjSecHead>();
                LstDirGuide1.WrapobjSecHeadLst.addall(lstDirGuideSecHead1);        
               LstDirGuide1.DeleteData(); 
               
        LstDirGuide1.first(); 
       LstDirGuide1.last();  
        LstDirGuide1.cancel();  
       
       
        LstDirGuide1.DeleteDisclaimerData();               
               LstDirGuide.UpdateHeadingDisclaimer();
                    List<DirectoryGuide.WrapobjSecHead> lstDirGuideSecHead2 = new List<DirectoryGuide.WrapobjSecHead>();
                    DirectoryGuide LstDirGuide2 = new DirectoryGuide(stdcontroller);
                    DirectoryGuide.WrapobjSecHead WrapSecHeadMap2 = new DirectoryGuide.WrapobjSecHead();
                    WrapSecHeadMap2.ischecked = true;
                    WrapSecHeadMap2.objSecHMap=objSecHeadMap;
                    lstDirGuideSecHead2.add(WrapSecHeadMap2);
                    LstDirGuide2.WrapobjSecHeadLst = new List<DirectoryGuide.WrapobjSecHead>();
                    LstDirGuide2.WrapobjSecHeadLst.addall(lstDirGuideSecHead2);
                    
               LstDirGuide2.DeleteDisclaimerData();

                
        }
        catch(Exception e){}
        
        //LstDirGuide.Beginning();
        LstDirGuide.Previous();
        LstDirGuide.Next();
       // LstDirGuide.End();
        //Boolean b1 = LstDirGuide.getDisablePrevious();
       // Boolean b2 = LstDirGuide.getDisableNext();
        //Integer i1 = LstDirGuide.getTotal_size();
        //Integer i2 = LstDirGuide.getPageNumber();
        //Integer i3 = LstDirGuide.getTotalPages();
         
        Test.StopTest();  
     }

}