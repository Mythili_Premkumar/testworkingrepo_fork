public class TelcoSOQLMethods {
	public static list<Telco__c> getTelco(set<Id> setCLECIds){
        return [SELECT Id,RecordtypeId FROM Telco__c WHERE Id IN : setCLECIds];
   } 
}