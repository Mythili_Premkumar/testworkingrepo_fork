public with sharing class SelectOrderSetControllerSFDC {

    // getters and setters
    public string accountID{get;set;}
    public Date todayDate{get;set;}
    Map<String,List<Order_Line_Items__c>> BundleProductValidationMap=new Map<String,List<Order_Line_Items__c>>();
    public boolean CheckBool{get;set;}
    public boolean CheckCancellationCaseFlag{get;set;}
    public string selectedCanvass{get;set;}
    public string OLIIdforcheck{get;set;}
    public list <string> OrderSetID{get;set;}
    public list <string> OrderLineID{get;set;}
    public string contactID{get;set;}
    public string caseID{get;set;}
    public Integer OLICount{get;set;}
    public List <Order_Group__c> OrderSetObj{get; set;}
    public list <Order_Line_Items__c> OLIs{get; set;}
    public Contact ContactObj{get; set;}
    public Case CaseObj{get; set;}
    public Boolean cancelpage2{get; set;}
    public string cp2{get; set;}
    public Integer pagenum{get; set;}
    public Decimal PendingFees{get; set;}
    public Decimal BilledFees{get; set;}
    public list < SelectOption > CanvassPickList{get;set;} 
    public string selectedReasonForCancel{get;set;}
    public List<Product2> productList;
    //Map<String, Date> mpP4PCancelDate = new Map<String, Date>();
    //Map<Id,Order_Line_Items__c> mpIdOLI = new Map<Id,Order_Line_Items__c>();
    List<Order_Line_Items__c> oliForInvoice=new List<Order_Line_Items__c>(); 
    Id branchManagerId;
    boolean zeroCancelFeeRequested;
    // constructor
    public SelectOrderSetControllerSFDC(ApexPages.StandardController controller) {
        //get url prams
        CanvassPickList = new list < SelectOption > ();
        zeroCancelFeeRequested=false;
        CanvassPickList.add(new SelectOption('none', '--None--')); 
        productList=new List<Product2>();
         productList =[select id from Product2 where name='Cancellation Fee' limit 1];             
        caseID = ApexPages.currentPage().getParameters().get('caseid');
        string tmp =ApexPages.currentPage().getParameters().get('pagenum');       
        if(tmp !=null && tmp !=''){
            pagenum = Integer.valueOf(tmp);
        }else {
            pagenum = 0;
        }
        
         System.debug('$$$$$$$$$$$selectedCanvass before '+selectedCanvass);
        if(caseID != Null){
            
            // get case record
            CaseObj = [SELECT Id, CaseNumber, ContactId, AccountId, SuppliedName, SuppliedEmail, SuppliedPhone, SuppliedCompany, Type, RecordTypeId, Status, Reason, 
                         Origin, Subject, Priority, Description, IsClosed, ClosedDate, HasCommentsUnreadByOwner, HasSelfServiceComments, OwnerId, CreatedDate, CreatedById,CheckNewlyCreatedCancellationCase__c,
                         Case_Category__c, Case_Source__c, Customer_Satisfied__c, Main_Listed_Phone_Number__c, Received_From__c, Resolution_Note__c, Total_Credit_Note__c, 
                         User_Responsible_del__c, Digital_Product_Requirement__c, Account.Name,Account.Billing_Anniversary_Date__c,Cancellation_Reason__c
                         FROM Case
                         WHERE Id = :caseID 
                         Limit 1];
                         
           accountID =   CaseObj.AccountId;  
          // CaseObj.Cancellation_Reason__c=null;   
           todayDate=Date.today();
           CheckCancellationCaseFlag=CaseObj.CheckNewlyCreatedCancellationCase__c;  
                   
        loadCanvassPickList(); 
       }
       
    }
    private void loadCanvassPickList() 
    {
        System.debug('$$$$$$$$$$$ '+accountID);
        Set < Id > canvassIdSet = new Set < Id > ();
        for (opportunity iterator: [select id, Account_Primary_Canvass__r.name, Account_Primary_Canvass__r.id from opportunity where AccountId = : accountID ]) {
            if (iterator.Account_Primary_Canvass__r.name != null && !canvassIdSet.contains(iterator.Account_Primary_Canvass__r.Id)) {
                CanvassPickList.add(new SelectOption(iterator.Account_Primary_Canvass__r.Id, iterator.Account_Primary_Canvass__r.name));
                canvassIdSet.add(iterator.Account_Primary_Canvass__r.Id);
                

            }

        }
    } 
    
    public void fetchOrderSets(){
        if(CheckCancellationCaseFlag==true)
        {
        system.debug('*******CheckCancellationCaseFlag*****'+CheckCancellationCaseFlag);
        fetchOrderSetsForCancellationCase();
        }
        
        else
        {
        fetchOrderSetsForNonCancellationCase();
        }
    }
     public void fetchOrderSetsForNonCancellationCase(){
        OrderSetObj = new list<Order_Group__c>();
        System.debug('$$$$$$$$$$$selectedCanvass '+selectedCanvass);
        if(selectedCanvass!='none')
        {
               OrderSetObj = [SELECT Id, IsDeleted, Name, Order__c, Order_Account__c, Opportunity__c ,Order_Set_Selected_Cancel__c , CreatedDate, oli_count__c,(select id from Order_Line_Items__r where isCanceled__c=false AND Cutomer_Cancel_Date__c=null) 
                     FROM Order_Group__c
                     WHERE Order_Account__c = :accountID and Opportunity__r.Account_Primary_Canvass__c =:selectedCanvass order by CreatedDate desc];
        }
        
        else
        {
            OrderSetObj =new List<Order_Group__c>();
        }
    }
    public void fetchOrderSetsForCancellationCase(){
        OrderSetObj = new list<Order_Group__c>();
        if(selectedCanvass!='none')
        {
               OrderSetObj = [SELECT Id, IsDeleted, Name, Order__c, Order_Account__c, Opportunity__c ,Order_Set_Selected_Cancel__c, CreatedDate, oli_count__c,(select id from Order_Line_Items__r where isCanceled__c=false AND Cutomer_Cancel_Date__c=null) 
                     FROM Order_Group__c
                     WHERE Order_Account__c = :accountID and Opportunity__r.Account_Primary_Canvass__c =:selectedCanvass  order by CreatedDate desc];
            for(Order_Group__c ordergroup:OrderSetObj)
            {
                ordergroup.Order_Set_Selected_Cancel__c=false;
            }
        }
        
        else
        {
            OrderSetObj =new List<Order_Group__c>();
        }
    }
    
    public pageReference ShowSelectOLI(){
       
        boolean selecterrormessage=false;
        OrderSetID = new list <string>();
        pageReference pr;
        string checkit='no'; 
        if(OrderSetObj != Null){
            for(Order_Group__c OSR:OrderSetObj){
                if(OSR.Order_Set_Selected_Cancel__c == true ){
                    OrderSetID.add(OSR.Id);
                    checkit='yes' ;   
                }
            } 
        }
        if(checkit == 'no'){
            OrderSetID= null;
          //  pr = new pageReference('/apex/selectordersetforcancel?caseID='+caseID);
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select at least one Order Set');
            ApexPages.addMessage(myMsg);
            pr=null;
            OLIs = null;
            OLICount = 0;
        } else {
              pagenum = 1;
             //
             OLIs = [SELECT Id,Directory_Edition__c,Selected_Cancel_Date__c,Canvass__r.Canvass_Code__c,Directory_Code__c,Billing_Partner_Account__c,Account__c,Order_Line_Items__c.Product2__r.Name,Order_Line_Total__c,Order_Line_Items__c.Product2__r.Family,Order_Line_Items__c.Product2__r.RGU__c,Order_Line_Items__c.Opportunity__r.Name, Name, Billing_Contact__c, Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, 
             Billing_Start_Date__c, isCanceled__c, Cancellation__c, Canvass__c, Continious_Billing__c, Cutomer_Cancel_Date__c, 
             Digital_Product_Requirement__c, Discount__c, Fulfilled_on__c, Description__c, ListPrice__c, Opportunity__c, 
             Order__c, Order_Anniversary_Start_Date__c, FulfillmentDate__c, Order_Group__c, Payment_Duration__c, Payment_Method__c, 
             Payments_Remaining__c, Product2__c, Total_Prorated_Days_for_contract__c, Prorate_Stored_Value__c, ProductCode__c, Quantity__c, Prorate_Credit__c, 
             Total_Prorate__c, Status__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c, Talus_OrderLineItem__c, Talus_Cancel_Date__c, 
             Successful_Payments__c, Quote_Signed_Date__c, Quote_signing_method__c, UnitPrice__c, Sent_to_fulfillment_on__c, Service_End_Date__c, Service_Start_Date__c, 
             Line_Status__c, Billing_Close_Date__c, Contract_End_Date__c, Current_Daily_Prorate__c, Next_Billing_Date__c, 
             Subscription_ID__c, Days_B_W__c, Product_Type__c, Current_Rate__c, Package_ID__c,Comments__c,
             Directory_Section__c, Directory_Heading__c, Scope__c, Last_successful_settlement__c, Telco__c, Directory__c, Directory__r.Companion_Book__c,
             Effective_Date__c, Selected_Cancel__c,Canvass__r.Branch_Manager__c, Cancelation_Fee__c, Current_Billing_Period_Days__c, zero_Cancel_Fee_Requested__c, Zero_Cancel_Fee_Approved__c ,Canvass__r.name,Package_ID_External__c,Parent_ID_of_Addon__c,Parent_ID__c,CMR_Name__c,Directory__r.Name,Directory_Heading__r.Name,Directory__r.Directory_Code__c,Telco__r.Accepts_Cancellation_Fee__c
                    FROM 
                    Order_Line_Items__c    
                    WHERE
                    Order_Group__c in :OrderSetID
                    AND
                    isCanceled__c !=true  AND Cutomer_Cancel_Date__c=null];
                    OLICount = OLIs.size();
                    
        if(CheckCancellationCaseFlag==true)
        {
        for(Order_Line_Items__c orderlineItem:OLIs)
            {
                orderlineItem.Selected_Cancel__c=false;
            }
        }
                    
         }
         
    return pr; 
    }
    public pageReference oliNext(){
     boolean goliveerror=false;
     pageReference pr;

            OrderLineID = new list <string>();
            for(Order_Line_Items__c OLI: OLIs){
                if( OLI.Selected_Cancel__c == True)
                {
                    OrderLineID.add(OLI.id);
                   /* OLI.Selected_Cancel__c = False;
                    if(OLI.Talus_Go_Live_Date__c==null)
                    {
                        goliveerror=true;
                        break;
                    }
                 */       
                }
            }
        if((OrderLineID.size()>0 && !goliveerror))
        {   
             pagenum = 2;  
             pr = new pageReference('/apex/selectordersetforcancel?caseID='+caseID);
             cancelpage2= true;  
                OLIs = null;
                OLIs = [SELECT Id,Directory_Edition__c,Canvass__r.Canvass_Code__c,Directory_Code__c,Billing_Partner_Account__c,Account__c,Order_Line_Items__c.Product2__r.Name,Order_Line_Total__c,Order_Line_Items__c.Product2__r.Family,Order_Line_Items__c.Product2__r.RGU__c,Order_Line_Items__c.Opportunity__r.Name,Media_Type__c, Name, Billing_Contact__c,Billing_Contact__r.Email, Billing_End_Date__c, Billing_Frequency__c, Billing_Partner__c, 
                     Billing_Start_Date__c, isCanceled__c, Cancellation__c, Canvass__c, Continious_Billing__c, Cutomer_Cancel_Date__c, 
                     Digital_Product_Requirement__c, Discount__c, Fulfilled_on__c, Description__c, ListPrice__c, Opportunity__c, 
                     Order__c, Order_Anniversary_Start_Date__c, FulfillmentDate__c, Order_Group__c, Payment_Duration__c, Payment_Method__c, 
                     Payments_Remaining__c, Product2__c, Total_Prorated_Days_for_contract__c, Prorate_Stored_Value__c, ProductCode__c, Quantity__c, Prorate_Credit__c, 
                     Total_Prorate__c, Status__c, Prorate_Credit_Days__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c, Talus_OrderLineItem__c, Talus_Cancel_Date__c, 
                     Successful_Payments__c, Quote_Signed_Date__c, Quote_signing_method__c, UnitPrice__c, Sent_to_fulfillment_on__c, Service_End_Date__c, Service_Start_Date__c, 
                     Line_Status__c, Billing_Close_Date__c, Contract_End_Date__c, Current_Daily_Prorate__c, Next_Billing_Date__c, 
                     Subscription_ID__c, Days_B_W__c, Product_Type__c, Current_Rate__c, Package_ID__c,Comments__c,
                     Directory_Section__c, Directory_Heading__c, Scope__c, Last_successful_settlement__c, Telco__c, Directory__c,Directory__r.Companion_Book__c,Selected_Cancel_Date__c, 
                     Effective_Date__c,Canvass__r.Branch_Manager__c, P4P_Billing__c, Is_P4P__c, P4P_Tracking_Number_Status__c, P4P_Tracking_Number_ID__c, Selected_Cancel__c, Cancelation_Fee__c, Current_Billing_Period_Days__c, zero_Cancel_Fee_Requested__c, Zero_Cancel_Fee_Approved__c ,Canvass__r.name,Package_ID_External__c,Parent_ID_of_Addon__c,Parent_ID__c,CMR_Name__c,Directory__r.Name,Directory_Heading__r.Name,Directory__r.Directory_Code__c,Telco__r.Accepts_Cancellation_Fee__c
                FROM 
                Order_Line_Items__c    
                WHERE
                Id in :OrderLineID
                AND
                isCanceled__c !=true  AND Cutomer_Cancel_Date__c=null];
              
                for(Order_Line_Items__c ol:  OLIs )
                {
                      System.debug('########## '+ol.Selected_Cancel_Date__c);
                      System.debug('########## '+ol.Contract_End_Date__c);
                  if(ol.Selected_Cancel_Date__c!=null)
                  {
                      ol.Cutomer_Cancel_Date__c=ol.Selected_Cancel_Date__c;
                  }
                  else if(ol.Contract_End_Date__c!=null ){
                      ol.Cutomer_Cancel_Date__c=ol.Contract_End_Date__c;
                  }
                  else{
                       ol.Cutomer_Cancel_Date__c=Date.today();
                  }
                   System.debug('########## '+ol.Cutomer_Cancel_Date__c);
                }
                OLICount = OLIs.size();
                cancelpage2= true;
        }
        else if(OrderLineID.size()==0)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select at least one Order Line Item to cancel');
            ApexPages.addMessage(myMsg);
            pr=null;
        }
        else if(OrderLineID.size()>0 && goliveerror)
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'In selected package all Order Line Items are not live');
            ApexPages.addMessage(myMsg);
            pr=null;        
        }

    return pr;
    }
    
 /*  public PageReference DoneCancel(){
        List<Messaging.SingleEmailMessage> lstmail = new List<Messaging.SingleEmailMessage>();
        EmailTemplate objEmailTemplate = new EmailTemplate();
        Map<id,List<order_line_items__c>> mapOfOrderSet=new Map<id,List<order_line_items__c>>();
        objEmailTemplate = [select id,name,DeveloperName from EmailTemplate where DeveloperName = 'Cancellation_Opty_Contract_Confirmation'];
        for(Order_Line_Items__c OLI: OLIs)
        {
            if(OLI.Selected_Cancel__c)
            {
                if(!mapOfOrderSet.containsKey(OLI.order_group__c))
                    mapOfOrderSet.put(OLI.order_group__c,new List<Order_Line_Items__c>{OLI});
                else
                    mapOfOrderSet.get(OLI.order_group__c).add(OLI);
            }
        }
        System.debug('@@@@@@@@@@@mapOfOrderSet '+mapOfOrderSet.size());
        for(Id tempId:mapOfOrderSet.keySet())
        {
                order_line_items__c tempOLI=mapOfOrderSet.get(tempId)[0];
                //String[] toAddresses = new String[] {'srinivas.mallela12@gmail.com'};
                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                //mail.setToAddress('srinivas.mallela12@gmail.com');
                mail.setTemplateId(objEmailTemplate.Id);
                //mail.setToAddresses(toAddresses);
                mail.setTargetObjectId(tempOLI.Billing_Contact__c);
                mail.setWhatId(tempOLI.Opportunity__c);
                mail.setSaveAsActivity(false); 
                system.debug('**************mail***********'+ mail);
                //Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                lstmail.add(mail);          
        }
        

        System.debug('@@@@@@@@@@@lstmail size'+lstmail.size());
        System.debug('@@@@@@@@@@@lstmail '+lstmail);
        Messaging.sendEmail(lstmail);
        PageReference pageRef = new PageReference('/'+ caseID);
        pageRef.setRedirect(true);
        return pageRef;
    }*/
    public pageReference oliCancel(){
        pageReference pr;
        integer counter=0;
        for(Order_Line_Items__c OLI: OLIs)
        { 
           System.debug('#########vikas '+OLI.Cutomer_Cancel_Date__c);
           System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@tempOLI.Selected_Cancel__c vikas '+OLI.Selected_Cancel__c);
            If(!BundleProductValidationMap.ContainsKey(OLI.Package_ID_External__c))
            {
                BundleProductValidationMap.put(OLI.Package_ID_External__c,new List<Order_Line_Items__c>{OLI});
            }
            
            else
            {
                 BundleProductValidationMap.get(OLI.Package_ID_External__c).add(OLI);
            }
        }
        
        if(BundleProductValidationMap.size()>0)
        {
            boolean error=false;
            for(string s:BundleProductValidationMap.keySet())
            {
                Set<String> CancelReasonSet=new Set<String>();
                Set<Date> CancelDateSet=new Set<Date>();
                for(Order_Line_Items__c tempOLI:BundleProductValidationMap.get(s))
                {
                    //CancelReasonSet.add(tempOLI.Cancellation__c);
                    
                    CancelDateSet.add(tempOLI.Cutomer_Cancel_Date__c);
                    if(tempOLI.Cutomer_Cancel_Date__c<Date.today())
                    {
                        error=true;
                        break;
                    }
                }
                if(error)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Cancellation date can not be the past date');
                    ApexPages.addMessage(myMsg);
                    counter=0;
                    pr=null;
                    break;                
                }
              /*  else if(CancelDateSet.size()>1)
                {
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Cancellation date should be same for every bundle products');
                    ApexPages.addMessage(myMsg);
                    counter=0;
                    pr=null;
                    break;
                }*/
                else
                    counter++;
            }
        }
        
       
          if(counter>0)
          {
               pagenum = 3;
               Integer billableperiods = 0;
               decimal multiplyer = 0.0;
               BilledFees = 0;
               PendingFees = 0;
                for(Order_Line_Items__c OLI: OLIs){
                    OLI.Selected_Cancel__c = true;
                    System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@OLI.Selected_Cancel__c '+OLI.Selected_Cancel__c);
                      System.debug('@@@@######### '+CaseObj.Cancellation_Reason__c); 
                    OLI.Cancellation__c=CaseObj.Cancellation_Reason__c;
                    System.debug('######### '+OLI.Cutomer_Cancel_Date__c); 
                     OLI.Cancelation_Fee__c = 0;
                     OLI.Case__c=CaseObj.Id; 
                     boolean checkForTelco=true;
                    if(OLI.Telco__c!=null && OLI.media_Type__c=='Digital' && OLI.Payment_Method__c=='Telco Billing' && !OLI.Telco__r.Accepts_Cancellation_Fee__c)
                    {
                        checkForTelco=false;
                    }
                    if(OLI.media_Type__c=='Digital' && OLI.Contract_End_Date__c!=null && checkForTelco)
                    {
                        System.debug('$$$$$$$$$$$$$$$zero_Cancel_Fee_Requested__c '+OLI.zero_Cancel_Fee_Requested__c);
                        if(OLI.zero_Cancel_Fee_Requested__c == false){
                            date ccd = date.valueOf(OLI.Cutomer_Cancel_Date__c);
                            date contractEndDate=date.valueOf(OLI.Contract_End_Date__c);                        
                            System.debug('@@@@@ '+ccd);
                            System.debug('@@@@@ '+contractEndDate);
                            billableperiods = ccd.monthsBetween(contractEndDate);
                            System.debug('$$$$$$$$$$$$$billableperiods '+billableperiods);
                            if(billableperiods >= 3)
                            {
                                multiplyer = 3.0;
                                System.debug('$$$$$OLI.UnitPrice__c '+OLI.UnitPrice__c);
                                if(OLI.UnitPrice__c != Null){
                                    OLI.Cancelation_Fee__c = OLI.UnitPrice__c * multiplyer;
                                }                           
                            }
                            if(billableperiods < 3 ){
                                integer noOfDays=ccd.daysBetween(contractEndDate);
                                if(noOfDays >0){
                                System.debug('$$$$$$$$$$$$$%%% '+OLI.Order_Line_Total__c / 365);
                                 System.debug('$$$$$$$$$$$$$%%% '+noOfDays);
                                    OLI.Cancelation_Fee__c = ((OLI.Order_Line_Total__c / 365)*noOfDays).setScale(2);
                                System.debug('$$$$$$$$$$$$$%%% '+OLI.Cancelation_Fee__c);
                                }
                            }
    
                                BilledFees+= OLI.Cancelation_Fee__c ;
                        }
                        else if(OLI.zero_Cancel_Fee_Requested__c == true)
                        {
                            zeroCancelFeeRequested=true;
                           /* if(branchManagerId==null && OLI.Canvass__r.Branch_Manager__c!=null)
                                branchManagerId=OLI.Canvass__r.Branch_Manager__c;*/
                            
                        }
                    }
                    /** commented as per QA-1312 assignment **/
                   // OLI.isCanceled__c=true;                    
                    BilledFees=BilledFees.setScale(2);
                   System.debug('$$$$$OLI.UnitPrice__c '+OLI.Cancelation_Fee__c);  
                   if(OLI.Cancelation_Fee__c>0)
                      oliForInvoice.add(OLI);    
  
                }
        
               /* if(selectedCanvass!='none')
                {
                    
                    for(Directory__c iterator:[select id, Branch_Manager__c,Canvass__c from  Directory__c where Canvass__c=:selectedCanvass])
                    {
                        if(iterator.Branch_Manager__c!=null)
                        {
                            branchManagerId=iterator.Branch_Manager__c;
                            break;
                        }
                    }
                    
                } */
    }

                
    return pr;
    }
    Public Pagereference backToStep2()  
    {     pagenum = 2;
        return Page.selectordersetforcancel;
    }   
    public pagereference FinalCancel()
    {
        //Savepoint objSavePoint = Database.setSavepoint();
        Boolean bolExecute = true;
        pagenum = 4;
        System.debug('$$$$$$$$$$$$$$$zeroCancelFeeRequested '+zeroCancelFeeRequested);
        if(zeroCancelFeeRequested == true)
        {
                CaseObj.Zero_Cancel_Fee_Requested__c=true;
                CaseObj.Status='Pending Approval';
        }
        if(!Test.isRunningTest()) {
             System.debug('CaseObj before '+CaseObj);
            CaseObj.CheckNewlyCreatedCancellationCase__c=false;
            update CaseObj;
            System.debug('Testingg 4');
            System.debug('CaseObj after '+CaseObj);
        }       
       /* if(zeroCancelFeeRequested == true )
        {
            if(branchManagerId!=null)
            {
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval the $0 cancellation Fee Request');
                req1.setObjectId(CaseObj.id);
                req1.setNextApproverIds(new Id[] {branchManagerId});
                if(!Test.isRunningTest()) {
                    Approval.ProcessResult result = Approval.process(req1);
                    System.debug('Testingg 5');
                }
            }
            else
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'There is no branch manager available for approval ');
                    ApexPages.addMessage(myMsg);
            }
        }*/
       
        for(order_line_Items__c OLI:OLIs) 
        {
            System.debug('@@@@@@@@@@@@@@@@@@@@@@@@@@@OLI.Selected_Cancel__c final '+OLI.Selected_Cancel__c);
            OLI.Selected_Cancel_Date__c=OLI.Cutomer_Cancel_Date__c;
            OLI.Cutomer_Cancel_Date__c=null;         
        }
        if(!Test.isRunningTest()){
          
                 update OLIs;
                 update OrderSetObj;
          
        }
        for(order_line_Items__c OLI:OLIs) 
        {
            OLI.Cutomer_Cancel_Date__c=OLI.Selected_Cancel_Date__c;
                    
        }
      /*  if(bolExecute) {
             //list<c2g__codaInvoice__c> invoiceList=createSalesInvoice(OLIs); 
             if(!zeroCancelFeeRequested)
                if(oliForInvoice.size()>0)
                    list<c2g__codaInvoice__c> invoiceList=SalesInvoiceHandlerController.createSalesInvoiceForCancel(oliForInvoice);   
             //System.debug('%%%%%%%%%%%%% '+invoiceList);   
         }*/
        return null;
    }
    
    
    public pagereference CheckBundle()
    {          
           Order_Line_Items__c tempOLI=[select id,media_type__c,Directory__c,Directory_Edition__c,Directory__r.Companion_Book__c,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c from Order_Line_Items__c where id=:OLIIdforcheck];
           if(CheckBool)
           {
                if(tempOLI.media_type__c=='Digital')
                {
                   if(tempOLI.Parent_ID__c!=null)
                   {
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Parent_ID__c!=null || ol.Parent_ID_of_Addon__c!=null)
                           {
                                if(ol.Parent_ID_of_Addon__c==tempOLI.Parent_ID__c || ol.Parent_ID__c==tempOLI.Parent_ID__c)
                                    ol.Selected_Cancel__c=true;
                           }  
                        }                   
                   }
                   else if(tempOLI.Parent_ID_of_Addon__c!=null)
                   {
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Parent_ID__c!=null || ol.Parent_ID_of_Addon__c!=null)
                           {
                                if(ol.Parent_ID_of_Addon__c==tempOLI.Parent_ID_of_Addon__c|| ol.Parent_ID__c==tempOLI.Parent_ID_of_Addon__c)
                                    ol.Selected_Cancel__c=true;
                           }  
                        }  
                   }
                   
                  if(tempOLI.Package_ID_External__c!=null)
                  { 
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Package_ID_External__c!=null)
                           {
                                if(ol.Package_ID_External__c==tempOLI.Package_ID_External__c)
                                    ol.Selected_Cancel__c=true;
                           }  
                        }
                  }
             }
             else if(tempOLI.media_type__c=='Print')
             {
                  if(tempOLI.Directory_Edition__c!=null)
                  { 
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Directory_Edition__c!=null)
                           {
                                if(ol.Directory_Edition__c==tempOLI.Directory_Edition__c)
                                    ol.Selected_Cancel__c=true;
                           }  
                            if(tempOLI.Directory__r.Companion_Book__c!=null || tempOLI.Directory__c!=null)
                            {
                                if(ol.Directory__c==tempOLI.Directory__r.Companion_Book__c || tempOLI.Directory__c==ol.Directory__r.Companion_Book__c)
                                    ol.Selected_Cancel__c=true;
                            }                      
                        }
                  }             
             }
           }
           if(!CheckBool)
           {
                if(tempOLI.media_type__c=='Digital')
                {
                   if(tempOLI.Parent_ID__c!=null)
                   {
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Parent_ID__c!=null || ol.Parent_ID_of_Addon__c!=null)
                           {
                                if(ol.Parent_ID_of_Addon__c==tempOLI.Parent_ID__c || ol.Parent_ID__c==tempOLI.Parent_ID__c)
                                    ol.Selected_Cancel__c=false;
                           }  
                        }                   
                   }
                   else if(tempOLI.Parent_ID_of_Addon__c!=null)
                   {
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Parent_ID__c!=null || ol.Parent_ID_of_Addon__c!=null)
                           {
                                if(ol.Parent_ID_of_Addon__c==tempOLI.Parent_ID_of_Addon__c|| ol.Parent_ID__c==tempOLI.Parent_ID_of_Addon__c)
                                    ol.Selected_Cancel__c=false;
                           }  
                        }  
                   }
                   
                  if(tempOLI.Package_ID_External__c!=null)
                  { 
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Package_ID_External__c!=null)
                           {
                                if(ol.Package_ID_External__c==tempOLI.Package_ID_External__c)
                                    ol.Selected_Cancel__c=false;
                           }  
                        }
                  }
              }
             else if(tempOLI.media_type__c=='Print')
             {
                  if(tempOLI.Directory_Edition__c!=null)
                  { 
                        for(Order_Line_Items__c ol:OLIs)
                        {
                           if(ol.Directory_Edition__c!=null)
                           {
                                if(ol.Directory_Edition__c==tempOLI.Directory_Edition__c)
                                    ol.Selected_Cancel__c=false;
                           }
                            if(tempOLI.Directory__r.Companion_Book__c!=null || tempOLI.Directory__c!=null)
                            {
                                if(ol.Directory__c==tempOLI.Directory__r.Companion_Book__c || tempOLI.Directory__c==ol.Directory__r.Companion_Book__c)
                                    ol.Selected_Cancel__c=false;                                
                            }
                        }
                  }             
             }            
           }
           return null;
    }
    
 
  /*  public static Map<Id,Order_Line_Items__c> deactivateTrackingNo(Map<String, Date> mpP4PCancelDate, Map<Id,Order_Line_Items__c> mpIdOLI) {
        //try{
            list<MediaTrax_API_Configuration__c> lstMediaConf = MediaTraxAPIConfigSOQLMethods.getMediaTraxOnAction(new set<String>{'DeactivateTrackingNumber'});
            List<String> lstTrackId = new List<String>();
            MediaTrax_API_Configuration__c objMedia;
            Map<Id,Order_Line_Items__c> mpOLIUpdt = new Map<Id,Order_Line_Items__c>();
            System.debug('B4 Ading media enrty  to list and mpP4PCancelDate '+mpP4PCancelDate);
            if(lstMediaConf!= null && lstMediaConf.size() > 0){
                objMedia = lstMediaConf[0];
                System.debug('Ading media enrty  to list');
            }
            for(String strTrackNo : mpP4PCancelDate.keySet()){
                DOM.Document objDOMResult = MediaTraxHTTPCalloutHandlerController_V1.TrackingInMediaTraxCallDeActivate(strTrackNo, mpP4PCancelDate.get(strTrackNo), objMedia);
                System.debug('Testingg objDOMResult '+objDOMResult);
                if(objDOMResult != null) {                  
                    map<String, dom.XmlNode> mapNodeResult = MediaTraxHTTPCalloutHandlerController_V1.parseDirectoryXML(objDOMResult, objMedia.SOAP_URL__c, objMedia.API_URL__c, objMedia.Return_Method__c, objMedia.Response_Method__c);
                    System.debug('Testingg mapNodeResult '+mapNodeResult);
                    if(mapNodeResult.size() > 0) {
                        if(mapNodeResult.get('result') != null) {
                            for(dom.XmlNode iterator1: mapNodeResult.get('result').getChildElements()) {
                                if(iterator1.getName() == 'result' && iterator1.getText() == 'Deactivated') {
                                    //mpTrackNoCancelDate.put(strTrackNo,mpP4PCancelDate.get(strTrackNo));
                                    lstTrackId.add(strTrackNo);
                                }
                            }
                        }
                    }
                }
            }
            
            if(lstTrackId != null && lstTrackId.size() > 0){
                List<Order_Line_Items__c> lstOLIs = [select id, P4P_Tracking_Number_Status__c, P4P_Tracking_Number_ID__c from Order_Line_Items__c where P4P_Tracking_Number_ID__c in :lstTrackId ];               
                for(Order_Line_Items__c objOLITrackNoUpdt : lstOLIs){
                    if(mpIdOLI.containsKey(objOLITrackNoUpdt.id)){
                        mpIdOLI.get(objOLITrackNoUpdt.id).P4P_Tracking_Number_ID__c = null;
                        mpIdOLI.get(objOLITrackNoUpdt.id).P4P_Tracking_Number_Status__c = 'Deactivated';
                    }
                    else
                    {
                        objOLITrackNoUpdt.P4P_Tracking_Number_ID__c = null;
                        objOLITrackNoUpdt.P4P_Tracking_Number_Status__c = 'Deactivated';
                        mpIdOLI.put(objOLITrackNoUpdt.id,objOLITrackNoUpdt);
                    }
                }
                mpOLIUpdt = mpIdOLI.clone();
            }
            /*if(mpIdOLI != null && mpIdOLI.size() > 0){
                update mpIdOLI.values();
            }*/
        /*}
        catch(CustomException objExp){
            System.debug('Exception occured in SelectOrderSetController class in method deactivateTrackingNo - Message '+objExp.getMessage());
            //Database.rollback(objSavePoint);
            mpIdOLI = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while performing the requested operation. Please try after some time or contact administrator if error persist.'));         
            futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.strStackTrace,'MediaTrax');
        }catch(Exception objExp){
            System.debug('Exception occured in SelectOrderSetController class in method deactivateTrackingNo - Message '+objExp.getMessage());
            //Database.rollback(objSavePoint);
            mpIdOLI = null;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception occured while performing the requested operation. Please try after some time or contact administrator if error persist.'));
            futureCreateErrorLog.createErrorRecord(objExp.getMessage(), objExp.getStackTraceString(),'MediaTrax');
        }*/
       /* return mpOLIUpdt;
    }    */
    

        
    
}