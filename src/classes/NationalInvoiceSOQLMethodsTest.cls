@isTest
public with sharing class NationalInvoiceSOQLMethodsTest {
    static testMethod void NationalInvoiceSOQLMethodsTest() {
        Set<Id> OrderSetId = new Set<Id>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        OrderSetId.add(newOrderSet.Id);
        ISS__c newIss = TestMethodsUtility.createISS();
        Directory__c newDirectory = TestMethodsUtility.createDirectory();        
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();        
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.generateDirectoryEdition(newDirectory.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);                
        NationalInvoice__c newNInvoice = TestMethodsUtility.generateNationalInvoice(newOrderSet);
        newNInvoice.ISS__c = newIss.Id;
        newNInvoice.IsNew__c = true;
        newNInvoice.Directory_Edition__c = newDirectoryEdition.Id;
        insert newNInvoice;
        system.assertNotEquals(null, NationalInvoiceSOQLMethods.getLastNationalInvoiceBYOrderSetID(OrderSetId));
        system.assertNotEquals(null, NationalInvoiceSOQLMethods.getNationalInvoiceForNationalViewByOrderSet(OrderSetId));
        NationalInvoiceSOQLMethods.getNatInvById(new Set<Id> {newNInvoice.Id});
        NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeWithISSNotNull(newNInvoice.Publication_Code__c, newNInvoice.Directory_Edition_Code__c);
        NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeWithISSNull(newNInvoice.Publication_Code__c, newNInvoice.Directory_Edition_Code__c);
        NationalInvoiceSOQLMethods.getNatInvByPubCodeEdtncodeDirId(newNInvoice.Publication_Code__c, newNInvoice.Directory_Edition_Code__c, newDirectory.Id);
    }
}