public class natManInput {

    public National_Staging_Line_Item__c NatLinObj{get; set;}
    public National_Staging_Order_Set__c NatHdrObj{get; set;}
    public List <National_Staging_Line_Item__c> NatLinLstObj{get; set;}
    public String HeaderId{get;Set;}
    public Integer rowNum{get;set;}
    public String UDAC{get;Set;} 
    private map<String, Account> mapClientAccount;
    private map<String, Directory_Edition__c> mapLSADirEdition;
    Id clientRTID;
    public ID ManualRT{get;set;}
    public natManInput(ApexPages.StandardController controller) 
    {
       // ManualRT= CommonMethods.getRecordTypeDetailsByDeveloperName('National_Staging_Order_Set__c','Manual_NS_RT');
        if (mapClientAccount == Null ){
            mapClientAccount = new map<String, Account>();
        }
        mapLSADirEdition = new map<String, Directory_Edition__c>();

        clientRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountNationalActRT, CommonMessages.accountObjectName);
        if(NatLinLstObj == Null){
            NatLinLstObj = New List <National_Staging_Line_Item__c>();
            NatLinLstObj.add(new National_Staging_Line_Item__c(National_Staging_Header__c= HeaderId));
        }
        
        if(NatHdrObj == Null)
        {
            NatHdrObj = new  National_Staging_Order_Set__c();
           // NatHdrObj.RecordTypeId=ManualRT.Id;
            NatHdrObj.To_Type__c = 'P';
            NatHdrObj.From_Type__c = 'C';
            
        }
    }

    public pageReference Cancel() 
    {
        pageReference pr;
        pr = new pageReference('/a3X/o');
        pr.setRedirect(true);
        return pr;
    }

    public void addLineItems()
    {
        National_Staging_Line_Item__c NatLine = new National_Staging_Line_Item__c();
        if(HeaderId != ''){
            NatLine.National_Staging_Header__c= HeaderId ;   
        }
        NatLinLstObj.add(NatLine);
    }



    public pageReference save() {
        pageReference pr;
        boolean bFlag = true;
        if(NatHdrObj.CMR_Name__c == null) {
            CommonMethods.addError('Please enter valid CMR name');
            bFlag = false;
        }

        system.debug(NatHdrObj.Client_Name__c);
        if(NatHdrObj.Client_Name__c == null && String.isBlank(NatHdrObj.Client_Name_Text__c)) {
            CommonMethods.addError('Please enter client name');
            bFlag = false;
        }

        if(bFlag) {
            boolean bFlagChild = true;
            set<String> setUDAC = new set<String>();
            for(National_Staging_Line_Item__c Natli : NatLinLstObj){
                if(String.isNotBlank(Natli.UDAC__c)) {
                    setUDAC.add(Natli.UDAC__c);
                }
            }
            
            list<Product2> lstUDAC = Product2SOQLMethods.getProductByUDAC(setUDAC);
            map<String, Id> mapUDAC = new map<String, Id>();
            for(Product2 iterator : lstUDAC) {
                mapUDAC.put(iterator.ProductCode, iterator.Id);
            }
            
            for(National_Staging_Line_Item__c Natli : NatLinLstObj){                
                if(String.isNotBlank(Natli.Line_Number__c)) {
                    if(Natli.Line_Number__c.length() < 5) {
                        Natli.Line_Number__c.addError('Line # should be 5 digit number.');
                        bFlagChild = false;
                    }
                }               
                if(String.isNotBlank(Natli.UDAC__c)) {
                    if(mapUDAC.get(Natli.UDAC__c) != null) {
                        Natli.Product2__c = mapUDAC.get(Natli.UDAC__c);
                    }
                    else {
                        Natli.UDAC__c.addError('UDAC is not valid.');
                        bFlagChild = false;
                    }
                }
            }
            
            if(bFlagChild) {
                if(NatHdrObj.Client_Name__c == null && String.isNotBlank(NatHdrObj.Client_Name_Text__c) && String.isNotBlank(NatHdrObj.Client_Number__c) ) {
                    createNewAccount(NatHdrObj);
                }
                System.debug('%%%%%%%%%%$$$$ '+NatHdrObj);
                PageReference pageRef = ApexPages.currentPage();
                ManualRT= pageRef.getParameters().get('RecordType');
                System.debug('%%%%%%%%%% '+ManualRT);
                NatHdrObj.RecordTypeId = ManualRT;
                insert NatHdrObj;
                HeaderId = NatHdrObj.id;
                List <National_Staging_Line_Item__c> NSLIL = new List <National_Staging_Line_Item__c>();
                for(National_Staging_Line_Item__c Natli : NatLinLstObj) {
                    Natli.National_Staging_Header__c = NatHdrObj.id;
                    Natli.Transaction_Id__c = NatHdrObj.Transaction_ID__c;
                    NSLIL.add(Natli);
                }
                insert NSLIL;
                pr = new pageReference('/'+NatHdrObj.id );
                pr.setRedirect(true);
                return pr;
            }
        }
        return null;
    }
    
    public void createNewAccount(National_Staging_Order_Set__c  NatHdrObj)
    {
       Account accNew = new Account ();
       accNew.Name = NatHdrObj.Client_Name_Text__c+' '+NatHdrObj.Client_Number__c;
       accNew.Client_Number__c = NatHdrObj.Client_Number__c;
       accNew.CMR_Number__c = NatHdrObj.CMR_Number__c;
       // Modified by : Ankit Task: BFTHREE-1295 Added Record type in SOQL for National Account
       // Commented ParentId as CMR for NewAccount would be null in order to avoid duplicate records and relation 
       // accNew.ParentId = NatHdrObj.CMR_Name__c;
       accNew.RecordTypeId= CommonMethods.getRedordTypeIdByName('National Account', 'Account');
       insert accNew;
       NatHdrObj.Client_Name__c = accNew.Id;
    }
    
    public List<SelectOption> getNatList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('A','A - National'));
        options.add(new SelectOption('B','B - Local'));
        options.add(new SelectOption('E','E - Emerging'));
        options.add(new SelectOption('R','R - Regional'));
        return options;
    }

    public List<SelectOption> getToFromList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('C','C - CMR'));
        options.add(new SelectOption('P','P - Publisher'));
        options.add(new SelectOption('Y','Y - YPA Elite'));
        return options;
    }

    public List<SelectOption> getTransList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',' --None--'));
        options.add(new SelectOption('A','A - Advice'));
        options.add(new SelectOption('C','C - Change Order'));
        options.add(new SelectOption('D','D - Delete Entire Order'));
        options.add(new SelectOption('F','F - Forward'));
        options.add(new SelectOption('H','H - Header Change'));
        options.add(new SelectOption('I','I - New Insert Order'));
        options.add(new SelectOption('K','K - Order Compare'));
        options.add(new SelectOption('L','L - Renumber Order'));
        options.add(new SelectOption('M','M - Memo'));
        options.add(new SelectOption('N','N - CMR Transfer (no-standing order)'));
        options.add(new SelectOption('Q','Q - Query'));
        options.add(new SelectOption('R','R - Resend Graphic Request'));
        options.add(new SelectOption('T','T - CMR Transfer (with standing order)'));
        options.add(new SelectOption('V','V - Verification of Order'));
        options.add(new SelectOption('X','X - View YPA Elite Standing Order'));
        return options;
    }

    public void fetchCMRbyNumber() {
        system.debug(NatHdrObj.CMR_Number__c);
        mapClientAccount = new map<String, Account>();
        NatHdrObj.CMR_Name__c = null;
        ID cmrAccRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        if(String.isNotBlank(NatHdrObj.CMR_Number__c)) {
            list<Account> lstCMRAcc = AccountSOQLMethods.getCMRAccountByNumber(NatHdrObj.CMR_Number__c, cmrAccRTID, clientRTID);
            if(lstCMRAcc.size() > 0) {
                for(Account iterator : lstCMRAcc) {
                    NatHdrObj.CMR_Name__c = iterator.Id;
                    
                    
                    
                    for(Account childIterator : iterator.ChildAccounts) {
                        if(String.isNotBlank(childIterator.Client_Number__c)) {
                            mapClientAccount.put(childIterator.Client_Number__c, childIterator);
                        }
                    }
                }
            }           
            if(NatHdrObj.CMR_Name__c == null) {
                //CommonMethods.addError('Entered CMR Number is not valid.');
                NatHdrObj.CMR_Number__c.addError('Entered CMR Number is not valid.');
            }
        }
    }

   /// fix by chris roberts to find the client id  the map does not contain any child records because that is not how they are stored anymore
    public void fetchClientbyNumber() {
        // test numbers 6083 for the CMR and 9987 for the Client # should yeld the account 001Z000000gHaDC
        // select Id,  Name, Type, RecordTypeId, ParentId, CMR_Number__c,Client_Number__c from Account WHERE Client_Number__c = '9987' AND CMR_Number__c ='6083' 
        // record type=national account
        if(NatHdrObj.CMR_Number__c == null) {
            NatHdrObj.CMR_Number__c.addError('CMR Number Value is required to get client id'); 
            NatHdrObj.Client_Number__c.addError('CMR Number Value is required to get client id CMR # is:'+NatHdrObj.CMR_Number__c);
        }
        system.debug(NatHdrObj.Client_Number__c);
        Account acc = new account();
        acc=AccountSOQLMethods.getClientNatAcctByNumbers(NatHdrObj.Client_Number__c, NatHdrObj.CMR_Number__c);
        if(acc.id != Null){
            mapClientAccount.put(NatHdrObj.Client_Number__c, acc);
        }
        NatHdrObj.Client_Name__c = null;
        if(mapClientAccount.size() > 0) 
        {
            if(mapClientAccount.get(NatHdrObj.Client_Number__c) != null) 
            {
                NatHdrObj.Client_Name__c = mapClientAccount.get(NatHdrObj.Client_Number__c).Id;
                NatHdrObj.Client_Name_Text__c=mapClientAccount.get(NatHdrObj.Client_Number__c).name;
            }           
            if(NatHdrObj.Client_Name__c == null) {
                //CommonMethods.addError('Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.');
                
              //  ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info, 'Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.'));
                NatHdrObj.Client_Number__c.addError('Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.');
            }
        } else {
        
            NatHdrObj.Client_Number__c.addError('Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.');
            
            /*
            commented out by chris roberts this is the condition where the client  should be created ?  if it is an error where we do not have a cmr this needs to be addressed
            */       
           // NatHdrObj.Client_Number__c.addError('Entered Client # '+NatHdrObj.Client_Number__c+' not in the system Map size = '+mapClientAccount.size()+' CMR #'+NatHdrObj.CMR_Number__c);
        }
        
    }

    public void fetchDirectoryByCode() 
    {
        System.debug('#################'+NatHdrObj.Directory_Number__c);
        mapLSADirEdition = new map<String, Directory_Edition__c>();
        list<Directory__c> lstDir = DirectorySOQLMethods.getDirectoryByCode(new set<String>{NatHdrObj.Directory_Number__c});
        if(lstDir.size() > 0) {
            for(Directory__c iterator : lstDir) 
            {
                NatHdrObj.Directory__c = iterator.id;
                NatHdrObj.State__c = iterator.State__c;
                for(Directory_Edition__c iteratorDE : iterator.Directory_Editions__r) 
                {
                    if(String.isNotBlank(iteratorDE.LSA_Directory_Version__c)) 
                    {
                        mapLSADirEdition.put(iteratorDE.LSA_Directory_Version__c, iteratorDE);
                    }
                }
            }
        }
    }

    public void fetchDEByLSANo(){
        system.debug(mapLSADirEdition+'#####################'+NatHdrObj.Directory_Edition_Number__c);
        if(String.isNotBlank(NatHdrObj.Directory_Edition_Number__c)) 
        {
            if(mapLSADirEdition.size() > 0) 
            {
                if(mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c) != null) 
                {
                    NatHdrObj.Publication_Date__c = mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c).Pub_Date__c;
                    NatHdrObj.Directory_Edition__c = mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c).Id;                
                }
            }

            if(NatHdrObj.Directory__c == null) {
                list<Directory_Edition__c> lstDE = DirectoryEditionSOQLMethods.getDirEditionByLSACode(new set<String>{NatHdrObj.Directory_Edition_Number__c});
                for(Directory_Edition__c iterator : lstDE) {
                    if(iterator.Book_Status__c == 'NI') {
                        NatHdrObj.Publication_Date__c = iterator.Pub_Date__c;
                        NatHdrObj.Directory_Edition__c = iterator.Id;
                        NatHdrObj.Directory__c = iterator.Directory__c;
                        NatHdrObj.State__c = iterator.Directory__r.State__c;
                        NatHdrObj.Directory_Number__c = iterator.Directory__r.directory_code__c;
                    }
                }
            }
        }
    }

    public void removeEntry(){
        System.debug('Testingg rowNum ');
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        System.debug('Testingg rowNum '+rowNum);
        NatLinLstObj.remove(rowNum);
    }

    public void productByUDAC(){
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        Product2 prod = new Product2();
        if( [SELECT count() FROM Product2 WHERE ProductCode= :UDAC LIMIT 1] ==1){
            prod = [SELECT Id, Name, ProductCode FROM Product2 WHERE ProductCode= :UDAC LIMIT 1];
            NatLinLstObj[rowNum].Product2__c = prod.Id;
        } else {
            //raise error 
        }
    }
}