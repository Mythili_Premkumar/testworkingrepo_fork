public with sharing class StrmBkng2Cntrlr {

    public List<Booking__c> ExistingBkngs{get;set;}
    public String Keyword{get;set;}
    
    public StrmBkng2Cntrlr (){
        ExistingBkngs= new List<Booking__c>();
    }
      public void SearchBkngs(){    
        ExistingBkngs= [SELECT Customer_ID__c, Customer_Name__c, Through_Date__c, UDAC__c, Media_Type__c FROM Booking__c WHERE Customer_Name__c LIKE: (Keyword+'%')];        
    }
}