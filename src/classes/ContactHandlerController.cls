public class ContactHandlerController {
    public static void onBeforeInsert(list<Contact> lstNewContact) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.contactObjectName);
         if(!skipTrigger){
         	validationForBillingContact(lstNewContact, null);
         }
    }
    
    public static void onAfterInsert(list<Contact> lstNewContact) {
        updateAccountWithBillingAndPrimaryCotactId(lstNewContact, null);
    }
    
    public static void onBeforeUpdate(list<Contact> lstNewContact, map<Id, Contact> oldMap) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.contactObjectName);
         if(!skipTrigger){
         	validationForBillingContact(lstNewContact, oldMap);
         }
    }
    
    public static void onAfterUpdate(list<Contact> lstNewContact, map<Id, Contact> oldMap) {
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.contactObjectName);
         if(!skipTrigger){
         	updateAccountWithBillingAndPrimaryCotactId(lstNewContact, oldMap);
         }
    }
    
    public static void validationForBillingContact(list<Contact> lstNewContact, map<Id, Contact> oldMap) {
        set<ID> accountID = new set<ID>();
        set<ID> contactID = new set<ID>();
        for(Contact iteratorContact : lstNewContact){
            if(iteratorContact.accountID != null){
                accountID.add(iteratorContact.accountID);
                contactID.add(iteratorContact.Id);
            }
        }
        system.debug('######'+accountID);
        if(accountID.size() > 0){
            //Fetched the associated contact based on account ID
            map<ID, Account> mapContactWithAccountID = AccountSOQLMethods.getContactByAccountID(accountID, contactID);
            if(mapContactWithAccountID.size() > 0){
                integer i = 0;
                
                for(Contact iteratorContact : lstNewContact){
                    Account objAccount = mapContactWithAccountID.get(iteratorContact.AccountID);
                    system.debug(objAccount);
                    for(Contact objContact : objAccount.Contacts){
                        // Prevent the duplicate contact for an account
                        system.debug(objContact.PrimaryBilling__c + '--> '+ iteratorContact.PrimaryBilling__c);
                        if(objContact.PrimaryBilling__c == iteratorContact.PrimaryBilling__c && iteratorContact.PrimaryBilling__c == true){
                            trigger.new[i].addError(CommonMessages.contactBillingPrimary);
                        }
                        
                        if(objContact.Primary_Contact__c == iteratorContact.Primary_Contact__c && iteratorContact.Primary_Contact__c == true){
                            trigger.new[i].addError(CommonMessages.mainContact);
                        }
                        /*
                        if(objContact.Proof_Contact__c == iteratorContact.Proof_Contact__c && iteratorContact.Proof_Contact__c == true){
                            trigger.new[i].addError(CommonMessages.proofContact);
                        }*/
                    }
                    i++;
                }
            }
        }
    }
    
    
    public static void updateAccountWithBillingAndPrimaryCotactId(list<Contact> lstNewContact, map<Id, Contact> oldMap) {
    	map<Id, Account> mapAccount = new map<Id, Account>();
    	list<Account> lstAccount = new list<Account>();
    	for(Contact objCon : lstNewContact) {
    		if(objCon.AccountId != null) {    			
    			if(!mapAccount.containsKey(objCon.AccountId)) {
    				mapAccount.put(objCon.AccountId, new Account());
    			}    			
    			Account objAcc = mapAccount.get(objCon.AccountId);
    			if(oldMap != null) {
    				Contact objOldCon = oldMap.get(objCon.Id);
	    			if(objCon.Primary_Contact__c != objOldCon.Primary_Contact__c) {
	    				if(objCon.Primary_Contact__c) {
	    					objAcc.Primary_Contact__c = objCon.ID;
	    					objAcc.Id = objCon.AccountId;
	    				}
	    				else {
	    					objAcc.Primary_Contact__c = null;
	    					objAcc.Id = objCon.AccountId;
	    				}
	    			}
	    			if(objCon.PrimaryBilling__c != objOldCon.PrimaryBilling__c) {
	    				objAcc.ACC_Primary_Billing_Contact__c = objCon.PrimaryBilling__c;
	    				objAcc.Id = objCon.AccountId;
	    			}
    			}
    			else {
    				if(objCon.Primary_Contact__c) {
	    				objAcc.Primary_Contact__c = objCon.ID;
	    				objAcc.Id = objCon.AccountId;
	    			}
	    			if(objCon.PrimaryBilling__c) {
	    				objAcc.ACC_Primary_Billing_Contact__c = objCon.PrimaryBilling__c;
	    				objAcc.Id = objCon.AccountId;
	    			}
    			}
    			
    			if(objAcc.Id != null) {
    				mapAccount.put(objCon.AccountId, objAcc);
    			}
    		}	
    	}
    	
    	if(mapAccount.size() > 0) {
	    	for(ID accID : mapAccount.keySet()) {
	    		Account objTemp = mapAccount.get(accID);
	    		if(objTemp.Id != null) {
	    			lstAccount.add(objTemp);
	    		}
	    	}
    	}
    	
    	if(lstAccount.size() > 0) {
    		update lstAccount;
    	}
    }
}