global class UnitPriceUpdateBatch implements Database.Batchable<sObject> {
    Date tody;
    global UnitPriceUpdateBatch(Date natRatDueDate) {
        tody = natRatDueDate;
    }
    global Database.QueryLocator start(Database.BatchableContext bc) {    
        Id nationalRecTypId = CommonMethods.getRedordTypeIdByName(CommonMessages.nationaOLIRT, CommonMessages.OLIObjectName);  
        String SOQL = 'SELECT Id, UnitPrice__c, National_Staging_Line_Item__r.Sales_Rate__c, National_Staging_Line_Item__c FROM Order_Line_Items__c WHERE Directory_Edition__r.National_Rates_Due__c = : tody AND RecordTypeId = :nationalRecTypId';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List <Order_Line_Items__c> listOLI) {
        for(Order_Line_Items__c OLI : listOLI) {
            OLI.UnitPrice__c = OLI.National_Staging_Line_Item__r.Sales_Rate__c;
        }           
        update listOLI;
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}