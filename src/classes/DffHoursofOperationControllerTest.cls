@isTest(seeAllData=true)
public class DffHoursofOperationControllerTest{

    static testMethod void dffHrs(){
    
        Test.startTest();
        
        Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
        dff.is_open_247__c = true;
        insert dff;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(dff);
        DffHoursofOperationController dffHrs = new DffHoursofOperationController(sc);
        dffHrs.mnDy = true;
        dffHrs.tsDy = true;
        dffHrs.weDy = true;
        dffHrs.thDy = true;
        dffHrs.frDy = true;
        dffHrs.stDy = true;
        dffHrs.snDy = true;
        dffHrs.cpyAll = true;
        dffHrs.copyHours();
        dffHrs.dffSave();
        dffHrs.getItems();
        dffHrs.checkBoxDsbl();
        dffHrs.copyAllValidate();
        dffHrs.openAllDay();
        dffHrs.dffHoursReqr();
        dffHrs.search();
        
        dff.is_Open_247__c = false;
        update dff;
        dffHrs.cpyAll = false;
        dffHrs.mnDy = true;
        dffHrs.tsDy = true;
        dffHrs.weDy = true;
        dffHrs.thDy = true;
        dffHrs.frDy = true;
        dffHrs.stDy = true;
        dffHrs.snDy = true;        
        dffHrs.copyHours();
        dffHrs.openAllDay();
        dffHrs.checkBoxDsbl();
        
        Test.stopTest();
    
    }
}