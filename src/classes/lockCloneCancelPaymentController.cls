public with sharing class lockCloneCancelPaymentController {
    String opportunityID {get;set;}
    public lockCloneCancelPaymentController(ApexPages.StandardController controller) {
        system.debug('*****************Constructor Start*******************');
        opportunityID = ApexPages.currentPage().getParameters().get('Id');        
        system.debug('**********opportunityID************** : '+ opportunityID);
        system.debug('*****************Constructor End*******************');
    }
    
    public Pagereference onLoad() {
    	List<pymt__PaymentX__c> listPayment = new List<pymt__PaymentX__c>();
    	listPayment = [SELECT Id FROM pymt__PaymentX__c WHERE pymt__Opportunity__c =: opportunityID AND Name =: CommonMessages.initialPymt];
    	if(listPayment.size() > 0) {
    		delete listPayment;
    	}
        return new Pagereference('/'+opportunityID);
    }
}