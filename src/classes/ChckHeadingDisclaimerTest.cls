//ChckHeadingDisclaimer Trigger Test Class
//class = HeadingDisclaimer
@isTest
public class ChckHeadingDisclaimerTest
{
    static testmethod void CHDtestmethod()
    {
        Test.starttest();
        Heading_Disclaimer__c  HDC = Testmethodsutility.generateheadingdisclaimer();
        HDC.Disclaimer_Text__c ='abc';
        insert HDC;

        //to create sectionheadingmapping
        Directory__c  Di = Testmethodsutility.createDirectory();
        Directory_Section__c DS = Testmethodsutility.createDirectorySection(Di);
        Section_Heading_Mapping__c SHM = Testmethodsutility.generateSectionHeadingMapping(DS.id);
        Directory_Heading__c DH = Testmethodsutility.createDirectoryHeading();

        SHM.Heading_Disclaimer__c = HDC.id;
        SHM.Directory_Heading__c=DH.id;
        insert SHM;
        HDC.Disclaimer_Text__c='xyz';
        update HDC;
        Test.stopTest();
    }
}