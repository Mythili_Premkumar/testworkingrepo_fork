global class MediaTraxServiceForUpdateHeadingBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String SOQL = '';
    global Database.QueryLocator start(Database.BatchableContext bc){
        SOQL = 'Select Id, Name, Media_Trax_Heading_ID__c from Directory_Heading__c where Media_Trax_Heading_ID__c != null and Is_Changed__c = true';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, list<Directory_Heading__c> lstHeading){
        
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForHeadingUpdateScope(lstHeading);
    }
    
    global void finish(Database.BatchableContext bc){
    }
}