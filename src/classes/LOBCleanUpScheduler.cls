global class LOBCleanUpScheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        LOBRecordDeleteBatch objLOB = new LOBRecordDeleteBatch();
        Database.executeBatch(objLOB,2000);
    }
}