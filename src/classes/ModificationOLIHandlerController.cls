public with sharing class ModificationOLIHandlerController {
    public static void onBeforeInsert(list<Modification_Order_Line_Item__c> lstModificationOLI){
    }
    
    public static void onAfterInsert(list<Modification_Order_Line_Item__c> lstModificationOLI){
        Boolean skipTrigger=CommonMethods.skipTriggerMethodLogic(CommonMessages.MOLIObjectName);
        if(!skipTrigger){
        //commenting for ticket BFTHREE-2393
           // updateOLISeniorityDate(lstModificationOLI);
        }
    }
    
    public static void onBeforeUpdate(list<Modification_Order_Line_Item__c> lstModificationOLI, Map<Id, Modification_Order_Line_Item__c> oldMap){    
    }
    
    public static void onAfterUpdate(list<Modification_Order_Line_Item__c> lstModificationOLI, Map<Id, Modification_Order_Line_Item__c> oldMap){
    }
    
    //commenting for ticket BFTHREE-2393
    /*public static void updateOLISeniorityDate(list<Modification_Order_Line_Item__c> lstModificationOLI){
        set<Id> OLIIds = new set<Id>();
        List<Order_Line_Items__c> orderLineItemList = new List<Order_Line_Items__c>();
        
        for(Modification_Order_Line_Item__c MOLI : lstModificationOLI){
            if(MOLI.Core_Opportunity_Line_ID__c == null && MOLI.UDAC_Group_Change__c){
                OLIIds.add(MOLI.Order_Line_Item__c);
            }
        }
        
        orderLineItemList = OrderLineItemSOQLMethods.getOLIByID(OLIIds);
        
        if(orderLineItemList.size() > 0){
            for(Order_Line_Items__c OLI : orderLineItemList){
                OLI.Seniority_Date__c = CommonMessages.systemDate;
            }
            
            update orderLineItemList;
        }
    } */
}