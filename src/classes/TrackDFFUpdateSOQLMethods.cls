public with sharing class TrackDFFUpdateSOQLMethods {
	public static List<Track_DFF_Update__c> fetchTrackDFFUpdatesByDFFIds(Set<Id> DFFIds) {
		return [SELECT Id, Data_Fulfillment_Form__c FROM Track_DFF_Update__c WHERE Data_Fulfillment_Form__c IN: DFFIds];
	}
}