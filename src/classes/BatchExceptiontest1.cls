global class BatchExceptiontest1 implements Database.Batchable<sObject>, Database.Stateful{
    global final String Query;
    public list<Exception_Error__c> logList = new list<Exception_Error__c> ();
    public String FailedRecords;
    string errorFields;
    global BatchExceptiontest1(String q)
    {
        Query=q;
        FailedRecords='';
        errorFields = '';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        Savepoint sp = Database.setSavepoint();
        
        List <Account> lstAccount = new list<Account>();
        for(Sobject s : scope)
        {
            Account a = (Account)s;
            lstAccount.add(a);
        }
        Database.SaveResult[] srList = Database.update(lstAccount, false);  
        integer i=0;
        for (Database.SaveResult sr : srList) {
            if (!sr.isSuccess()) {
                for(Database.Error err : sr.getErrors())
                {   
                    
                    Account accObj = lstAccount[i];     
                    logList.add(new Exception_Error__c(Exception_Message__c=err.getMessage(),Record_Id__c=accObj.Id));
                    FailedRecords+= '<br>Record Id :'+accObj.Id+'Exception Msg '+err.getMessage()+'<br>';
                    i++;
                }   
            }       
        }
        
        
    }
    global void finish(Database.BatchableContext BC)
    {
        if(logList.size() > 0)
        {
            insert logList;
        }
        String[] toAddresses = new String[] {'bpallan@csc.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses,'FailedRecords','LogSize'+logList.size()+'<br></br>'+FailedRecords);
    }
    

}