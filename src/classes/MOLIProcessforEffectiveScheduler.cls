global class MOLIProcessforEffectiveScheduler Implements Schedulable {
    global void execute(SchedulableContext sc) {
        MOLIProcessforEffective obj = new MOLIProcessforEffective(system.today());
		Database.executeBatch(obj, Integer.valueOf(label.MOLI_batch_job_size));
    }
}