public with sharing class LocalBillingReconcileStatementController {
    public Dummy_Object__c objDummy {get;set;}
    public String strNoofAccountAndInvoice {get;set;}
    //set<Id> setSIId;
    //set<Id> setSCNId;
    @Testvisible Id batchId;
    @Testvisible Id SCNbatchId;
    @Testvisible Id SIReconciliationbatchId;
    @Testvisible Id SCNReconciliationbatchId;
    public boolean bReconciled {get;set;}
    public boolean bPostAllInvoice {get;set;}
    public String strBatchStatus {get;set;}
    public String strSCNBatchStatus {get;set;}
    public String strSIReconcileBatchStatus {get;set;}
    public String strSCNReconcileBatchStatus {get;set;}
    public boolean bEnabledPoller {get;set;}
    public boolean bReconciliationEnabledPoller {get;set;}
    public Integer StmtReconciledCount{get;set;}
    public double stmtSITotal{get;set;}
    public double stmtSCNTotal{get;set;}
    public boolean bShowReconciliation{get;set;}
    public boolean bShowPostFF {get;set;}
    public String strNoofAccountAndSCN {get;set;}
    //Report Ids Custom Settings
    public Id LOBMatchingReportId{get;set;}
    public Id goLiveReportId {get;set;}
    public Id UnReconciledSLIOLIReportID{get;set;}
    public Id EverythingGoneLivewithInvoice{get;set;}
    public Id rptPostingInvoice {get;set;}
    public Id rptPostingSCN {get;set;}
    public Id StmtSubsequentReconcileReportId{get;set;}
    
    Set<Id> setSIBatchIds = new Set<id>();
    Set<Id> setSCNBatchIds = new Set<id>();
    
    public LocalBillingReconcileStatementController () {
         objDummy = new Dummy_Object__c();
         clearDatas();    
         Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
         if(reportIds.get('Go Live')!=null) goLiveReportId = reportIds.get('Go Live').Report_Id__c;
         if(reportIds.get('EverythingGoneLivewithInvoice')!=null) EverythingGoneLivewithInvoice=reportIds.get('EverythingGoneLivewithInvoice').Report_Id__c;
         if(reportIds.get('LOB Matching Report')!=null) LOBMatchingReportId= reportIds.get('LOB Matching Report').Report_Id__c;
         if(reportIds.get('UnReconciled Sales Invoice OLI')!=null) UnReconciledSLIOLIReportID=reportIds.get('UnReconciled Sales Invoice OLI').Report_Id__c;
         if(reportIds.get('Reconcile SI Report Posting')!=null) rptPostingInvoice = reportIds.get('Reconcile SI Report Posting').Report_Id__c;
        if(reportIds.get('Reconcile SCN Report Posting')!=null) rptPostingSCN = reportIds.get('Reconcile SCN Report Posting').Report_Id__c;
        if(reportIds.get('Stmt Subsequent Reconcile Report')!=null) StmtSubsequentReconcileReportId= reportIds.get('Stmt Subsequent Reconcile Report').Report_Id__c;
    }

    private void clearDatas() {
        strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        //setSIId = new set<Id>();
        //setSCNId =new set<Id>();
        strBatchStatus = '';
        strSCNBatchStatus = '';
        strSIReconcileBatchStatus= '';
        strSCNReconcileBatchStatus= '';
        bPostAllInvoice = false;
        bEnabledPoller = false;
        stmtSCNTotal=0.00;
        stmtSITotal=0.00;
        StmtReconciledCount=0;
        bShowPostFF=false;
        bShowReconciliation=false;
        bReconciliationEnabledPoller =false;
    }
     
     public void clickGoNew() {
        clearDatas();
        set<Id> setAccountSCNID = new set<Id>();       
       set<Id> setSIId = new set<Id>();
       set<Id> setSCNId = new set<Id>();
       set<Id> setAccountID = new set<Id>();
       set<id> unreconcileInvStmt=new set<id>();
        if(objDummy.From_Date__c!=null && objDummy.To_Date__c!=null){   
           stmtSITotal=0.00;
           stmtSCNTotal=0.00;
           list<c2g__codaInvoice__c> lstSalesInvoice =[Select Customer_Name__c, c2g__NetTotal__c, Name, Id, c2g__InvoiceDate__c, Reconcile__c 
           From c2g__codaInvoice__c WHERE SI_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__InvoiceStatus__c = 'In Progress' AND SI_Payment_Method__c = 'Statement' AND c2g__InvoiceDate__c<=:objDummy.To_Date__c and c2g__InvoiceDate__c>=:objDummy.From_Date__c];
           if(lstSalesInvoice .size()>0){
                 for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
                      if(!iterator.Reconcile__c){
                           unreconcileInvStmt.add(iterator.id);
                      }
                      stmtSITotal=stmtSITotal+iterator.c2g__NetTotal__c;
                      setAccountID.add(iterator.Customer_Name__c);
                      setSIId.add(iterator.id);
                 }
                 StmtReconciledCount=unreconcileInvStmt.size();
                 strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
                 bShowReconciliation=true;
           }
           list<c2g__codaCreditNote__c> lstCreditNote =[Select Name, c2g__NetTotal__c, Reconcile__c,Customer_Name__c From c2g__codaCreditNote__c where SC_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__CreditNoteStatus__c = 'In Progress' AND SC_Payment_Method__c = 'Statement' AND c2g__CreditNoteDate__c<=:objDummy.To_Date__c and c2g__CreditNoteDate__c>=:objDummy.From_Date__c];
           if(lstCreditNote .size()>0){           
                for(c2g__codaCreditNote__c SCNLIIterator : lstCreditNote){
                    setSCNId.add(SCNLIIterator.id);
                    setAccountSCNID.add(SCNLIIterator.Customer_Name__c);
                    stmtSCNTotal= stmtSCNTotal+ SCNLIIterator.c2g__NetTotal__c;
                }                
                 strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
                 strNoofAccountAndSCN = concatenateSCNString(setAccountSCNID.size(), setSCNId.size());
                 bShowReconciliation=true;
           }
        }
     }

    public void updateSIwithReconciledCheckBox() {
       set<Id> setSIId = new set<Id>();
       set<Id> setSCNId = new set<Id>();
       setSIBatchIds = new Set<id>();
       setSCNBatchIds = new Set<id>();
       list<c2g__codaInvoice__c> lstSalesInvoice =[Select Customer_Name__c, c2g__NetTotal__c, Name, Id, c2g__InvoiceDate__c, Reconcile__c 
           From c2g__codaInvoice__c WHERE SI_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__InvoiceStatus__c = 'In Progress' AND SI_Payment_Method__c = 'Statement' AND c2g__InvoiceDate__c<=:objDummy.To_Date__c and c2g__InvoiceDate__c>=:objDummy.From_Date__c and Reconcile__c = false];
        if(lstSalesInvoice .size()>0){
             for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
                  setSIId.add(iterator.id);
             }
        }
        
        list<c2g__codaCreditNote__c> lstCreditNote =[Select Name, c2g__NetTotal__c, Reconcile__c,Customer_Name__c From c2g__codaCreditNote__c where SC_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__CreditNoteStatus__c = 'In Progress' AND SC_Payment_Method__c = 'Statement' AND c2g__CreditNoteDate__c<=:objDummy.To_Date__c and c2g__CreditNoteDate__c>=:objDummy.From_Date__c and Reconcile__c = false];
       if(lstCreditNote .size()>0){
            for(c2g__codaCreditNote__c SCNLIIterator : lstCreditNote){
                setSCNId.add(SCNLIIterator.id);
            }
       }
           
        if(setSIId.size() > 0 && bReconciled) {
            if(!Test.isRunningTest()) {
                //Modified by Mythili for CC-2412
              //  SIReconciliationbatchId=Database.executeBatch(new LocalBillingSIReconciliationBatch(setSIId));
               Batch_Size__c reconcileBatchSize=Batch_Size__c.getValues('WizardSIReconcile');
               Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
              List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Reconcile Statement');
              if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0){
                for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
                    setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Reconcile Statement','reconcile',objDummy,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(reconcileBatchSize.Size__c)));
                }
              }else{
                setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Reconcile Statement','reconcile',objDummy,null,null,null,null),Integer.valueOf(defaultBatchSize.Size__c)));
              }
              strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';  
            }
        }       
        if(setSCNId.size()>0 && bReconciled){
                if(!Test.isRunningTest()) 
                    setSCNBatchIds.add(Database.executeBatch(new LocalBillingSCNReconciliationBatch(setSCNId)));
                    strSCNReconcileBatchStatus='Status of Reconciliation Credit Note Batch : Queued';

                    
        }
      bReconciliationEnabledPoller =true;
    }
    
    public void apexReconciliationJobStatus() {
    	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : : Completed';
        }
        else {
        	strSIReconcileBatchStatus = 'Status of Reconciliation Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : : Completed';
        }
        else {
        	strSCNReconcileBatchStatus = 'Status of Reconciliation Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
            bReconciliationEnabledPoller = false;
            bShowPostFF=true;
        }
        /*
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(SIReconciliationbatchId != null) {
            setBatchIds.add(SIReconciliationbatchId);
        }
        if(SCNReconciliationbatchId != null) {
            setBatchIds.add(SCNReconciliationbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(SIReconciliationbatchId)) {
             strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : ' + mapBatchFFJob.get(SIReconciliationbatchId).Status;
                if(mapBatchFFJob.get(SIReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
            }
            else {

                bStatus = false;
            }
            if(mapBatchFFJob.containsKey(SCNReconciliationbatchId)) {
                strSCNReconcileBatchStatus= 'Status of Reconciliation Credit Note Batch : ' + mapBatchFFJob.get(SCNReconciliationbatchId).Status;
                if(bStatus && mapBatchFFJob.get(SCNReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
                else {
                    bStatus = false;
                }
            }
            if(bStatus) {
                bReconciliationEnabledPoller = false;
                bShowPostFF=true;
            }
        }
        else{
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Record found for Reconciliation Batch.'));
             bReconciliationEnabledPoller = false;
             bShowPostFF=true;
            
        }*/
    }
    public void postFFInvoice() {
      	setSIBatchIds = new Set<id>();
        setSCNBatchIds = new Set<id>();
        set<Id> setSIId = new set<Id>();
        set<Id> setSCNId = new set<Id>();
        list<c2g__codaInvoice__c> lstSalesInvoice =[Select Customer_Name__c, c2g__NetTotal__c, Name, Id, c2g__InvoiceDate__c, Reconcile__c 
           From c2g__codaInvoice__c WHERE SI_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__InvoiceStatus__c = 'In Progress' AND SI_Payment_Method__c = 'Statement' AND c2g__InvoiceDate__c<=:objDummy.To_Date__c and c2g__InvoiceDate__c>=:objDummy.From_Date__c];
        if(lstSalesInvoice .size()>0){
             for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
                  setSIId.add(iterator.id);
             }
        }
        
        list<c2g__codaCreditNote__c> lstCreditNote =[Select Name, c2g__NetTotal__c, Reconcile__c,Customer_Name__c From c2g__codaCreditNote__c where SC_Billing_Partner__c = 'THE BERRY COMPANY' AND c2g__CreditNoteStatus__c = 'In Progress' AND SC_Payment_Method__c = 'Statement' AND c2g__CreditNoteDate__c<=:objDummy.To_Date__c and c2g__CreditNoteDate__c>=:objDummy.From_Date__c];
       if(lstCreditNote .size()>0){           
            for(c2g__codaCreditNote__c SCNLIIterator : lstCreditNote){
                setSCNId.add(SCNLIIterator.id);
            }
       }
        if(bPostAllInvoice ) {
            if(setSIId.size() > 0) {  
            if(!Test.isRunningTest()){   
                //Modified by Mythili for CC-2412
            /*  batchId = Database.executeBatch(new FFSalesInvoicePostBatchController(setSIId), 2);
              strBatchStatus = 'Status of Financial Force Invoice Batch : Queued'; */
              Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
              List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('Reconcile Statement');
                  if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0){
                    for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
                        setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Reconcile Statement','post',objDummy,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c), Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(splitBatch.Batch_Size__c)));
                    }
                  }else{
                    setSIBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('Reconcile Statement','post',objDummy,null,null,null,null), Integer.valueOf(defaultBatchSize.Size__c)));
                  }
                strBatchStatus = 'Status of Financial Force Invoice Batch : Queued'; 
              }
            }
            if(setSCNId.size()>0){
             if(!Test.isRunningTest()){ 
                  setSCNBatchIds.add(Database.executeBatch(new FFCreditNotePostBatchController(setSCNId), 2));
                  strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : Queued';
                  }
            }
            bEnabledPoller = true;    
        }
    }
  
  public void apexJobStatus() {
  	boolean bStatusSI = LocalBillingCommonMethods.apexBatchJobStatus(setSIBatchIds);
    	if(bStatusSI) {
    		strBatchStatus = 'Status of Financial Force Invoice Batch : Completed';
        }
        else {
        	strBatchStatus = 'Status of Financial Force Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        boolean bStatusSCN = LocalBillingCommonMethods.apexBatchJobStatus(setSCNBatchIds);
    	if(bStatusSCN) {
    		strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : : Completed';
        }
        else {
        	strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        
        if(bStatusSI && bStatusSCN) {
        	bEnabledPoller = false;
        }
    /*Set<Id> setBatchIds = new Set<id>();
    Boolean bStatus = false;
    if(batchId != null) {
        setBatchIds.add(batchId);
    }
    if(SCNbatchId != null) {
        setBatchIds.add(SCNbatchId);
    } 
    Map<id,AsyncApexJob> mapBatchFFJob;
    if(setBatchIds.size() > 0) {
        mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
        if(mapBatchFFJob.containsKey(batchId)) {
            strBatchStatus = 'Status of Financial Force Invoice Batch : ' + mapBatchFFJob.get(batchId).Status;
            if(mapBatchFFJob.get(batchId).Status == 'Completed') {
                bStatus = true;
            }
        }
        else {
            bStatus = true;
        }
        if(mapBatchFFJob.containsKey(SCNbatchId)) {
            strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : ' + mapBatchFFJob.get(SCNbatchId).Status;
            if(bStatus && mapBatchFFJob.get(SCNbatchId).Status == 'Completed') {
                bStatus = true;
            }
            else {
                bStatus = false;
            }
        }
        if(bStatus) {
            bEnabledPoller = false;
        }
    }*/
  }
  
private String concatenateString(Integer iAccCount, Integer iSICount) {
    return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
  }
  
  private String concatenateSCNString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Credit Notes for Selections';
    }
 
}