@isTest
public class DirectoryPagination_AIUDTriggerTest {
    static testMethod void dirPaginationTest() {
        Test.StartTest();
        
        Directory_Heading__c dirHead = TestMethodsUtility.createDirectoryHeading();
        
        Canvass__c c = TestMethodsUtility.createCanvass();
        
        Account a = TestMethodsUtility.generateCustomerAccount();
        insert a;     
        
        Contact cnt = TestMethodsUtility.createContact(a);       
        
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', a);              
        
        Order__c ord = TestMethodsUtility.createOrder(a.Id);        
        
        Order_Group__c og = TestMethodsUtility.createOrderSet(a, ord, oppty);
        
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        oln.Directory_Heading__c = dirHead.Id;
        insert oln;
        
        Directory__c dir = TestMethodsUtility.createDirectory();
        
        Directory_Mapping__c DM = TestMethodsUtility.createDirectoryMapping();
        
        Telco__c telco = TestMethodsUtility.createTelco(a.Id);
        
        Directory_Edition__c DE = TestMethodsUtility.createDirectoryEdition(dir.Id, c.id, telco.Id, DM.Id);
        
        Line_Item_History__c LIH = TestMethodsUtility.createLineItemHistory(oln.Id, DE.Id);     
        
        Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(dir);
        
        Section_Heading_Mapping__c secHeadMapping = TestMethodsUtility.generateSectionHeadingMapping(dirSection.Id);
        secHeadMapping.Directory_Heading__c = dirHead.Id;
        insert secHeadMapping;
        
        Directory_Pagination__c dirPagination = TestMethodsUtility.generateDirectoryPagination(dirSection.Id, oln.Id, DE.Id);      
        insert dirPagination;
        
        Test.StopTest();        
    }
}