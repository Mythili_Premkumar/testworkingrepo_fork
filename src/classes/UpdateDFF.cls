public with sharing class UpdateDFF {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String accountId {
        get;
        set;
    }
    public String fpId {
        get;
        set;
    }
    public String profileId {
        get;
        set;
    }
    public Integer fpCnt {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    public Map < Id, Fulfillment_Profile__c > mapFProfile {
        get;
        set;
    }
    public list < wrapperFpClass > lstFpWrapper {
        get;
        set;
    }
    private Digital_Product_Requirement__c dff;

    public UpdateDFF(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c', 'Account__c', 'Fulfillment_Profile__c', 'Core_Opportunity_Line_ID__c', 'Core_Oppty_Id_Data_Migration__c', 'Talus_Subscription_Id__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
    }

    public pageReference fpLoad() {
        dffId = dff.Id;
        accountId = dff.Account__c;
        fpId = dff.Fulfillment_Profile__c;
        lstFpWrapper = new list < wrapperFpClass > ();

        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.DFFFpLabelNames(orderSetId, mOrderSetId);

        if (allDffs.size() > 0) {
            
            //system.debug('************DFFs************' + allDffs.size());

            if (String.isNotEmpty(accountId)) {

                mapFProfile = CommonUtility.fpByAccountId(new set < Id > { accountId });
                //System.debug('************FPList************' + mapFProfile.size());
                if (mapFProfile.size() > 0) {
                    
                    for (Fulfillment_Profile__c iterator: mapFProfile.values()) {
                        if (iterator.id == fpId) {
                            //System.debug('************FP************' + iterator);
                            lstFpWrapper.add(new wrapperFpClass(true, iterator));
                        } else {
                            //System.debug('************FP************' + iterator);
                            lstFpWrapper.add(new wrapperFpClass(false, iterator));
                        }
                    }
                } else {
                    CommonUtility.msgError(commonUtility.fulfillmentProfileisEmpty);
                }
            } else {
                CommonUtility.msgError(commonUtility.dffAccountIsEmpty);
            }
        }
        fpCnt = lstFpWrapper.size();
        
        //User message to let them notify about Dff data migration fallout
        //String.isNotBlank(dff.Core_Opportunity_Line_ID__c)
        if(dff.Core_Oppty_Id_Data_Migration__c && String.isBlank(dff.Talus_Subscription_Id__c)){
            CommonUtility.msgWarning('This DFF has not yet completed the Data Migration process, please note that NO changes will be able to be saved to this record until this process is completed, please use Notify Admin Custom Button available on top section to get this issue fixed');
        }        
        return null;
    }

    public pageReference updateDff() {
        return null;
    }

    public pageReference updateDffFp() {
        if (fpCheck()) {
            Fulfillment_Profile__c objFProfile = mapFProfile.get(profileId);
            Digital_Product_Requirement__c UpdateCurrentDFF = new Digital_Product_Requirement__c(id = dffId, Fulfillment_Profile__c = profileId);
            try {
                update UpdateCurrentDFF;
                CommonUtility.msgConfirm(commonUtility.flmntPrflUpdtd);
            } catch (Exception e) {
                CommonUtility.msgError(String.valueof(e.getMessage()));     
            }
        }
        return null;
    }

    private boolean fpCheck() {
        integer iCount = 0;
        for (wrapperFpClass iterator: lstFpWrapper) {
            if (iterator.bFlag == true) {
                iCount++;
                profileId = iterator.objProfile.Id;
            }
        }
        if (iCount == 0) {
            CommonUtility.msgError(commonUtility.selectFlmntRcd);
            return false;
        } else if (iCount > 1) {
            CommonUtility.msgError(commonUtility.selectOneFlmntRcd);
            profileId = null;
            return false;
        }
        return true;
    }

    public class wrapperFpClass {
        public boolean bFlag {
            get;
            set;
        }
        public Fulfillment_Profile__c objProfile {
            get;
            set;
        }
        public wrapperFpClass(boolean bFlag, Fulfillment_Profile__c objProfile) {
            if (objProfile.Default_Profile__c == true) {
                this.bFlag = bFlag;
                this.objProfile = objProfile;
            } else {
                this.bFlag = bFlag;
                this.objProfile = objProfile;
            }
        }
    }
}