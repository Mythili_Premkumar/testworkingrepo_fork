@IsTest(SeeAllData=true)
public class ReadyToUpdateDFFTest{

    public static testMethod void ReadyToUpdateDFFTest01(){
        
        Test.startTest();
        
        Product2 prdt = CommonUtility.createPrdtTest();
        prdt.Product_Type__c = 'Video';
        insert prdt;
        
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;
        
        
        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;
        
        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        insert oln;
        
        Fulfillment_Profile__c fp = CommonUtility.createFpTest(a);
        insert fp;
        
        Directory_Heading__c dirHead = TestMethodsUtility.createDirectoryHeading();  
        Digital_Product_Requirement__c DFF = CommonUtility.createDffTest();
        DFF.RecordTypeId = Label.DFF_Spotzer_RT_Id;
        DFF.Account__c = a.Id;
        DFF.OrderLineItemID__c = oln.Id;
        DFF.DFF_Product__c = prdt.Id;
        DFF.Contact__c = cnt.Id;
        DFF.Proof_Contact__c = cnt.Id;
        DFF.Fulfillment_Profile__c = fp.Id;
        DFF.Heading_1__c = dirHead.Id;
        DFF.Bundle__c = 'Spotzer BASE';
        DFF.business_url__c = 'http://www.theberrycompany.com';
        DFF.Competitor_1__c = 'test';
        DFF.Competitor_2__c = 'test';
        DFF.Competitor_3__c = 'test';   
        DFF.Employee_1__c = 'test';
        DFF.Employee_2__c = 'test';
        DFF.Employee_3__c = 'test';
        DFF.Affiliation_1__c = 'test';
        DFF.Affiliation_2__c = 'test';   
        DFF.Affiliation_3__c = 'test';
        DFF.Affiliation_4__c = 'test';         
        DFF.Also_Known_As_aka_1__c = 'test';
        DFF.Also_Known_As_aka_2__c = 'test';
        DFF.Also_Known_As_aka_3__c = 'test';   
        DFF.Also_Known_As_aka_4__c = 'test';
        DFF.Also_Known_As_aka_5__c = 'test'; 
        DFF.CpnDesc_CstLnkUrl_CstLnkTxt__c = true;
        DFF.coupon_description__c = 'test';
        DFF.coupon_url__c = 'http://www.csc.com';
        insert DFF;
        
        Digital_Product_Requirement__c DFF1 = CommonUtility.createDffTest();
        DFF1.RecordTypeId = Label.DFF_Boostability_RT_Id;
        DFF1.Account__c = a.Id;
        DFF1.OrderLineItemID__c = oln.Id;
        DFF1.DFF_Product__c = prdt.Id;
        DFF1.Contact__c = cnt.Id;
        DFF.business_url__c = 'http://www.theberrycompany1.com';
        DFF.Bundle__c = 'Spotzer SEO';
        insert DFF1;
        
        ApexPages.currentPage().getParameters().put('Id', DFF.id);
        ApexPages.StandardController sc1 = new ApexPages.StandardController(DFF);
        ReadyToUpdateDFF rdyToUpdt = new ReadyToUpdateDFF(sc1);
        rdyToUpdt.fpDffs(); 
        rdyToUpdt.updateDffs();          
        
        ApexPages.currentPage().getParameters().put('Id', DFF.id);
        ApexPages.StandardController sc2 = new ApexPages.StandardController(DFF);
        UpdateDFF updtDff = new UpdateDFF(sc2);
        updtDff.fpLoad(); 
        updtDff.updateDffFp();
        
        Test.stopTest();
    } 
    
}