public class DirectorysectionTollfree{
    public static void updateDirsecTollfree(list<Directory__c> ObjDirLst, map<Id,Directory__c> oldMap){
        set<Id> dirId = new set<Id>();
        for(Directory__c iterator : ObjDirLst){
            if(oldMap != null) {
                Directory__c objOldDir = oldMap.get(iterator.Id);
                if(objOldDir.Toll_Free_Phrase__c != iterator.Toll_Free_Phrase__c){
                    dirId.add(iterator.Id);
                }
            }
        }
        List<Directory__c> dirLst = new List<Directory__c>();
        if(dirId.size()>0){ 
            dirLst = DirectorySOQLMethods.getDirectoryDirsectionById(dirId);
        }
        List<Directory_Section__c> dirsecLst = new List<Directory_Section__c>();
        if(dirLst.size()>0){
            for(Directory__c objdir : dirLst){
                for(Directory_section__c objDirsec : objdir.Directory_Sections__r){
                    if(objDirsec.Toll_Free_Phrase__c == null || objDirsec.Toll_Free_Phrase__c != null ){
                        objDirsec.Toll_Free_Phrase__c = objdir.Toll_Free_Phrase__c;
                        objDirsec.Horizontalscale__c = 'True';
                        dirsecLst.add(objDirsec);   
                    }
                }
            }
        }
        if(dirsecLst.size()> 0){
            update dirsecLst;
        }
    }
}