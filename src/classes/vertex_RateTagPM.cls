/***********************************************************************************************
* Controller Name   : vertex_RateTagPM     
* Date              :   
* Author            : Kishore.A   
* Purpose           : Class for AddNewProductMapping      
* Change History    : 
* Date                  Programmer              Reason  
* -------------------- ------------------- -------------------------    
*                     PraveenKumar.B          Initial Version  
    
**************************************************************************************************/  


public class vertex_RateTagPM{


    public Directory_Product_Mapping__c DP{get;set;}
    public string k;
    public string type;
    public vertex_RateTagPM(ApexPages.StandardController controller) {    
         DP= (Directory_Product_Mapping__c )controller.getRecord();
         k=ApexPages.currentPage().getParameters().get('objDirid');
         type=ApexPages.currentPage().getParameters().get('type');
         System.debug('k value is****'+k);
         if(!Test.isRunningTest()){
             DP.Directory__c =  ApexPages.currentPage().getParameters().get('objDirid');
         }
    }
    public PageReference save() {
    Pagereference  page;
       if(type=='local'){
       page=new Pagereference('/apex/vertex_NationalRateCardPage_v3?Id='+k+'&Type=local');    
       }
       else{
        page=new Pagereference('/apex/vertex_NationalRateCardPage_v3?Id='+k+'&Type=national');   
       }
       try{
            insert DP; 
            page.setRedirect(false); 
            return page;
       }
       catch(Exception ex){
       Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,''+'Please Select Another Product'));
       }
       return null;
    }
    public PageReference cancel(){
    Pagereference  page;
    
              
            if(type=='local'){
       page=new Pagereference('/apex/vertex_NationalRateCardPage_v3?Id='+k+'&Type=local');    
       }
       else{
        page=new Pagereference('/apex/vertex_NationalRateCardPage_v3?Id='+k+'&Type=national');   
       }    
            page.setRedirect(false); 
            return page;
    
    
    
    }
}