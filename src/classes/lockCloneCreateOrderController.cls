public with sharing class lockCloneCreateOrderController {
    String opportunityID;
    String PaymentId;
    String PaymentMethodId;
    list<Order_Line_Items__c> newOLI = new list< Order_Line_Items__c>();
    public List<Order_Line_Items__c> testLit{get;set;}
   
    //map<Id, String> mapDFFRT = new map<Id, String>();
    
    public lockCloneCreateOrderController(ApexPages.StandardController controller) {
        opportunityID = ApexPages.currentPage().getParameters().get('Id');
        //Fetching Payment ID from URL
        PaymentId = ApexPages.currentPage().getParameters().get('paymentid');
        system.debug('**********opportunityID************** : '+ opportunityID);
        //mapDFFRT = CommonMethods.getsObjectRecordTypeIdKeyset(CommonMessages.dffObjectName);
    }
    
    public Pagereference onLoad() {
        testLit=new List<Order_Line_Items__c>();
        opportunityID = ApexPages.currentPage().getParameters().get('Id');
        Opportunity objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneCreateOrder(new set<ID>{opportunityID})[0];   
        list<OpportunityLineItem> lstOLI = objOpportunity.OpportunityLineItems;
        //map<Id, Fulfillment_Profile__c> mapFProfile = FulfillmentProfileSOQLMethods.getFulFillmentProfileByAccountId(new set<Id>{objOpportunity.AccountId});
        list<pymt__PaymentX__c> lstPayment = PaymentXSOQLMethods.getPaymentxByID(new set<Id>{PaymentId});
        if(lstPayment != null && lstPayment.size() > 0) {
            PaymentMethodId = lstPayment[0].pymt__Payment_Method__c;
        }
        Order__c objOrder = lockCloneHandlerController.CheckAndCreateNewOrderIfNotExists(objOpportunity);
        Order_Group__c orderGroup = CreateNewOrderGroup(objOpportunity, objOrder);
        CreateOrderLineItem(objOpportunity, objOrder, orderGroup);
        if(newOLI.size() > 0) {
            map<id, id> mapOLIIdAnchorId = lockCloneHandlerController.populateAnchorCaption(newOLI);
            System.debug('Testingg mapOLIIdAnchorId '+mapOLIIdAnchorId);
            lockCloneHandlerController.FindAddOnProductForDFFAndCreateDFF(newOLI, objOpportunity, mapOLIIdAnchorId);
            if(String.isNotEmpty(PaymentId)) {
                lockCloneHandlerController.SendReceipt(PaymentId, objOpportunity.Billing_Contact__r.Email);
            }
        }
        
        //added by Mythili - ATP-3293
        if(lstOLI!=null && lstOLI.size()>0){
            lockCloneHandlerController.checkAndUpdateBCCRecord(lstOLI); 
        }
        
        lockCloneHandlerController.setOpportunityToClosedWon(objOpportunity);
        
        if(objOpportunity.StageName == 'Closed UTC' && objOpportunity.Billing_Contact__c != null) {
          lockCloneHandlerController.sendemailnotification(objOpportunity, newOLI);
        }
        //Added by Mythili - ATP-4258
      /*  if(objOpportunity.AccountId!=null){
                lockCloneHandlerController.populateHighestCanvassOnAccount(objOpportunity.AccountId);
        } */
        
        PageReference pageRef;
        pageRef = new PageReference('/'+orderGroup.Id);
        return pageRef;
    }
    
    /*private Order__c CheckAndCreateNewOrderIfNotExists(Opportunity objOpportunity) {
        list<Order__c> lstOrder = OrderSOQLMethods.getOrderForNationalByAccountID(new set<Id>{objOpportunity.AccountId});
        system.debug('*****************Order Size : '+ lstOrder.size());
        Order__c objOrder = new Order__c();
        if(lstOrder.size() > 0) {
            //If order is already exists
            objOrder = lstOrder[0];
            system.debug('*****************Order Exists**********' + objOrder);
        }
        else {
            //if new order create the order name
            system.debug('*****************New Order**********');
            objOrder = lockCloneHandlerController.newOrder(objOpportunity);
            insert objOrder;
            system.debug('*****************New Order : ' + objOrder);
        }
        return objOrder;
    }*/
    
    private Order_Group__c CreateNewOrderGroup(Opportunity objOpportunity, Order__c objOrder) {
        Order_Group__c orderGroup = lockCloneHandlerController.newOrderSet(objOpportunity, objOrder);
        insert orderGroup;
        return orderGroup;
    }
     
    private void CreateOrderLineItem(Opportunity objOpportunity, Order__c objOrder, Order_Group__c orderGroup) {
        Id OLIRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.beryyOLIRT, CommonMessages.OLIObjectName);
        set<String> setLeadCombo = new set<String>();
        for(OpportunityLineItem iterator : objOpportunity.OpportunityLineItems) {
            setLeadCombo.add(iterator.strOppliCombo__c);
        }
        
        map<String, Lead_Assignment_Rules__c> mapLead = new map<String, Lead_Assignment_Rules__c>();
        if(setLeadCombo.size() > 0) {
            List<Lead_Assignment_Rules__c> lstLeadAssign = [SELECT Area_Code__c,Directory_Section__c,Directory__c,Exchange__c,Id,Name,Telco__c,strForeignListingCombo__c FROM Lead_Assignment_Rules__c where strForeignListingCombo__c IN:setLeadCombo];
            
            for(Lead_Assignment_Rules__c iterator : lstLeadAssign) {
                system.debug('Lead String : '+ iterator.strForeignListingCombo__c);
                mapLead.put(iterator.strForeignListingCombo__c, iterator);
            }
        }
        
        boolean bFlagP4PBilling = false;
        
        for(OpportunityLineItem iterator : objOpportunity.OpportunityLineItems) {
            //added by Mythili - for ticket ATP-2293
            if(iterator.PricebookEntry.ProductCode!='BCC') {
                //Add by Sathish - ATP-3690
                if(iterator.Is_P4P__c && iterator.P4P_Price_Per_Click_Lead__c != null && iterator.P4P_Price_Per_Click_Lead__c > 0 && !bFlagP4PBilling) {
                    bFlagP4PBilling = true;
                    iterator.P4P_Billing__c = true;
                }
                // create the order line item entry
                Order_Line_Items__c objOLI = new Order_Line_Items__c(RecordTypeId = OLIRecordTypeID);        
                lockCloneHandlerController.newOrderLineItem(objOLI, orderGroup, objOrder, iterator, objOpportunity, mapLead, PaymentMethodId);
                // add the order line item to the list
                system.debug('OLI Value : '+ objOLI);
                newOLI.add(objOLI);
            }
        }
        
        if(newOLI.size() >0) {
            insert newOLI;
            Set<Id> OLIIds=new Set<ID>();            
            for(Order_Line_Items__c  OLIterator : newOLI) {
                OLIIds.add(OLIterator.id);
            }
            if(OLIIds.size()>0) {
                newOLI = OrderLineItemSOQLMethods.getOLIByID(OLIIds);
                if(objOpportunity != null){
                    SoldLetterNotification.lineitemSoldNew(newOLI, new list<Opportunity>{objOpportunity},null);
                   //testEmail(newOLI);
                   // emailSoldLetter(objOpportunity.Id);    
                }
                lockCloneHandlerController.orderLineItemHistoryProcess(newOLI);    
            }
        }
    }
    
    
    /*private void orderLineItemHistoryProcess(list<Order_Line_Items__c> newOLI) {
        //Insert OrderLineItem History record with Original OLI Information
        list<Line_Item_History__c> OlihList = new list<Line_Item_History__c>();
        for(Order_Line_Items__c olis: newOLI) {
            system.debug('***********OLI================ '+ olis);
            Line_Item_History__c newOrderLineItemHistoryrecord = lockCloneHandlerController.newOrderLineItemHistory(olis);
            OlihList.add(newOrderLineItemHistoryrecord);
        }
        if(OlihList.size()>0) {
            system.debug('***********OlihList================ '+ OlihList);
            insert OlihList;
        }
    }*/
    
    /*private void SendReceipt(String PaymentId, String Contactemail) {
        PageReference pdf = Page.PaymentReceipt;
        pdf.getParameters().put('ipymt',PaymentId);

        // the contents of the attachment from the pdf
        Blob body;

        try {
            // returns the output of the page as a PDF
            body = pdf.getContent();
        }
        catch (VisualforceException e) {
            body = Blob.valueOf('There are some issues while genrating the receipt!! Kindly bear with us.');
        }        
        
        Datetime myDT = Datetime.now();
        String htmlBody = 'Thank you for your business!! <br/><br/> If you have questions about this receipt, please contact us at 303-867-1571.<br/<br/>Best Regards<br/><br/>Berry Company';
        CommonEmailUtils.sendHTMLEmailWithAttachment(new list<String>{Contactemail}, 'Payment Receipt Dated:'+myDT.format('MM/dd/yyyy HH:mm:ss'), htmlBody, new List<Messaging.Emailfileattachment>{CommonEmailUtils.generateEmailFileAttachment('PaymentReceipt.pdf',body)});
    }
    
    // set the opportunity to closed/won
    private void setOpportunityToClosedWon(Opportunity objOpportunity) {
        system.debug('*****************setOpportunityToClosedWon Start*******************');
        // Added by sathishkumar [01/23/2013]
        // set the opportunity to lock.
        system.debug('*****************Lock : '+ objOpportunity.isLocked__c);
        //list<Opportunity> lstThrowError;
        //System.debug('Testingg Purposefully causing Null pointer exception to test exception handling '+lstThrowError.size());
        if (objOpportunity.isLocked__c == false) {
            objOpportunity.isLocked__c = true;
            system.debug('*****************Lock : '+ objOpportunity.isLocked__c);
            if(!Test.isRunningTest()) {
                update objOpportunity;
            }
        }
        system.debug('*****************setOpportunityToClosedWon End*******************');
    }
    
    private static void sendemailnotification(Opportunity objOpportunity,list<Order_Line_Items__c> newOLI) {
        User usr=[SELECT ManagerId FROM User where id=:UserInfo.getUserId()];
        if(usr.ManagerId != null) {
            System.debug('############'+objOpportunity.LastModifiedBy.ManagerId);
            CommonEmailUtils.sendEmailWithEmailTemplate(new list<String>{objOpportunity.LastModifiedBy.Email}, Label.ClosedUTCTemplate, usr.ManagerId, objOpportunity.id);
        }
        //Sending Email to Customer        
        String pdfString=buildPDFString(objOpportunity,objOpportunity.Account,newOLI);
        CommonEmailUtils.sendEmailWithEmailTemplateAttachement(null, Label.UTCRenewal, objOpportunity.Billing_Contact__c, null, new List<Messaging.Emailfileattachment>{CommonEmailUtils.generateEmailFileAttachment('Closed_UTC_Confirmation.pdf', Blob.toPDF(pdfString))});
        System.debug(objOpportunity.Billing_Contact__c+'###############'+pdfString );
     }
     
     private static string buildPDFString(Opportunity objOpportunity,Account objAccount,list<Order_Line_Items__c> newOLI) {
        String pdfString = '';
        pdfString += '<html><body>';
        if(objAccount != null) {
            pdfString += generateHeaderPDFString(objAccount);
        }
        if(newOLI.size() > 0) {
            pdfString += generateDirectoryEditionString(newOLI);
        }
        pdfString += '</body><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></html>';
        pdfString += '<html><body>';
        pdfString += '<div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><img width="90" src="'+System.Label.BerryLogo+'"></td></tr></table></div>';
        pdfString += '<table width="100%"><tr><td border="0" bgColor="#848484" align="left" width="40%">Products</td><td border="0" bgColor="#848484" align="center" width="20%">Intial Term</td><td border="0" bgColor="#848484" align="center" width="20%">Renewal Terms</td><td border="0" bgColor="#848484" align="right" width="20%">Monthy Rate</td></tr></table>'; 
        if(newOLI.size() > 0) {
            pdfString += generateDirectoryEditionAccountString(newOLI);
            for(Order_Line_Items__c iterator : newOLI) {
                pdfString += generateDirOlitemsString(iterator);
            }
            pdfString += generateTotalPrintInvestmentString(newOLI);
        }
        System.debug('###############'+pdfString );
        return pdfString ;
    }
    
    private static string generateHeaderPDFString(Account objAccount) {
        String strOLIHPDF = '';
        strOLIHPDF += '<div><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td><img width="90" src="'+System.Label.BerryLogo+'"></td></tr></table></div>';
        strOLIHPDF += '<div style="border:5px solid black;padding-left:10%;padding-right:10%;height:700px"><div style="float:left"><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px" align="left"><tr><td nowrap="true" style="font-size:9pt;"><br><span>'+objAccount.Name+'</span><br><span>'+objAccount.BillingStreet+'</span><br><span>'+objAccount.BillingCity+'</span>,&nbsp;<span>'+objAccount.BillingState+'</span>,&nbsp;<span>'+objAccount.BillingPostalCode+'</span><br>&nbsp;<span>'+objAccount.Phone+'</span><br><b>Account Number:</b>&nbsp;<span>'+objAccount.Account_Number__c+'</span><br><br></td></tr><tr><td border="1"><hr></td></tr></table></div></div><br/>';
        strOLIHPDF += '<table width="100%"><tr><td border="0" bgColor="#848484" align="center">Advertising Order - Investment Summary</td></tr></table>';
        strOLIHPDF += '<table border= "0" width="100%"><tr><td width="50%" style="font-size:10px;color:#2E2E2E">PRINT DIRECTORY ADVERTISING</td><td width="25%" style="font-size:10px;color:#2E2E2E"; align="center">Effective Billing Date</td><td style="font-size:10px;color:#2E2E2E";width="25%" align= "right">Monthly Investment</td></tr></table>';
        system.debug('*********LETTER RENWAL STRING GENERATION ***********'+strOLIHPDF );
        return strOLIHPDF;
    }
    
    private static string generateDirectoryEditionString(list<Order_Line_Items__c> lstOLI) {
        decimal monthlyPayment = 0.00;
        String strOLIPDF = '';
        string Dt;
        List<Order_line_Items__c> oliLbdLst = new List<Order_line_Items__c>();
        set<Id> oliLbdId = new set<Id>();
    
        for(Order_Line_Items__c objOli : lstOLI) {
            monthlyPayment += objOli.UnitPrice__c;
            oliLbdId.add(objOli.Id);
        }
    
        Order_Line_Items__c objOli = lstOLI[0];
    
        if(oliLbdId.size() > 0) {
            oliLbdLst = [Select Id,Name, Directory_Section__r.Columns__c, Last_Billing_Date__c from Order_Line_Items__c where Id IN:oliLbdId ORDER BY Last_Billing_Date__c ASC];
        }
        System.debug('*********Last Billing Date OLI List **********'+oliLbdLst);
        System.debug('*********Last Billing Date OLI List **********'+oliLbdLst.size());
        for(Integer i = 0;i < oliLbdLst.size();) {
            if(oliLbdLst[i].Last_Billing_Date__c != null) {
                Integer day=0;
                Integer month=0;
                Integer year=0;
                day = oliLbdLst[i].Last_Billing_Date__c.day();
                month = oliLbdLst[i].Last_Billing_Date__c.month();
                year = oliLbdLst[i].Last_Billing_Date__c.year();
                Dt = month+'-'+ day+'-'+ year;
                break;
            }
            i++;
        }
    
        strOLIPDF += '<table width="100%" cellpadding="0" cellspacing="0" style="margin:0px auto;" border="0"><tr>';
        strOLIPDF += '<td style="font-weight:normal;color:#848484" width="50%">'+objOli.Directory_Edition__r.Name+'</td><td style="font-weight:normal;color:#848484" width="25%" align="center">'+Dt+'</td><td style="font-weight:normal;color:#848484" width="25%" align="right">$'+monthlyPayment+'</td></tr></table>';
        strOLIPDF += '<br><table width="100%" cellpadding="0" cellspacing="0"><tr><td width="75%" align="left">Total Monthly Investment</td><td align="right" width="25%">$'+monthlyPayment+'</td></tr></table>';
        system.debug('*********LETTER RENWAL STRING GENERATION ***********'+strOLIPDF);
        return strOLIPDF;
    }
    
    private static string generateDirectoryEditionAccountString(list<Order_Line_Items__c> lstOLIDE) {
        Order_Line_Items__c objOliDEAct = lstOLIDE[0];
        String strOLIDEACT = '';
        strOLIDEACT += '<table width="100%" cellpadding="0" cellspacing="0" style="margin:0px auto;" border="0"><tr><td style="font-weight:bold;" width="50%">'+objOliDEAct.Directory_Edition__r.Name+'</td></tr><tr><td nowrap="true" style="font-size:9pt;font-style:italic;color:#848484"><span>'+objOliDEAct.Account__r.Phone+'</span><br><span>'+objOliDEAct.Account__r.Name+'</span><br><span>'+objOliDEAct.Account__r.BillingStreet+'</span><br><span>'+objOliDEAct.Account__r.BillingCity+'</span>,&nbsp;<span>'+objOliDEAct.Account__r.BillingState+'</span> &nbsp;<span>'+objOliDEAct.Account__r.BillingPostalCode+'</span><br></td></tr></table>';
        return strOLIDEACT;
    }
    
    private static string generateDirOlitemsString(Order_Line_Items__c iterator){
        String oliDirString = '';
        system.debug('*********Monthly Payment*********'+iterator.UnitPrice__c);
        oliDirString += '<table width="100%"><tr><td border="0" align="left" width="40%">'+iterator.Product2__r.Name+'</td><td border="0"  align="center" width="20%">12 Months</td><td border="0"  align="center" width="20%">12 Months</td><td border="0" align="right" width="40%">$'+iterator.UnitPrice__c.setscale(2)+'</td></tr></table>'; 
        return oliDirString;
    }
    
    private static string generateTotalPrintInvestmentString(list<Order_Line_Items__c> lstTotalOli) {
        decimal totalmonthlyPayment = 0.00;  
        Order_Line_Items__c objTotalOli = lstTotalOli[0];
        for(Order_Line_Items__c objOli : lstTotalOli) {
            totalmonthlyPayment += objOli.UnitPrice__c;
        }
        String strTotalOli = '';
        strTotalOli += '<table width="100%" border="0" style="table-layout:fixed;"><tr><td border="0" style="font-weight:bold;" align="left" width="80%">Total Print Investment for '+objTotalOli.Account__r.Phone+'</td><td border="0" style="font-weight:bold;text-align:right;" width="20%">$'+totalmonthlyPayment+'</td></tr></table>';
        strTotalOli += '</body></html>';
        return strTotalOli;
    }*/
}