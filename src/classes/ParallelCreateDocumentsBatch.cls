/**
 * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
 * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
 * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
 * result in criminal or other legal proceedings.
 *
 * Copyright FinancialForce.com, inc. All rights reserved.
 * Created by Agustina Garcia
 */

public with sharing class ParallelCreateDocumentsBatch implements Database.Batchable<sObject>, Database.Stateful
{
    private Integer jobPos;
    private Integer minRange;
    private Integer maxRange;

    private String sourceObjectName;
    private String sourceProcessedFieldName;

    private String nameMask;
    private String minRangeWithMask;
    private String maxRangeWithMask;
    private List<String> maskValues;

    private Id integrationRuleId;
    private SObject integrationRule;

    private Integer scopeSize;

    public List<Id> targetObjectIds;
    public List<String> errorMessages;

    public ParallelCreateDocumentsBatch(SObject rule, Integer scopeValue, String nameMaskVal, Integer minRangeVal, Integer maxRangeVal, Integer jobPosVal)
    {
        nameMask = nameMaskVal;
        scopeSize = scopeValue;

        minRange = minRangeVal;
        maxRange = maxRangeVal;

        jobPos = jobPosVal;

        targetObjectIds = new List<Id>();
        errorMessages = new List<String>();

        maskValues = nameMask.split('\\{');
        maskValues[1] = maskValues.get(1).split('}').get(0);

        Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, minRange, maxRange);

        minRangeWithMask = minAndMaxWithMask.get(1);
        maxRangeWithMask = minAndMaxWithMask.get(2);

        integrationRule = rule;
        integrationRuleId = integrationRule.Id;
        sourceObjectName = (String)integrationRule.get(ParallelCreateAndPostDocumentsService.SOURCEOBJECT_NAME);
        sourceProcessedFieldName = (String)integrationRule.get(ParallelCreateAndPostDocumentsService.SOURCEOBJECT_PROCESSEDFIELD_NAME);
    }

    public ParallelCreateDocumentsBatch(SObject rule, Integer scopeValue, String nameMaskVal, Integer minRangeVal, Integer jobPosVal)
    {
        nameMask = nameMaskVal;
        scopeSize = scopeValue;

        minRange = minRangeVal;
        maxRange = minRange*2;

        jobPos = jobPosVal;

        targetObjectIds = new List<Id>();
        errorMessages = new List<String>();

        maskValues = nameMask.split('\\{');
        maskValues[1] = maskValues.get(1).split('}').get(0);

        Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, minRange, 0);

        minRangeWithMask = minAndMaxWithMask.get(1);
        maxRangeWithMask = minAndMaxWithMask.get(2);

        integrationRule = rule;
        integrationRuleId = integrationRule.Id;
        sourceObjectName = (String)integrationRule.get(ParallelCreateAndPostDocumentsService.SOURCEOBJECT_NAME);
        sourceProcessedFieldName = (String)integrationRule.get(ParallelCreateAndPostDocumentsService.SOURCEOBJECT_PROCESSEDFIELD_NAME);
    }

    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        String qry;
        Boolean processed = false;

        if(jobPos <= 2)
        {
            qry = 'SELECT Id ';
            qry += 'FROM ' + sourceObjectName + ' ';
            qry += 'WHERE ' + sourceProcessedFieldName + ' = :processed ';
            qry += 'AND Name >= :minRangeWithMask ';
            qry += 'AND Name <= :maxRangeWithMask ';
            qry += 'ORDER BY Name ';
        }
        else
        {
            if(jobPos == 3)
            {
                qry = 'SELECT Id ';
                qry += 'FROM ' + sourceObjectName + ' ';
                qry += 'WHERE ' + sourceProcessedFieldName + ' = :processed ';
                qry += 'AND Name >= :minRangeWithMask ';
                qry += 'ORDER BY Name ';
            }
        }

        return Database.getQueryLocator(qry);
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        System.Savepoint sp = Database.setSavepoint();

        try
        {
            List<Id> sourceIds = new List<Id>();
            List<c2g.CODAAPICommon.Reference> refs = new List<c2g.CODAAPICommon.Reference>();

            for(SObject so : scope)
            {
                sourceIds.add(so.Id);
            }

            targetObjectIds.addAll(ffirule.RuleService.run(sourceIds, integrationRuleId));
        }
        catch (Exception e)
        {
            Database.rollback(sp);

            //hola @UVES, REMOVE BELLOW LINE AND LEAVE THE ONE THAT IS RIGHT NOT COMMENTED DURING MERGE PROCESS
            errorMessages.add('Error during document creation; Exception: ' + e.getMessage() +  '\n' + e.getStackTraceString() + '\n' + e.getLineNumber() + '\n\n');
            //errorMessages.add('Error during document creation; Exception: ' + e.getMessage() + '\n\n');
        }
    }

    public void finish(Database.BatchableContext BC)
    {
        sendEmail(jobPos, BC.getJobId());

        if(jobPos >= 2)
        {
            Map<Integer, String> minAndMaxWithMask = ParallelCreateAndPostDocumentsService.calculateMinAndMaxWithMask(maskValues, 0, (maxRange+1));

            Integer remainingRecords = ParallelCreateAndPostDocumentsService.calculateRemainingSourceRecords(sourceObjectName, sourceProcessedFieldName, minAndMaxWithMask.get(2));

            if(remainingRecords > 0)
            {
                Boolean canRun = ParallelCreateAndPostDocumentsService.canRunAJob(false,'ParallelCreateDocumentsBatch');

                if(canRun)
                {
                    ParallelCreateDocumentsBatch createDocuments = new ParallelCreateDocumentsBatch(integrationRule, scopeSize, nameMask, (maxRange+1), 3);
                    Database.executeBatch(createDocuments, scopeSize);
                }
            }
        }
    }

    private void sendEmail(Integer pos, Id jobId)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.saveAsActivity = false;
        mail.setTargetObjectId(UserInfo.getUserId());

        String body = 'Results of Bulk Create Documents ';
        body += pos == 0 ? ' during pre process job: ' : pos == 1 ? ' on 1st parallel job:' : pos == 2 ? ' on 2nd parallel job: ' : ' on remaining records: ';
        body += '\n\n';

        if( errorMessages.size() > 0 )
        {
            body += 'Errors occurred.\n\n';
            for( String error : errorMessages )
            {
                body += error +'\n';
            }
            body += '\n';

            if (pos == 0)
            {
                mail.setSubject( 'Bulk Pre Process Create Documents completed with errors.');
            }
            else
            {
                mail.setSubject( 'Bulk Create Documents completed with errors.');
            }
        }
        else
        {
            if(targetObjectIds.size() > 0)
            {
                body += targetObjectIds.size() + ' Documents have been created.\n\n';
            }
            else
            {
                body += 'Although there is no error, no document has been created.';
            }

            mail.setSubject( 'Bulk Create Documents completed.');
        }

        mail.setPlainTextBody( body );

        Messaging.sendEmail( new Messaging.SingleEmailMessage[] { mail } );
    }
}