@isTest
 private class TestSmartStreetLeadVerification
 {
     
              
       public static testMethod void myUnitTest_1()
       {
        
                Lead lead_obj = new Lead();
        
                lead_obj.LastName ='voch';
                lead_obj.Company ='vertexcs';
                lead_obj.PostalCode = '35004';
                lead_obj.City = 'NV';
                lead_obj.Phone = '9985123456';
                lead_obj.Country = 'US';
                lead_obj.State = 'AL';
                lead_obj.Do_Not_Validate_Address__c=true;
                lead_obj.street_status__c='valid';
                insert lead_obj;
                
            Test.startTest();
                ApexPages.CurrentPage().getParameters().put('id',lead_obj.id);
                ApexPages.StandardController sc = new ApexPages.standardController(lead_obj);
                SmartStreetLeadVerification  obj_sla=new SmartStreetLeadVerification(sc); 
                // obj_sla.acc_incremnt=acct_obj;
                // obj_sla.acc=acct_obj;
                obj_sla.incrementCounter();
                SmartStreetleadVerification.updatelead(lead_obj.id);
               
           Test.Stoptest();
        
        }  
        
         public static testMethod void myUnitTest_2()
       {
       
                Lead lead_obj = new Lead();
        
                lead_obj.LastName ='voch';
                lead_obj.Company ='vertexcs';
                lead_obj.PostalCode = '35004';
                lead_obj.City = 'NV';
                lead_obj.Phone = '9985123456';
                lead_obj.Country = 'US';
                lead_obj.State = 'AL';
                lead_obj.Do_Not_Validate_Address__c=false;
                lead_obj.street_status__c='invalid';
                insert lead_obj;
                
           Test.startTest();
                ApexPages.CurrentPage().getParameters().put('id',lead_obj.id);
                ApexPages.StandardController sc = new ApexPages.standardController(lead_obj);
                SmartStreetLeadVerification  obj_sla=new SmartStreetLeadVerification(sc); 
                // obj_sla.acc_incremnt=acct_obj;
                // obj_sla.acc=acct_obj;
                obj_sla.incrementCounter();
                SmartStreetleadVerification.updatelead(lead_obj.id);
                
           Test.Stoptest();
        
        }  
        
       
    }