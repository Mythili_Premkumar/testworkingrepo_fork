@isTest
    public class Changeemailoncontactbatchtest{
        static testmethod void tm(){
            Account acc = TestMethodsUtility.createAccount('customer');
            contact con = TestMethodsUtility.createContact(acc.id);
            con.email='testemail@csc.com';
            update con;
            Test.starttest();
            Database.executebatch(new Changeemailoncontactbatch('.btest'),1);
            Test.stoptest();
        }
    }