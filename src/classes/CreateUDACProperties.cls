/*********************************************************
Batch class to create properties' records for each Product
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/07/2014
$Id$
**********************************************************/
global class CreateUDACProperties implements Database.Batchable < sObject > , Database.AllowsCallouts {

    global String query;

    global CreateUDACProperties(String query) {
        this.query = query;
    }

    global Database.Querylocator start(Database.BatchableContext BC) {
        //System.debug('************The Query************' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Package_ID__c > scope) {

        List < Product_Properties__c > lstproperties = new List < Product_Properties__c > ();
        List < Product_Properties__c > packageproperties = new List < Product_Properties__c > ();

        for (Package_ID__c pkgs: scope) {

            //Invoke pkg prdts prpts creation class
            packageproperties = CreateUDACPackageProperties_2.createProperties(pkgs.id);

            if (packageproperties.size() > 0) {

                for (Product_Properties__c prts: packageproperties) {

                    lstproperties.add(prts);

                }

            }

        }

        if (lstproperties.size() > 0) {

            insert lstproperties;

        }

    }

    global void finish(Database.BatchableContext BC) {

        //Get the ID of AsyncApexJob representing this batch job   
        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email
        from AsyncApexJob where Id = : BC.getJobId()];

        //Send an email to Apex jobs' submitter notifying of job completion  
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {
            a.CreatedBy.Email
        };
        mail.setToAddresses(toAddresses);
        mail.setSubject('Fulfillment Catalog Informaiton creation was ' + a.Status);
        mail.setPlainTextBody('The batch Apex job is processed for ' + a.TotalJobItems +
            ' batches with ' + a.NumberOfErrors + ' failures');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });

    }

}