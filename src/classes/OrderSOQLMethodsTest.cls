@isTest(SeeAllData=True)
public class OrderSOQLMethodsTest{
 @IsTest static void OrderSOQLMethodsTest() {
        Set<Id> AccountIdSet = new Set<Id>();
        Set<String> AccountIdStringSet = new Set<String>();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        AccountIdSet.add(newAccount.Id);
        AccountIdStringSet.add(newAccount.Id);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        System.assertNotEquals(null, OrderSOQLMethods.getOrderByAccountID(AccountIdStringSet));
        System.assertNotEquals(null, OrderSOQLMethods.getOrderForNationalByAccountID(AccountIdSet));
        System.assertNotEquals(null, OrderSOQLMethods.getOrderListByAccountID(AccountIdSet));
    }
    }