public class NationalChildLinesSOQLMethods {    
    public static list<National_Child_Lines__c> getNCLWithOutOmitByOrderLineItemIds(set<Id> lstOliNiLi) {
        return [SELECT Id,Advertising_Data__c,Order_Line_Item__c,Line_Number__c, BAS__c,DAT__c,SPINS__c FROM National_Child_Lines__c 
                                                 where Order_Line_Item__c IN:lstOliNiLi and Action__c != 'O'];
    }
}