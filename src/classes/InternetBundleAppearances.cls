public class InternetBundleAppearances {
	public static void IncreaseDecreaseIBAppearances(list<Order_Line_Items__c> lstOLI, map<Id, Order_Line_Items__c> oldMap) {
		//Map is used for Internet bundle appreance count
		map<String, list<Order_line_items__c>> mapIncreaseInternetBundle = new map<String, list<Order_line_items__c>>();
        map<String, list<Order_line_items__c>> mapDecreaseInternetBundle = new map<String, list<Order_line_items__c>>();
                
        //Map is used for Leader Ads Sold count
		map<String, list<Order_line_items__c>> mapIncreaseLeaderAdsSold = new map<String, list<Order_line_items__c>>();
        map<String, list<Order_line_items__c>> mapDecreaseLeaderAdsSold = new map<String, list<Order_line_items__c>>();
        
        //Map is used for Appearance Count
		map<String, list<Order_line_items__c>> mapIncreaseAppearanceCount = new map<String, list<Order_line_items__c>>();
        map<String, list<Order_line_items__c>> mapDecreaseAppearanceCount = new map<String, list<Order_line_items__c>>();
        
        //Map is used for Pagination Appearance Count
		map<String, list<Order_line_items__c>> mapIncreasePaginationAppearanceCount = new map<String, list<Order_line_items__c>>();
        map<String, list<Order_line_items__c>> mapDecreasePaginationAppearanceCount = new map<String, list<Order_line_items__c>>();
        
        //Map is used for Pagination IBUN Appearance Count
		map<String, list<Order_line_items__c>> mapIncreasePaginationIBUNAppearanceCount = new map<String, list<Order_line_items__c>>();
        map<String, list<Order_line_items__c>> mapDecreasePaginationIBUNAppearanceCount = new map<String, list<Order_line_items__c>>();
        
        set<String> setComboValue = new set<String>();
        
        //Product Ineventory Tracing Process
        set<String> setDIRPROD2 = new set<String>();
        map<String, list<Order_line_Items__c>> mapInventoryProdOLI = new map<String, list<Order_line_Items__c>>();
        
        for(Order_line_items__c iterator : lstOLI) {
        	if(iterator.Media_Type__c != null && iterator.Media_Type__c.equals(CommonMessages.oliPrintProductType)) {
	        	if(iterator.Product_Is_IBUN_Bundle_Product__c == true && iterator.Directory_section__c != null && iterator.Directory_Heading__c != null) {
	        		String strDHDSID = iterator.Directory_section__c +''+ iterator.Directory_Heading__c;
	        		if(oldMap != null) {
	        			Order_line_items__c objOldOLI = oldMap.get(iterator.Id);
	        			if(iterator.isCanceled__c) {
	        				setComboValue.add(strDHDSID);
	                        if(!mapDecreaseInternetBundle.containsKey(strDHDSID)){
	                            mapDecreaseInternetBundle.put(strDHDSID, new list<order_line_items__c>());
	                        }
	                        mapDecreaseInternetBundle.get(strDHDSID).add(iterator);
	                    }
	                    else if(objOldOLI.Directory_section__c != iterator.Directory_section__c || iterator.Directory_Heading__c != objOldOLI.Directory_Heading__c) {
	                    	if(iterator.RecordTypeId == System.Label.TestOLIRTNational) {
		                    	String oliDirSHStrOld = objOldOLI.Directory_section__c +''+ objOldOLI.Directory_Heading__c;
		                    	setComboValue.add(oliDirSHStrOld);
		                    	setComboValue.add(strDHDSID);
		                    	if(!mapDecreaseInternetBundle.containsKey(oliDirSHStrOld)) {
		                            mapDecreaseInternetBundle.put(oliDirSHStrOld, new list<order_line_items__c>());
		                        }
		                        mapDecreaseInternetBundle.get(oliDirSHStrOld).add(iterator);
		                        
		                        if(!mapIncreaseInternetBundle.containsKey(strDHDSID)) {
			                        mapIncreaseInternetBundle.put(strDHDSID, new list<order_line_items__c>());
			                    }
			                    mapIncreaseInternetBundle.get(strDHDSID).add(iterator);
		                        
		                        if(iterator.Product_Inventory_Tracking_Group__c == CommonMessages.ypLeaderAd){
			                        if(!mapDecreaseLeaderAdsSold.containsKey(oliDirSHStrOld)) {
			                            mapDecreaseLeaderAdsSold.put(oliDirSHStrOld, new list<order_line_items__c>());
			                        }
			                        mapDecreaseLeaderAdsSold.get(oliDirSHStrOld).add(iterator);
			                        
			                        if(!mapIncreaseLeaderAdsSold.containsKey(strDHDSID)) {
				                        mapIncreaseLeaderAdsSold.put(strDHDSID, new list<order_line_items__c>());
				                    }
				                    mapIncreaseLeaderAdsSold.get(strDHDSID).add(iterator);
		                        }
		                        
		                        if(iterator.Section_Page_Type__c == CommonMessages.ypSecPageType) {
		                        	if(!mapDecreaseAppearanceCount.containsKey(oliDirSHStrOld)) {
			                            mapDecreaseAppearanceCount.put(oliDirSHStrOld, new list<order_line_items__c>());
			                        }
			                        mapDecreaseAppearanceCount.get(oliDirSHStrOld).add(iterator);
			                        
			                        if(!mapIncreaseAppearanceCount.containsKey(strDHDSID)) {
				                        mapIncreaseAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
				                    }
				                    mapIncreaseAppearanceCount.get(strDHDSID).add(iterator);
				                    
				                    if(iterator.Directory_Edition_Book_Status__c == CommonMessages.bots) {
				                    	if(!mapDecreasePaginationAppearanceCount.containsKey(oliDirSHStrOld)) {
				                            mapDecreasePaginationAppearanceCount.put(oliDirSHStrOld, new list<order_line_items__c>());
				                        }
				                        mapDecreasePaginationAppearanceCount.get(oliDirSHStrOld).add(iterator);
				                        
				                        if(!mapIncreasePaginationAppearanceCount.containsKey(strDHDSID)) {
					                        mapIncreasePaginationAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
					                    }
					                    mapIncreasePaginationAppearanceCount.get(strDHDSID).add(iterator);
					                    
					                    if(iterator.Product_Is_IBUN_Bundle_Product__c) {
					                    	if(!mapDecreasePaginationIBUNAppearanceCount.containsKey(oliDirSHStrOld)) {
					                            mapDecreasePaginationIBUNAppearanceCount.put(oliDirSHStrOld, new list<order_line_items__c>());
					                        }
					                        mapDecreasePaginationIBUNAppearanceCount.get(oliDirSHStrOld).add(iterator);
					                        
					                        if(!mapIncreasePaginationIBUNAppearanceCount.containsKey(strDHDSID)) {
						                        mapIncreasePaginationIBUNAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
						                    }
						                    mapIncreasePaginationIBUNAppearanceCount.get(strDHDSID).add(iterator);		                        
					                    }
				                    }
		                        }
		                    }
	                    }
	        		}
	        		else {
	        			setComboValue.add(strDHDSID);
	        			if(!mapIncreaseInternetBundle.containsKey(strDHDSID)) {
	                        mapIncreaseInternetBundle.put(strDHDSID, new list<order_line_items__c>());
	                    }
	                    mapIncreaseInternetBundle.get(strDHDSID).add(iterator);
	                    
	                    if(iterator.Product_Inventory_Tracking_Group__c == CommonMessages.ypLeaderAd){
		                    if(!mapIncreaseLeaderAdsSold.containsKey(strDHDSID)) {
		                        mapIncreaseLeaderAdsSold.put(strDHDSID, new list<order_line_items__c>());
		                    }
		                    mapIncreaseLeaderAdsSold.get(strDHDSID).add(iterator);
	                    }
	                    
	                    if(iterator.Section_Page_Type__c == CommonMessages.ypSecPageType) {
		                    if(!mapIncreaseAppearanceCount.containsKey(strDHDSID)) {
		                        mapIncreaseAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
		                    }
		                    mapIncreaseAppearanceCount.get(strDHDSID).add(iterator);
		                    
		                    if(iterator.Directory_Edition_Book_Status__c == CommonMessages.bots) {
			                    if(!mapIncreasePaginationAppearanceCount.containsKey(strDHDSID)) {
			                        mapIncreasePaginationAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
			                    }
			                    mapIncreasePaginationAppearanceCount.get(strDHDSID).add(iterator);
			                    
			                    if(iterator.Product_Is_IBUN_Bundle_Product__c) {
				                    if(!mapIncreasePaginationIBUNAppearanceCount.containsKey(strDHDSID)) {
				                        mapIncreasePaginationIBUNAppearanceCount.put(strDHDSID, new list<order_line_items__c>());
				                    }
				                    mapIncreasePaginationIBUNAppearanceCount.get(strDHDSID).add(iterator);
			                    }
		                    }
	                    }
	        		}
	        	}
	        	
	        	if(iterator.Product_Inventory_Tracking_Group__c != null && iterator.Directory__c != null && iterator.Product2__c != null) {
	        		String strDIRPROD2 = iterator.Directory__c +''+ iterator.Product2__c;
	        		if(oldMap != null) {
	        			
	        		}
	        		else {
	        			setDIRPROD2.add(strDIRPROD2);
	        			if(!mapInventoryProdOLI.containsKey(strDIRPROD2)) {
	        				mapInventoryProdOLI.put(strDIRPROD2, new list<Order_Line_Items__c>());
	        			}
	        			mapInventoryProdOLI.get(strDIRPROD2).add(iterator);
	        		}
	        	}
        	}
        }
        
        if(setComboValue.size() > 0) {
        	list<Section_Heading_Mapping__c> lstSHM = SectionHeadingMappingSOQLMethods.getSecHeadMapByDSDHCombo(setComboValue);
        	if(lstSHM.size() > 0) {
        		for(Section_Heading_Mapping__c objSHM : lstSHM) {
        			list<Order_Line_Items__c> lstOLIIncDec = mapIncreaseInternetBundle.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			integer iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Internet_Bundle_Appearances__c = (objSHM.Internet_Bundle_Appearances__c != null ? objSHM.Internet_Bundle_Appearances__c + iCount : iCount);
        			}
        			
        			lstOLIIncDec = mapDecreaseInternetBundle.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Internet_Bundle_Appearances__c = (objSHM.Internet_Bundle_Appearances__c != null ? objSHM.Internet_Bundle_Appearances__c - iCount : 0);
        				if(objSHM.Internet_Bundle_Appearances__c < 0) {
        					objSHM.Internet_Bundle_Appearances__c = 0;
        				}
        			}
        			
        			//Inc / Dec Leader Ad Sold
        			lstOLIIncDec = mapIncreaseLeaderAdsSold.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Leader_Ads_Sold__c = (objSHM.Leader_Ads_Sold__c != null ? objSHM.Leader_Ads_Sold__c + iCount : iCount);
        			}
        			
        			lstOLIIncDec = mapDecreaseLeaderAdsSold.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Leader_Ads_Sold__c = (objSHM.Leader_Ads_Sold__c != null ? objSHM.Leader_Ads_Sold__c - iCount : 0);
        				if(objSHM.Leader_Ads_Sold__c < 0) {
        					objSHM.Leader_Ads_Sold__c = 0;
        				}
        			}
        			
        			//Inc / Dec Appearance Count
        			lstOLIIncDec = mapIncreaseAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Appearance_Count__c = (objSHM.Appearance_Count__c != null ? objSHM.Appearance_Count__c + iCount : iCount);
        			}
        			
        			lstOLIIncDec = mapDecreaseAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Appearance_Count__c = (objSHM.Appearance_Count__c != null ? objSHM.Appearance_Count__c - iCount : 0);
        				if(objSHM.Appearance_Count__c < 0) {
        					objSHM.Appearance_Count__c = 0;
        				}
        			}
        			
        			//Inc / Dec Pagination Appearance Count
        			lstOLIIncDec = mapIncreasePaginationAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Pagination_Appearance_Count__c = (objSHM.Pagination_Appearance_Count__c != null ? objSHM.Pagination_Appearance_Count__c + iCount : iCount);
        			}
        			
        			lstOLIIncDec = mapDecreasePaginationAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Pagination_Appearance_Count__c = (objSHM.Pagination_Appearance_Count__c != null ? objSHM.Pagination_Appearance_Count__c - iCount : 0);
        				if(objSHM.Pagination_Appearance_Count__c < 0) {
        					objSHM.Pagination_Appearance_Count__c = 0;
        				}
        			}
        			
        			//Inc / Dec Pagination IBUN Appearance Count
        			lstOLIIncDec = mapIncreasePaginationIBUNAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Pagination_IBUN_Appearance_Count__c = (objSHM.Pagination_IBUN_Appearance_Count__c != null ? objSHM.Pagination_IBUN_Appearance_Count__c + iCount : iCount);
        			}
        			
        			lstOLIIncDec = mapDecreasePaginationIBUNAppearanceCount.get(objSHM.SHM_strDirectorySecAndHeading__c);
        			iCount = (lstOLIIncDec != null ? lstOLIIncDec.size() : 0);
        			if(iCount > 0) {
        				objSHM.Pagination_IBUN_Appearance_Count__c = (objSHM.Pagination_IBUN_Appearance_Count__c != null ? objSHM.Pagination_IBUN_Appearance_Count__c - iCount : 0);
        				if(objSHM.Pagination_IBUN_Appearance_Count__c < 0) {
        					objSHM.Pagination_IBUN_Appearance_Count__c = 0;
        				}
        			}
        		}
        	}
        	
        	update lstSHM;
        }
        
        if(setDIRPROD2.size() > 0) {
        	list<Product_Inventory__c> prodInventoryInsertList = new list<Product_Inventory__c>();
        	list<Directory_Product_Mapping__c> dirProdMapList = DirectoryProductMappingSOQLMethods.getDirProdMapByComboValue(setDIRPROD2);
        	for(Directory_Product_Mapping__c iteratorDPM : dirProdMapList) {
        		list<Order_line_items__c> lstINVPRODOLI = mapInventoryProdOLI.get(iteratorDPM.DPM_Dir_Prod_External_ID__c);
        		if(lstINVPRODOLI != null) {
        			for(Order_line_items__c iteratorOLI : lstINVPRODOLI) {
        				prodInventoryInsertList.add(createProdInventory(iteratorOLI, iteratorDPM));
        			}
        		}
        	}
        }
	}
	
	public static Product_Inventory__c createProdInventory(Order_Line_Items__c objOLI, Directory_Product_Mapping__c objDPM){
        Product_Inventory__c prodInventory = new Product_Inventory__c();
        prodInventory.Active__c = true;
        prodInventory.Order_Line_Item__c = objOLI.Id;
        prodInventory.Product2__c = objOLI.Product2__c;
        prodInventory.Directory_Section__c = objOLI.Directory_Section__c;
        prodInventory.Directory_Product_Mapping__c = objDPM.Id;
        return prodInventory;
    }
	
    public static void IncreaseDecreaseIBAppearancesNew(list<Order_Line_Items__c> lstOLI, map<Id, Order_Line_Items__c> oldMap) {
        if(CommonVariables.InternetBundleLogic) {
           
            map<String,list<Order_line_items__c>> mapIncreaseStrOli = new map<String,list<Order_line_items__c>>();
            map<String,list<Order_line_items__c>> mapDecreaseStrOli = new map<String,list<Order_line_items__c>>();
            
            //set<String> setComboValue = new set<String>();
            set<Id> setDirectorySectionId = new set<Id>();
            set<Id> setDirectoryHeadingId = new set<Id>();
            system.debug('********OLI List size*********'+lstOLI.size());
            for(Order_line_items__c oli : lstOLI) {
                if(OLI.Media_Type__c != null && OLI.Media_Type__c.equals(CommonMessages.oliPrintProductType)) { 
                    if(oli.Product_Is_IBUN_Bundle_Product__c == true) {
                        if(oli.Directory_section__c != null && oli.Directory_Heading__c != null) {
                            boolean bFlag = true;
                            if(oldMap != null && oli.isCanceled__c == false) {
                                bFlag = false;
                                Order_line_items__c objOldOLI = oldMap.get(oli.Id);
                                if(objOldOLI.Directory_section__c != oli.Directory_section__c || oli.Directory_Heading__c != objOldOLI.Directory_Heading__c) {
                                    bFlag = true;
                                    String oliDirSHStrOld = String.valueOf(objOldOLI.Directory_section__c).substring(0, 15) + String.valueOf(objOldOLI.Directory_Heading__c).substring(0, 15);
                                    //setDecreaseAppearances.add(oliDirSHStrOld);
                                    if(!mapDecreaseStrOli.containsKey(oliDirSHStrOld)){
                                        mapDecreaseStrOli.put(oliDirSHStrOld, new list<order_line_items__c>());
                                    }
                                    mapDecreaseStrOli.get(oliDirSHStrOld).add(oli);
                                    //setComboValue.add(oliDirSHStrOld);
                                    setDirectorySectionId.add(objOldOLI.Directory_section__c);
                                    setDirectoryHeadingId.add(objOldOLI.Directory_Heading__c);
                                }
                            }
                            if(bFlag) {
                                string oliDirSHStr = String.valueOf(oli.Directory_section__c).substring(0, 15) + String.valueOf(oli.Directory_Heading__c).substring(0, 15);                     
                                system.debug('********String combination********'+oliDirSHStr);
                                //setComboValue.add(oliDirSHStr);
                                setDirectorySectionId.add(oli.Directory_section__c);
                                setDirectoryHeadingId.add(oli.Directory_Heading__c);
                                if(oli.isCanceled__c) {
                                    //setDecreaseAppearances.add(oliDirSHStr);
                                    if(!mapDecreaseStrOli.containsKey(oliDirSHStr)){
                                        mapDecreaseStrOli.put(oliDirSHStr, new list<order_line_items__c>());
                                    }
                                    mapDecreaseStrOli.get(oliDirSHStr).add(oli);
                                }
                                else {
                                    //setIncreaseAppearances.add(oliDirSHStr);
                                    if(!mapIncreaseStrOli.containsKey(oliDirSHStr)){
                                        mapIncreaseStrOli.put(oliDirSHStr, new list<order_line_items__c>());
                                    }
                                    mapIncreaseStrOli.get(oliDirSHStr).add(oli);
                                }
                            }
                        }
                    }
                }
            }
            system.debug('********Increase value map**********'+mapIncreaseStrOli.size());
            system.debug('********decrease value map**********'+mapDecreaseStrOli.size());
            map<string,Section_Heading_Mapping__c> mapSHM = new map<string,Section_Heading_Mapping__c>();
            if(setDirectorySectionId.size() > 0 && setDirectoryHeadingId.size() > 0 ) {
                for(Section_Heading_Mapping__c objSHM : SectionHeadingMappingSOQLMethods.getSecHeadMapByDirSecDirHeadIds(setDirectorySectionId,setDirectoryHeadingId)) {
                //[Select Id,Name,Directory_section__c,Directory_Heading__c,Internet_Bundle_Appearances__c,strBundleAppearenceCombo__c from Section_Heading_Mapping__c where strBundleAppearenceCombo__c IN :setComboValue]){
                    system.debug('********String combination SHM********'+objSHM.strBundleAppearenceCombo__c);
                    if(!mapSHM.containsKey(objSHM.strBundleAppearenceCombo__c)) {
                        mapSHM.put(objSHM.strBundleAppearenceCombo__c,objSHM);
                    }
                }
                
                if(mapSHM.size() > 0) {
                    list<Section_Heading_Mapping__c> objSHMUpdateLst = new list<Section_Heading_Mapping__c>();
                    for(string objStr : mapSHM.keyset()) {
                        if(mapIncreaseStrOli.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                            for(integer i=1;i<=mapIncreaseStrOli.get(objStr).size();i++){
                            system.debug('********Increase value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmInc = mapSHM.get(objStr);
                                if(objSecHmInc.Internet_Bundle_Appearances__c == null || objSecHmInc.Internet_Bundle_Appearances__c == 0){
                                    objSecHmInc.Internet_Bundle_Appearances__c = 1;
                                }
                                else {
                                    objSecHmInc.Internet_Bundle_Appearances__c = objSecHmInc.Internet_Bundle_Appearances__c + 1;
                                }
                                //objSHMUpdateLst.add(objSecHmInc);
                            }
                            objSHMUpdateLst.add(objSecHmInc);
                        }
                        else if(mapDecreaseStrOli.get(objStr) != null) {
                            Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                            for(integer i=1;i<=mapDecreaseStrOli.get(objStr).size();i++){
                            system.debug('********decrease value map count**********'+i);
                                //Section_Heading_Mapping__c objSecHmDec = mapSHM.get(objStr);
                                if(objSecHmDec.Internet_Bundle_Appearances__c == 0 || objSecHmDec.Internet_Bundle_Appearances__c == null ){
                                    objSecHmDec.Internet_Bundle_Appearances__c = 0;
                                }
                                else {
                                    objSecHmDec.Internet_Bundle_Appearances__c = objSecHmDec.Internet_Bundle_Appearances__c - 1;
                                }
                                //objSHMUpdateLst.add(objSecHmDec);
                            }
                            objSHMUpdateLst.add(objSecHmDec);
                        }
                    }
                    system.debug('********SHM records list**********'+objSHMUpdateLst);
                    if(objSHMUpdateLst.size() > 0) {
                        update objSHMUpdateLst;
                        CommonVariables.InternetBundleLogic = false;
                    }
                }
            }
        }
    }
}