public with sharing class LocalBillingCreditCardController {
    public Dummy_Object__c objDummy {get;set;}
    public String strNoofAccountAndInvoice {get;set;}
    public boolean bReconciled {get;set;}
    public boolean bPostAllInvoice {get;set;}
    public String paymentType{get;set;}
    public String strBatchStatus {get;set;}
    public String strSCNBatchStatus {get;set;}
    public string strNoofAccountAndSCN{get;set;}
    public boolean bEnabledPoller {get;set;}
    public boolean bSCNEnabledPoller {get;set;}
    public boolean bShowReconciliation{get;set;}
    public boolean bShowPostFF {get;set;}
    public String strSIReconcileBatchStatus {get;set;}
    public String strSCNReconcileBatchStatus {get;set;}
    public boolean bReconciliationEnabledPoller {get;set;}
    
    //Report Ids Custom Settings 
    public Id LOBMatchingReportId {get;set;}
    public Id goLiveReportId {get;set;}
    public Id UnReconciledSLIOLIReportID {get;set;}
    public Id EverythingGoneLivewithInvoice {get;set;}
    public Id rptPostingInvoice {get;set;}
    public Id CCSubsequentReconcileReportId{get;set;}
    public Id rptPostingSCN {get;set;}
    @testvisible Id SIReconciliationbatchId;
    @testvisible Id SCNReconciliationbatchId;
    @testvisible Id batchId;
    @testvisible Id SCNbatchId;
    set<Id> setSIId;
    set<Id> setSCNId;
    
    public LocalBillingCreditCardController() {
        objDummy = new Dummy_Object__c();
        clearDatas();
        Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();
        if(reportIds.get('Go Live')!=null) goLiveReportId = reportIds.get('Go Live').Report_Id__c;
        if(reportIds.get('EverythingGoneLivewithInvoice')!=null) EverythingGoneLivewithInvoice=reportIds.get('EverythingGoneLivewithInvoice').Report_Id__c;
        if(reportIds.get('LOB Matching Report')!=null) LOBMatchingReportId= reportIds.get('LOB Matching Report').Report_Id__c;
        if(reportIds.get('UnReconciled Sales Invoice OLI')!=null) UnReconciledSLIOLIReportID=reportIds.get('UnReconciled Sales Invoice OLI').Report_Id__c;
        if(reportIds.get('CC SI Report Posting')!=null) rptPostingInvoice = reportIds.get('CC SI Report Posting').Report_Id__c;
        if(reportIds.get('CC SCN Report Posting')!=null) rptPostingSCN = reportIds.get('CC SCN Report Posting').Report_Id__c;
        if(reportIds.get('CC Subsequent Reconcile Report')!=null) CCSubsequentReconcileReportId = reportIds.get('CC Subsequent Reconcile Report').Report_Id__c;
    }
    
    private void clearDatas() {
        strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        setSIId = new set<Id>();
        setSCNId =new set<Id>();
        strBatchStatus = '';
        strSCNBatchStatus = '';
        bPostAllInvoice = false;
        bEnabledPoller = false;
        bSCNEnabledPoller =false;
        bShowPostFF=false;
        bShowReconciliation=false;
        strSIReconcileBatchStatus= '';
        strSCNReconcileBatchStatus= '';
        bReconciliationEnabledPoller =false;
    }
    
    public void clickGoNew() {
        if(objDummy.From_Date__c != null && objDummy.To_Date__c != null) {
            clearDatas();
            setSIId = new set<Id>();
            set<Id> setAccountID = new set<Id>();
            set<Id> setSCNAccountID = new set<Id>();
            list<c2g__codaInvoice__c> lstSalesInvoice =[Select Customer_Name__c,ID,Name From c2g__codaInvoice__c where c2g__InvoiceStatus__c = 'In Progress' AND SI_Payment_Method__c = :paymentType 
                                  AND c2g__InvoiceDate__c <=:objDummy.To_Date__c and c2g__InvoiceDate__c >=:objDummy.From_Date__c];
                                      
            list<c2g__codaCreditNote__c> lstCreditNote = [Select  Id, Name, c2g__Account__c From c2g__codaCreditNote__c 
                                  where c2g__CreditNoteStatus__c='In Progress' AND Transaction_Type__c IN ('FC - Frequency Change','TD - Billing Transfer Invoice','BB - Bulk Billing' ) 
                                  AND SC_Payment_Method__c = :paymentType AND c2g__CreditNoteDate__c <=:objDummy.To_Date__c and c2g__CreditNoteDate__c >=:objDummy.From_Date__c];
            if(lstSalesInvoice .size()>0) {
                for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
                    setAccountID.add(iterator.Customer_Name__c);
                    setSIId.add(iterator.id);                    
                }
                strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
                bShowReconciliation=true;
            }
                
            if(lstCreditNote .size()>0) {
                for(c2g__codaCreditNote__c iterator : lstCreditNote) { 
                    setSCNAccountID.add(iterator.c2g__Account__c);
                    setSCNId.add(iterator.id);
                }
                strNoofAccountAndSCN = concatenateSCNString(setSCNAccountID.size(), setSCNId.size());
                bShowReconciliation=true;
            }
        }
    }

    public void clickGo() {
        if(objDummy.From_Date__c != null && objDummy.To_Date__c != null) {
            list<c2g__codaInvoiceLineItem__c> lstSalesInvoiceLineItem =new List<c2g__codaInvoiceLineItem__c>();
            setSIId = new set<Id>();
            set<Id> setAccountID = new set<Id>();
            lstSalesInvoiceLineItem =[Select c2g__Invoice__r.Customer_Name__c,ID,Name,c2g__Invoice__c,
                                      (Select c2g__CreditNote__c, Id, Name From Sales_Credit_Note_Line_Items__r 
                                      where c2g__CreditNote__r.c2g__CreditNoteStatus__c='In Progress' AND c2g__CreditNote__r.Transaction_Type__c IN ('FC - Frequency Change','TD - Billing Transfer Invoice','BB - Bulk Billing' )) 
                                      From c2g__codaInvoiceLineItem__c where c2g__Invoice__r.c2g__InvoiceStatus__c = 'In Progress' AND Order_Line_Item__r.Payment_Method__c = :paymentType 
                                      AND c2g__Invoice__r.c2g__InvoiceDate__c <=:objDummy.To_Date__c and c2g__Invoice__r.c2g__InvoiceDate__c >=:objDummy.From_Date__c];
            if(lstSalesInvoiceLineItem .size()>0) {
                for(c2g__codaInvoiceLineItem__c iterator : lstSalesInvoiceLineItem) {
                    setAccountID.add(iterator.c2g__Invoice__r.Customer_Name__c);
                    setSIId.add(iterator.c2g__Invoice__c);
                    if(iterator.Sales_Credit_Note_Line_Items__r.size()>0) {
                        for(c2g__codaCreditNoteLineItem__c SCNLIIterator : iterator.Sales_Credit_Note_Line_Items__r) {
                            setSCNId.add(SCNLIIterator.c2g__CreditNote__c);
                        }
                    }
                }
                strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
                bShowReconciliation=true;
            }
            else
                strNoofAccountAndInvoice = concatenateString(0, 0);
        }
    }

    public void updateSIwithReconciledCheckBox() {
        if(setSIId.size() > 0 && bReconciled) {
                if(!Test.isRunningTest()){ 
                    SIReconciliationbatchId=Database.executeBatch(new LocalBillingSIReconciliationBatch(setSIId));
                    strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';
                }
        }
        if(setSCNId.size()>0 && bReconciled){
                if(!Test.isRunningTest()){ 
                    SCNReconciliationbatchId=Database.executeBatch(new LocalBillingSCNReconciliationBatch(setSCNId));
                    strSCNReconcileBatchStatus='Status of Reconciliation Credit Note Batch : Queued';
                }    
        }
          bReconciliationEnabledPoller =true;
    } 
    
    public void apexReconciliationJobStatus() {
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(SIReconciliationbatchId != null) {
            setBatchIds.add(SIReconciliationbatchId);
        }
        if(SCNReconciliationbatchId != null) {
            setBatchIds.add(SCNReconciliationbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(SIReconciliationbatchId)) {
             strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : ' + mapBatchFFJob.get(SIReconciliationbatchId).Status;
                if(mapBatchFFJob.get(SIReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
            }
            else {
                bStatus = true;
            }
            if(mapBatchFFJob.containsKey(SCNReconciliationbatchId)) {
                strSCNReconcileBatchStatus= 'Status of Reconciliation Credit Note Batch : ' + mapBatchFFJob.get(SCNReconciliationbatchId).Status;
                if(bStatus && mapBatchFFJob.get(SCNReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
                else {
                    bStatus = false;
                }
            }
            if(bStatus) {
                bReconciliationEnabledPoller = false;
                bShowPostFF=true;
            }
        }
    }
    public void postFFInvoice() {
        if(bPostAllInvoice) {
            if(setSIId.size() > 0) {
                try{
                    if(!Test.isRunningTest()) 
                    batchId = Database.executeBatch(new FFSalesInvoicePostBatchController(setSIId), Integer.valueOf(system.label.LocalBillingPostingBatchSize));
                    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Invoices are queued for the process.');
                    //ApexPages.addMessage(myMsg);
                    strBatchStatus = 'Status of Financial Force Invoice Batch : Queued';
                    bEnabledPoller = true;
                }
                catch(Exception ex){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occured while posting Invoice. Error : '+ex);
                    ApexPages.addMessage(myMsg);
                }
            }
            if(setSCNId.size()>0){
                try{
                    if(!Test.isRunningTest()) 
                    SCNbatchId = Database.executeBatch(new FFCreditNotePostBatchController(setSCNId), Integer.valueOf(system.label.LocalBillingPostingBatchSize));
                    //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm,'Sales Credit Notes are queued for the process.');
                    //ApexPages.addMessage(myMsg);
                    strSCNBatchStatus = 'Status of Financial Force SCN Batch : Queued';
                    bSCNEnabledPoller = true;
                }
                catch (Exception ex){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Error Occurred while posting Sales Credit Notes. Error : '+ex);
                    ApexPages.addMessage(myMsg);
                }
            }
        }
    }
    
    public void apexJobStatus() {        
        Set<Id> setBatchIds = new Set<id>();
        if(batchId != null) {
            setBatchIds.add(batchId);
        }
        if(SCNbatchId != null) {
            setBatchIds.add(SCNbatchId);
        } 
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.get(batchId) != null) {
                strBatchStatus = 'Status of Financial Force Invoice Batch : ' + mapBatchFFJob.get(batchId).Status;
                if(mapBatchFFJob.get(batchId).Status == 'Completed') {
                    bEnabledPoller = false;
                }
            }
            if(mapBatchFFJob.get(SCNbatchId) != null) { 
                strSCNBatchStatus = 'Status of Financial Force Credit Note Batch : ' + mapBatchFFJob.get(SCNbatchId).Status;
                if(mapBatchFFJob.get(SCNbatchId).Status == 'Completed') {
                    bEnabledPoller = false;
                }
            }
        }
    }

    public List<SelectOption> getPaymentMethods() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Credit Card','Credit Card'));
        options.add(new SelectOption('ACH','ACH'));
        return options;
    }

    private String concatenateString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
    }
    
    private String concatenateSCNString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Credit Notes for Selections';
    }
}