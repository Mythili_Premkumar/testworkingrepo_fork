@isTest(SeeAllData = true)
public class CaseHandlerControllerTestclass{
    static testmethod void caseTest(){
        List<Case> lstCase = new List<Case>();
        Test.startTest();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Case newCase1 = TestMethodsUtility.generateCase('National Claim');
        newCase1.AccountId = newAccount.Id;
        newCase1.Subject = 'Test1--Test2';
        newCase1.PAN1__c =  '4';
        lstCase.add(newCase1);
        insert lstCase;
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet =  TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Digital_Product_Requirement__c newDFF = TestMethodsUtility.generateDataFulfillmentForm('Print Graphic');
        newDFF.Talus_DFF_Id__c = 'Test2';
        insert newDFF;
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AccountTestJSON');
        mock.setStatusCode(200);
        mock.setHeader('Content-Type', 'application/json');
        Test.setMock(HttpCalloutMock.class, mock);   
        //caseHandlerController.createTaskwithDFFOwner(lstCase);
        Test.stopTest();
    }
    
    static testmethod void caseTestUpdate(){
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Case newCase1 = TestMethodsUtility.generateCase('CS Claim');
        newCase1.AccountId = newAccount.Id;
        newCase1.Subject = 'Test1--Test2';
        newCase1.PAN1__c =  '4';
        insert newCase1;
        system.assertNotEquals(newCase1, null);
        newCase1.Status = 'Closed';
        update newCase1;
    }
}