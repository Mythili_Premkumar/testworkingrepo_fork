@IsTest 
    public with sharing class createTestControllerTest1 {
        
         
        
        public static testmethod void CreateOrderTestControllerTest03(){
            list<Account> lstAccount = new list<Account>();
            lstAccount.add(TestMethodsUtility.generateAccount('telco'));
            lstAccount.add(TestMethodsUtility.generateAccount('customer'));
            lstAccount.add(TestMethodsUtility.generateAccount('publication'));
            insert lstAccount;  
            Account newAccount = new Account();
            Account newPubAccount = new Account();
            Account newTelcoAccount = new Account();
            for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
            newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
            newPubAccount = iterator;
            }
            else {
            newTelcoAccount = iterator;
            }
            }
            system.assertNotEquals(newAccount.ID, null);
            system.assertNotEquals(newPubAccount.ID, null);
            system.assertNotEquals(newAccount.Primary_Canvass__c, null);
            system.assertNotEquals(newTelcoAccount.ID, null);
            Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
            objTelco.Telco_Code__c = 'Test';
            update objTelco;
            system.assertNotEquals(newTelcoAccount.ID, null);
            Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
            Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
            Division__c objDiv = TestMethodsUtility.createDivision();
            Directory__c objDir = TestMethodsUtility.generateDirectory();
            objDir.Telco_Provider__c = objTelco.Id;
            objDir.Canvass__c = newAccount.Primary_Canvass__c;        
            objDir.Publication_Company__c = newPubAccount.Id;
            objDir.Division__c = objDiv.Id;
            insert objDir;
            
            Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
            Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
            Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
            insert objSHM;
            
            Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
            insert objDirEd;
            
            //Directory_Edition__c objDirEd1 = TestMethodsUtility.generateDirectoryEdition(objDir);
            //insert objDirEd1;
            
            Directory_Mapping__c objDM = TestMethodsUtility.generateDirectoryMapping(null);
            objDM.Telco__c = objDir.Telco_Provider__c;
            objDM.Canvass__c = objDir.Canvass__c;
            objDM.Directory__c = objDir.Id;
            insert objDM;
            
            system.assertNotEquals(objDir.ID, null);
            list<Product2> lstProduct = new list<Product2>();      
            for(Integer x=0; x<3;x++){
            Product2 newProduct = TestMethodsUtility.generateproduct();
            newProduct.Product_Type__c = 'SEO';
            newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
            lstProduct.add(newProduct);
            }
            insert lstProduct;
            List<pricebookentry> lstinsertPBE = new List<pricebookentry>();
            //pricebook2 objPB = TestMethodsUtility.createpricebook();
            list<Directory_Product_Mapping__c> lstDPM = new list<Directory_Product_Mapping__c>();        
            for(Product2 iterator : lstProduct) {
            
            PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = iterator.ID, Pricebook2Id = newPriceBook.id, IsActive = iterator.IsActive);
            lstinsertPBE.add(pbe);
            Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(objDir);
            objDPM.Product2__c = iterator.Id;
            }
            insert lstinsertPBE;
            insert lstDPM;
            list<PricebookEntry> lstPBE = [Select Id, Product2Id from PricebookEntry where Product2Id IN:lstProduct];
            
            list<Opportunity> lstOpp = new list <Opportunity> ();
            Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
            newOpportunity.AccountId = newAccount.Id;
            newOpportunity.Pricebook2Id = newPriceBook.Id;
            newOpportunity.Billing_Contact__c = newContact.Id;
            
           newOpportunity.modifyid__c = null;
            newOpportunity.Billing_Partner__c = objTelco.Telco_Code__c;
            newOpportunity.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
            newOpportunity.CORE_Migration_ID__c  = '12345';
            newOpportunity.isLocked__c=false;
            newOpportunity.stagename='Closed Won';
            newOpportunity.NotProcessed__c = false;
                    //newOpportunity.isclosed=true;
            //newOpportunity.iswon=true;
            //insert newOpportunity;
            lstOpp.add(newOpportunity);
            
             Opportunity newOpportunity1 = TestMethodsUtility.generateOpportunity('new');
            newOpportunity1.AccountId = newAccount.Id;
            newOpportunity1.Pricebook2Id = newPriceBook.Id;
            newOpportunity1.Billing_Contact__c = newContact.Id;
            
            newOpportunity1.modifyid__c = '12344';
            
            
            //newOpportunity1.Billing_Partner__c = objTelco.Telco_Code__c;
            newOpportunity1.Payment_Method__c = CommonMessages.telcoPaymentMethod;        
            newOpportunity1.CORE_Migration_ID__c  = '12335';
            newOpportunity1.isLocked__c=false;
            newOpportunity1.stagename='Closed Won';
            //newOpportunity1.NotProcessed__c = false;
            
            
           
                   
            //lstOpp.add(newOpportunity1); 
            
            system.debug('----test----'+lstOPP);
            insert lstOpp; 
            
            
            
           ID pricebookId;
            
           // list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
            map<Id, PricebookEntry> mapPBE = new map<Id, PricebookEntry>();
            for(PricebookEntry iterator : lstPBE) {
                pricebookId=iterator.Id;/*
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = iterator.Id;
            
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '123456';
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            objOLI.DoNotConvert__c=false;
           // lstOLI.add(objOLI);*/
            mapPBE.put(iterator.Id, iterator);
            }
            integer coremigrationID=1001;
           
            OpportunityLineItem[] lstOLI = new List<OpportunityLineItem>();
           for (Integer i=0;i<50;i++) {
            coremigrationID++;
            OpportunityLineItem objOLI = TestMethodsUtility.generateOpportunityLineItem();
            objOLI.PricebookEntryId = pricebookId;
            objOLI.Billing_Duration__c = 12;
            objOLI.Directory__c = objDir.Id;
            objOLI.Directory_Edition__c = objDirEd.Id;
            objOLI.Full_Rate__c = 30.00;
            objOLI.UnitPrice = 30.00;
            objOLI.Package_ID__c = '124456';
            objOLI.Billing_Partner__c = objOLI.Id;
            objOLI.OpportunityId = newOpportunity.Id;
            objOLI.Directory_Heading__c = objDH.Id;
            objOLI.Directory_Section__c = objDS.Id;
            objOLI.DoNotConvert__c=false;  
            
            objOLI.Original_Line_Item_ID__c=null;
            objOLI.Renewals_Action__c='Cancel';
            objOLI.CORE_Migration_ID__c=string.valueOF(coremigrationID); 
            system.debug('------core --'+objOLI.CORE_Migration_ID__c) ;  
                   
                   
               lstOLI.add(objOLI);
              /* coremigrationID++;
               
               OpportunityLineItem objOLI1 = TestMethodsUtility.generateOpportunityLineItem();
            objOLI1.PricebookEntryId = pricebookId;
            objOLI1.Billing_Duration__c = 12;
            objOLI1.Directory__c = objDir.Id;
            objOLI1.Directory_Edition__c = objDirEd.Id;
            objOLI1.Full_Rate__c = 30.00;
            objOLI1.UnitPrice = 30.00;
            objOLI1.Package_ID__c = '122456';
            objOLI1.Billing_Partner__c = objOLI.Id;
            objOLI1.OpportunityId = newOpportunity1.Id;
            objOLI1.Directory_Heading__c = objDH.Id;
            objOLI1.Directory_Section__c = objDS.Id;
            objOLI1.DoNotConvert__c=false;
            
            objOLI1.Original_Line_Item_ID__c=null;
            objOLI1.Renewals_Action__c='Cancel';
           objOLI1.CORE_Migration_ID__c=string.valueOF(coremigrationID);           
               system.debug('----------migrationID---'+objOLI.CORE_Migration_ID__c);
              lstOLI.add(objOLI1); */
               
               
           }
           
            
            
            
            
            
            insert lstOLI;
           
            Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
            Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
            Test.startTest();
            Order_Line_Items__c newOrLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
            newOrLI.Parent_ID__c='pkg1';
            newOrLI.Parent_ID_of_Addon__c='addpkg1';
            newOrLI.Product2__c =lstProduct[0].id;
            newOrLI.Media_Type__c = 'Digital';
            list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
            lstOrderLI.add(newOrLI);
            if(lstOrderLI.size()>0)
            insert lstOrderLI;
            
            System.debug('Testingg lstOLI '+lstOLI.size());
            
            PageReference pageRef = Page.CreateOrderforMigratedOpp;
            Test.setCurrentPage(pageRef);
            
            ApexPages.StandardController oppCtrl = new ApexPages.standardController(newOpportunity);
            
            CreateOrderTestController objController = new CreateOrderTestController(oppCtrl);
            objController.intBatchCount=1;
            objController.intToProcess=1;
            objController.intRecordCount=1;
            //objController.onLoad();
            //Test.startTest();
           objController.closeOppsandCreateNIOrderBatch2();
            
            objController.closeOppsandCreateNIOrderBatch3();
            
           objController.closeOppsandCreateNIOrderBatch4();
            
            objController.closeOppsandCreateNIOrderBatch5();
          
            
            Test.stopTest();  
            
                   
            
                     
                                                             
                                                             
              
        }
           
    
    }