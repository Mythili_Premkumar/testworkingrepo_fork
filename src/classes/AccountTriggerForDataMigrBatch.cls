global class AccountTriggerForDataMigrBatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String MigrationUserID = '\''+System.Label.MigrationUserID+'\'';
        String SOQL = 'SELECT Id, RecordTypeId, Name, Account_Number__c, Primary_Canvass__c, Phone, Billingcity, BillingStreet, BillingState,';
        SOQL += 'BillingPostalCode, Website, ParentId, Delinquency_Indicator__c, Bankruptcy__c, Collection_Status__c ';
        SOQL += 'FROM Account WHERE CreatedById =  '+ MigrationUserID;
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> listAccount) {
        //map<String, Id> RecordTypeIdName = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.accountObjectName);
        Map<Id, Account> mapAcct = new Map<Id, Account>();
        set<Id> acctIds = new set<Id>();
        
        for(Account acct : listAccount) {
            mapAcct.put(acct.Id, acct);
            acctIds.add(acct.Id);
        }
        
        AccountTriggerHandlerController.createFulfillmentProfile(acctIds, mapAcct);
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }
}