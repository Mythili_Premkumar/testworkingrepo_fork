public with sharing class NewSalesInvoiceLineItemController {
	public c2g__codaInvoiceLineItem__c newSILI {get;set;}
	
	public NewSalesInvoiceLineItemController(ApexPages.StandardController controller) {
		newSILI = new c2g__codaInvoiceLineItem__c();
		Id SIId = ApexPages.currentPage().getParameters().get('retURL').replace('/','').substring(0,15);
		if(SIId != null) {
			newSILI.c2g__Invoice__c = SIId;
		}
    }
    
    public PageReference insertSILI() {
    	try {
    		newSILI.Is_Manually_Created__c = true;
    		insert newSILI;
    		PageReference pg = new PageReference('/' + newSILI.Id);
    		pg.setRedirect(true);
    		return pg;
    	} catch(Exception e) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
    		return null;
    	}
    }
}