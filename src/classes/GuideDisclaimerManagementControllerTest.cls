@isTest
public with sharing class GuideDisclaimerManagementControllerTest {
	static testMethod void guideDiscTest() {
		Test.StartTest();               
		Canvass__c c = TestMethodsUtility.createCanvass();        
        Account a = TestMethodsUtility.generateCustomerAccount();
        insert a;             
        Contact cnt = TestMethodsUtility.createContact(a);               
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', a);                      
        Order__c ord = TestMethodsUtility.createOrder(a.Id);                
        Order_Group__c og = TestMethodsUtility.createOrderSet(a, ord, oppty);     
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Print_Specialty_Product_Type__c = 'Banner';
        insert product2;   
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        oln.Product2__c = product2.Id;
        insert oln;
        Directory__c dir = TestMethodsUtility.createDirectory();              
        Directory_Mapping__c DM = TestMethodsUtility.createDirectoryMapping();        
        Telco__c telco = TestMethodsUtility.createTelco(a.Id);        
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir.Id, c.id, telco.Id, DM.Id);  
        DE.Pub_Date__c = System.Today();
        insert DE;
        Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(dir);
        Directory_Guide__c dirGuide = TestMethodsUtility.createDirectoryGuide();
        Heading_Disclaimer__c headDisc = TestMethodsUtility.createHeadingDisclaimer();
        Directory_Heading__c dirHead  = TestMethodsUtility.createDirectoryHeading();
        Section_Heading_Mapping__c newSectionHeadingMapping = TestMethodsUtility.generateSectionHeadingMapping(dirSection.Id);
        newSectionHeadingMapping.Heading_Disclaimer__c = headDisc.Id;
        newSectionHeadingMapping.Directory_Guide__c = dirGuide.Id;
        newSectionHeadingMapping.Directory_Heading__c = dirHead.Id;
        newSectionHeadingMapping.Directory_Section__c = dirSection.Id;
        newSectionHeadingMapping.Guide_Telltale__c = '1234';
        insert newSectionHeadingMapping;
        ApexPages.currentPage().getParameters().put('Id', dirSection.Id);
		GuideDisclaimerManagementController obj = new GuideDisclaimerManagementController(new ApexPages.StandardController(dirSection));
		obj.cancel();
		obj.TxtDirecGuideID = dirGuide.Id;
		GuideDisclaimerManagementController.WrapobjSecHead wrapper = new GuideDisclaimerManagementController.WrapobjSecHead();
		wrapper.objSecHMap = newSectionHeadingMapping;
		wrapper.ischecked = true;
		obj.WrapobjSecHeadLst = new List<GuideDisclaimerManagementController.WrapobjSecHead>{wrapper};
		obj.UpdateDirGuides();
		obj.TxtHeadDisclmID = headDisc.Id;
		obj.WrapobjSecHeadLst = new List<GuideDisclaimerManagementController.WrapobjSecHead>{wrapper};
		obj.UpdateHeadingDisclaimer();
		obj.WrapobjSecHeadLst = new List<GuideDisclaimerManagementController.WrapobjSecHead>{wrapper};
		String str = obj.DeleteData();
		obj.WrapobjSecHeadLst = new List<GuideDisclaimerManagementController.WrapobjSecHead>{wrapper};
		str = obj.DeleteDisclaimerData();
		obj.page = 2;
		obj.totalPages = 4;
		obj.doNext();
		obj.doPrevious();
        Test.StopTest(); 
	}
}