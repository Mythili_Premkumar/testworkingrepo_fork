global class ExceptionErrordeletionBatch implements Database.Batchable<sObject> {
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        date dt = date.today().addmonths(-1);
        String query = 'SELECT Id from Exception_Error__c where createddate < :dt';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Exception_Error__c > objExceptionLst) {
        system.debug('Exception List'+objExceptionLst.size());
         if(objExceptionLst.size()> 0){
             delete objExceptionLst;
             DataBase.emptyRecycleBin(objExceptionLst);
         }
    }   
    
    global void finish(Database.BatchableContext BC) {
       
    }
}