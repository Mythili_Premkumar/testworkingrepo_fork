public class NationalBillingStatusSOQLMethods {
    public static List<National_Billing_Status__c > getNatBillByPubEditionDirCode(String pubCode, String editionCode, String dirCode) {
        return [SELECT Id, IV_VALIDATE_ORDER__c, IV_VALIDATE_PAGE_NUMBER__c, IV_VALIDATE_TAX__c, IV_VALIDATE_HEADER__c, IV_CREATE_INVOICE__c,
                IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c, ISSVAL_VALIDATE_TOTAL_ISS_GROSS_AMOUNT__c, ISSVAL_VALIDATE_TAX__c, ISSVAL_VALIDATE_NET_AMOUNT__c,
                ISSVAL_VALIDATE_ISS_TO_CRM__c, ISSVAL_VALIDATE_COMMISSION__c, TP_ELITE_TEAR_PAGE_TRANSFER_COMPLETE__c, TP_Covers_Spines_And_Indexes__c,
                SCI_NATIONAL_CLIENT_INVOICES_BALANCED__c, SCI_National_Invoice_Complete__c, IV_Create_Client_Invoice_Link__c, TP_Directory_Start_SAC_Returned__c,
                IV_Create_National_Client_Invoice__c, DI_COMPLETE_EOJ_FROM_INFORMATICA__c, DI_SEND_TO_ELITE_CHECK__c, TP_SEND_TO_ELITE_INITIATE_INFORMATICA__c,
                Number_of_Tear_Pages_Sent__c, Number_of_Tear_Pages_Received__c, DI_SEND_TO_ELITE_INITIATE_INFORMATICA__c, SCI_INITIATE_INFORMATICA__c,
                Directory_Index_Transfer_Completed__c, Client_Invoice_Transfer_Completed__c, SCI_CLIENT_INVOICES_BALANCED__c, RSAP_Send_Remittance_Started__c,
                RSAP_REMITTANCE_COMPLETE__c, RSAP_FF_INVOICES_BALANCED__c, ISS_ISS_COMPLETE__c, ISSVAL_Invoice_Summary_Statements__c, ISS_Send_ISS_Started__c,
                ISSVAL_DIRECTORIES_COMPLETE__c, ISS_Transfer_Completed__c, RSAP_COMPLETE_EOJ_FROM_INFORMATICA__c , ISS_SEND_TO_ELITE_INITIATE_INFORMATICA__c,
                RSAP_Remittance_Sheet_Created__c, NB_RSAP_Send_Remittance__c, NBS_Number_of_Tear_Pages_Expected__c, Client_Invoice_Transmission_Date__c 
                FROM National_Billing_Status__c WHERE Edition_Code__c =: editionCode AND Directory_Code__c =: dirCode AND Pub_Code__c =: pubCode];
    }
    
    public static List<National_Billing_Status__c > getNatBillByPubEditionCode(String pubCode, String editionCode) {
        return [SELECT Id, ISSVAL_VALIDATE_COMMISSION__c, ISSVAL_VALIDATE_ISS_TO_CRM__c, ISSVAL_VALIDATE_NET_AMOUNT__c, 
                ISSVAL_VALIDATE_TOTAL_ISS_GROSS_AMOUNT__c, RSAP_REMITTANCE_COMPLETE__c, RSAP_FF_INVOICES_BALANCED__c, ISS_ISS_COMPLETE__c, ISS_Balanced__c,
                ISSVAL_Invoice_Summary_Statements__c, ISSVAL_DIRECTORIES_COMPLETE__c, ISS_Send_ISS_Started__c, RSAP_Send_Remittance_Started__c,   
                ISS_Transfer_Completed__c, RSAP_COMPLETE_EOJ_FROM_INFORMATICA__c, IV_VALIDATE_CMR_BILLING_RESPONSIBILITY__c, ISSVAL_VALIDATE_TAX__c,
                RSAP_Remittance_Sheet_Created__c, RSAP_POST_TO_FINANCIALFORCE__c, NB_RSAP_Send_Remittance__c, ISS_COMPLETE_EOJ_FROM_INFORMATICA__c,
                ISS_Transmission_Date__c, ISS_SEND_TO_ELITE_INITIATE_INFORMATICA__c
                FROM National_Billing_Status__c WHERE Edition_Code__c =: editionCode AND Pub_Code__c =: pubCode];
    }
    
    public static List<National_Billing_Status__c > getNatBillByDirEdLSACodeDirCode(set<String> setLSAVerCode, set<String> setDirCode) {
        return [SELECT Id, Directory_Edition__r.LSA_Directory_Version__c, Directory_Edition__r.Directory_Code__c FROM National_Billing_Status__c WHERE 
                Directory_Edition__r.LSA_Directory_Version__c IN :setLSAVerCode AND Directory_Edition__r.Directory_Code__c IN :setDirCode];
    }
    
    public static List<National_Billing_Status__c > getNatBillByDirEdIds(set<Id> setDirEdIds) {
        return [SELECT Id, Directory_End_SAC__c, Directory_Edition__c FROM National_Billing_Status__c WHERE Directory_Edition__c IN :setDirEdIds];
    }
}