public with sharing class SalesCreditNoteSOQLMethods {
    
   
    
    public static list<c2g__codaCreditNote__c> getSalesCreditNoteByCreditID(set<Id> setCreditId) {
        return [select c2g__CreditNoteTotal__c, Id, c2g__Invoice__c, c2g__NetTotal__c,c2g__Account__c, Name,Transaction_Type__c,c2g__CreditNoteDate__c,
                Case__c,c2g__OutstandingValue__c,c2g__CreditNoteStatus__c,c2g__PaymentStatus__c from c2g__codaCreditNote__c where Id IN :setCreditId 
                AND Is_Manually_Created__c = false];
    }
}