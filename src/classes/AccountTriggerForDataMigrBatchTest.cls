@isTest
public class AccountTriggerForDataMigrBatchTest {
        
    static testMethod void test_accountTriggerBatch(){
    	String userId=System.Label.MigrationUserID;
        User sysAdmin=[select id from User where id=:userId];
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        system.runAs(sysAdmin){
            insert lstAccount;
            Database.executeBatch(new AccountTriggerForDataMigrBatch());
        }
    }
}