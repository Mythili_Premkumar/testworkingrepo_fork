@isTest
private class SpecArtAttachmentTestClass {
 
    static testmethod void CreatespecArtAttachmentTest()
    {
    
        Test.startTest();
        
       // User objUser = [Select UserRole.Name, UserRole.Id, UserRoleId, Id From User where UserRole.Name = 'Account Manager' limit 1];
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        Account objAccount = new Account(Primary_Canvass__c = objCanvass.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');        
        
        insert objAccount;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        
        Spec_Art_Request__c objSAR = new Spec_Art_Request__c();
        //bjSAR.Section_Code__c  = 'Consultant';
        objSAR.Log_on_name__c='r276';
        objSAR.Colors__c='full';
        //objSAR.Design_Style__c='STEWP';
        //objSAR.Size__c='10';
        objSAR.Description_of_Artwork_to_be_completed__c='Test Art Work';
        objSAR.Account__c=objAccount.Id;
        
        insert objSAR;
        set<Id> spId = new set<Id>();
        
        system.debug('********Spec Art*******'+objSAR);
        system.debug('********Spec Art*******'+objSAR.Id);
        String prefix;
        List<Attachment> atLst = new List<Attachment>();
        Attachment atPdf = new Attachment();
        atPdf.Name = 'Test1';
        atPdf.parentId=objSAR.id;
        atPdf.body=blob.topdf('test');
        prefix= String.valueOf(objSAR.id).substring(0, 3);
        spId.add(objSAR.id);
        atLst.add(atpdf);
        insert atLst;
        
        system.debug('********Spec Art Attachment*******'+atLst);
        
        Test.stopTest(); 
    }
}