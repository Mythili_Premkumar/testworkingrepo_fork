global class scheduledBatchSOInsert implements Schedulable {
   global void execute(SchedulableContext sc) {
      ServiceOrderBatchController  b = new ServiceOrderBatchController(); 
      database.executebatch(b);
   }
}