global class NIOpportunityBundlesBatch implements Database.Batchable<sObject>{
    
    global List<OpportunityLineItem> lstOppLINew;
    
    global NIOpportunityBundlesBatch(List<OpportunityLineItem> lstOppLI){
        lstOppLINew = lstOppLI;
    }
    
    global NIOpportunityBundlesBatch(){
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        /*Set<id> setId = new Set<Id>();
        for(String strId : label.NI_Opportunity_Bundle.split(',')) {
            setId.add(strId);
        }*/
        /*setId.add('00kG000001sES9dIAG');
        setId.add('00kG000001sES9gIAG');
        setId.add('00kG000001sDc9UIAS');
        setId.add('00kG000001sDc9XIAS');
        setId.add('00kG000001sCd9TIAS');
        setId.add('00kG000001sCd9UIAS');
        setId.add('00kG000001sDTRxIAO');
        setId.add('00kG000001sDTS4IAO');
        setId.add('00kG000001teupRIAQ');
        setId.add('00kG000001teupjIAA');
        setId.add('00kG000001tfO02IAE');
        setId.add('00kG000001tfO04IAE');
        setId.add('00kG000001tf4dVIAQ');
        setId.add('00kG000001tf4dhIAA');*/
        String strSOQL = 'select id, Renewals_Action__c, OpportunityId, Original_Line_Item_ID__c, Directory_Heading__c, Directory_Heading1__c, Directory_Heading2__c, Directory_Heading3__c, Directory_Heading4__c, UnitPrice from Opportunitylineitem where id in : lstOppLINew';
        return Database.getQueryLocator(strSOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<OpportunityLineItem> lstOppLI){
        //Set<String> setOrigOLI = new Set<String>();
        map<String,Set<OpportunityLineItem>> mpOrigLiOppLI = new map<String,Set<OpportunityLineItem>>();
        //for(AggregateResult objResult : lstAggregResult) {
        //    setOrigOLI.add(String.valueOf(objResult.get('origOLI')));
        //}
        
        //List<OpportunityLineItem> lstOppLI = [select id, OpportunityId, Original_Line_Item_ID__c, Directory_Heading__c, Directory_Heading1__c, Directory_Heading2__c, Directory_Heading3__c, Directory_Heading4__c, UnitPrice  from OpportunityLineItem where Original_Line_Item_ID__c in :setOrigOLI];
        String strOppId;
        String strOrigId;
        Set<Id> setUpdtOpp = new Set<Id>();
        for(OpportunityLineItem objOppLiIter : lstOppLI) {
            strOppId = String.valueOf(objOppLiIter.OpportunityId);
            strOrigId = String.valueOf(objOppLiIter.Original_Line_Item_ID__c);
            if(!mpOrigLiOppLI.containsKey(strOppId+strOrigId)) {
                mpOrigLiOppLI.put(strOppId+strOrigId, new Set<OpportunityLineItem>());
            }
            mpOrigLiOppLI.get(strOppId+strOrigId).add(objOppLiIter);
        }
        
        Set<Id> setOppLiDHs;
        Set<OpportunityLineItem> setOppLiCode;
        Boolean bolMainOppReqr;
        OpportunityLineItem objMainOppLI;
        Id idOpp;
        Boolean bolProcessOpp;
        List<Opportunity> lstUpdtOpp = new List<opportunity>();
        List<OpportunityLineItem> lstUpdtOppLi = new List<OpportunityLineItem>();
        
        for(String strIter : mpOrigLiOppLI.KeySet()) {
            bolProcessOpp = true;
            setOppLiCode = mpOrigLiOppLI.get(strIter);
            setOppLiDHs = new Set<Id>();
            objMainOppLI = null;
            if(setOppLiCode .size() > 1) {
                bolMainOppReqr = true;
                for(OpportunityLineItem iterator : setOppLiCode) {
                    idOpp = iterator.opportunityId;
                    System.debug('Testingg idOpp '+idOpp);
                    if(iterator.Renewals_Action__c  != 'Cancel') {
                        if(bolMainOppReqr && iterator.UnitPrice != null && iterator.UnitPrice != 0) {
                            objMainOppLI = iterator;
                            bolMainOppReqr = false;
                        }
                        else {
                            
                            if(iterator.Directory_Heading__c != null) {
                                setOppLiDHs.add(iterator.Directory_Heading__c);
                            }
                            if(iterator.Directory_Heading1__c != null) {
                                setOppLiDHs.add(iterator.Directory_Heading1__c);
                            }
                            if(iterator.Directory_Heading2__c != null) {
                                setOppLiDHs.add(iterator.Directory_Heading2__c);
                            }
                            if(iterator.Directory_Heading3__c != null) {
                                setOppLiDHs.add(iterator.Directory_Heading3__c);
                            }
                            if(iterator.Directory_Heading4__c != null) {
                                setOppLiDHs.add(iterator.Directory_Heading4__c);
                            }
                            lstUpdtOppLi.add(new OpportunityLineItem(id=iterator.id, DoNotConvert__c = true));
                        }
                    }
                }
                if(objMainOppLI != null) {
                    if(objMainOppLI.Renewals_Action__c == 'New' || objMainOppLI.Renewals_Action__c == 'Modify') {
                        objMainOppLI.Renewals_Action__c = 'Renew';
                    }
                    for(Id idDH : setOppLiDHs) {
                        if(!setUpdtOpp.contains(objMainOppLI.opportunityId)) {
                            if(objMainOppLI.Directory_Heading__c == null) {
                                objMainOppLI.Directory_Heading__c = idDH;
                            }
                            else if(objMainOppLI.Directory_Heading1__c == null) {
                                objMainOppLI.Directory_Heading1__c = idDH;
                            }
                            else if(objMainOppLI.Directory_Heading2__c == null) {
                                objMainOppLI.Directory_Heading2__c = idDH;
                            }
                            else if(objMainOppLI.Directory_Heading3__c == null) {
                                objMainOppLI.Directory_Heading3__c = idDH;
                            }
                            else if(objMainOppLI.Directory_Heading4__c == null) {
                                objMainOppLI.Directory_Heading4__c = idDH;
                            }
                            else {
                                setUpdtOpp.add(objMainOppLI.opportunityId);
                                lstUpdtOpp.add(new Opportunity(id=objMainOppLI.opportunityId, NotProcessed__c = true));
                                bolProcessOpp = false;
                                System.debug('Testingg objMainOppLI.opportunityId '+objMainOppLI.opportunityId);
                            }
                        }
                    }
                }
                if(bolProcessOpp && objMainOppLI != null) {
                    lstUpdtOppLi.add(objMainOppLI);
                }
                else if(objMainOppLI == null) {
                    futureCreateErrorLog.createErrorRecordBatch('Error Message : Multiple OpportunityLineItems in same Opportunity refer to same Original OrderLineItem and Main Opportunity Line Item cannot be identified for Opportunity '+idOpp, null, 'Batch update of migrated Opportunity');
                    lstUpdtOpp.add(new Opportunity(id = idOpp, NotProcessed__c = true)); 
                    System.debug('Testingg idOpp 2 '+idOpp);
                }
            }
        }
        if(lstUpdtOppLi != null && lstUpdtOppLi.size() > 0) {
            update lstUpdtOppLi;
        }
        if(lstUpdtOpp != null && lstUpdtOpp.size() > 0) {
            update lstUpdtOpp;
        }
    }
    
    global void finish(Database.BatchableContext bc){
        String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //String[] toAddresses = new String[] {'Mythreyee.Kumar@theberrycompany.com'};
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Migrated NI Opportunity Records processing status ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage);
    }
}