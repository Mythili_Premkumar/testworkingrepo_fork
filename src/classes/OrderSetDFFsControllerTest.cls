@isTest
public class OrderSetDFFsControllerTest{
    public static testMethod void OSDmethod(){

        Product2 prdt = CommonUtility.createPrdtTest();
        insert prdt;
                
        Canvass__c c= CommonUtility.createCanvasTest();
        insert c;
        
        Account a = CommonUtility.createAccountTest(c);
        insert a;       
        
        Opportunity oppty = CommonUtility.createOpptyTest(a);
        insert oppty;
        
        Contact cnt = CommonUtility.createContactTest(a);
        insert cnt;
        
        Order__c ord = CommonUtility.createOrderTest(a);
        insert ord;
        
        
        Order_Group__c og = CommonUtility.createOGTest(a, ord, oppty);
        insert og;
        
        Order_Line_Items__c oln = CommonUtility.createOLITest(c, a, cnt, oppty, ord, og);
        insert oln;
        
        Modification_Order_Line_Item__c mOln = CommonUtility.createMOLITest(c, a, cnt, oppty, ord, og);
        insert mOln;
        
        Digital_Product_Requirement__c dff1 = CommonUtility.createDffTest();
        dff1.OrderLineItemID__c = oln.id; dff1.Talus_Subscription_Id__c = ''; dff1.Talus_DFF_Id__c = ''; dff1.DFF_Product__c = prdt.id;
        insert dff1;
        
        Digital_Product_Requirement__c dff2 = CommonUtility.createDffTest();
        dff2.ModificationOrderLineItem__c = mOln.id; dff2.Talus_Subscription_Id__c = ''; dff2.Talus_DFF_Id__c = ''; dff2.DFF_Product__c = prdt.id;
        insert dff2;

        Digital_Product_Requirement__c dff3 = CommonUtility.createDffTest();
        dff3.Talus_Subscription_Id__c = ''; dff3.Talus_DFF_Id__c = ''; dff3.DFF_Product__c = prdt.id;
        insert dff3;
                
        test.starttest();

        ApexPages.StandardController controller1 = new ApexPages.StandardController(dff1); 
        OrderSetDFFsController ordStDffs1 = new OrderSetDFFsController(controller1);
        ordStDffs1.getDffs();
        
        ApexPages.StandardController controller2 = new ApexPages.StandardController(dff2);
        OrderSetDFFsController ordStDffs2 = new OrderSetDFFsController(controller2);
        ordStDffs2.getDffs();        

        ApexPages.StandardController controller3 = new ApexPages.StandardController(dff3);
        OrderSetDFFsController ordStDffs3 = new OrderSetDFFsController(controller3);
        ordStDffs3.getDffs();
                
        OrderSetDFFsSummaryController smryDffs = new OrderSetDFFsSummaryController(controller1);
        smryDffs.getSmrryFlds();
        smryDffs.getAllDffsSmmry();
        smryDffs.submitfulfillment();

        test.stoptest();
    }
}