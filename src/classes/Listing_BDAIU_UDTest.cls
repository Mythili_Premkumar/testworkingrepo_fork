//DB_Custom_Listing_c Trigger Test class
@isTest(SeeAllData=True)
public class Listing_BDAIU_UDTest{
    static testmethod void dbtest(){
        Test.starttest();
    
        Account objAccount = TestMethodsUtility.generateAccount('customer');
        insert objAccount;
        
        Account newPubAccount = TestMethodsUtility.generateAccount('publication');
        insert newPubAccount;
        
        Account telcoAccount = TestMethodsUtility.generateAccount('telco');
        insert telcoAccount;
        
        Telco__c objTelco = TestMethodsUtility.createTelco(telcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact objContact = TestMethodsUtility.createContact(objAccount);
        insert objContact;
        
        Opportunity objOpportunity = TestMethodsUtility.createOpportunity(objAccount, objContact);
        insert objOpportunity;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        
        insert objProd;
        
        Listing__c objMListing = TestMethodsUtility.generateListing('Main Listing');
        objMListing.Account__c = objAccount.Id;
        objMListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        insert objMListing;
        
        Listing__c objListing = TestMethodsUtility.generateListing('Additional Listing');
        objListing.Account__c = objAccount.Id;
        objListing.Primary_Canvass__c = objAccount.Primary_Canvass__c;
        objListing.Main_Listing__c = objMListing.id;
        insert objListing;
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = objAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        
        Directory_Listing__c objDL = TestMethodsUtility.generateDirectoryListing('Caption Member');
        objDL.Listing__c = objListing.Id;
        objDL.Directory_Section__c = objDS.Id;
        insert objDL;
        
        List<OpportunityLineItem> LstOppLI = TestMethodsUtility.selectOpportunityLineItem(objOpportunity);
        insert LstOppLI;
             
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
        
        Order_Group__c objOrderGroup = new Order_Group__c (Name = 'Unit Testing', selected__c = true, Order_Account__c = objAccount.ID, Opportunity__c = objOpportunity.ID);                                            
        insert objOrderGroup ;
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id);
        objOrderLineItem.Listing__c = objListing.Id;
        insert objOrderLineItem ;
        
        objListing.Phone__c = '9999999999';
        update objListing;
        
        ListingToScopedListingSync.syncListingToScopedListing(new Set<Id> {objListing.Id});
        
        try{
        delete objListing;
        }catch(exception e){
        system.debug('Error!');
        }
        
        Test.stoptest();
    }
}