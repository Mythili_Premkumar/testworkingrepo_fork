public with sharing class SectionHeadingMappingSOQLMethods {

    public static List<Section_Heading_Mapping__c> getSecHeadMappingsByIds(set<Id> dirSecHeadMapIds){
        return [SELECT Leader_Ads_Sold__c, National_Leader_Ads_Sold__c FROM Section_Heading_Mapping__c WHERE Id IN : dirSecHeadMapIds]; 
    }
    
    
    public static list<Section_Heading_Mapping__c> getSecHeadMappingsByComboForIBAppearances(set<String> setComboValue){
        return [Select Id,Name,Directory_section__c,Directory_Heading__c,Internet_Bundle_Appearances__c,strBundleAppearenceCombo__c FROM 
                Section_Heading_Mapping__c where strBundleAppearenceCombo__c IN :setComboValue];   
    }
    
   
    
    public static list<Section_Heading_Mapping__c> getSecHeadMapByDirSecDirHeadIds(set<Id> dirSecIds, set<Id> dirHeadIds){
        return [SELECT Appearance_Count__c, Leader_Ads_Remaining__c, DirSecDirHeadIds__c, Leader_Ads_Sold__c, strBundleAppearenceCombo__c, 
                Internet_Bundle_Appearances__c, Pagination_Appearance_Count__c, Pagination_IBUN_Appearance_Count__c FROM Section_Heading_Mapping__c 
                WHERE Directory_Section__c IN : dirSecIds AND Directory_Heading__c IN :dirHeadIds]; 
    }

    public static list<Section_Heading_Mapping__c> getSecHeadMappingsByDHDS(Set<id> setDirHeading, Set<id> setDirSection) {
        return [select id, Directory_Heading__c, Directory_Section__c from Section_Heading_Mapping__c where Directory_Heading__c in :setDirHeading AND 
                Directory_Section__c in :setDirSection];
    }
    
    public static map<Id,Set<Id>> getMapSecHeadMappingsByDHDS(Set<id> setDirSection) {
        map<Id,Set<Id>> mapSHMByDSDH=new  map<Id,Set<Id>>();
        for(Section_Heading_Mapping__c  shmIterator : [select id, Directory_Heading__c, Directory_Section__c from Section_Heading_Mapping__c where Directory_Section__c in :setDirSection]){
                  if(!mapSHMByDSDH.containskey(shmIterator.Directory_Section__c)){
                      mapSHMByDSDH.put(shmIterator.Directory_Section__c,new Set<Id>());
                  }
                  mapSHMByDSDH.get(shmIterator.Directory_Section__c).add(shmIterator.Directory_Heading__c); 
                }
        return mapSHMByDSDH;
    }
    public static list<Section_Heading_Mapping__c> getSecHeadMapByDSDHCombo(Set<string> setDirSecDirHeaStr){
         return [SELECT Id,SHM_strDirectorySecAndHeading__c, Directory_heading__c, Directory_section__c, Internet_Bundle_Appearances__c, Leader_Ads_Sold__c, Appearance_Count__c, Pagination_Appearance_Count__c, Pagination_IBUN_Appearance_Count__c FROM Section_Heading_Mapping__c 
                WHERE SHM_strDirectorySecAndHeading__c IN :setDirSecDirHeaStr];
    }
}