public class FalloutOrders_v3 {
String type;
String rtype;
public boolean ShowReport{get;set;}
public boolean ShowDigitalReport{get;set;}
public String Reporttitle{get;set;}
public String selectedreptype{get;set;}
public string directories;
public string[] directoriesSection;
public string selectedtype{get;set;}
public String selectedcanvasses{get;set;}
public string canvasses{get;set;}
public String selecteddirectories{get;set;}
public String[] selecteddirectoriesSection{get;set;}
public String selectedRtype{get;set;}
public transient List<Digital_Product_Requirement__c> DFFList{get;set;}
public transient  List<Spec_Art_Request__c> SARList{get;set;}
public transient  List<PrintReportWrapper> PrintReportWrapLst{get;set;}
public transient  List<DigitalReportWrapper> DigitalReportWrapLst{get;set;}
public FalloutOrders_v3 ()
{
  PrintReportWrapLst=new List<PrintReportWrapper>();
  DigitalReportWrapLst=new List<DigitalReportWrapper>();
  ShowReport=false;
  ShowDigitalReport=false;
}
public List<SelectOption> getrtypes() 
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('1','Print Graphics'));
    options.add(new SelectOption('2','Digital'));
 
    return options;
}

public String getrtype() 
{
    return rtype;
}
    
public void setrtype(String rtype) 
{
    if(rtype=='1')
    {
      ShowDigitalReport=false;
    }
    if(rtype=='2')
    {
      ShowReport=false;
    }
    this.rtype= rtype;
    this.selectedreptype=rtype;
}

public List<SelectOption> gettypes() 
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('0','--None--'));
    options.add(new SelectOption('1','DFF'));
    options.add(new SelectOption('2','SAR'));
 
    return options;
}

public String gettype() 
{
    return type;
}
    
public void settype(String type) 
{
    this.type= type;
    this.selectedtype=type;
}
//Canvass Selection
public void populateCanvass()
{
getCanvass();
}
public List<SelectOption> getCanvass()
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('0','--None--'));
    if(type !=null)
    {
        for(Canvass__c cl:[Select id,Name from Canvass__c Order By Name ASC])
        {
           options.add(new SelectOption(cl.id,cl.Name));
        }
    }
    return options; 
}
public String getcanvasses() 
 {
       return canvasses;
 }
        
public void setcanvass(String canvasses) {
    this.canvasses= canvasses;
    this.selectedcanvasses=canvasses;
   
}

public void populateDirectory()
{
  getdirectory();
}
// For Directory
public List<SelectOption> getdirectory() 
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('0','--None--'));
    if(type !=null && canvasses!=null)
    {
        for(Directory__c tl:[Select id,Name from Directory__c where Canvass__c=:canvasses Order By Name ASC])
        {
           options.add(new SelectOption(tl.id,tl.Name));
        }
    }
    return options; 
}
        
public String getdirectories() 
 {
       return directories;
 }
        
public void setdirectories(String directories) {
    this.directories= directories;
    this.selecteddirectories=directories;
   
}
public void populateSection()
{
  getdirectorySection();

}
public String[] getdirectoriesSection() 
{
   return directoriesSection;
}  
public void setdirectoriesSection(String[] directoriesSection) 
{
    this.directoriesSection= directoriesSection;
    this.selecteddirectoriesSection=directoriesSection;
   
}
public List<SelectOption> getdirectorySection() 
{
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('0','--None--'));
     for(Directory_Section__c tl:[Select id,Name from Directory_Section__c where Directory__c=:this.selecteddirectories Order By Name ASC])
    {
     options.add(new SelectOption(tl.id,tl.Name));
    }
    
    return options;
}

public void generateReport()
{
   if(rtype!='0')
   {
       string filterlogic;
       string qry;
       if(rtype=='1') //For Print Graphics
       {
           if(type!='0')
           {
               if(directories!='0')
               {
                  System.debug('##################'+directoriesSection);
                   if(directoriesSection[0]!='0')
                   {
                        String slist='';
                        for (String s: directoriesSection) 
                        {
                        slist += '\'' + s + '\',';
                        }
                       slist = slist.substring (0, slist.length() -1);
                       PrintReportWrapLst=new List<PrintReportWrapper>(); 
                       if(type=='1') //DFF
                       {
                           //Fetching DFF Record Query
                           if(canvasses!='0')
                           {
                           qry='Select id, URN_text__c,OrderLineItemID__r.Order_Group__r.Name, DFF_Directory_Section__r.name,Directory_Edition__r.name,URN_number__c,ownerId, owner.name,Miles_Status__c,Migrate_to_Miles_33__c, Status__c, Order_URN__c, Name, Date_Graphics_Received__c,DFF_Status__c, Account__r.Name, Account__c, Graphics_Complete_Date__c,CreatedBy.Name,(Select id,title from Notes Order By CreatedDate desc Limit 1) From Digital_Product_Requirement__c where Miles_Status__c!=\'Complete\' AND OrderLineItemID__r.Product2__r.Media_Type__c=\'Print\' AND OrderLineItemID__r.Canvass__c=\''+canvasses+'\' AND OrderLineItemID__r.Directory_Section__c IN ('+slist +')';
                           
                           }
                           else
                           {
                           qry='Select id, URN_text__c,OrderLineItemID__r.Order_Group__r.Name, DFF_Directory_Section__r.name,Directory_Edition__r.name,URN_number__c,ownerId, owner.name,Miles_Status__c,Migrate_to_Miles_33__c, Status__c, Order_URN__c, Name, Date_Graphics_Received__c,DFF_Status__c, Account__r.Name, Account__c, Graphics_Complete_Date__c,CreatedBy.Name,(Select id,title from Notes Order By CreatedDate desc Limit 1) From Digital_Product_Requirement__c where Miles_Status__c!=\'Complete\' AND OrderLineItemID__r.Product2__r.Media_Type__c=\'Print\' AND OrderLineItemID__r.Directory_Section__c IN ('+slist +')';
                           
                           }
                           System.debug('##################'+qry);
                           DFFList=new List<Digital_Product_Requirement__c>();
                           DFFList=Database.Query(qry);
                           if(DFFList.size()>0)
                           {
                               Reporttitle='DFF';
                               for(Digital_Product_Requirement__c DPRIterator : DFFList)
                               {
                                  if(DPRIterator.Notes.size()>0)
                                  {
                                     String title=DPRIterator.Notes[0].Title;
                                     String NoteTitle=title.substring(0, 3);
                                     if(NoteTitle=='ERR')
                                     {
                                     PrintReportWrapLst.add(new PrintReportWrapper(String.valueof(DPRIterator.Name),DPRIterator.id,String.valueof(DPRIterator.OrderLineItemID__r.Order_Group__r.Name),String.valueof(DPRIterator.URN_number__c),null,String.valueof(DPRIterator.Miles_Status__c),String.valueof(DPRIterator.Account__r.Name),String.valueof(DPRIterator.Directory_Edition__r.name),String.valueof(DPRIterator.DFF_Directory_Section__r.name),String.valueof(DPRIterator.Migrate_to_Miles_33__c),String.valueof(NoteTitle),String.valueof(DPRIterator.CreatedBy.Name)));
                                     ShowReport=true;
                                     ShowDigitalReport=false;
                                     }
                                  }
                                  
                               }
                           
                           }
                           else
                           {
                               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                               ApexPages.addMessage(myMsg);
                           }
                       }
                       if(type=='2') //SRR
                       {
                           //Fetch Spec Art Record Query
                        String srlist='';
                        for (String s: directoriesSection) 
                        {
                        srlist += '\'' + s + '\',';
                        }
                        srlist = srlist.substring (0, srlist.length() -1);
                           SARList=new List<Spec_Art_Request__c>();
                           qry='SELECT id,Name,About_Business__c,Account__c,Account__r.name,Edition__r.name,Miles_Status__c,Directory_Section__r.Name,Advertised_business_email__c,Advertised_Business_Potalcode__c,Advertised_Listed_Address__c,Advertised_Listed_City__c,Advertised_Listed_Phone_Number__c,Advertised_Listed_State__c,Appointment_Date__c,Contact__c,Date_Graphics_Complete__c ,CreatedBy.Name,Directory_Section__c,Directory__c,Division__c,Edition__c,Migrate_to_Miles_33__c,Product__c,Section_Code__c,Section_Heading_Mapping__c,Spec_Art_URL_Completed__c,Spec_Art_URL__c,(Select id,title from Notes Order By CreatedDate desc Limit 1) FROM Spec_Art_Request__c where Miles_Status__c!=\'Complete\' AND Product__r.Media_Type__c=\'Print\' AND Directory__c=\''+directories+'\' AND Directory_Section__c IN ('+ srlist +') Order By Migrate_to_Miles_33__c ASC';
                           SARList=Database.Query(qry);
                           if(SARList.size()>0)
                           {
                              Reporttitle='Spec Art';
                              for(Spec_Art_Request__c SRIterator : SARList)
                              {
                                 if(SRIterator.Notes.size()>0)
                                  {
                                     String title=SRIterator.Notes[0].Title;
                                     String NoteTitle=title.substring(0, 3);
                                     if(NoteTitle=='ERR')
                                     {
                                     PrintReportWrapLst.add(new PrintReportWrapper('',null,'',String.valueof(SRIterator.Name),SRIterator.Id,String.valueof(SRIterator.Miles_Status__c),String.valueof(SRIterator.Account__r.Name),String.valueof(SRIterator.Edition__r.name),String.valueof(SRIterator.Directory_Section__r.name),String.valueof(SRIterator.Migrate_to_Miles_33__c),String.valueof(NoteTitle),String.valueof(SRIterator.CreatedBy.Name)));
                                     ShowReport=true;
                                     ShowDigitalReport=false;
                                     }
                                  }
                                  
                              }
                           }  
                           else
                           {
                               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                               ApexPages.addMessage(myMsg);
                           }
                       }
                   }
                   else
                   {
                      ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Directory Section Selected');
                      ApexPages.addMessage(myMsg);
                   
                   }
               }
               else  //if none filters are selected
               {
                      
                      PrintReportWrapLst=new List<PrintReportWrapper>(); 
                       if(type=='1') //DFF
                       {
                           //Fetching DFF Record Query
                           if(canvasses!='0')
                           {
                              qry='Select id, URN_text__c,OrderLineItemID__r.Order_Group__r.Name, DFF_Directory_Section__r.name,Directory_Edition__r.name,URN_number__c,ownerId, owner.name,Miles_Status__c,Migrate_to_Miles_33__c, Status__c, Order_URN__c, Name, Date_Graphics_Received__c,DFF_Status__c, Account__r.Name, Account__c, Graphics_Complete_Date__c,CreatedBy.Name,(Select id,title from Notes Order By CreatedDate desc Limit 1) From Digital_Product_Requirement__c where Miles_Status__c!=\'Complete\' AND OrderLineItemID__r.Product2__r.Media_Type__c=\'Print\' AND OrderLineItemID__r.Canvass__c=\''+canvasses+'\'';
                                
                           }
                           else
                           {
                              qry='Select id, URN_text__c,OrderLineItemID__r.Order_Group__r.Name, DFF_Directory_Section__r.name,Directory_Edition__r.name,URN_number__c,ownerId, owner.name,Miles_Status__c,Migrate_to_Miles_33__c, Status__c, Order_URN__c, Name, Date_Graphics_Received__c,DFF_Status__c, Account__r.Name, Account__c, Graphics_Complete_Date__c,CreatedBy.Name,(Select id,title from Notes Order By CreatedDate desc Limit 1) From Digital_Product_Requirement__c where Miles_Status__c!=\'Complete\' AND OrderLineItemID__r.Product2__r.Media_Type__c=\'Print\' Limit 100';
                               
                           }
                               DFFList=new List<Digital_Product_Requirement__c>();
                               DFFList=Database.Query(qry);
                               if(DFFList.size()>0)
                               {
                                   Reporttitle='DFF';
                                   for(Digital_Product_Requirement__c DPRIterator : DFFList)
                                   {
                                      if(DPRIterator.Notes.size()>0)
                                      {
                                         String title=DPRIterator.Notes[0].Title;
                                         String NoteTitle=title.substring(0, 3);
                                         if(NoteTitle=='ERR')
                                         {
                                         PrintReportWrapLst.add(new PrintReportWrapper(String.valueof(DPRIterator.Name),DPRIterator.Id,String.valueof(DPRIterator.OrderLineItemID__r.Order_Group__r.Name),String.valueof(DPRIterator.URN_number__c),null,String.valueof(DPRIterator.Miles_Status__c),String.valueof(DPRIterator.Account__r.Name),String.valueof(DPRIterator.Directory_Edition__r.name),String.valueof(DPRIterator.DFF_Directory_Section__r.name),String.valueof(DPRIterator.Migrate_to_Miles_33__c),String.valueof(NoteTitle),String.valueof(DPRIterator.CreatedBy.Name)));
                                         ShowReport=true;
                                         ShowDigitalReport=false;
                                         }
                                      }
                                      
                                   }
                               
                               }
                               else
                               {
                                   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                                   ApexPages.addMessage(myMsg);
                               }
                          
                       }
                       if(type=='2') //SRR
                       {
                           //Fetch Spec Art Record Query
                           SARList=new List<Spec_Art_Request__c>();
                           if(canvasses!='0')
                           {
                           qry='SELECT Id,Name,About_Business__c,Account__c,Account__r.name,Edition__r.name,Miles_Status__c,Directory_Section__r.Name,Advertised_business_email__c,Advertised_Business_Potalcode__c,Advertised_Listed_Address__c,Advertised_Listed_City__c,Advertised_Listed_Phone_Number__c,Advertised_Listed_State__c,Appointment_Date__c,Contact__c,Date_Graphics_Complete__c ,CreatedBy.Name,Directory_Section__c,Directory__c,Division__c,Edition__c,Migrate_to_Miles_33__c,Product__c,Section_Code__c,Section_Heading_Mapping__c,Spec_Art_URL_Completed__c,Spec_Art_URL__c,(Select id,title from Notes Order By CreatedDate desc Limit 1) FROM Spec_Art_Request__c where Miles_Status__c!=\'Complete\' AND Product__r.Media_Type__c=\'Print\' AND Directory__r.Canvass__c=\''+canvasses+'\'Order by Migrate_to_Miles_33__c ASC';
                           }
                           else
                           {
                           qry='SELECT Id,Name,About_Business__c,Account__c,Account__r.name,Edition__r.name,Miles_Status__c,Directory_Section__r.Name,Advertised_business_email__c,Advertised_Business_Potalcode__c,Advertised_Listed_Address__c,Advertised_Listed_City__c,Advertised_Listed_Phone_Number__c,Advertised_Listed_State__c,Appointment_Date__c,Contact__c,Date_Graphics_Complete__c ,CreatedBy.Name,Directory_Section__c,Directory__c,Division__c,Edition__c,Migrate_to_Miles_33__c,Product__c,Section_Code__c,Section_Heading_Mapping__c,Spec_Art_URL_Completed__c,Spec_Art_URL__c,(Select id,title from Notes Order By CreatedDate desc Limit 1) FROM Spec_Art_Request__c where Miles_Status__c!=\'Complete\' AND Product__r.Media_Type__c=\'Print\' Order by Migrate_to_Miles_33__c ASC';
                           
                           }
                           
                           SARList=Database.Query(qry);
                           if(SARList.size()>0)
                           {
                              Reporttitle='Spec Art';
                              for(Spec_Art_Request__c SRIterator : SARList)
                              {
                                 if(SRIterator.Notes.size()>0)
                                  {
                                     String title=SRIterator.Notes[0].Title;
                                     String NoteTitle=title.substring(0, 3);
                                     if(NoteTitle=='ERR')
                                     {
                                     PrintReportWrapLst.add(new PrintReportWrapper('',null,'',String.valueof(SRIterator.Name),SRIterator.Id,String.valueof(SRIterator.Miles_Status__c),String.valueof(SRIterator.Account__r.Name),String.valueof(SRIterator.Edition__r.name),String.valueof(SRIterator.Directory_Section__r.name),String.valueof(SRIterator.Migrate_to_Miles_33__c),String.valueof(NoteTitle),String.valueof(SRIterator.CreatedBy.Name)));
                                     ShowReport=true;
                                     ShowDigitalReport=false;
                                     }
                                  }
                                  
                              }
                           }  
                           else
                           {
                               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                               ApexPages.addMessage(myMsg);
                           }
                       }
                      
               }
           }
           
           if(PrintReportWrapLst.size()==0)
           {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                ApexPages.addMessage(myMsg);
           }
      }
      if(rtype=='2') //For Digital
      {
          DFFList=new List<Digital_Product_Requirement__c>();
          DigitalReportWrapLst=new List<DigitalReportWrapper>();
          qry='Select id, URN_text__c,OrderLineItemID__r.Order_Group__r.Name, DFF_Directory_Section__r.name,Directory_Edition__r.name,URN_number__c,ownerId, owner.name,Miles_Status__c,Migrate_to_Miles_33__c, Status__c, Order_URN__c, Name, Date_Graphics_Received__c,DFF_Status__c, Account__r.Name, Account__c, Graphics_Complete_Date__c,CreatedBy.Name,(Select id,title from Notes Order By CreatedDate desc Limit 1),(SELECT CreatedBy.Name,Account_Name__c,DFF__r.Account__r.Name,Date_Graphics_Complete__c,Date_Graphics_Received__c,Date_Submitted__c,DFF__c,DFF__r.Miles_Status__c,DFF__r.URN_number__c,DFF__r.Name,DFF__r.Migrate_to_Miles_33__c,DFF__r.OrderLineItemID__r.Order_Group__r.Name,Id,LoResCopy__c,Migrate_to_Miles_33__c,Miles33_ImportAdvert__c,Miles_Status__c,Name,Production_Notes_for_YPC_Graphic__c,URN__c,YPC_Graphics_Type__c,YPC_Graphics_URL__c FROM YPC_Graphics__r) From Digital_Product_Requirement__c where Miles_Status__c!=\'Complete\' AND OrderLineItemID__r.Product2__r.Media_Type__c=\'Digital\' AND OrderLineItemID__r.Product2__r.Vendor__c=\'YP\'';
          DFFList=Database.Query(qry);
          if(DFFList.size()>0)
          {
              Reporttitle='Digital';
              for(Digital_Product_Requirement__c dprIterator: DFFList)
              {
                  if(dprIterator.Notes.size()>0)
                  {
                      String title=dprIterator.Notes[0].Title;
                      String NoteTitle=title.substring(0, 3);
                      if(NoteTitle=='ERR')
                      {
                          if(dprIterator.YPC_Graphics__r.size()==1)
                          {
                            for(YPC_Graphics__c ypcIterator : dprIterator.YPC_Graphics__r)
                            {
                                DigitalReportWrapLst.add(new DigitalReportWrapper(String.valueof(ypcIterator.DFF__r.Name),ypcIterator.DFF__c,String.valueof(ypcIterator.DFF__r.OrderLineItemID__r.Order_Group__r.Name),String.valueof(ypcIterator.DFF__r.URN_number__c),null,String.valueof(ypcIterator.DFF__r.Miles_Status__c), String.valueof(ypcIterator.DFF__r.Account__r.Name),String.valueof(ypcIterator.Name), String.valueof(ypcIterator.Miles_Status__c),String.valueof(ypcIterator.DFF__r.Migrate_to_Miles_33__c),String.valueof(dprIterator.Notes[0].title),String.valueof(dprIterator.CreatedBy.Name)));
                                ShowDigitalReport=true;
                                ShowReport=false;
                            }
                          }
                          else if(dprIterator.YPC_Graphics__r.size()>1)
                          {
                              for(YPC_Graphics__c ypcIterator : dprIterator.YPC_Graphics__r)
                              {
                                DigitalReportWrapLst.add(new DigitalReportWrapper(String.valueof(ypcIterator.DFF__r.Name),ypcIterator.DFF__c,String.valueof(ypcIterator.DFF__r.OrderLineItemID__r.Order_Group__r.Name),String.valueof(ypcIterator.DFF__r.URN_number__c),null,String.valueof(ypcIterator.DFF__r.Miles_Status__c), String.valueof(ypcIterator.DFF__r.Account__r.Name),String.valueof(ypcIterator.Name), String.valueof(ypcIterator.Miles_Status__c),String.valueof(ypcIterator.Date_Submitted__c),String.valueof(dprIterator.Notes[0].title),String.valueof(dprIterator.CreatedBy.Name)));
                                ShowDigitalReport=true;
                                ShowReport=false;
                              }
                          }
                       }
                  }
                  
              }
          }
          else
          {
               ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
               ApexPages.addMessage(myMsg);
          }
          
          if(DigitalReportWrapLst.size()==0)
           {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'No Record Found');
                ApexPages.addMessage(myMsg);
           }
      }
      
   }
}

public class DigitalReportWrapper
{
    public String DFFNum{get;set;}
    public Id DFFId{get;set;}
    public String OrderSet {get;set;}
    public String ParentURN{get;set;}
    public Id ParentURNId{get;set;}
    public String Status{get;set;}
    public String AccountName{get;set;}
    public String ChildURN{get;set;}
    public String ChildURNStatus{get;set;}
    public String DateSubmitted{get;set;}
    public String Title{get;set;}
    public String Owner{get;set;}
    
    public DigitalReportWrapper(String DFFNum,Id DFFId,String OrderSet,String ParentURN,Id ParentURNId,String Status, String AccountName,String ChildURN, String ChildURNStatus,String DateSubmitted,String Title,String Owner)
    {
        this.DFFNum=DFFNum;
        this.DFFId=DFFId;
        this.OrderSet=OrderSet;
        this.ParentURN=ParentURN;
        this.ParentURNId=ParentURNId;
        this.Status=Status; 
        this.AccountName=AccountName;
        this.ChildURN=ChildURN;
        this.ChildURNStatus=ChildURNStatus;
        this.DateSubmitted=DateSubmitted;
        this.Title=Title;
        this.Owner=Owner;
    
    }

}

public class PrintReportWrapper
{
    public String DFFNum{get;set;}
    public Id DFFId{get;set;}
    public String OrderSet {get;set;}
    public String ParentURN{get;set;}
    public Id ParentURNId{get;set;}
    public String Status{get;set;}
    public String AccountName{get;set;}
    public String DEName{get;set;}
    public String DSectionHeadingName{get;set;}
    public String DateSubmitted{get;set;}
    public String Title{get;set;}
    public String Owner{get;set;}
    
    public PrintReportWrapper(String DFFNum,Id DFFId,String OrderSet,String ParentURN,Id ParentURNId,String Status, String AccountName,String DEName, String DSectionHeadingName,String DateSubmitted,String Title,String Owner)
    {
        this.DFFNum=DFFNum;
        this.DFFId=DFFId;
        this.OrderSet=OrderSet;
        this.ParentURN=ParentURN;
        this.ParentURNId=ParentURNId;
        this.Status=Status; 
        this.AccountName=AccountName;
        this.DEName=DEName;
        this.DSectionHeadingName=DSectionHeadingName;
        this.DateSubmitted=DateSubmitted;
        this.Title=Title;
        this.Owner=Owner;
    
    }

}
}