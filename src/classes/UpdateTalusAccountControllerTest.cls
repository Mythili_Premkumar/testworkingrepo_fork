@isTest
public class UpdateTalusAccountControllerTest {
    public static testmethod void updtTalusAcct() {

        Id accMngrId = System.Label.TestUserPerformanceAdvisorId;
        
        Account a1 = new Account(name = 'UpdateAccountTest', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Is_Active__c = true,
                            BillingStreet = 'TestBillingStreet', BillingState = 'AZ', BillingPostalCode = '850015656', 
                            BillingCountry = 'US');
                            
        insert a1;

        Test.startTest();

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('AccountTestJSONUpdate');
        mock.setStatusCode(202);
        mock.setHeader('Content-Type', 'application/json');

        //Set mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
                
        ApexPages.currentPage().getParameters().put('Id', a1.id);

        ApexPages.StandardController sc = new ApexPages.StandardController(a1);
        UpdateTalusAccountController ITP = new UpdateTalusAccountController(sc);
        ITP.updtTalusAcc();

        a1.TalusAccountId__c = '122';
        a1.TalusPhoneId__c = '1224452';
        update a1;
        ITP.updtTalusAcc();
        
        a1.Account_Manager__c = accMngrId;
        a1.phone = '444-444-2525';
        update a1;
        ITP.updtTalusAcc();
        ITP.rtnAccnt();
             
        Test.stopTest();
    }
}