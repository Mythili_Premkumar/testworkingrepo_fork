@isTest(SeeAllData=true)
public class Test_BsureCustomSelectContact  
{
    public static testMethod void testCustomSelectContact() 
    {
     Account obj_acct = new Account();
     obj_acct.Name = 'Test Account';
     obj_acct.Phone = '(989)448-4989';  
     insert obj_acct;
    bSure_CustomSelectContact ObjCSC = new bSure_CustomSelectContact();
    ApexPages.currentPage().getParameters().put('AccId', 'sId%'); 
    ApexPages.currentPage().getParameters().put('searchString', 'sId'); 
    
    //ObjCSC.CustomSelectContact();
    ObjCSC.search();
    ObjCSC.runSearch();
    ObjCSC.performSearch('searchString');
    ObjCSC.saveAccount() ;   
    ObjCSC.showErrorMessage('TestMessage');
    ObjCSC.ContactCancel();
    }
}