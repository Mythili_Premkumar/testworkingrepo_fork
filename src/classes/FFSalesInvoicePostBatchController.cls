global class FFSalesInvoicePostBatchController implements Database.Batchable<sObject>{
    global set<Id> setIds {get;set;}
    global FFSalesInvoicePostBatchController(set<Id> setIds) {
        this.setIds = setIds;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        String strQuery = 'Select Id from c2g__codaInvoice__c where ID IN:setIds';
        return Database.getQueryLocator(strQuery);
    }
    
    global void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> lstSI) {
        set<Id> setInvoiceID = new set<Id>();
        for(c2g__codaInvoice__c iterator : lstSI) {
            setInvoiceID.add(iterator.Id);
        }
        
        if(setInvoiceID.size() > 0)
         {
          System.debug('##########InvoiceIDS#####'+setInvoiceID.size());
            BillingWizardCommonController.postSalesInvoice(BillingWizardCommonController.generateCODAAPICommonReference(setInvoiceID));
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        String strErrorMessage = '';
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmail(new list<String>{a.CreatedBy.Email, 'speriyasamy2@csc.com'}, 'Sales Invoice Post ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
    }
}