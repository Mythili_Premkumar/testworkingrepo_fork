global class BillingAmountPopulationOnOLIFromLOBBatch Implements Database.Batchable <sObject> {
    global Database.queryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id, LOB_OLI_Ids__c, LOB_Billing_Amount__c FROM List_of_Business__c';
        return Database.getQueryLocator(SOQL);
    }

    global void execute(Database.BatchableContext bc, List<List_of_Business__c> listLOB) {
    	Map<Id, Order_Line_Items__c> mapOLI = new Map<Id, Order_Line_Items__c>(); 
    	for(List_of_Business__c LOB : listLOB) {
    		for(String OLIId : LOB.LOB_OLI_Ids__c.split(';')) {
    			mapOLI.put(OLIId, new Order_Line_Items__c(Id = OLIId, OLI_Billing_Amount__c = LOB.LOB_Billing_Amount__c));
    		}
    	}
    	if(mapOLI.size() > 0) {
    		update mapOLI.values();
    	}
    }

    global void finish(Database.BatchableContext bc) {
    }
}