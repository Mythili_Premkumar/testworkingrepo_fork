@
isTest(seealldata = true)
public class PrintLOBReport_v1Test {

    public static testMethod void testPrintLOBReportv1() {
        PageReference pageRef = Page.PrintLOBReport_v1;
        Test.setCurrentPage(pageRef);
        list < Account > lstAccount = new list < Account > ();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for (Account iterator: lstAccount) {
            if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            } else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            } else {
                newTelcoAccount = iterator;
            }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;

        List < String > telcosList1 = new List < String > ();
        List < String > telcosList = new List < String > ();

        for (Telco__c Telcolst: [Select Id, Name from Telco__c where Account__c = : newTelcoAccount.Id]) {
            if (Telcolst.Id != null)
                telcosList.add(String.valueof(Telcolst.get('Id')));

        }
        system.assertNotEquals(newTelcoAccount.ID, null);

        Division__c objDiv = TestMethodsUtility.createDivision();

        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;

        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;

        //positive test

        Apexpages.currentpage().getparameters().put('telcoProduct', String.valueof(objTelco.id));
        Apexpages.currentpage().getparameters().put('directoryId', String.valueof(objDir.id));
        Apexpages.currentpage().getparameters().put('Edition', String.valueof(objDirEd.id));
        Apexpages.currentpage().getparameters().put('IsNational', 'true');
        Test.startTest();
        PrintLOBReport_v1 PLOBR_P = new PrintLOBReport_v1();
        PLOBR_P.doGenerateCSV_email(telcosList, String.valueof(objDir.id), String.valueof(objDirEd.id));
        //Negative
        PLOBR_P.doGenerateCSV_email(telcosList1, String.valueof(objDir.id), String.valueof(objDirEd.id));
        PLOBR_P.doGenerateCSV_email(telcosList1, '', '');
        List<PrintLOBReport_v1.FinalWrapper> finalWrapList=new List<PrintLOBReport_v1.FinalWrapper>();
        finalWrapList.add(new PrintLOBReport_v1.FinalWrapper('Test','2343453456','AT-01245',500,null,'Monthly','Berry','9900999898',null,'Test',null,'Test','Test','12234544'));
        PLOBR_P.cancel();
        PLOBR_P.ExportReport();
        Test.stopTest();
    }

}