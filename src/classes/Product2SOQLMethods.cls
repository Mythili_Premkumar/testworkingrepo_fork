public with sharing class Product2SOQLMethods {
   
    public static map<ID, Product2> getProductMapByID(set<ID> productIds) {
        return new map<ID, Product2>([SELECT Id, Inventory_Tracking_Group__c, Print_Specialty_Product_Type__c FROM Product2 WHERE ID IN :productIds]);
    }
    public static list<Product2> getProductByUDAC(set<String> setUDAC) {
        return [SELECT Id, Name, ProductCode,AllowX15Discount__c,AllowX25Discount__c,AllowX30Discount__c,AllowX35Discount__c,AllowX40Discount__c,AllowX45Discount__c,AllowX50Discount__c,AllowX55Discount__c,AllowX60Discount__c,AllowX65Discount__c,AllowX70Discount__c from Product2 WHERE ProductCode IN:setUDAC and IsActive = true];
    }
     public static map<ID, Product2> getProductDetailsWithPBEByProductID(set<ID> productIds) {
        return new map<ID, Product2>([Select Name, IsBundle__c, IsActive, Id, Family,P4P_Eligible__c, Description, Product_Type__c, Product_or_Addon__c, 
                                            (Select Id, Name, Pricebook2Id, Product2Id, UnitPrice, IsActive, UseStandardPrice, ProductCode From PricebookEntries)
                                             From Product2 Where ID IN :productIds]);
    }
}