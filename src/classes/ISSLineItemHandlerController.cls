public with sharing class ISSLineItemHandlerController {
	public static void onAfterInsert(List<ISS_Line_Item__c> listISSLineItem) {
		populateCommission(listISSLineItem);
	}
	
	public static void onAfterUpdate(List<ISS_Line_Item__c> listISSLineItem) {
		populateCommission(listISSLineItem);
	}
	
	public static void populateCommission(List<ISS_Line_Item__c> listISSLineItem) {
		if(CommonVariables.ISSCommissionRepeatCheck) {
			CommonVariables.ISSCommissionRepeatCheck = false;
			Set<Id> ISSLineIds = new Set<Id>();
			
			for(ISS_Line_Item__c ISSLi : listISSLineItem) {
				ISSLineIds.add(ISSLi.Id);
			}
			
			listISSLineItem = ISSLineItemSOQLMethods.getISSLIByIds(ISSLineIds);
			
			for(ISS_Line_Item__c ISSLi : listISSLineItem) {
				ISSLi.Commission__c = ISSLi.Rate_for_Commission__c;
			}
			
			update listISSLineItem;
		}
	}
}