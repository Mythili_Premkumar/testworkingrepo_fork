public class ADPriceBookUtilityController {

    public static void deactivateEntry() 
    {
    
        List<PriceBookEntrySetting__c> pbsettinglist =PriceBookEntrySetting__c.getall().values();
        Set<Id> pricebookentriesId = new Set<Id>();
        List<PricebookEntry> updatePricebookentryList = new List<PricebookEntry>();
        for(PriceBookEntrySetting__c pbsetting : pbsettinglist)
        {
            pricebookentriesId.add(pbsetting.name);
        }
        for(PricebookEntry updatepbe: [Select id from PricebookEntry where Id In : pricebookentriesId])
        {
            updatepbe.IsActive = false;
            updatePricebookentryList.add(updatepbe);
        }
        if(updatePricebookentryList.size()>0)
        update updatePricebookentryList;
        
        if(pbsettinglist.size()>0)
        delete pbsettinglist;
        
        return;
   
   }


    public static void activateEntry()
    {
        
        List<PricebookEntry> pricebookentrylist = new List<PricebookEntry>();
        
        List<Id> InactivepricebookentriesId = new List<Id>();
        List<PriceBookEntrySetting__c> pbsettinglist = new List<PriceBookEntrySetting__c>();
        
        
        for(PricebookEntry pbentry : [SELECT Id,IsActive FROM PricebookEntry where IsActive=false])
        {
            PriceBookEntrySetting__c pbsetting = new PriceBookEntrySetting__c(Name=pbentry.id);
            pbsettinglist.add(pbsetting);
            InactivepricebookentriesId.add(pbentry.id);
            pbentry.IsActive =true;
            pricebookentrylist.add(pbentry);
        }
        
        if(pricebookentrylist.size()>0)
        update pricebookentrylist;
        
        if(pbsettinglist.size()>0)
        insert pbsettinglist;
    
        return;
    }
    
    
    public static void activateProduct()
    {
        List<Product2> productlist = new List<Product2>();
        
        List<Id> InactiveproductsId = new List<Id>();
        List<Product2Setting__c> productsettinglist = new List<Product2Setting__c>();
        
        
        for(Product2 product : [SELECT Id,IsActive FROM Product2 where IsActive=false])
        {
            Product2Setting__c productsetting = new Product2Setting__c(Name=product.id);
            productsettinglist.add(productsetting);
            InactiveproductsId.add(product.id);
            product.IsActive =true;
            productlist.add(product);
        }
        
        if(productlist.size()>0)
        update productlist;
        
        if(productsettinglist.size()>0)
        insert productsettinglist;
        
    }
    
    public static void deactivateProduct()
    {
        List<Product2Setting__c> productsettinglist =Product2Setting__c.getall().values();
        Set<Id> productsId = new Set<Id>();
        List<Product2> updateProductList = new List<Product2>();
        for(Product2Setting__c productsetting : productsettinglist)
        {
            productsId.add(productsetting.name);
        }
        for(Product2 updateproduct: [Select id from Product2 where Id In : productsId])
        {
            updateproduct.IsActive = false;
            updateProductList.add(updateproduct);
        }
        if(updateProductList.size()>0)
        update updateProductList;
        
        if(productsettinglist.size()>0)
        delete productsettinglist;       
       
    }





}