public with sharing class SACRACTransmissionHandlerController {
	public static void onBeforeInsert(List<SACRAC_Transmission__c> listSACRAC) {
		populateCMRAccount(listSACRAC);
        populateClientAccount(listSACRAC);
        populatePubAccount(listSACRAC);
        populateNatBillStatus(listSACRAC);
        populateDirEdition(listSACRAC);
	}
	
	public static void onBeforeUpdate(List<SACRAC_Transmission__c> listSACRAC) {
		populateCMRAccount(listSACRAC);
        populateClientAccount(listSACRAC);
        populatePubAccount(listSACRAC);
        populateNatBillStatus(listSACRAC);
        populateDirEdition(listSACRAC);
	}
	
	public static void onAfterInsert(List<SACRAC_Transmission__c> listSACRAC) {
		populateDirectoryEndSAC(listSACRAC);
	}
	
	public static void onAfterUpdate(List<SACRAC_Transmission__c> listSACRAC, Map<Id, SACRAC_Transmission__c> oldMap) {
		populateDirectoryEndSAC(listSACRAC);
	}
	
	private static void populateCMRAccount(list<SACRAC_Transmission__c> listSACRAC) {
        set<String> setCMRNumber = new set<String>();
        map<String, Account> mapCMRAccount = new map<String, Account>();
        for(SACRAC_Transmission__c iterator : listSACRAC) {
            if(String.isNotBlank(iterator.CMR_Number__c)) {
                setCMRNumber.add(iterator.CMR_Number__c);
            }
        }
        
        if(setCMRNumber.size() > 0) {
            list<Account> lstAccount = AccountSOQLMethods.getCMRAccountByNumber(setCMRNumber);
            for(Account iterator : lstAccount) {
                if(!mapCMRAccount.containsKey(iterator.AccountNumber)) {
                    mapCMRAccount.put(iterator.AccountNumber, iterator);
                }
            }        
        
	        if(mapCMRAccount.size() > 0) {
	            for(SACRAC_Transmission__c iterator : listSACRAC) {
	                if(mapCMRAccount.containsKey(iterator.CMR_Number__c)) {
	                    iterator.CMR_Name__c = mapCMRAccount.get(iterator.CMR_Number__c).Id;
	                }
	            }
	        }       
        }
    }
    
    private static void populateClientAccount(list<SACRAC_Transmission__c> listSACRAC) {
        set<String> setClientNumber = new set<String>();
        map<String, Account> mapClientAccount = new map<String, Account>();
        for(SACRAC_Transmission__c iterator : listSACRAC) {
            if(String.isNotBlank(iterator.Client_Number__c)) {
                setClientNumber.add(iterator.Client_Number__c);
            }
        }
        
        if(setClientNumber.size() > 0) {
            list<Account> lstAccount = AccountSOQLMethods.getClientAccountByNumber(setClientNumber);
            for(Account iterator : lstAccount) {
                if(!mapClientAccount.containsKey(iterator.AccountNumber)) {
                    mapClientAccount.put(iterator.AccountNumber, iterator);
                }
            }        
        
	        if(mapClientAccount.size() > 0) {           
	            for(SACRAC_Transmission__c iterator : listSACRAC) { 
                    if(mapClientAccount.containsKey(iterator.Client_Number__c)) {
                        iterator.Client_Name__c = mapClientAccount.get(iterator.Client_Number__c).Id;
                    }
	            }
	        }
   	 	}
    }
    
    private static void populatePubAccount(list<SACRAC_Transmission__c> listSACRAC) {
        set<String> setPubCode = new set<String>();
        map<String, Account> mapPubAccount = new map<String, Account>();
        for(SACRAC_Transmission__c iterator : listSACRAC) {
            if(String.isNotBlank(iterator.PUB_CODE__c)) {
                setPubCode.add(iterator.PUB_CODE__c);
            }
        }
        
        if(setPubCode.size() > 0) {
            list<Account> lstAccount = AccountSOQLMethods.getAccountByPubCode(setPubCode);
            for(Account iterator : lstAccount) {
                if(!mapPubAccount.containsKey(iterator.Publication_Code__c)) {
                    mapPubAccount.put(iterator.Publication_Code__c, iterator);
                }
            }        
        
	        if(mapPubAccount.size() > 0) {
	            for(SACRAC_Transmission__c iterator : listSACRAC) {
	                if(mapPubAccount.containsKey(iterator.PUB_CODE__c)) {
	                    iterator.Publication_Company__c = mapPubAccount.get(iterator.PUB_CODE__c).Id;
	                }
	            }
	        }       
        }
    }
    
    private static void populateDirEdition(list<SACRAC_Transmission__c> listSACRAC) {
        set<String> setDirCode = new set<String>();
        set<String> setLSAVerCode = new set<String>();        
        map<String, Directory_Edition__c> mapLSADirCode = new map<String, Directory_Edition__c>();
        for(SACRAC_Transmission__c iterator : listSACRAC) {
            if(String.isNotBlank(iterator.DIRECTORY_CODE__c)) {
                setDirCode.add(iterator.DIRECTORY_CODE__c);
            }
            if(String.isNotBlank(iterator.DIRECTORY_VERSION__c)) {
                setLSAVerCode.add(iterator.DIRECTORY_VERSION__c);
            }
        }
        
        if(setDirCode.size() > 0 && setLSAVerCode.size() > 0) {
            list<Directory_Edition__c> listDirEd = DirectoryEditionSOQLMethods.getDirEditionByLSACodeDirCode(setLSAVerCode, setDirCode);
            for(Directory_Edition__c iterator : listDirEd) {
                if(!mapLSADirCode.containsKey(iterator.LSA_Directory_Version__c + iterator.DIRECTORY_CODE__c)) {
                    mapLSADirCode.put(iterator.LSA_Directory_Version__c + iterator.DIRECTORY_CODE__c, iterator);
                }
            }        
        
	        if(mapLSADirCode.size() > 0) {
	            for(SACRAC_Transmission__c iterator : listSACRAC) {
	                if(mapLSADirCode.containsKey(iterator.DIRECTORY_VERSION__c + iterator.DIRECTORY_CODE__c)) {
	                    iterator.Directory_Edition__c = mapLSADirCode.get(iterator.DIRECTORY_VERSION__c + iterator.DIRECTORY_CODE__c).Id;
	                }
	            }
	        }       
        }
    }
    
    private static void populateNatBillStatus(list<SACRAC_Transmission__c> listSACRAC) {
        set<String> setDirCode = new set<String>();
        set<String> setLSAVerCode = new set<String>();        
        map<String, National_Billing_Status__c> mapLSADirCode = new map<String, National_Billing_Status__c>();
        for(SACRAC_Transmission__c iterator : listSACRAC) {
            if(String.isNotBlank(iterator.DIRECTORY_CODE__c)) {
                setDirCode.add(iterator.DIRECTORY_CODE__c);
            }
            if(String.isNotBlank(iterator.DIRECTORY_VERSION__c)) {
                setLSAVerCode.add(iterator.DIRECTORY_VERSION__c);
            }
        }
        
        if(setDirCode.size() > 0 && setLSAVerCode.size() > 0) {
            list<National_Billing_Status__c> listNatBill = NationalBillingStatusSOQLMethods.getNatBillByDirEdLSACodeDirCode(setLSAVerCode, setDirCode);
            for(National_Billing_Status__c iterator : listNatBill) {
                if(!mapLSADirCode.containsKey(iterator.Directory_Edition__r.LSA_Directory_Version__c + iterator.Directory_Edition__r.Directory_Code__c)) {
                    mapLSADirCode.put(iterator.Directory_Edition__r.LSA_Directory_Version__c + iterator.Directory_Edition__r.DIRECTORY_CODE__c, iterator);
                }
            }        
        
	        if(mapLSADirCode.size() > 0) {
	            for(SACRAC_Transmission__c iterator : listSACRAC) {
	                if(mapLSADirCode.containsKey(iterator.DIRECTORY_VERSION__c + iterator.DIRECTORY_CODE__c)) {
	                    iterator.National_Billing_Status__c = mapLSADirCode.get(iterator.DIRECTORY_VERSION__c + iterator.DIRECTORY_CODE__c).Id;
	                }
	            }
	        }       
        }
    }
    
    private static void populateDirectoryEndSAC(list<SACRAC_Transmission__c> listSACRAC) {
    	Map<Id, Id> mapDirEdIdSACRACId = new Map<Id, Id>();
    	for(SACRAC_Transmission__c iterator : listSACRAC) {
    		if(String.isNotBlank(iterator.Directory_Edition__c) && iterator.TRANS_CODE__c == 'DE') {
    			mapDirEdIdSACRACId.put(iterator.Directory_Edition__c, iterator.Id);
    		}
    	}
    	if(mapDirEdIdSACRACId.size() > 0) {
    		List<National_Billing_Status__c > listNatBill = NationalBillingStatusSOQLMethods.getNatBillByDirEdIds(mapDirEdIdSACRACId.keySet());
    		
    		if(listNatBill.size() > 0) {
    			for(National_Billing_Status__c NB : listNatBill) {
    				NB.Directory_End_SAC__c = mapDirEdIdSACRACId.get(NB.Directory_Edition__c);
    			}
    			update listNatBill;
    		}
    	}
    }
}