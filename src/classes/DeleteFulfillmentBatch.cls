/************************************************************
Batch class to send Fulfillment cancellations from MOLI Batch
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 08/13/2015
*************************************************************/
global class DeleteFulfillmentBatch implements Database.Batchable < sObject > , Database.AllowsCallouts {

    /*
    global Set<Id> setOlnIds = new Set<Id>();
    global DeleteFulfillmentBatch(Set<Id> OlSet) {

        this.setOlnIds = olSet;

    }
    */
    global List < Order_Line_Items__c > start(Database.BatchableContext BC) {

        List < Order_Line_Items__c > fnlLst = new List < Order_Line_Items__c > ();

        List < Order_Line_Items__c > allOLns = [SELECT id, Parent_ID__c, Cutomer_Cancel_Date__c, Cancellation__c, Order__r.Account__r.TalusAccountId__c, Digital_Product_Requirement__r.Id, Digital_Product_Requirement__r.OwnerId, Digital_Product_Requirement__r.Final_Status__c, Digital_Product_Requirement__r.Talus_Subscription_Id__c,
            Digital_Product_Requirement__r.Submit_Yodle_Cancel__c, Digital_Product_Requirement__r.Contact__c, Digital_Product_Requirement__r.Fulfillment_Submit_Status__c, Digital_Product_Requirement__r.Customer_Cancel_Date__c, Digital_Product_Requirement__r.Bundle__c, Spotzer_Bundle__c,
            Digital_Product_Requirement__r.Enterprise_Customer_ID__c, Digital_Product_Requirement__r.UDAC__c, Digital_Product_Requirement__r.business_name__c, Digital_Product_Requirement__r.business_email__c, Digital_Product_Requirement__r.business_url__c, Digital_Product_Requirement__r.business_phone_number_office__c,
            Digital_Product_Requirement__r.business_address1__c, Digital_Product_Requirement__r.business_city__c, Digital_Product_Requirement__r.business_state__c, Digital_Product_Requirement__r.business_postal_code__c, Digital_Product_Requirement__r.CreatedById, Billing_Contact__c, Digital_Product_Requirement__r.Effective_Date__c,
            Digital_Product_Requirement__r.Talus_DFF_Id__c, Status__c, Digital_Product_Requirement__c, Digital_Product_Requirement__r.Name, Digital_Product_Requirement__r.RecordTypeId, Order_Group__r.oli_count__c, Digital_Product_Requirement__r.RecordType.DeveloperName, Digital_Product_Requirement__r.Fulfillment_Type__c from Order_Line_Items__c
            where Action_Code__c = 'Cancel'
            and Cutomer_Cancel_Date__c <= today and Status__c not in ('Cancelation Requested', 'Cancelled') and media_type__c = 'Digital' 
            limit 50000
        ];
        //where id IN: setOlnIds 

        //Passing Queried List to parentAddonOLI method, in order to seperate records based on bundle logic
        fnlLst = DeleteTalusSubscription.parentAddonOLIs(allOLns);

        return fnlLst;

    }

    global void execute(Database.BatchableContext BC, List < Order_Line_Items__c > scope) {

        if (scope.size() > 0) {

            DeleteTalusSubscription.deleteSubscriptionNonFuture(scope);
        }

    }

    global void finish(Database.BatchableContext BC) {

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        //Send email to admins on Job completion 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List < String > admEmails = Label.Fulfillment_Batch_Notification_Emails.split(',');
        mail.setToAddresses(admEmails);
        mail.setSubject('Batch Job For Cancel Fulfillment Records has ' + a.Status + ' in BQA1');
        mail.setPlainTextBody('Batch Job has processed Total of ' + a.TotalJobItems +
            ' batches with ' + a.NumberOfErrors + ' failures');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });

    }
}