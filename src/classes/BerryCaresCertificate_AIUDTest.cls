@isTest(seeAllData = true)
public class BerryCaresCertificate_AIUDTest {
    static testMethod void testBerryCareCertificate() {
        Test.StartTest();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        /*Account newAccount = new Account();
        Contact newContact = new Contact();
        Id AccountCustomerRTId = TestMethodsUtility.getCustomerAccountRecordType();
        List<Account> newlstAccount = [Select Id, Name, Account_Manager__c,Primary_Canvass__c, RecordTypeId, (Select Id, Name, Email From Contacts Where Email <> Null) From Account Where Account_Manager__c != null and Primary_Canvass__c != null and RecordTypeId =: AccountCustomerRTId Limit 10];
        for(Account acc: newlstAccount){
            List<Contact> lstContact = acc.contacts;
            for(Contact con: lstContact){
                newContact = con;
                newAccount = acc;
                break;
            }
        } */       
        Case newCase = TestMethodsUtility.generateCase('CS Claim');
        newCase.AccountId = newAccount.Id; 
        newCase.ContactId = newContact.Id;   
        insert newCase;
        system.assertNotEquals(newAccount, null);    
        Berry_Cares_Certificate__c newBCC = TestMethodsUtility.generateBerryCaresCertificate(newAccount);
        newBCC.Case__c = newCase.Id;
        newBCC.CheckBCCValue__c = 'BCC_Checked';
        newBCC.UnCheckBCCValue__c = 'BCC_Unchecked';
        system.assertEquals(newBCC.Account__c, newAccount.Id);
        insert newBCC;
        /*newBCC.Branding__c = CommonMessages.frontierCare;
        newBCC.CheckBCCValue__c = 'BCC_Unchecked';
        newBCC.UnCheckBCCValue__c = 'BCC_Checked';
        update newBCC;*/
        /*
        User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null and userRole.Name = 'Account Manager' limit 1];
        
        Canvass__c c= new Canvass__c(Name='Test Canvas');
        insert c;
        
        Account a = new Account(Name='Test Account', Account_Manager__c=u.id, Primary_Canvass__c=c.id, Phone='4555522255', 
                                RecordTypeId = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCustomerRT, CommonMessages.accountObjectName), TalusAccountId__c='12');
        insert a;
        
        Contact cnt = new Contact(AccountId=a.id, FirstName='Test FName', Primary_Contact__c=false,
                                  MailingCity='Test Mailing City', MailingState='CA',
                                  MailingStreet='Test Mailing Street', MailingPostalCode='94538',
                                  LastName='Test LName', Email='test.test@test.com', Phone='(422)552-4422');
        insert cnt;
        
        Case cas = new Case(ContactId = cnt.Id);
        insert cas;
        
        Berry_Cares_Certificate__c berryCareCertificate = new Berry_Cares_Certificate__c(Account__c = a.id, Case__c = cas.Id, Branding__c = CommonMessages.berryCare);
        insert berryCareCertificate;
        berryCareCertificate.Branding__c = CommonMessages.frontierCare;
        update berryCareCertificate; */
        Test.stopTest();
    }
    static testMethod void testBerryCareCertificate2() {
        Test.StartTest();
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        /*Account newAccount = new Account();
        Contact newContact = new Contact();
        Id AccountCustomerRTId = TestMethodsUtility.getCustomerAccountRecordType();
        List<Account> newlstAccount = [Select Id, Name, Account_Manager__c,Primary_Canvass__c, RecordTypeId, (Select Id, Name, Email From Contacts Where Email <> Null) From Account Where Account_Manager__c != null and Primary_Canvass__c != null and RecordTypeId =: AccountCustomerRTId Limit 10];
        for(Account acc: newlstAccount){
            List<Contact> lstContact = acc.contacts;
            for(Contact con: lstContact){
                newContact = con;
                newAccount = acc;
                break;
            }
        }        */
        Case newCase = TestMethodsUtility.generateCase('CS Claim');
        newCase.AccountId = newAccount.Id; 
        newCase.ContactId = newContact.Id;   
        insert newCase;
        Berry_Cares_Certificate__c newBCC = TestMethodsUtility.generateBerryCaresCertificate(newAccount);
        newBCC.Case__c = newCase.Id;
        newBCC.Branding__c = CommonMessages.frontierCare;
        newBCC.CheckBCCValue__c = 'BCC_Unchecked';
        newBCC.UnCheckBCCValue__c = 'BCC_Checked';
        insert newBCC;
        Test.stopTest();
    }
}