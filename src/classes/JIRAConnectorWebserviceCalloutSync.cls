global class JIRAConnectorWebserviceCalloutSync {
 @future (callout=true)
    WebService static void synchronizeWithJIRAIssue( String caseId) {
        //Modify these variables:
        String username = 'SFDC Admin';
        String password = 'Berryadmin1asUtYaSsEuvITIW75vptllaS';
        String jiraURL = 'https://cscberryteam.atlassian.net';
        String connectionId = '4';
        String remoteObject ='CASE';
 
        HttpRequest req = new HttpRequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
        Blob headerValue = Blob.valueOf(username+':'+password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type','application/json');
        req.setHeader('Content-Length','0');
        req.setEndpoint(jiraURL+'/rest/customware/connector/1.0/'+connectionId+'/'+remoteObject+'/'+caseId+'/issue/synchronize.json');
        req.setMethod('PUT');
  
        try {
            res = http.send(req);           
        } catch(System.CalloutException e) {         
            System.debug(res.toString());
        }
    }
 
}