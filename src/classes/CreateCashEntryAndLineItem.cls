global class CreateCashEntryAndLineItem implements database.batchable<sObject>{
	String strWhere;
	public CreateCashEntryAndLineItem(string strWhere) {
		this.strWhere = strWhere;
	}
	global Database.QueryLocator start(Database.BatchableContext BC) {
		//String idAcc = '001Z000000z8VB3';
		String strQuery = 'Select Id from Account where ';// where Cash_batching_3__c = TRUE';
		strQuery = strQuery + strWhere;
		//String strQuery = 'Select Id from Account where ID =:idAcc';
		return Database.getQueryLocator(strQuery);
	} 
	
	global void execute(Database.BatchableContext BC, List<Account> lstAccount)  {
		c2g__codaCashEntry__c objCE = createCashEntry();
		insert objCE;
		list<c2g__codaCashEntryLineItem__c> lstChildCE = new list<c2g__codaCashEntryLineItem__c>();
		for(Account objAccount : lstAccount) {
			lstChildCE.add(createCashEntryLineItem(objAccount, objCE));
		}
		insert lstChildCE;
	}
	
	global void finish(Database.BatchableContext BC) {
		
	} 
	
	
	private c2g__codaCashEntry__c createCashEntry() {
		Date objDate = System.today().addDays(-30);
		return new c2g__codaCashEntry__c(Transaction_Type__c = 'P - Payment', Payment_Type__c = 'Check', c2g__BankAccount__c = 'a0HG000000LYRkB', 
		c2g__Date__c = objDate, c2g__Type__c = 'Receipt', c2g__PaymentMethod__c = 'Lockbox', c2g__Status__c = 'In Progress', ffcash__DerivePeriod__c = true, 
		ffcash__DeriveBankAccount__c = true, ffcash__DeriveBankAccountDimensions__c = true, ffcash__DeriveBankChargesAnalysis__c = true);
	}
	
	private c2g__codaCashEntryLineItem__c createCashEntryLineItem(Account objAcc, c2g__codaCashEntry__c objCE) {
		return new c2g__codaCashEntryLineItem__c(c2g__CashEntry__c = objCE.Id, c2g__Account__c = objAcc.Id, c2g__CashEntryValue__c = 100.00,
		Transaction_Types__c = 'P - Payment', Payment_Type__c = 'Check', Payment_Reason__c = 'P - Payment', c2g__AccountPaymentMethod__c = 'Lockbox');
	}
}