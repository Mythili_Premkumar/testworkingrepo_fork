@isTest
public class SectionHeadingMappingSOQLMethodsTest{
    static testMethod void SectionHeadingMappingSOQLMethodsTest(){
        Test.StartTest();
        Set<Id> setSHMIds = new Set<Id>();
        Set<String> DSDH = new Set<String>();
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Directory_Section__c newDirectorySection = TestMethodsUtility.createDirectorySection(newDirectory);
        Directory_Heading__c newDirectoryHeading = TestMethodsUtility.createDirectoryHeading();
        Section_Heading_Mapping__c newSectionHeadingMapping = TestMethodsUtility.generateSectionHeadingMapping(newDirectorySection.Id);
        newSectionHeadingMapping.Directory_Heading__c = newDirectoryHeading.Id;
        insert newSectionHeadingMapping;
        DSDH.add(newDirectorySection.Id+''+newDirectoryHeading.Id);
        setSHMIds.add(newSectionHeadingMapping.Id);
        set<Id> setdirSecIds = new set<Id>();
        setdirSecIds.add(newDirectorySection.id);
        set<Id> setdirHeadIds = new set<Id>();
        set<String> setdirHeadIdsString = new set<String>();
        setdirHeadIds.add(newDirectoryHeading.id);
        setdirHeadIdsString.add(newDirectoryHeading.id);
        System.assertNotEquals(null, SectionHeadingMappingSOQLMethods.getSecHeadMappingsByComboForIBAppearances(DSDH));
        System.assertNotEquals(null, SectionHeadingMappingSOQLMethods.getSecHeadMapByDirSecDirHeadIds(setdirSecIds,setdirHeadIds));
        System.assertNotEquals(null, SectionHeadingMappingSOQLMethods.getSecHeadMappingsByDHDS(setdirHeadIds, setdirSecIds));
        Test.StopTest();
    }
}