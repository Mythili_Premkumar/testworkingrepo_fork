public class NationalOrderCreationHandler_V2 {
  
    static Map<Id,Id> mapOLIIdCloneDFF = new Map<Id,Id>();

    public static void createNationalOrder(set<Id> setOpportunity, set<Id> accountID) {
        Map<Id, String> opptyLIIdNatGraphIdMap = new Map<Id, String>();
        list<Opportunity> lstOPP = OpportunitySOQLMethods.getOpportunityForNational(setOpportunity); //This will get NEW OppLI with Opportunity
        set<String> sepOPPLI = new set<String>();
        set<Id> setNSLID=new Set<Id>();
        for(Opportunity iteratorOPP : lstOPP) {
            for(OpportunityLineItem objOPP : iteratorOPP.OpportunityLineItems) {
                sepOPPLI.add(objOPP.Id);
                setNSLID.add(objOPP.National_Staging_Line_Item__c);
                if(String.isNotBlank(objOPP.National_Staging_Line_Item__r.National_Graphics_ID__c)){
                    opptyLIIdNatGraphIdMap.put(objOPP.Id, objOPP.National_Staging_Line_Item__r.National_Graphics_ID__c);
                }
            }
        }
       
       list<Order_Line_Items__c> lstOLINew = OrderLineItemSOQLMethods.getOLIbyNSLI(setNSLID); //Fetch OLI based on NSLI
       // list<Order_Line_Items__c> lstOLINew = OrderLineItemSOQLMethods.getOLIbyOPPLIIDs(sepOPPLI);
        System.debug('**********lstOLINew ********'+lstOLINew);
        map<Id, Order_Line_Items__c> mapOLIByNSLI = new map<Id, Order_Line_Items__c>();
        map<Id, Order_Line_Items__c> mapOSAndOPP = new map<Id, Order_Line_Items__c>();
        set<Id> setOLI = new set<Id>();
        for(Order_Line_Items__c objOLI : lstOLINew) {
            System.debug('**********objOLI.Seniority_Date__c********'+objOLI.Seniority_Date__c);
            mapOLIByNSLI.put(objOLI.National_Staging_Line_Item__c, objOLI);
            //mapOLIByNSLI.put(objOLI.Opportunity_line_Item_id__c, objOLI);
            mapOSAndOPP.put(objOLI.Opportunity__c, objOLI);
            if(objOLI.Seniority_Date__c != null) {
                setOLI.add(objOLI.id);
            }            
        }
        System.debug('**********setOLI********'+setOLI);
        set<Id> setOLIIDs = new set<Id>(); 
        list<Order__c> lstOrder = OrderSOQLMethods.getOrderForNationalByAccountID(accountID);
        map<Id, Order__c> mapOrd = new map<Id, Order__c>();
        for(Order__c iteratorOrd : lstOrder) {
            mapOrd.put(iteratorOrd.Account__c, iteratorOrd);
        }
        
        list<Order__c> lstInsertOrder = new list<Order__c>();
        map<Id, Order__c> mapInsertOrder = new map<Id, Order__c>();
        map<Id,OpportunityLineItem> mapOppLineItems=new map<Id,OpportunityLineItem >();
        map<Id, list<Order_Group__c>> mapInsertOrderGroup = new map<Id, list<Order_Group__c>>();
        map<Id, list<Order_Line_Items__c>> mapInsertOLI = new map<Id, list<Order_Line_Items__c>>();
        set<String> setOpppLineId = new set<String>();
        list<Order_Line_Items__c> lstOLI = new list<Order_Line_Items__c>();
        set<Id> setAccID = new set<Id>();
        Map<id,Digital_Product_Requirement__c> OLIDFFMap = new Map<id,Digital_Product_Requirement__c>();
        ID nRecordTypeID = CommonMethods.getRedordTypeIdByName(CommonMessages.nationaOLIRT, CommonMessages.OLIObjectName);
        
        
        for(Opportunity iteratorOPP : lstOPP) {
            if(mapOrd.get(iteratorOPP.AccountID) == null) {
                //Create Order
                if(!mapInsertOrder.containsKey(iteratorOPP.AccountID)) {
                    Order__c objOrder = newOrderforNational(iteratorOPP);
                    lstInsertOrder.add(objOrder);
                    mapInsertOrder.put(iteratorOPP.AccountID, objOrder);
                }
            }
            else {
                if(!setAccID.contains(iteratorOPP.AccountID)) {
                    setAccID.add(iteratorOPP.AccountID);
                    lstInsertOrder.add(mapOrd.get(iteratorOPP.AccountID));
                }
            }
            
            //Create A order set
            Order_Group__c orderGroup = newOrderSetNational(iteratorOPP, mapOSAndOPP);
            if(!mapInsertOrderGroup.containsKey(iteratorOPP.AccountID)) {
                mapInsertOrderGroup.put(iteratorOPP.AccountID, new list<Order_Group__c>());
            }
            mapInsertOrderGroup.get(iteratorOPP.AccountID).add(orderGroup);
            
            //Create Order Line Item -- Required for loop of opportunity product
            for(OpportunityLineItem oppLis : iteratorOPP.OpportunityLineItems) {
                setOpppLineId.add(oppLis.Id);
                mapOppLineItems.put(oppLis.Id,oppLis);
                Order_Line_Items__c objOLI = newOrderLineItemNational(oppLis, iteratorOPP, mapOLIByNSLI, nRecordTypeID, setOLI);
                if(!mapInsertOLI.containsKey(iteratorOPP.Id)) {
                    mapInsertOLI.put(iteratorOPP.Id, new list<Order_Line_Items__c>());
                }
                mapInsertOLI.get(iteratorOPP.Id).add(objOLI);
            }
        }
        
        if(lstInsertOrder.size() > 0) {
            if(lstInsertOrder.size() > 0) {
                upsert lstInsertOrder;
            }
            list<Order_Group__c> lstOS = new list<Order_Group__c>();
            for(Order__c iteratorOrd : lstInsertOrder) {
                list<Order_Group__c> lstTempOS = mapInsertOrderGroup.get(iteratorOrd.Account__c);
                for(Order_Group__c iterator : lstTempOS) {
                    iterator.Order__c = iteratorOrd.Id;
                }
                lstOS.addAll(lstTempOS);
            }
            
            if(lstOS.size() > 0) {
                upsert lstOS;
            }
            
           
            for(Order_Group__c iterator : lstOS) {
                list<Order_Line_Items__c> lstTempOLI = mapInsertOLI.get(iterator.Opportunity__c);
                for(Order_Line_Items__c iteratorOLI : lstTempOLI) {
                    iteratorOLI.Order__c = iterator.Order__c;
                    iteratorOLI.Order_Group__c = iterator.Id;
                }
                lstOLI.addAll(lstTempOLI);
            }
            
            if(lstOLI.size() > 0) {
                upsert lstOLI;
               
                for(Order_Line_Items__c iteratorOLI : lstOLI) 
                {
                    setOLIIDs.add(iteratorOLI.Id);
                    
                }
            }                 
        }
        if(mapOLIIdCloneDFF != null && mapOLIIdCloneDFF.size() > 0) {
            Set<Id> setOLIIdCloneDFF = mapOLIIdCloneDFF.keyset();
            String strQuery = CommonMethods.getCreatableFieldsSOQL('Digital_Product_Requirement__c');
            strQuery+=' where OrderLineItemID__c in :setOLIIdCloneDFF';         
            List<Digital_Product_Requirement__c> DFFList = Database.query(strQuery);            
            for(Digital_Product_Requirement__c DFF : DFFList){
                OLIDFFMap.put(DFF.id, DFF);
            }
        }
        Schema.DescribeSObjectResult d = Schema.SObjectType.Digital_Product_Requirement__c;
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        list<Digital_Product_Requirement__c> lstDFF = new list<Digital_Product_Requirement__c>();
        list<Order_Line_Items__c> lstOLIFinal = OrderLineItemSOQLMethods.getOLIForNationalDFF(setOLIIDs);
        map<id,Order_Line_Items__c> mapOLI=new map<id,Order_Line_Items__c>();  
        map<id,id> MapOppLiOLI =new map<id,id>();                                      
        map<Id,List<Order_Line_Items__c>> mapAnchorOLI=new map<Id,List<Order_Line_Items__c>>();
        map<string,Order_Line_Items__c>MapAnchor=new map<string,Order_Line_Items__c>();
        map<string,list<Order_Line_Items__c>>MapDisplay=new map<string,list<Order_Line_Items__c>>();
        //added by Mythili for ticket ATP-3231
      //  map<String, Id> mapRT = CommonMethods.getsObjectRecordTypeNameKeyset(CommonMessages.dffObjectName);
        
        for(Order_Line_Items__c iteratorOLI : lstOLIFinal) {
            
            if(iteratorOLI.Product2__r.Is_Anchor__c){
                    if(String.isNotBlank(iteratorOLI.strOLICombo__c)){
                        MapAnchor.put(iteratorOLI.strOLICombo__c,iteratorOLI);
                    }
                }
            
            mapOLI.put(iteratorOLI.Id,iteratorOLI);
            if(iteratorOLI.Product2__r.Trademark_Product__c !=null){
            MapOppLiOLI.put(iteratorOLI.Opportunity_line_Item_id__c,iteratorOLI.id); 
            }
            String PhoneLine='';
            if(iteratorOLI.Listing__r.Phone__c!=null)
            {
              PhoneLine=String.valueof(iteratorOLI.Listing__r.Phone__c).substring(String.valueof(iteratorOLI.Listing__r.Phone__c).length()-4,String.valueof(iteratorOLI.Listing__r.Phone__c).length());
            }
            String businessAddress='';
            if(iteratorOLI.Listing__r.Listing_Street_Number__c!=null)
            businessAddress += iteratorOLI.Listing__r.Listing_Street_Number__c;
            if(iteratorOLI.Listing__r.Listing_Street__c!=null){
            if(businessAddress.length()>0)
            businessAddress += ','+ iteratorOLI.Listing__r.Listing_Street__c;
            else
            businessAddress += iteratorOLI.Listing__r.Listing_Street__c;
            }
            if(iteratorOLI.Listing__r.Listing_PO_Box__c!=null){
            if(businessAddress.length()>0)
            businessAddress += ','+ iteratorOLI.Listing__r.Listing_PO_Box__c;
            else
            businessAddress += iteratorOLI.Listing__r.Listing_PO_Box__c;
            }
            Digital_Product_Requirement__c objDFF;
            if(mapOLIIdCloneDFF.containsKey(iteratorOLI.id)) {
                Digital_Product_Requirement__c objDFFOrig = OLIDFFMap.get(mapOLIIdCloneDFF.get(iteratorOLI.id));
                objDFF = OLIDFFMap.get(mapOLIIdCloneDFF.get(iteratorOLI.id)).clone(false, false, false, false);
                objDFF.URN_number__c = null;
                if(iteratorOLI.Digital_Product_Requirement__c != null) {
                    lstDFF.add(new Digital_Product_Requirement__c(id=iteratorOLI.Digital_Product_Requirement__c, OrderLineItemID__c = null));
                    //lstDFF.add(objDFF);
                }
            }
            else {
                objDFF = new Digital_Product_Requirement__c();
                if(iteratorOLI.Digital_Product_Requirement__c != null) {
                    objDFF.Id = iteratorOLI.Digital_Product_Requirement__c;
                }
            }
            objDFF.OpportunityId__c = iteratorOLI.Opportunity__c;
            objDFF.DFF_Product__c = iteratorOLI.Product2__c;
            objDFF.UDAC__c = iteratorOLI.Product2__r.ProductCode;
            objDFF.Account_Manager__c = iteratorOLI.Opportunity__r.Account_Manager__c;
            //objDFF.Sales_Rep_Email1__c = UserInfo.getUserEmail();
            //ATp-3883
            //Sales_Rep__c=UserInfo.getUserId(),
            objDFF.DFF_Sales_Rep_NickName__c = (iteratorOLI.Opportunity__r.Owner.CommunityNickname != null ? (iteratorOLI.Opportunity__r.Owner.CommunityNickname.length() > 20 ? iteratorOLI.Opportunity__r.Owner.CommunityNickname.substring(0, 20) : iteratorOLI.Opportunity__r.Owner.CommunityNickname) : '');
            objDFF.sales_rep_first_name__c = UserInfo.getFirstName();
            objDFF.sales_rep_last_name__c = UserInfo.getLastName();
            objDFF.DFF_Directory__c = iteratorOLI.directory__c;
            objDFF.DFF_Directory_Section__c = iteratorOLI.Directory_Section__c;
            objDFF.account__c = iteratorOLI.Opportunity__r.AccountId;
            //objDFF.Sales_Rep_s_Name__c = UserInfo.getName();
            objDFF.Sales_Rep_Email__c = UserInfo.getUserEmail();
            //objDFF.Account_ID__c = iteratorOLI.Opportunity__r.Account.account_number__c;
            objDFF.Destination_Phone_Website__c = iteratorOLI.Opportunity__r.Account.Phone;
            objDFF.Proof__c='No';
            //objDFF.Account_Manager_Name__c = iteratorOLI.Opportunity__r.Account.Account_Manager_Name__c;
            //objDFF.Account_Manager_Email__c = iteratorOLI.Opportunity__r.Account.Account_Manager_Email__c;
            objDFF.OrderLineItemID__c = iteratorOLI.Id;
            objDFF.Issue_Number__c=iteratorOLI.Edition_Code__c;
            objDFF.Directory_Number__c=iteratorOLI.Directory_Code__c;
            //objDFF.Directory_Section__c=iteratorOLI.Directory_Section__r.Name;
            objDFF.Directory_Section_Code__c=iteratorOLI.Directory_Section__r.Section_Code__c;
            objDFF.Directory_Edition__c=iteratorOLI.Directory_Edition__c;
            objDFF.Heading__c=iteratorOLI.Directory_Heading__r.Name;
            objDFF.Product_Name__c=iteratorOLI.Product2__r.Name;
            objDFF.business_name__c=iteratorOLI.Listing_Name__c;
            objDFF.business_address1__c=businessAddress ;
            objDFF.business_city__c =iteratorOLI.Listing__r.Listing_City__c;
            //objDFF.Main_Listed_Zip_Postal_Code__c =iteratorOLI.Listing__r.Listing_Postal_Code__c;
            objDFF.business_phone_number_office__c =iteratorOLI.Listing__r.Phone__c;
            objDFF.Advertised_Phone_Area_Code__c =iteratorOLI.Listing__r.Area_Code__c;
            objDFF.Advertised_Phone_Exchange__c =iteratorOLI.Listing__r.Exchange__c;
            objDFF.Advertised_Phone_Line__c = PhoneLine;
            objDFF.Advertised_Phone_Number__c=iteratorOLI.Listing__r.Phone__c;
            
            if(iteratorOLI.Product2__r.Trademark_Product__c==null){
               if(iteratorOLI.National_Staging_Line_Item__r.Is_Caption_Header__c)
               {
                 objDFF.Caption_Header__c=iteratorOLI.National_Staging_Line_Item__r.Is_Caption_Header__c;
               }
               else
               {
                 objDFF.Caption_Member__c=iteratorOLI.National_Staging_Line_Item__r.Is_Caption_Member__c;
               }
                 if(iteratorOLI.National_Staging_Line_Item__r.SPINS__c=='SP'){
                     objDFF.Caption_Display_Text__c=iteratorOLI.National_Staging_Line_Item__r.Caption_Display_Text__c;
                 }
                 else{
                  objDFF.Caption_Display_Text__c=String.valueof(iteratorOLI.National_Staging_Line_Item__r.Caption_Display_Text__c).toLowerCase();
                 }
                 objDFF.Caption_Phone_Number__c=iteratorOLI.National_Staging_Line_Item__r.Caption_Phone_Number__c;
                 objDFF.Caption_Indent_Order__c=iteratorOLI.National_Staging_Line_Item__r.Caption_Indent_Order__c;
                 objDFF.Caption_Indent_Level__c=iteratorOLI.National_Staging_Line_Item__r.Caption_Indent_Level__c;
            }
            
            Schema.RecordTypeInfo rtByName = rtMapByName.get(iteratorOLI.Product_Type__c);
            if(!Test.isRunningTest() && rtByName != null) {
                objDFF.RecordTypeId = rtByName.getRecordTypeId();
            }
            if(opptyLIIdNatGraphIdMap.containsKey(iteratorOLI.Opportunity_line_Item_id__c)){
                objDFF.National_Graphics_ID__c = opptyLIIdNatGraphIdMap.get(iteratorOLI.Opportunity_line_Item_id__c);
            }
            
        //added  by Mythili for ticket ATP-3231
     /*  if((objDFF.RecordTypeID==mapRT.get('Print Graphic') || objDFF.RecordTypeID==mapRT.get('Multi Ad') ||
                objDFF.RecordTypeID==mapRT.get('Print Listing Content') || objDFF.RecordTypeID==mapRT.get('Print Listing No Content')) && iteratorOLI.Directory_Section__r.Section_Page_Type__c=='YP' && objDFF.Heading_1__c!=null){
                objDFF.Heading__c=objDFF.Heading_1__r.Name;
         }else if((objDFF.RecordTypeID==mapRT.get('Print Graphic') || objDFF.RecordTypeID==mapRT.get('Multi Ad') ||
                objDFF.RecordTypeID==mapRT.get('Print Listing Content') || objDFF.RecordTypeID==mapRT.get('Print Listing No Content')) && iteratorOLI.Directory_Section__r.Section_Page_Type__c!='YP'){
                objDFF.Heading__c='No Heading - WP & Speciality';            
         } */
         
         if((objDFF.RecordType.Name=='Print Graphic' || objDFF.RecordType.Name=='Multi Ad' ||
            objDFF.RecordType.Name=='Print Listing Content' || objDFF.RecordType.Name=='Print Listing No Content') && iteratorOLI.Directory_Section__r.Section_Page_Type__c=='YP' && objDFF.Heading_1__c!=null){
            objDFF.Heading__c=objDFF.Heading_1__r.Name;
         }else if((objDFF.RecordType.Name=='Print Graphic' || objDFF.RecordType.Name=='Multi Ad' ||
            objDFF.RecordType.Name=='Print Listing Content' || objDFF.RecordType.Name=='Print Listing No Content') && iteratorOLI.Directory_Section__r.Section_Page_Type__c!='YP'){
            objDFF.Heading__c='No Heading - WP & Speciality';            
         }
         if(iteratorOLI.OL_Print_Specialty_Product_Type__c == CommonMessages.ALProd) {
            objDFF.Caption_Print_Phone_Number__c = true;
            objDFF.Caption_Print_Address__c = false;
        } else if(iteratorOLI.OL_Print_Specialty_Product_Type__c == CommonMessages.ELProd) {
            objDFF.Caption_Print_Phone_Number__c = false;
            objDFF.Caption_Print_Address__c = false;
        } else if(iteratorOLI.OL_Print_Specialty_Product_Type__c == CommonMessages.CrossRefListProd) {
            objDFF.Caption_Print_Phone_Number__c = false;
            objDFF.Caption_Print_Address__c = false;
        }
         
              lstDFF.add(objDFF);     
        }
        
        if(lstDFF.size() >0) {
            upsert lstDFF;
        }
        
        list<Order_Line_Items__c>  updateOLI = new list<Order_Line_Items__c>();
         //list<Digital_Product_Requirement__c>  updateDFFList = new list<Digital_Product_Requirement__c>();
        if(lstDFF.size() > 0) {
        
            for(Digital_Product_Requirement__c iteratorDFF : lstDFF) 
            {
                if(mapOLI.containskey(iteratorDFF.OrderLineItemID__c))
                {
                       Order_Line_Items__c iteratorOLI=mapOLI.get(iteratorDFF.OrderLineItemID__c);
                       if(mapOppLineItems.containskey(iteratorOLI.Opportunity_line_Item_id__c)){
                       if(mapOppLineItems.get(iteratorOLI.Opportunity_line_Item_id__c).Trade_Mark_Parent_Id__c!=null){
                           if(MapOppLiOLI.containskey(mapOppLineItems.get(iteratorOLI.Opportunity_line_Item_id__c).Trade_Mark_Parent_Id__c)){
                               iteratorOLI.Trademark_Finding_Line_OLI__c=MapOppLiOLI.get(mapOppLineItems.get(iteratorOLI.Opportunity_line_Item_id__c).Trade_Mark_Parent_Id__c);
                               
                           }
                       }
                    
                    }
                    iteratorOLI.Digital_Product_Requirement__c = iteratorDFF.Id;
                    if(iteratorOLI.Cutomer_Cancel_Date__c == null)
                    {
                        if(iteratorOLI.Product2__r.Print_Product_Type__c!=null){
                            if(!iteratorOLI.Product2__r.Is_Anchor__c && iteratorOLI.Product2__r.Print_Product_Type__c == 'Display') {
                                if(String.isNotBlank(iteratorOLI.strOLICombo__c)){
                                    if(MapAnchor.containskey(iteratorOLI.strOLICombo__c))
                                        iteratorOLI.Anchor_Listing_Caption_Header__c = MapAnchor.get(iteratorOLI.strOLICombo__c).Id;
                                }
                            }
                        }
                    }
                    updateOLI.add(iteratorOLI);
                    //updateDFFList.add(iteratorDFF);
                }
                
            }
            if(updateOLI.size()>0) update updateOLI;
            //if(updateDFFList.size()>0) update updateDFFList;
        }
    }
    
    private static Order__c newOrderforNational(Opportunity objOpportunity) {
        return new Order__c(Name = 'Order-'+objOpportunity.Account.Name, Account__c = objOpportunity.AccountId);
    }
    
    public static Order_Group__c newOrderSetNational(Opportunity objOpportunity, map<Id, Order_Line_Items__c> mapOSAndOPP) {
        Order_Group__c insertOrderGroup = new Order_Group__c(Name= 'OS-'+objOpportunity.Name, Opportunity__c = objOpportunity.Id, 
                                                Order_Account__c = objOpportunity.AccountId, Type__c=CommonMessages.osNational, IsActive__c = true, 
                                                National_Staging_Order_Set__c = objOpportunity.National_Staging_Order_Set__c);        
        if(mapOSAndOPP.size() > 0) {
            if(mapOSAndOPP.get(objOpportunity.Id) != null) {
                insertOrderGroup.Id = mapOSAndOPP.get(objOpportunity.Id).Order_Group__c;
                insertOrderGroup.Name = mapOSAndOPP.get(objOpportunity.Id).Order_Group__r.Name;
            }
        }
        return insertOrderGroup;
    }
    
    public static Order_Line_Items__c newOrderLineItemNational(OpportunityLineItem objOPPLI, Opportunity objOpportunity, map<Id, Order_Line_Items__c> mapOLIByNSLI, Id nRecordTypeID, set<id> setOLI) {
        boolean bolSetSeniorityDate = true;
        Order_Line_Items__c insertOLI = new Order_Line_Items__c(Opportunity_line_Item_id__c = objOPPLI.Id, Canvass__c = objOpportunity.Account_Primary_Canvass__c, 
        Opportunity__c = objOpportunity.Id, Product2__c = objOPPLI.PricebookEntry.Product2Id, Trans_Version__c = objOPPLI.Trans_Version__c, 
        Type__c = objOPPLI.Type__c, Transaction_ID__c = objOPPLI.Transaction_ID__c, Trans__c=objOPPLI.Trans__c, To__c=objOPPLI.To__c, 
        Publication_Company__c=objOPPLI.Publication_Company__c, Publication_Date__c=objOPPLI.Publication_Date__c, NAT__c=objOPPLI.NAT__c, 
        NAT_Client_ID__c=objOPPLI.NAT_Client_ID__c, Line_Number__c=objOPPLI.Line_Number__c, ListPrice__c=objOPPLI.Full_Rate__c, From__c=objOPPLI.From__c, 
        Directory__c=objOPPLI.Directory__c, Date__c=objOPPLI.Date__c, Account__c = objOPPLI.Client_Name__c, CMR_Number__c=objOPPLI.CMR_Number__c, 
        CMR_Name__c=objOPPLI.CMR_Name__c, Advertising_Data__c=objOPPLI.Advertising_Data__c, Action__c=objOPPLI.Action__c, 
        Client_Number__c = objOPPLI.Client_Number__c, Publication_Code__c = objOPPLI.Publication_Code__c, UnitPrice__c = objOPPLI.UnitPrice,
        Directory_Edition__c = objOPPLI.Directory_Edition__c, DAT__c = objOPPLI.DAT__c, RecordTypeId = nRecordTypeID,Discount__c=objOPPLI.Discount, 
        Payment_Duration__c = 1, Payments_Remaining__c = 1, Successful_Payments__c = 0, Billing_Partner__c = CommonMessages.berryTelcoName, 
        Product_Type__c =objOPPLI.Product_Type__c, Media_Type__c = objOPPLI.PricebookEntry.Product2.Media_Type__c, Directory_Section__c = objOPPLI.Directory_Section__c, Directory_Heading__c = objOPPLI.Directory_Heading__c,
        National_Staging_Line_Item__c = objOPPLI.National_Staging_Line_Item__c,Listing__c=objOPPLI.Listing__c
        ,Seniority_Date__c=objOPPLI.Seniority_Date__c,Billing_Entity__c=objOpportunity.Account_Primary_Canvass__r.Billing_Entity__c
        );
        if(mapOLIByNSLI.size() > 0) {
            Order_Line_Items__c objOLITemp = mapOLIByNSLI.get(objOPPLI.National_Staging_Line_Item__c);
            if(objOLITemp != null) {
                insertOLI.Id = objOLITemp.Id;
                /*if(objOLITemp.Directory_Heading__c != objOPPLI.Directory_Heading__c || objOLITemp.UDAC__c != objOPPLI.UDAC__c ||              
                objOLITemp.Listing__c != objOPPLI.Listing__c || objOLITemp.Digital_Product_Requirement__r.National_Graphics_ID__c != objOPPLI.OP_National_Graphics_ID__c ||
                objOLITemp.Listing__r.Phone__c != objOPPLI.Listing__r.Phone__c) {
                    mapOLIIdCloneDFF.put(objOLITemp.Id,objOLITemp.Digital_Product_Requirement__c);
                }*/
                
                if(objOLITemp.Digital_Product_Requirement__r.National_Graphics_ID__c != objOPPLI.OP_National_Graphics_ID__c) {
                    mapOLIIdCloneDFF.put(objOLITemp.Id,objOLITemp.Digital_Product_Requirement__c);
                }
                else {
                    insertOLI.Digital_Product_Requirement__c = objOLITemp.Digital_Product_Requirement__c;
                }
                /*if(setOLI.contains(objOLITemp.Id)) {
                    bolSetSeniorityDate = false;
                } */
                System.debug('**********insertOLI.Seniority_Date__c********'+insertOLI.id);             
            }
        }
        System.debug('**********insertOLI.Seniority_Date__c********'+insertOLI.Seniority_Date__c);
        /*
        if(bolSetSeniorityDate) {
            insertOLI.Seniority_Date__c = datetime.newInstance(objOPPLI.Date__c.year(), objOPPLI.Date__c.month(),objOPPLI.Date__c.day(),datetime.now().hour(), datetime.now().minute(),datetime.now().second());
        }*/
        if(objOPPLI.Action__c=='O'){
        insertOLI.isCanceled__c=true;
        //insertOLI.Talus_Cancel_Date__c=date.today();
        insertOLI.Status__c='Cancelation Requested';
        insertOLI.Cutomer_Cancel_Date__c=date.today();
        }
        return insertOLI;
    }
}