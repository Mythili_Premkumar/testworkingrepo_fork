global class changeemailonUserbatch implements Database.batchable<sObject>{

    public string addtext1;
    
    global changeemailonUserbatch (String addtext){
    addtext1 = addtext;
    }
  
    global Database.querylocator start(Database.batchablecontext bc){
        String query = 'select email from user';
        return Database.getquerylocator(query);
    }
    
    global void execute(Database.batchablecontext bc, List<user>listusrs){
             // to skip the trigger execution
        String strCurrentUserId = Userinfo.getUserId();
         if(strCurrentUserId.length() > 15) {
            strCurrentUserId = strCurrentUserId.substring(0, 15);
        }
        SkipTriggerExecution__c objSkip1 = SkipTriggerExecution__c.getvalues(CommonMessages.dffObjectName);
       objSkip1.User_IDs__c = strCurrentUserId ;
        update objSkip1;
    
    
    
        for(user users: listusrs){
        users.email= users.email + addtext1;
        }
        
        update listusrs;
        
        
    }
    
    global void finish(Database.batchablecontext bc){
    
        //revert the changes
     SkipTriggerExecution__c objSkip = SkipTriggerExecution__c.getvalues(CommonMessages.contactObjectName );
        objSkip.User_IDs__c = '';
        update objSkip;  
    }

}