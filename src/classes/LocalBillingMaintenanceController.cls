public with sharing class LocalBillingMaintenanceController {
	
	public String selectedType{get;set;}
	public Integer set1StartNum{get;set;}
	public Integer set1EndNum{get;set;}
	public Integer set2StartNum{get;set;}
	public Integer set2EndNum{get;set;}
	public Integer set3StartNum{get;set;}
	public Integer set3EndNum{get;set;}
	private static Set<String> priorRandoms;
	public Boolean show{get;set;}
	
	public LocalBillingMaintenanceController(){
		show=false;
	}
	
	public List<SelectOption> getTypes() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('First Month Print','First Month Print'));
		options.add(new SelectOption('Subsequent Print','Subsequent Print'));
		options.add(new SelectOption('Digital Local','Digital Local'));
		options.add(new SelectOption('Reconcile Statement','Reconcile Statement'));
		return options;
	}
	
	public void saveInvoiceRange(){
		List<SplitBatchJobs__c> splitBatchList=new List<SplitBatchJobs__c>();
		SplitBatchJobs__c spliBatchObj=null;
		String strName = '';
		if(set1StartNum!=null && set1EndNum!=null && set1StartNum!=0 && set1EndNum!=0){
			spliBatchObj=createSplitBatchJob(set1StartNum,set1EndNum, returnSettingsName(selectedType, '1'));
			splitBatchList.add(spliBatchObj);	
		}
		if(set2StartNum!=null &&  set2EndNum!=null && set2StartNum!=0 && set2EndNum!=0){
			spliBatchObj=createSplitBatchJob(set2StartNum,set2EndNum, returnSettingsName(selectedType, '2'));
			splitBatchList.add(spliBatchObj);	
		}
		if(set3StartNum!=null && set3EndNum!=null && set3StartNum!=0 && set3EndNum!=0){
			spliBatchObj=createSplitBatchJob(set3StartNum,set3EndNum, returnSettingsName(selectedType, '3'));
			splitBatchList.add(spliBatchObj);	
		}
		//Save SplitBatchJobs__c list in custom setting
		if(splitBatchList!=null && splitBatchList.size()>0){
			upsert splitBatchList Name;
			show=true;
		}
	}
	
	private String returnSettingsName(String selectedType, String strNumber) {
		String strName = '';
		if(selectedType.equals('First Month Print')) {
			strName = 'FMP_' + strNumber;
		}
		else if(selectedType.equals('Subsequent Print')) {
			strName = 'SubPrint_' + strNumber;
		}
		else if(selectedType.equals('Digital Local')) {
			strName = 'Digital_' + strNumber;
		}
		else if(selectedType.equals('Reconcile Statement')) {
			strName = 'Statement_' + strNumber;
		}
		return strName;
	}
	
	public SplitBatchJobs__c createSplitBatchJob(Integer startNum,Integer endNum, String strName){
		SplitBatchJobs__c spliBatchObj=new SplitBatchJobs__c();
		spliBatchObj.Name = strName;
		spliBatchObj.Type__c=selectedType;
		spliBatchObj.Start_Number__c=startNum;
		spliBatchObj.End_Number__c=endNum;
		//spliBatchObj.Name=generateRandomString(6);
		return spliBatchObj;
	}
	
	public void clearValue() {
		set1StartNum = null;
		set1EndNum = null;
		set2StartNum = null;
		set2EndNum = null;
		set3StartNum = null;
		set3EndNum = null;
		show = false;
	}
	
	 public static String generateRandomString(Integer length){
        if(priorRandoms == null)
            priorRandoms = new Set<String>(); 

        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        if(priorRandoms.contains(returnString)) {
            return 'a'+generateRandomString(length);
        } else {
            priorRandoms.add(returnString);
            return 'a'+returnString;
        }
    }

}