@isTest(seealldata=true)
public with sharing class TearPagesControllerTest {
    public static testMethod void tearPagesTest() {
        Test.StartTest();
        Canvass__c canvass = TestMethodsUtility.createCanvass();
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account pubAcct = TestMethodsUtility.generatePublicationAccount();
        insert pubAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.generateOrderSet(acct, ord, oppty);
        og.selected__c = true;
        insert og;
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(acct, cnt, oppty, ord, og);              
        oln.Quote_Signed_Date__c=system.today()+1;
        oln.Order_Anniversary_Start_Date__c=system.today()-5;
        oln.Talus_Go_Live_Date__c=system.today()+30;
        oln.UnitPrice__c = 300;
        insert oln; 
        
        Directory__c dir = TestMethodsUtility.generateDirectory();
        dir.Publication_Company__c = pubAcct.Id;        
        insert dir;
        
        Directory_Edition__c DE = TestMethodsUtility.generateDirectoryEdition(dir);
        DE.Pub_Date__c = system.today();
        insert DE;
        
        DE = [SELECT Id, Edition_Code__c, Directory__c, Directory__r.Directory_Code__c, Directory__r.Name FROM Directory_Edition__c WHERE Id = : DE.Id];
        
        National_Billing_Status__c natBillStatus = TestMethodsUtility.createNatBillStatus(pubAcct.Id, DE.Id);  
        
        TearPagesController TPC = new TearPagesController();
        TPC.pubCode = pubAcct.Publication_Code__c;
        TPC.editionCode = DE.Edition_Code__c;
        TPC.fetchDirs();
        TPC.dirId = dir.Id;
        TPC.mapDirIdDirCode.put(dir.Id, dir.Directory_Code__c);
        TPC.go();
        TPC.tearPagesReadyBool = true;
        TPC.tearPagesReady();
        TPC.tearPagesReadyBool = false;
        TPC.tearPagesReady();
        TPC.showNatInv();
        TPC.closePopup();
        TPC.goButtonCheck();
        TPC.updateSendToElite();
        TPC.updateSendDirIndex();
        TPC.updateStatusTable();
        TPC.updateSendTearPagesOnDirSecs();
        Test.StopTest();
    } 
}