global class testbatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id,Name,Highest_Spend_Canvass__c,(select id,Name,Total_Order_Value__c,Canvass__c from Order_Sets__r where OS_No_of_Digital_OLI__c!=0 or OS_No_of_Print_OLI__c!=0) from Account ';
                      
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accList) {
    }
    
    global void finish(Database.BatchableContext bc) {
    }
}