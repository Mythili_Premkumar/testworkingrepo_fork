global class MultiScopingBasedOnDirectory {
	webservice static void callMultiScopBatch(String dirId) {
		MultiScopingBatch obj = new MultiScopingBatch(dirId);
		Database.executeBatch(obj, 50);
	}
}