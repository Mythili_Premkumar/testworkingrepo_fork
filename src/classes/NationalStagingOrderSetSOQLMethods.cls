public with sharing class NationalStagingOrderSetSOQLMethods {
    /*public static List<National_Staging_Order_Set__c> getNationalStagingOrderSetListBytransUniqueIds(set<String> transUniqueIds){
        return [SELECT CMR_Number__c, CMR_Name__c,Publication_Code__c , Client_Number__c, Client_Name__c, Date__c, Directory__c, Directory_Number__c, 
                Directory_Edition__c, Directory_Edition_Number__c, Header_Error_Description__c, PreviousTransactionUniqueID__c,
                NAT__c, NAT_Client_Id__c, Name, OwnerId, Publication_Code__c, Publication_Company__c, Publication_Date__c, Id, 
                TRANS_Code__c,  Transaction_ID__c, Transaction_Unique_ID__c, Transaction_Version__c, Transaction_Version_ID__c, 
                (SELECT Id, Name, Line_Number__c, Action__c, UDAC__c, Product2__c, BAS__c, DAT__c, SPINS__c, Advertising_Data__c, Full_Rate_f__c, Sales_Rate_f__c, 
                Row_Type__c, Type__c, Discount__c, Ready_for_Processing__c, Line_Error_Description__c, National_Staging_Header__c, 
                Parent_ID__c, Transaction_Unique_ID__c, Auto_Number__c, Listing__c, Scoped_Listing__c FROM National_Staging_Line_Items__r)
                FROM National_Staging_Order_Set__c WHERE PreviousTransactionUniqueID__c IN : transUniqueIds];
    }*/
    
    public static list<National_Staging_Order_Set__c> getNationalStagingOrderSetById(Id NSOSId) {
        return [SELECT Directory_Edition__r.Name,Directory_Edition__r.National_Billing_Lockout_Date__c, Publication_Company__c,
        		Directory_Edition__r.Edition_Code__c,State__c,Directory_Version__c, CMR_Number__c, CMR_Name__c, Client_Number__c, Client_Name__c, Date__c, 
        		Directory__c, Directory_Number__c, Directory_Edition__c, Directory_Edition_Number__c, Header_Error_Description__c, CreatedDate,
                NAT__c, NAT_Client_Id__c, Name, OwnerId, Publication_Date__c, Id, Is_Converted__c,Ready_for_Process__c,
                TRANS_Code__c,  Transaction_ID__c,Publication_Code__c , Transaction_Unique_ID__c, Transaction_Version__c, Transaction_Version_ID__c, 
                Directory__r.Name, Client_Name__r.Name, CMR_Name__r.Name, From_Number__c, To_Number__c, opportunity__c,
                Late_Order__c, Approved_to_Process__c, (SELECT Id, New_Transaction__c FROM National_Staging_Line_Items__R)
                /***** commented for the task BFTHREE-1442 *****/
                
                /*, 
                (Select Id, Name, Advice_Query__c, Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c, 
                Process_Completed__c From Advice_Querys__r where Process_Completed__c = false)*/
                
                /***** End commented for the task BFTHREE-1442 *****/
                FROM National_Staging_Order_Set__c WHERE Id = : NSOSId];
    }
    
    /*public static list<National_Staging_Order_Set__c> getNationalStagingStandingOrders(Id NSOSId){
        return [SELECT Auto_Number__c,Publication_Code__c, Publication_Company__c, ChildReadyForProcess__c, Directory__r.Name,
                Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,
                Child_Total__c, Client_Name_Text__c, Client_Name__c, Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,
                CMR_Number__c,CORE_Migration_ID__c,Late_Order__c, Approved_to_Process__c, CreatedById, CreatedDate, Date__c, Directory_Edition_Number__c,
                Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,Header_Error_Description__c,
                Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId, To_Type__c,Transaction_ID__c,
                Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,                
                Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,
                (SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c, National_Staging_Header__r.CreatedDate,Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,
                Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,Listing__r.account__r.recordtype.name,
                Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
                Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,
                Name,National_Discount__c,National_Staging_Header__c,Parent_ID__c,Product2__r.Print_Product_Type__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c, 
                Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Caption_Header__c,National_Pricing__c,Processed_Order_Line_Item__c,
                Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,Is_Processed__c                
                FROM National_Staging_Line_Items__r where Standing_Order__c = true order by Line_Number__c)
                FROM National_Staging_Order_Set__c where id=:NSOSId];*/
                
                public static list<National_Staging_Order_Set__c> getNationalStagingStandingOrders(Id NSOSId){
        return [SELECT Directory__r.Name,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,
                Client_Name_Text__c, Client_Name__c, Client_Number__c,CMR_Name__c,Ready_for_Process__c,
                CMR_Number__c,Late_Order__c, Approved_to_Process__c, Date__c, Directory_Edition_Number__c,
                Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,
                Id,Is_Converted__c,Is_Ready__c,Name,NAT__c,OwnerId, To_Type__c,Transaction_ID__c,
                Publication_Date__c,Reference_Date__c,State__c,To_Number__c,                
                Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,
                (SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c,Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,
                BAS__c,DAT__c,
                Directory_Heading__c,Discount__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
                Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,
                Name,National_Staging_Header__c,Product2__r.Print_Product_Type__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c, 
                SPINS__c,Caption_Header__c,National_Pricing__c,
                Transaction_Id__c,UDAC__c                
                FROM National_Staging_Line_Items__r where Standing_Order__c = true order by Line_Number__c)
                FROM National_Staging_Order_Set__c where id=:NSOSId];
    }
    
    public static list<National_Staging_Order_Set__c> getNationalStagingLITXNHStandingOrders(Id NSOSId){
        return [SELECT Auto_Number__c,Publication_Code__c, Publication_Company__c, ChildReadyForProcess__c, Directory__r.Name,
                Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,
                Child_Total__c, Client_Name_Text__c,Client_Name__c, Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,
                CMR_Number__c,CORE_Migration_ID__c,Late_Order__c, Approved_to_Process__c, CreatedById, CreatedDate, Date__c, Directory_Edition_Number__c,
                Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,Header_Error_Description__c,
                Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId, To_Type__c,Transaction_ID__c,
                Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,                
                Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,
                (SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c, National_Staging_Header__r.CreatedDate,Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,
                Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,Listing__r.account__r.recordtype.name,Product2__r.Print_Product_Type__c,
                Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
                Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,
                Name,National_Discount__c,National_Staging_Header__c,Parent_ID__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c, 
                Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Caption_Header__c,National_Pricing__c,Processed_Order_Line_Item__c,
                Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,Is_Processed__c                
                FROM National_Staging_Line_Items__r where Standing_Order__c = true order by Transaction_Id__c)
                FROM National_Staging_Order_Set__c where id=:NSOSId];
    }
    
    public static list<National_Staging_Order_Set__c> getNSLITXNHStandingOrders(Id NSOSId,String TransType){
        List<National_Staging_Order_Set__c> ListNSOS=new List<National_Staging_Order_Set__c>();
        if(TransType=='T' || TransType=='N' || TransType=='H'){
        ListNSOS=[SELECT Auto_Number__c,Publication_Code__c, Publication_Company__c, ChildReadyForProcess__c, Directory__r.Name,
                Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,
                Child_Total__c, Client_Name_Text__c,Client_Name__c, Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,
                CMR_Number__c,CORE_Migration_ID__c,Late_Order__c, Approved_to_Process__c, CreatedById, CreatedDate, Date__c, Directory_Edition_Number__c,
                Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,Header_Error_Description__c,
                Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId, To_Type__c,Transaction_ID__c,
                Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,                
                Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,
                (SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c, National_Staging_Header__r.CreatedDate,
                Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,Product2__r.Print_Product_Type__c,
                Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,Listing__r.account__r.recordtype.name,
                Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
                Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,
                Name,National_Discount__c,National_Staging_Header__c,Parent_ID__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c, 
                Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Caption_Header__c,National_Pricing__c,Processed_Order_Line_Item__c,
                Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,Is_Processed__c                
                FROM National_Staging_Line_Items__r where Standing_Order__c = true order by Transaction_Id__c)
                FROM National_Staging_Order_Set__c where id=:NSOSId];
         }
         else{
         ListNSOS=[SELECT Auto_Number__c,Publication_Code__c, Publication_Company__c, ChildReadyForProcess__c, Directory__r.Name,
                Directory_Edition__r.National_Billing_Lockout_Date__c,Directory_Edition__r.Name,CMR_Name__r.Name, Client_Name__r.Name,
                Child_Total__c, Client_Name_Text__c, Client_Name__c, Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,
                CMR_Number__c,CORE_Migration_ID__c,Late_Order__c, Approved_to_Process__c, CreatedById, CreatedDate, Date__c, Directory_Edition_Number__c,
                Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c, From_Number__c,From_Type__c,Header_Error_Description__c,
                Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId, To_Type__c,Transaction_ID__c,
                Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,                
                Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,Override__c,CMR_Name__r.National_Credit_Status__c,
                (SELECT Needs_Review__c,Action__c,Standing_Order__c,Advertising_Data__c, National_Staging_Header__r.CreatedDate,Prior_Directory_Heading_SFDC_ID__c,Prior_UDAC_Group__c,
                Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,Listing__r.account__r.recordtype.name,
                Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
                Seniority_Date__c ,Listing__r.Disconnected__c,Directory_Section__c,Is_Changed__c,Seniority_Date_Reset__c,Product2__r.Print_Product_Type__c,
                Name,National_Discount__c,National_Staging_Header__c,Parent_ID__c,Product2__c,Ready_for_Processing__c,National_Graphics_ID__c, 
                Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,Caption_Header__c,National_Pricing__c,Processed_Order_Line_Item__c,
                Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c,Is_Processed__c                
                FROM National_Staging_Line_Items__r where Standing_Order__c = true order by Line_Number__c)
                FROM National_Staging_Order_Set__c where id=:NSOSId];
         }
         return ListNSOS;
    }
    
   
    
    public static list<National_Staging_Order_Set__c> getSortedNationalStagingOSWithLineItem() {
        return [Select Transaction_Version__c,Publication_Company__c,Transaction_Version_ID__c, Transaction_Unique_ID__c, Transaction_ID__c,  TRANS_Code__c, Publication_Date__c, Publication_Code__c,
                Name, NAT__c, NAT_Client_Id__c, Id, Directory__c, Directory_Number__c, Directory_Edition__c, 
                Directory_Edition_Number__c, Date__c, Client_Number__c, Client_Name__c, CMR_Number__c, CMR_Name__c, CMR_Name__r.Name, Auto_Number__c, Opportunity__c, 
                (Select Id, Name, Line_Number__c, Action__c, UDAC__c, Product2__c, BAS__c, DAT__c, SPINS__c, Advertising_Data__c, Full_Rate_f__c, Sales_Rate__c, 
                Row_Type__c, Type__c, Discount__c, Ready_for_Processing__c, Line_Error_Description__c, National_Staging_Header__c, 
                Parent_ID__c, Transaction_Unique_ID__c, Auto_Number__c, Listing__c, Scoped_Listing__c, Directory_Section__c, Directory_Heading__c,National_Discount__c, National_Graphics_ID__c From National_Staging_Line_Items__r 
                Where UDAC__c != null order by Line_Number__c)
                From National_Staging_Order_Set__c where Ready_for_Process__c = true and Is_Converted__c = false];
    }
    
    
    
    public static list<National_Staging_Order_Set__c> getNationalStagingOrderSetWithOpportunity() {
        return [Select Transaction_Version__c,Publication_Company__c,Publication_Code__c, Transaction_Version_ID__c, Transaction_Unique_ID__c, Transaction_ID__c, To_Type__c, To_Number__c, TRANS_Code__c,
        State__c, SAC_Date__c, Reference_Date__c, RecordTypeId, Ready_for_Process__c, Publication_Date__c, Publication_Company_Code_c__c, Opportunity__c, Number_of_Graphics__c, Name, NAT__c, NAT_Client_Id__c, Is_Converted__c, Informatica_Unique_ID__c, Id, From_Type__c, From_Number__c, Directory__c, 
        Directory_Version__c, Directory_Number__c, Directory_Edition__c, Directory_Edition_Number__c, Date__c, CreatedDate, Client_Number__c, Client_Name__c,
        ChildReadyForProcess__c, CMR_Number__c,Directory__r.Canvass__c, CMR_Name__c, CMR_Name__r.Name, (Select Id, RecordTypeId, StageName, Transaction_Unique_ID__c From Opportunities__r) From 
        National_Staging_Order_Set__c where Ready_for_Process__c = true and Is_Converted__c = false];
    }
 
    public static list<National_Staging_Order_Set__c> getNationalStagingOrderSetWithOpportunity(set<Id> setId) {
        return [Select Transaction_Version__c,Publication_Company__c,Publication_Code__c, Transaction_Version_ID__c, Transaction_Unique_ID__c, Transaction_ID__c, To_Type__c, To_Number__c, TRANS_Code__c,
        State__c, SAC_Date__c, Reference_Date__c, RecordTypeId, Ready_for_Process__c, Publication_Date__c, Publication_Company_Code_c__c, Opportunity__c, Number_of_Graphics__c, Name, NAT__c, NAT_Client_Id__c, Is_Converted__c, Informatica_Unique_ID__c, Id, From_Type__c, From_Number__c, Directory__c, 
        Directory_Version__c, Directory_Number__c, Directory_Edition__c, Directory_Edition_Number__c, Date__c, CreatedDate, Client_Number__c, Client_Name__c,
        ChildReadyForProcess__c, CMR_Number__c, CMR_Name__c,Directory__r.Canvass__c, CMR_Name__r.Name, (Select Id, RecordTypeId, StageName, Transaction_Unique_ID__c From Opportunities__r) From 
        National_Staging_Order_Set__c where Ready_for_Process__c = true and Is_Converted__c = false and ID IN:setId];
    }
    
    public static list<National_Staging_Order_Set__c> getNationalReferenceRecordByNationalOrderId(set<Id> setNSOSId) {
        return [SELECT Id,Publication_Company__c,Publication_Code__c,(SELECT Id,Line_Number__c, Action__c, BAS__c, DAT__c, SPINS__c, Advertising_Data__c, UDAC__c, Standing_Order__c From National_Staging_Line_Items__r where UDAC__c = null AND Standing_Order__c = true) 
                From National_Staging_Order_Set__c where Id IN:setNSOSId];
        
    }
    
    public static list<National_Staging_Order_Set__c> getNSOSByDirIdPubCodeEditionCodeWithoutActionO(String pubCode, String edtnCode, Id dirId) {
        return [SELECT Id, Publication_Company__c,Publication_Code__c ,(SELECT Id, Full_Rate_f__c, UDAC__c, Sales_Rate__c FROM National_Staging_Line_Items__r 
                WHERE Standing_Order__c = true AND Action__c != 'O'), (SELECT Id, InvoiceGenerationStatus__c FROM Order_Sets__r) FROM National_Staging_Order_Set__c WHERE 
                Directory__c = : dirId AND Publication_Company_Code_c__c = : pubCode AND Directory_Edition__r.Edition_Code__c = : edtnCode];
        
    }
    
    public static list<National_Staging_Order_Set__c> getNSOSByCMRAndClientNumberForTransI(set<String> setCMRNumber, set<String> setClientNumber) {
        return [SELECT Id, Publication_Company__c,Publication_Code__c,CMRClientCombo__c, Name FROM National_Staging_Order_Set__c WHERE CMR_Name__c IN : setCMRNumber AND Client_Number__c IN : setClientNumber 
                AND TRANS_Code__c = 'I'];
        
    }
}