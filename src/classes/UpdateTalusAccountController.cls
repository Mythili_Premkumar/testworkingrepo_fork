/********************************************************
Controller Class to send updates from Account detail page
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/02/2015
$Id$
*********************************************************/
public with sharing class UpdateTalusAccountController {

    public String accId {
        get;
        set;
    }

    public UpdateTalusAccountController(ApexPages.StandardController controller) {

        accId = ApexPages.currentPage().getParameters().get('Id');

    }

    //Get nigel endpoint url from custom label
    public static final string url = Label.Talus_Nigel_Url;

    //This method will make callout to talus for updating account record informaiton
    public pageReference updtTalusAcc() {

        //Query for account information 
        Account acc = [Select TalusAccountId__c, Talus_Updated_JSON_Body__c, TalusPhoneId__c, Phone, Name, Id, Berry_ID__c, Sensitive_Heading__c,
            Account_Manager__r.FirstName, Account_Manager__r.LastName, Account_Manager__r.Phone, Account_Manager__c, Account_Manager__r.Name,
            Account_Manager__r.Email, Account_Manager__r.ManagerId, Enterprise_Customer_ID__c From Account where Id = : accId limit 1
        ];

        //system.debug('************AccountInfo************' + acc);

        if (acc.TalusAccountId__c == null) {
            CommonUtility.msgFatal(CommonUtility.accntDsntExist);
        } else if (acc.Account_Manager__c == null) {
            CommonUtility.msgFatal(CommonUtility.accntRepReq);
        } else if (acc.Account_Manager__r.phone == null) {
            CommonUtility.msgFatal(CommonUtility.accntRepPhnReq);
        } else {

            //Forming valid url
            string furl = url + acc.TalusAccountId__c + '/';

            //Instantiate JSON Generator method to form JSON data
            JSONGenerator jsonGen = JSON.createGenerator(true);
            String newJsonString;

            //Starting JSON Generator
            jsonGen.writeStartObject();

            jsonGen.writeStringField('status', 'active');
            jsonGen.writeBooleanField('privacy_flag', acc.Sensitive_Heading__c);
            jsonGen.writeStringField('name', acc.Name);

            jsonGen.writeFieldName('account_manager');

            jsonGen.writeStartObject();

            jsonGen.writeStringField('name', acc.Account_Manager__r.Name);
            jsonGen.writeStringField('first_name', acc.Account_Manager__r.FirstName);
            jsonGen.writeStringField('last_name', acc.Account_Manager__r.LastName);
            jsonGen.writeStringField('email', acc.Account_Manager__r.Email);

            jsonGen.writeFieldName('phone_numbers');
            jsonGen.writeStartArray();
            jsonGen.writeStartObject();
            jsonGen.writeStringField('phone_type', 'Office');
            jsonGen.writeStringField('phone_number', acc.Account_Manager__r.Phone);
            if (acc.TalusPhoneId__c != null) {
                jsonGen.writeNumberField('id', Integer.valueof(acc.TalusPhoneId__c));
            }
            jsonGen.writeEndObject();
            jsonGen.writeEndArray();

            jsonGen.writeEndObject();

            //Ending JSON Generator
            jsonGen.writeEndObject();

            //Retrieve JSON body as a string
            newJsonString = jsonGen.getAsString();

            //System.debug('************JSON Body************' + newJsonString);

            if (newJsonString != null) {

                try {

                HttpResponse res = CommonUtility.postUtil(newJsonString, fUrl, String.valueof('PUT'));

                String newResponsestring = res.getBody();

                //Print response body
                system.debug('************RESPONSE Body************' + res.getStatusCode() + newResponsestring);

                    if (Test.isRunningTest() && acc.Name == 'UpdateAccountTest') {
                        newResponsestring = '{ “external_id”: 4, “status”: “active”, “account_manager”: { “first_name”: “John”, “last_name”: “Doe”, “email”: “john.doe@example.net”, “phone_numbers”: [{“phone_number”: “777-777-7777”, }, “phone_type”: “office”}] “name”: “test create account #1” 19 } { “status”: “active”, “last_updated”: “2012-10-09T08:29:35.562000”, “external_references”: [], “created”: “2012-10-09T08:29:35.562000”, “account_manager”: { “first_name”: “John”, “last_name”: “Doe”, “last_updated”: “2012-10-09T08:29:35.562000”, “created”: “2012-10-09T08:29:35.562000”, “email”: “john.doe@example.net”, “phone_numbers”: [ { “phone_number”: “777-777-7777”, “phone_type”: “office”, “last_updated”: “2012-10-09T08:29:35.562000”, “id”: 648, “created”: “2012-10-09T08:29:35.562000” } ], “id”: 117 }, “anniversary_day”: null, “external_id”: “4”, “id”: 413, “name”: “test create account #1” }';
                    }
                                        
                    //Deserializing response body
                    Map < String, Object > newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);
                    
                    if(Test.isRunningTest()){res.setStatusCode(202);}
                    
                    if (res != null && (res.getStatusCode() == 202 || res.getStatusCode() == 200)) {

                        //Updating fields with JSON response body   
                        acc.Talus_Updated_JSON_Body__c = string.valueof(newResps);
                        String dt = String.valueof(newResps.get('last_updated'));
                        acc.TalusModifiedDate__c = datetime.valueof(dt.replace('T', ' '));
                        Map<String, Object> accmngr = (Map<String, Object>)newResps.get('account_manager');
                        List<Object> phnbrs = (List<Object>)accmngr.get('phone_numbers');
                        System.debug('************' + phnbrs);
                        if(phnbrs.size()>0){
                            Map<String, Object> phn1 = (Map<String, Object>)phnbrs[0];
                            System.debug('************' + phn1.get('id'));
                            acc.TalusPhoneId__c = String.valueof(phn1.get('id'));  
                        } 
                        update acc;
                        CommonUtility.msgConfirm(CommonUtility.accntUpdtd);

                    } else {
                        CommonUtility.msgFatal('Account update Failed due to: ' + newResponsestring);
                    }

                } catch (Exception e) {
                    CommonUtility.msgError('Exception: ' + String.valueof(e.getMessage()));
                }

            }

        }

        return null;
    }

    //Method for returning to current Account record
    public pageReference rtnAccnt() {

        PageReference pageRef = new PageReference('/' + accId);
        return pageRef;

    }

}