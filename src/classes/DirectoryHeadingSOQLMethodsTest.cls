@isTest
public with sharing class DirectoryHeadingSOQLMethodsTest {
	static testMethod void DirectoryHeadingSOQLMethodsTest(){
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        System.assertNotEquals(objDH, null);
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {        
            System.assertNotEquals(null, DirectoryHeadingSOQLMethods.getDirectoryHeadingGetMediaTrax());
            DirectoryHeadingSOQLMethods.getDirectoryHeadingUpdateMediaTrax();
        }    		
	}
}