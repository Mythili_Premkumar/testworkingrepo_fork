global class changeemailoncontactbatch implements Database.batchable<sObject>{

   public string addtext2;
   
   global changeemailoncontactbatch(String addtext){
   addtext2 = addtext;

   }

    global Database.querylocator start(Database.batchablecontext bc){
        String soql = 'Select id,email from contact where email != null';
        return Database.getquerylocator(soql);
    }
    
    global void execute(Database.batchablecontext bc, List<contact>listcontact){
       
               
        
        for(contact c: listcontact){
        
            c.email = c.email+ addtext2;
        }

        
        update listcontact;
    }
     
    global void finish(Database.batchablecontext bc){
    
    SkipTriggerExecution__c objSkip = SkipTriggerExecution__c.getvalues(CommonMessages.contactObjectName );
        objSkip.User_IDs__c = '';
        update objSkip;  
        
    }
    }