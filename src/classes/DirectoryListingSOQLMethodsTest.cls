@isTest
private class DirectoryListingSOQLMethodsTest {

    static testMethod void DirectoryListingSOQLMethodsCoverage() {
        List<String> customerids = new List<String>();
        Set<Id> canvasId = new Set<Id>();        
        Account newAccountCustomer = TestMethodsUtility.createAccount('customer');
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccountCustomer.Id);
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping(newTelco);
        customerids.add(newAccountCustomer.Id);     
        canvasId.add(newAccountCustomer.Primary_Canvass__c);
        User u = TestMethodsUtility.generateStandardUser();
        System.runAs(u) {
            Test.startTest();
            System.assertNotEquals(null, DirectoryListingSOQLMethods.getDirectoryListingByCanvassID(canvasId));
            Test.stopTest(); 
        }
    }
}