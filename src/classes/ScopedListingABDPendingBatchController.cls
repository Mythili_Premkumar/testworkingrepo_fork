global class ScopedListingABDPendingBatchController implements Database.Batchable<sObject>, Database.Stateful{
    
    //Variables to store the values from the parameters
    private Id directoryId;
    private string TelcoId;
    private integer batchcount =0;
    private string strLocalForeign;
  
    private set<string> BusIndicator= new set<string>();
    private set<string> setDFValue= new set<string>();
    private set<string> rightselectedvalues = new set<string>();
    private set<String> setAreaCode = new set<String>();
    private set<string> setPhoneNumber = new set<string>();
   
    private List<Directory_Listing__c> lstDL; 
    private boolean ABDPendingFlag;
   
    global ScopedListingABDPendingBatchController (Id dirId,string UtelcoID, set<string> rtValues,set<String> UAreaCode,string UlocalForeign,set<string> UsetDFValue,set<string> UBusIndicator,boolean ABDFlag){
        if(dirId != null){
            directoryId = dirId;
        }
        if(UtelcoID != null){
            TelcoId= UtelcoID;
        }
        if(rtValues != null && rtValues.size()>0){
            rightselectedvalues.addAll(rtValues);
        }
        if(UAreaCode != null && UAreaCode.size()>0){
            setAreaCode.addAll(UAreaCode);
        }
        if(UBusIndicator != null && UBusIndicator.size()>0){
            BusIndicator.addAll(UBusIndicator);
        }
        if(UsetDFValue != null && UsetDFValue.size()>0){
            setDFValue.addAll(UsetDFValue);
        }
        strLocalForeign = UlocalForeign;
        ABDPendingFlag = ABDFlag;
      
    }
    global Database.QueryLocator  start(Database.BatchableContext bc){
        
        //Dynamic SOQL for fetching the Scoped Listing records
        
        String soqlquery='Select Id,Name,Directory__c,Directory_section__c,Directory_section__r.Name,Telco_Provider__c,Area_code__c,Exchange__c,Listing_State__c,Listing_City__c,Listing_Street__c,Phone_Number__c,Listing__c,Listing_Locality__c,Data_Feed_Type_2__c,ABD_Pending_Review__c,StrBOCDisconnect__c,Disconnected_Via_BOC_Purge__c,Disconnected__c,Bus_Res_Gov_Indicator__c from Directory_Listing__c where';
        
        if(directoryId != null){
            soqlquery=soqlquery+' Directory__c = :'+'directoryId'+' '+'AND';
        }
        if(TelcoId != null){
            soqlquery=soqlquery+' Telco_Provider__c != :'+'TelcoId'+' '+'AND';
        }
        if(rightselectedvalues.size()>0){
       
            soqlquery=soqlquery+' Directory_section__c NOT IN :rightselectedvalues'+' '+'AND';
        }
        if(BusIndicator != null){
            soqlquery=soqlquery+' Bus_Res_Gov_Indicator__c NOT IN :'+'BusIndicator'+' '+'AND';
        }
        if(setAreaCode.size()>0){
            soqlquery=soqlquery+' strAreaCodeExchange__c NOT IN :setAreaCode'+' '+'AND';
        }
        if(strLocalForeign != null){
            soqlquery=soqlquery+' Listing_Locality__c != :strLocalForeign'+' '+'AND';
        }
        if(setDFValue != null){
            soqlquery=soqlquery+' Data_Feed_Type_2__c NOT IN :setDFValue'+' '+'AND';
        }
        if(ABDPendingFlag == true){
            soqlquery=soqlquery+' ABD_Pending_Review__c = :ABDPendingFlag'+' '+'AND';
        }
        if(ABDPendingFlag == false){
            soqlquery=soqlquery+' ABD_Pending_Review__c = :ABDPendingFlag'+' '+'AND';
            soqlquery=soqlquery+' Disconnected__c = false'+' '+'AND';
        }
        soqlquery = soqlquery.removeEnd('AND');
        return database.getQuerylocator(soqlquery);
    }
   
    global void execute(Database.BatchableContext bc, List<Directory_Listing__c> objDirList){
        set<Id> objLstId = new set<Id>();
        list<directory_listing__c> objdirLstUpdate = new list<directory_listing__c>();
        list<Listing__c> objListUpdate = new list<Listing__c>();
        if(objDirList.size()>0){
           batchcount=batchcount+objDirList.size();
        }
        for(Directory_Listing__c objDir : objDirList){
            objLstId.add(objDir.Listing__c);
            if(objDir.ABD_Pending_Review__c == false){
                system.debug('****Enter ABD Batch****'+objDir);
                objDir.ABD_Pending_Review__c = true;
                if(objDir.Bus_Res_Gov_Indicator__c == 'Residential' && objDir.Disconnected_Via_BOC_Purge__c == false){
                	system.debug('*** enter for Scoped listing for ABD***');
                    objDir.Disconnected_Via_BOC_Purge__c = true;
                    objDir.Disconnected__c = true;
                    objDir.Disconnect_reason__c = 'Disconnect'; 
                }    
                objdirLstUpdate.add(objDir);
            }
            else{
                objDir.ABD_Pending_Review__c = false;
                 objdirLstUpdate.add(objDir);
            }
        }
        if(objdirLstUpdate.size()>0){
            update objdirLstUpdate;
        }
        if(objLstId.size()>0){
            list<listing__c> objListLst = [Select Id,Disconnected__c,Disconnect_reason__c,ABD_Pending_Review__c,Bus_Res_Gov_Indicator__c,Disconnected_Via_ABD__c from Listing__c where Id IN : objLstId and Disconnected__c=false];
                for(listing__c objList :objListLst){
                    if(objList.ABD_Pending_Review__c == false && !ABDPendingFlag){
                        objList.ABD_Pending_Review__c = true;
                        if(objList.Bus_Res_Gov_Indicator__c == 'Residential' && objList.Disconnected_Via_ABD__c == false){
                            objList.Disconnected_Via_ABD__c = true;
                            objList.Disconnected__c = true;
                            objList.Disconnect_reason__c = 'Disconnect';
                        }   
                        objListUpdate.add(objList);
                    }
                    else if(ABDPendingFlag){
                        objList.ABD_Pending_Review__c = false;
                        objListUpdate.add(objList);
                    }
                }
        }
        if(objListUpdate.size()>0){
            Update objListUpdate;
        }
    }

    global void finish(Database.BatchableContext bc){
        SkipTriggerExecution__c objSkip = SkipTriggerExecution__c.getvalues(CommonMessages.LObjectName);
        objSkip.User_IDs__c = '';
        update objSkip;
        
        Directory__c objDir = [Select Id,Name from Directory__c where ID =:directoryId LIMIT 1];
        String strErrorMessage = '';
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'ABD Pending Review Update :' + a.Status + ' for Directory ' + objDir.Name, 'The batch Apex job Items processed ' + a.JobItemsProcessed+
          ' & Total number of records ' + batchcount + ' with  '+ a.NumberOfErrors +'  failures '+ strErrorMessage + '. Successfull records'); 
    }
  
}