@isTest
public class NationalRateCardControllerTest{

    public static testmethod void NationalRateCardControllerTest(){
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.LOY__c = true;
        newProduct.AllowX15Discount__c = true;
        newProduct.AllowX25Discount__c = true;
        newProduct.AllowX30Discount__c = true;
        newProduct.AllowX35Discount__c = true;
        newProduct.AllowX40Discount__c = true;
        newProduct.AllowX45Discount__c = true;
        newProduct.AllowX50Discount__c = true;
        newProduct.AllowX55Discount__c = true;
        newProduct.AllowX60Discount__c = true;
        newProduct.AllowX65Discount__c = true;
        newProduct.AllowX70Discount__c = true;
        insert newProduct;
        Directory_Product_Mapping__c objDP = TestMethodsUtility.generateDirectoryProductMapping(objDir);
        objDP.Product2__c = newProduct.id;
        insert objDP;
        
        ApexPages.StandardController objA = new ApexPages.StandardController(objDir);
        ApexPages.currentPage().getParameters().put('type', 'local');
        NationalRateCardController_v3 objASHC = new NationalRateCardController_v3(objA);
        objASHC.onLoad();
        objASHC.changeType();
        objASHC.doCalculaion();
        objASHC.doCalculaionPerProduct();
        objASHC.cancel();
        objASHC.save();
        objASHC.exportXLS();
        objASHC.generateCSV();
        //objASHC.GenerateNCSVFileClass();
        
        
        ApexPages.currentPage().getParameters().put('type', 'national');
        objASHC = new NationalRateCardController_v3(objA);
        objASHC.onLoad();
        objASHC.generateCSV();
        NationalRateCardController_v3.NationalPriceClass np = new NationalRateCardController_v3.NationalPriceClass();
        np.updateRate = 1;
        
        
        NationalRateCardController_v3.NationalPriceChildClass npc = new NationalRateCardController_v3.NationalPriceChildClass();
        npc.allowX25 = true;
        npc.fullRate = 30.9;
        npc.productId = newProduct.id;
        np.lstChild.add(npc);
        
        objASHC.objNationalNP = np;
        //np.updateRate = 1;
        objASHC.doCalculaion();
        objASHC.doCalculaionPerProduct();

    }
}