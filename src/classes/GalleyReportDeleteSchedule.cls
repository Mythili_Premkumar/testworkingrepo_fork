global class GalleyReportDeleteSchedule implements Schedulable {
    global void execute(SchedulableContext SC) {
        GalleyReportStageDeleteBatch obj = new GalleyReportStageDeleteBatch();
        Database.executeBatch(obj, 2000);
    }
}