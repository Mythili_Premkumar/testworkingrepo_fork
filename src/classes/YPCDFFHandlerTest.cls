@isTest
public class YPCDFFHandlerTest {
	static testmethod void dfftest() {
		Test.starttest();
		Digital_Product_Requirement__c dff1 = new Digital_Product_Requirement__c(
	    business_name__c ='b name',
	    business_address1__c ='b addr',
	    business_city__c ='b city',
	    business_state__c ='b state1',
	    business_postal_code__c ='77777',
	    dir_codes__c ='bc0000',
	    URN_Number__c = 4324.0,
	    omit_phone_ind__c ='N',
	    Logo_Graphic_Url__c='https://www.yahoo.com',
	    Production_Notes_for_PrintAdOrderGraphic__c ='NoteAd1',
	    Production_Notes_for_Logo__c ='NoteLogo1',
	    Production_Notes_for_Banner_Graphic__c ='NoteBanner1',
	    Banner_Url__c='http://www.facebook.com',
	    RecordTypeID = system.label.TestDFFPrintGraphicsRT);
	    
	    insert dff1;
	    
	    list<YPC_Graphics__c> lstYPCGraphics = new list<YPC_Graphics__c>();
	    lstYPCGraphics.add(new YPC_Graphics__c(name = 'test', dff__c = dff1.id, YPC_Graphics_type__C = 'Banner', Ypc_Graphics_URL__c = 'www.yahoo.com', Miles_Status__c ='In Progress'));
	    lstYPCGraphics.add(new YPC_Graphics__c(name = 'test1', dff__c = dff1.id, YPC_Graphics_type__C = 'Diamond Logo', Ypc_Graphics_URL__c = 'http://www.yahoo.com', Miles_Status__c ='In Progress'));
	    lstYPCGraphics.add(new YPC_Graphics__c(name = 'test2', dff__c = dff1.id, YPC_Graphics_type__C = 'Diamond Distribution', Ypc_Graphics_URL__c = 'http://www.yahoo.com', Miles_Status__c ='In Progress'));
	    lstYPCGraphics.add(new YPC_Graphics__c(name = 'test3', dff__c = dff1.id, YPC_Graphics_type__C = 'Banner Distribution', Ypc_Graphics_URL__c = 'http://www.yahoo.com', Miles_Status__c ='In Progress'));
	   
	    insert lstYPCGraphics;
	   
	    for(YPC_Graphics__c iteraor : lstYPCGraphics) {
	   		iteraor.ypc_Graphics_URL__c= 'http://www.google.com';
	   		iteraor.Miles_Status__c = 'Complete';
	    }
	   
	    update lstYPCGraphics;
	   
	    Test.stoptest();
   }
}