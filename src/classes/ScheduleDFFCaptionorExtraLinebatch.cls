global class ScheduleDFFCaptionorExtraLinebatch   Implements Schedulable {
    global void execute(SchedulableContext sc) {
        DFFCaptionorExtraLineContentTextbatch  obj = new DFFCaptionorExtraLineContentTextbatch  ();
        Database.executeBatch(obj);
    }
}