public class ScopedDirListings_v1 {

    public static void DirListingIdentfier(List<Directory_Listing__c> lstDirectoryListings,list<service_order_stage__c> lstSOCM, set<String> setDirectory, set<String> setLeadAssign, set<String> setExistDL,map<String,Service_Order_Stage__c> mapServiceOrderDataforDirLstngs,map<Id,Id> mapSOUC,map<Id,Id>mapSOAssocList,map<Id,Id> mapSOAssocDirList,set<Id> setUCID) {
    
        List<Directory_Listing__c> lstMasterDirectoryListings = new List<Directory_Listing__c>();
        map<Id, Directory__c> mapDirectory = new map<Id, Directory__c>([Select Id, (Select Id, Name, Section_Page_Type__c, Directory__c From Directory_Sections__r where Section_Page_Type__c='YP') From Directory__c where ID IN:setDirectory]);
        list<Lead_Assignment_Rules__c> lstLeadAssign = [SELECT Area_Code__c,Directory_Section__c,Directory__c,Exchange__c,Id,Name,Telco__c,strLeadAssgnCombo__c FROM Lead_Assignment_Rules__c WHERE strLeadAssgnCombo__c IN :setLeadAssign];
        map<String, Lead_Assignment_Rules__c> mapLead = new map<String, Lead_Assignment_Rules__c>();
        for(Lead_Assignment_Rules__c iterator : lstLeadAssign) { 
            system.debug('Lead String : '+ iterator.strLeadAssgnCombo__c);
            mapLead.put(iterator.strLeadAssgnCombo__c, iterator);
        }
        if(lstDirectoryListings.size() > 0) {
            insert lstDirectoryListings;
        }
        lstMasterDirectoryListings.addAll(lstDirectoryListings);
        
        //Adding as per the new change in SO batch process
         List<Service_Order_Stage__c> objSOAssociateLst = new List<Service_Order_Stage__c>(); 
        map<Id,Id> mapassocListing = new map<Id,Id>();
        map<Id,Id> mapassocDirListing = new map<Id,Id>();       
        //Associating Directory Listing to Service Order Record && populating scheduled process
        system.debug('***********SOS ENTRE HERE ASSOCIATION**************'+lstDirectoryListings);
        for(Directory_Listing__c objDirList : lstDirectoryListings){
            String strDirList = objDirList.SL_Last_Name_Business_Name__c+''+objDirList.Phone_Number__c+''+objDirList.Listing_Street_Number__c+''+objDirList.Listing_Street__c+''+objDirList.Listing_City__c+''+objDirList.Listing_State__c+''+objDirList.Listing_Postal_Code__c+''+objDirList.Listing_Country__c+''+objDirList.Directory__c+''+objDirList.Directory_Section__c+''+objDirList.Service_Order__c;
                if(objDirList.Bus_Res_Gov_Indicator__c == 'Residential'){
                    strDirList = strDirList+''+objDirList.First_Name__c;
                }
                if(mapServiceOrderDataforDirLstngs.get(strDirList) != null){
                system.debug('***********SOS ENTRE HERE ASSOCIATION string combination**************'+strDirList);
                    Service_Order_Stage__c objSerOr = new Service_Order_Stage__c(Id = mapServiceOrderDataforDirLstngs.get(strDirList).Id);
                        if(objSerOr.Associated_to_Dir_Listing__c == null){
                                objSerOr.Associated_to_Dir_Listing__c = objDirList.Id;
                                objSerOr.Associated_to_Listing__c = objDirList.Listing__c;
                                objSerOr.Scheduled_Process_Complete__c = true;
                                objSerOr.On_Hold_Due_to_Blackout__c = false;
                                objSOAssociateLst.add(objSerOr);
                                if(objSerOr.Associated_to_Listing__c  != null){
                                    mapassocListing.put(objSerOr.Id,objSerOr.Associated_to_Listing__c);
                                }
                                if(objSerOr.Associated_to_Dir_Listing__c  != null){
                                    mapassocDirListing.put(objSerOr.Id,objSerOr.Associated_to_Dir_Listing__c);
                                }
                        }
                }
        }
        if(objSOAssociateLst.size()>0){
            Update objSOAssociateLst;
        }
        system.debug('###############masterdata dirlistings'+lstMasterDirectoryListings.size());
        //to check crossreference indicator
        map<Id,directory_listing__c> mapDirCR = new map<Id,directory_listing__c>();
        set<Id> setDirId = new set<Id>();
        set<Id> setLstId = new set<Id>();
        if(lstMasterDirectoryListings.size() > 0) {
            for(directory_listing__c objdirCR : lstMasterDirectoryListings){
                setDirId.add(objdirCR.Id);
                if(objdirCR.Bus_Main_or_Additional__c == 'Additional'){
                    setLstId.add(objdirCR.Listing__c);
                }
            }
        }
        list<directory_listing__c> dirCRLst = [Select id,Cross_reference_indicator__c,Directory_code__c from directory_listing__c where Id in :setDirId];
         if(dirCRLst.size() > 0) {
            for(directory_listing__c objdirCR : dirCRLst){
                mapDirCR.put(objdirCR.Id,objdirCR);
            }
        }
        system.debug('***set listing id**'+setLstId);
        //Associating Main Listing to Additonal Listing
        list<listing__c> addLst = new list<listing__c>();
        if(setLstId.size()>0){
            addLst = [Select Id,Main_Listing__c,service_order_stage_item__c from listing__c where Id IN : setLstId];
        }
        system.debug('***Additional Listing record***'+addLst);
        list<listing__c> addLstUpdate = new list<listing__c>();
        for(listing__c objLstAdd : addLst){
            if(objLstAdd.Main_Listing__c == null){
            system.debug('***Additional Listing record***');
                objLstAdd.Main_Listing__c = mapassocListing.get(mapSOUC.get(objLstAdd.service_order_stage_item__c));
                addLstUpdate.add(objLstAdd);
            }
        }
        if(addLstUpdate.size()>0){
            update addLstUpdate;
        }
        /*map<String, Directory_Listing__c > mapDL = new map<String, Directory_Listing__c >();
        system.debug('Exists Set1 : '+ setExistDL);
        list<Directory_Listing__c> lstDirList = new list<Directory_Listing__c>();
        if(setExistDL.size()> 0){
            lstDirList = DirectoryListingSOQLMethods.fetchExistingDL(setExistDL);
        }
        system.debug('Existing List : '+ lstDirList);
        for(Directory_Listing__c DL : lstDirList){  
            mapDL.put(DL.SL_StrPhoneDLDS__c, DL);
        }*/
        list<string>productnamelist=new list<string>{'WCRL','WRL','WRLF','SRL'};
        Map<String, Product2> mapProducts = new Map<String, Product2>();
        for(Product2 setProd : [SELECT Id, Name,ProductCode from Product2 WHERE ProductCode IN:productnamelist]) {
            mapProducts.put(setProd.ProductCode,setProd);
        }
        list<Directory_Listing__c> additionalDL=new list<Directory_Listing__c>();
        Map<String, RecordType> mapRecordType = new Map<String, RecordType>();
        List <RecordType> rec_type = [SELECT ID, Name from RecordType where SObjectType = 'Directory_Listing__c'];
        for(RecordType rec_type_new : rec_type){
            mapRecordType.put(rec_type_new.Name, rec_type_new);
        }
        set<string> setUPDC = new set<string>();
        setUPDC.addall(System.Label.UCONNPrintDirectoriesCode.split(','));
        for(Directory_Listing__c DL : lstMasterDirectoryListings) {
            String strDirSec;
            String strDir;
            String getphone;
            String strSOSCombination;
            if(DL.Directory_Section__c != null){
                strDirSec = DL.Directory_Section__c;
                
            }
            if(DL.Directory__c != null){
                strDir = DL.Directory__c;
                
            }
            if((DL.Phone_Number__c != null && DL.Phone_Number__c.contains('-')==true)){
                getphone=DL.Phone_Number__c.Replace('(','').Replace(') ','-');
            }
            list<string>PhoneLst = new list<string>();
            if(getphone != null){
                PhoneLst=getphone.split('-');
            }
            if(PhoneLst.size()>0 && strDir != null && strDirSec != null){
                 strSOSCombination = PhoneLst[0] + PhoneLst[1]+''+strDir.left(15)+''+strDirSec.left(15);
            }
            if(DL.Data_Feed_Type_2__c != 'YPONLY'){
                if(DL.DataFeedType__c != 'EAS' && mapLead.containsKey(strSOSCombination)){
                    string combos = DL.Phone_Number__c+''+strDir.left(15)+''+strDirSec.left(15);
                   // if(mapDL.containsKey(combos)) {
                        if(DL.Caption_Header__c != true || DL.Caption_Member__c != true) {
                            if(DL.Disconnected__c != true && DL.Disconnected_Via_BOC_Purge__c != true) {
                                if(mapDirCR.get(DL.Id).Cross_reference_indicator__c == true) {
                                DL.RecordTypeId = mapRecordType.get('Non Override RT').Id;
                                DL.Listing_Locality__c = 'Local';
                                DL.Product__c = (mapProducts.get('WCRL')!=null?mapProducts.get('WCRL').Id:null);
                                }
                                else{
                                DL.RecordTypeId = mapRecordType.get('Non Override RT').Id;
                                DL.Listing_Locality__c = 'Local';
                                DL.Product__c = (mapProducts.get('WRL')!=null?mapProducts.get('WRL').Id:null);
                                }
                            }
                        }
                        if(DL.Bus_Main_or_Additional__c == 'Main' && !setUPDC.contains(mapDirCR.get(DL.Id).Directory_code__c) && DL.Bus_Res_Gov_Indicator__c != 'Residential'){
                            Directory__c tempDir = mapDirectory.get(DL.Directory__c);
                            for(Directory_Section__c iteratorDS : tempDir.Directory_Sections__r) {
                                if(iteratorDS.Section_Page_Type__c == 'YP') {
                                    Directory_Listing__c objAddDL = DL.clone(false);
                                    system.debug('Master List size'+objAddDL);
                                    objAddDL.Directory_Section__c = iteratorDS.Id;
                                    //objAddDL.Directory_Heading__c = Label.Directory_Heading_YP;
                                    objAddDL.Created_by_Batch__c = TRUE;
                                    if(DL.Caption_Member__c == false && DL.Caption_Header__c == false){
                                        objAddDL.RecordTypeId = mapRecordType.get('Non Override RT').Id;
                                    }
                                    else{
                                        objAddDL.RecordTypeId = mapRecordType.get('Caption Member').Id;
                                    }
                                    objAddDL.Free_WP_Directory_Listing__c = DL.id;
                                    objAddDL.Section_Page_Type_Lookup__c = iteratorDS.section_page_type__c;
                                    objAddDL.Product__c = (mapProducts.get('SRL')!=null?mapProducts.get('SRL').Id:null);
                                    additionalDL.add(objAddDL);
                                }
                            }
                            system.debug('Additional Directory Listing :' + additionalDL);
                        }
                   // }
                }
                else {
                     if(DL.Caption_Header__c != true || DL.Caption_Member__c != true) {
                        if(DL.Disconnected__c == false && DL.Disconnected_Via_BOC_Purge__c == false){
                            if(mapDirCR.get(DL.Id).Cross_reference_indicator__c == true) {
                            DL.RecordTypeId = mapRecordType.get('Non Override RT').Id;
                            DL.Listing_Locality__c = 'Foreign';
                            DL.Product__c = (mapProducts.get('WCRL')!=null?mapProducts.get('WCRL').Id:null);
                            }
                            else{
                            DL.RecordTypeId = mapRecordType.get('Non Override RT').Id;
                            DL.Listing_Locality__c = 'Foreign';
                            DL.Product__c = (mapProducts.get('WRLF')!=null?mapProducts.get('WRLF').Id:null);
                            }
                        }
                    }
                }
            }
        }
        system.debug('Before Additional Dl :' +additionalDL);
        update lstMasterDirectoryListings;
        if(additionalDL.size() > 0) {
            CommonVariables.setLstDirListing(additionalDL);
            insert additionalDL;
        }
    }
}