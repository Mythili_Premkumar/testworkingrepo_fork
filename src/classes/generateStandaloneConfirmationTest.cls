@IsTest (SeeAllData=true)
public class generateStandaloneConfirmationTest{
    static testmethod void generateStandaloneConfirm(){
        Account newAccount= TestMethodsUtility.generateCustomerAccount();
        insert newAccount;   
        Case newcase =TestMethodsUtility.generateCase();  
        insert newcase; 
        Contact newContact= TestMethodsUtility.createContact(newAccount);       
        Opportunity newOpportunity= TestMethodsUtility.createOpportunity('new', newAccount);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOG = TestMethodsUtility.createOrderSet(newAccount,newOrder,newOpportunity);
        Order_Line_Items__c newOLI = TestMethodsUtility.generateOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOG);
        insert newOLI;
            
        test.starttest();
        generateStandaloneConfirmation gstc = new generateStandaloneConfirmation();
        gstc.thisOpportunity_Id = newOpportunity.Id;
        gstc.getOpp();
        cancellationConfirmation canc = new cancellationConfirmation();
        canc.CaseId = newcase.Id;
        canc.getCaseObj1();
         test.stoptest();
    }
}