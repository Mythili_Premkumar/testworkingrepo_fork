/***************************************************************
Utility class to generate Request and Response body for GET call
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 12/17/2014
$Id$
****************************************************************/
public with sharing class TalusRequestUtilityGET {
    
    //This is a common Utility Class
    public static string initiateRequest(String requestUrl){
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        //Default timeout is in milliseconds - this is now set to TWO minutes
        req.setTimeout(120000); 
        req.setMethod('GET');
        if(Test.isRunningTest()){
        //requestUrl+'param=1 & param=2'
            req.setEndpoint(requestUrl+'q?param=1 & param=2');
        }else{
            req.setEndpoint(requestUrl);
        }
        req.setHeader('Content-Type','application/json');
        req.setHeader('Accept','application/json');
        Oauth1 request=new Oauth1();
        request.sign(req);
        //System.debug('************' + request);
        HttpResponse res=new HttpResponse();
        try{
         res=h.send(req);
         System.debug('************HttpResponse************' + res);
         if(res.getStatusCode()!=200){
            ErrorLog.CreateErrorLog(res.getbody(),null,res.getStatusCode(),'Failed in GetUtility');
         }
        }catch (Exception e){
            ErrorLog.CreateErrorLog(e.getMessage(),null,500,'Failed in RequestUtilityGetClass');
        }
        //System.debug('************RESPONSE BODY************'+ res.getBody());
        return res.getBody();
    }
    
}