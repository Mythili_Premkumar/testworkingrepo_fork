global class P4PBillingHistoryMonthlyBatch implements Database.Batchable<sObject> {	
	public String strQuery;
	
	global P4PBillingHistoryMonthlyBatch(String strQuery) {
		this.strQuery = strQuery;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){		
		return Database.getQueryLocator(strQuery);
	}
	
	global void execute(Database.BatchableContext BC, List<Order_Line_Items__c> lstOLI) {
		list<P4P_Billing_History__c> lstP4PBHInsert = new list<P4P_Billing_History__c>();
		Date currentDate = Date.today();
		for(Order_Line_Items__c iterator : lstOLI) {
			Decimal decTotalCount = 0; 
			for(P4P_Transaction__c p4pTransaction: iterator.P4P_Transactions__r){
            	decTotalCount = decTotalCount + p4pTransaction.Total_Billable_Calls__c ;
          	}
          	
			P4P_Billing_History__c objP4PBH = new P4P_Billing_History__c();
			objP4PBH.Name = Date.Today().month() +','+ Date.Today().year();
			objP4PBH.P4P_Quantity_Per_Month__c = decTotalCount;
			objP4PBH.Price_Per_Click_Lead_Call__c = iterator.P4P_Price_Per_Click_Lead__c;
			objP4PBH.Order_Line_Item__c = iterator.id;
			objP4PBH.End_Of_Month_Date__c = Date.Today();
			lstP4PBHInsert.add(objP4PBH);
		}
		
		if(lstP4PBHInsert.size() > 0) {
			insert lstP4PBHInsert;
		}
	}
	
	global void finish(Database.BatchableContext BC){
	}

}