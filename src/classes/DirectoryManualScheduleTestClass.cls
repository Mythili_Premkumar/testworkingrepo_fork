@IsTest(SeeAllData=true)
private class DirectoryManualScheduleTestClass {
    public static testMethod void DirectoryManualScheduleInsert(){
    // Insert a Directory
    
        
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.EBD__c = System.Today().addDays(1);
        objDir.DCR_Close__c = System.Today().addDays(5);
        objDir.BOC__c = System.Today().addDays(2);
        objDir.Final_Service_Order_Due_to_Berry__c = System.Today().addDays(1);
        objDir.Sales_Lockout__c = System.Today();
        objDir.Book_Extract_YP__c = System.Today().addDays(3);
        objDir.Ship_Date__c = System.Today().addDays(-2);
        objDir.Pub_Date__c = System.Today().addDays(4);
        objDir.Directory_code__c = '123456';
        insert objDir;
        
    DirectoryManualSchedule dirMS = new DirectoryManualSchedule();
        try{
            dirMS.doRun();
        }
        catch(exception e){}
    }
}