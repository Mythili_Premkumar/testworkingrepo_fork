public class EditionCostController{

    public List<Edition_Cost__c> deLst{get; set;}
    public List<Edition_Cost__c> picklst{get; set;}
    public List<Edition_Cost__c> objEdCostList;
    public String Id;
    public decimal TelcoCostShare;
    public boolean otherCosttype{get;set;}
    public boolean boolVal {get;set;}
    public String newpicklistvalue{get;set;}
    public decimal newAmount{get;set;}
    public boolean telcoShare{get;set;}
    public string costType{get;set;}
    
        public EditionCostController(){
            
            Id = System.currentPageReference().getParameters().get('deId');
            deLst= new List<Edition_Cost__c>();
            deLst.add(new Edition_Cost__c());
            otherCosttype  = false;
            boolVal = false;
            bsTelcoRecord();
            getEditionCostTypes();
        }
        //fetching Billing Settlement record for Telco Cost Share
        public void bsTelcoRecord(){
            
            List<Billing_Settlement__c> objBSLst = [Select Id,Name,Telco_Cost_Share__c from Billing_Settlement__c where Directory_Edition__c = :Id Limit 1];
                for(Billing_Settlement__c objBS : objBSLst){
                    if(objBS.Telco_Cost_Share__c != null){
                        TelcoCostShare = objBS.Telco_Cost_Share__c;
                    } 
                }
        }
        public void addrow(){
            deLst.add(new Edition_Cost__c());
        }
        
        //Insertion of Edition Cost Record
        public void editionCostInsert(){
         try{
            
             for(Edition_Cost__c objEC : deLst){
                objEC.Directory_Edition__c = apexpages.currentpage().getparameters().get('deId');
                if(objEC.Telco__c != true && TelcoCostShare != null){
                    objEC.Amount__c = (objEC.Amount__c * TelcoCostShare)/100;
                }
            }
            if(deLst.size()>0){
                insert deLst;
            }
           
          }
          catch(Exception e){}
            
        }
       
        public void dosave(){
            //try{
            editionCostInsert();
            /*if(newpicklistvalue != null){
                addPicklist();
            }*/
            if(lstWD != null){
            wrapDataInsert();
            }
            //}
            //catch(Exception e){
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: Please select a record');
                //ApexPages.addMessage(myMsg);
                //}
        }
        
        public void newCtype(){
            otherCosttype = true;
            //system.debug('--OtherCost is -->'+otherCosttype);
            lstWD = new List<wrapData>();
            lstWD.add(new wrapData());
            //return null;
    }
        
        
       /* public List<Edition_Cost__c> addPicklist(){
        system.debug('*********METHOD INVOKED********');
             picklst = new List<Edition_Cost__c>();
                 Edition_Cost__c objEdct = new Edition_Cost__c();
                 objEdct .Cost_Type__c = newpicklistvalue;
                 objEdct.Telco__c = telcoShare;
                 objEdct.Directory_Edition__c = Id;
                
             if(objEdct.Telco__c != true && TelcoCostShare != null){
                 objEdct .Amount__c = (newAmount * TelcoCostShare)/100;   
             }
             else{
                 objEdct .Amount__c = newAmount;
             }
             picklst.add(objEdct);
             
             if(picklst.size()>0){
                insert picklst;
             }
             system.debug('*********METHOD INVOKED********'+picklst);
             
            newpicklistvalue = null;
            newAmount= null;
            telcoShare = false;
            
            return picklst;
        }*/
        public list<wrapData> lstWD {get;set;}
        public PageReference doAdd() {
            lstWD.add(new wrapData());
            boolVal = true;
            return null;
        }
        public void wrapDataInsert(){
            objEdCostList = new List<Edition_Cost__c>();
            for(wrapData objWD : lstWD){
                Edition_Cost__c objEcost = new Edition_Cost__c();
                objEcost.Cost_Type__c = objWD.PicklistName;
                objEcost.Telco__c = objWD.telcosharepercent;
                objEcost.Directory_Edition__c = Id;
                if(objEcost.Telco__c != true && TelcoCostShare != null){
                     objEcost.Amount__c = (objWD.Amount * TelcoCostShare)/100;   
                 }
                 else{
                     objEcost.Amount__c = objWD.Amount;
                 }
                 objEdCostList.add(objEcost);
            }
            if(objEdCostList.size()>0){
            insert objEdCostList;
            }
        }

        public List<SelectOption> options{get;set;}
        public void getEditionCostTypes(){
               options = new List<SelectOption>();
                Schema.DescribeFieldResult fieldResult = Edition_Cost__c.Cost_Type__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                
                
                List<STring> lstValues = new List<String>();
                
                for( Schema.PicklistEntry f : ple)
                {
                      options.add(new SelectOption(f.getLabel(), f.getValue()));
                      lstValues.add(f.getLabel());
                      
                }
                
                set<String> setValues = new set<String>();
                List<Edition_Cost__c> editionCostlist = new List<Edition_Cost__c>();
                editionCostlist = [Select Id, Cost_Type__c FROM Edition_Cost__c where Cost_Type__c NOT IN : lstValues AND (cost_type__c != null OR cost_type__c != '')];
                
                
                
                for (Integer i =0; i<editionCostlist.size();i++)
                {
                    if(!setValues.contains(editionCostlist[i].Cost_Type__c)){
                        options.add(new SelectOption(editionCostlist[i].Cost_Type__c ,editionCostlist[i].Cost_Type__c ));
                        setValues.add(editionCostlist[i].Cost_Type__c);
                        lstValues.add(editionCostlist[i].Cost_Type__c);
                    }
                }
                 
                system.debug('--lstValues are -->'+lstValues);
                options = new List<selectOption>();
                for(String s : lstValues){
                    options.add(new selectOption(s,s));
                }
                
                system.debug('---All Options --->'+options);
                system.debug('---All Options Count --->'+options.size());
                
                
                
             }
             
            public class wrapData{
            public String PicklistName{get;set;}
            public decimal Amount{get;set;}
            public boolean telcosharepercent{get;set;}
                public wrapData(){

        }
    }
}