Global class SalesRepTaskNotificationBatchController implements Database.Batchable<sObject>,Database.Stateful
{
    Global List<Task> taskListForInsert=new List<Task>(); // it will hold all tasks for insert.
    Global Map<Id,directory_edition__c> MapforDirectoryWithDirectoryEdition=new Map<Id,directory_edition__c>();
    Global Map<ID,Map<Id,List<Order_Line_Items__c>>> MapOfDirectoryEdition=new  Map<ID,Map<Id,List<Order_Line_Items__c>>>(); // this map will hold all print products OLIs for processing.
    Global SalesRepTaskNotificationBatchController(){}
    Global void execute(SchedulableContext ctx)
    {
        Database.executeBatch(new SalesRepTaskNotificationBatchController(), 2000);   
    } 
    Global Database.QueryLocator start(Database.BatchableContext bc)
    {
        for(directory__c directoryIterator:[select id,  (select id,Name,Sales_Lockout__c,Book_Status__c  from directory_editions__r )from directory__c ])
        {
            if(directoryIterator.directory_editions__r.size()>1)
            {
                for(directory_edition__c iterator: directoryIterator.directory_editions__r)
                {
                    if(iterator.Book_Status__c=='NI')
                    {
                        MapforDirectoryWithDirectoryEdition.put(directoryIterator.id,iterator);
                        break;
                    }
                }
               
            }
        }
        Set<Id> setIds=new Set<Id>();
        setIds=MapforDirectoryWithDirectoryEdition.keySet();
        string query='select id,Days_For_Task_Notification__c,directory__c,isCanceled__c,Name,Last_Billing_Date__c,Contract_End_Date__c,Directory_Edition__r.name,ProductCode__c,Payment_Duration__c,Account__c,Account__r.Name,Account__r.ownerID,Account__r.owner.IsActive,Directory_Section__c,Directory_Edition__c,Directory_Edition__r.Sales_Lockout__c,Product2__c,Product2__r.Media_Type__c ,(select id, Effective_Date__c,Order_Line_Item__c from Order_Line_Items__c.Modification_Order_Line_Items__r where Effective_Date__c >= today )  from Order_Line_Items__c where isCanceled__c=false AND (Directory__c IN :setIds OR Media_Type__c=\'Digital\')';
        return database.getQuerylocator(query);
    }
    
    Global void execute(Database.BatchableContext bc, List<Order_Line_Items__c > objOLIList)
    {
        System.debug('@@@@@@@@@@@@@@@ '+objOLIList.size());
        Map<string,List<Order_Line_Items__c>> MapOfDigitalOLI=new Map<string,List<Order_Line_Items__c>>();
        List<Order_Line_Items__c> OliListforPrint =new List<Order_Line_Items__c>();
        List<Order_Line_Items__c> OliListforDigital =new List<Order_Line_Items__c>();
        for(Order_Line_Items__c tempOLI:objOLIList){
            if(tempOLI.Product2__r.Media_Type__c=='Print'){
                if(tempOLI.Directory_Edition__c!=null && tempOLI.Directory__c!=null)
                    OliListforPrint.add(tempOLI);
            }
            else if(tempOLI.Product2__r.Media_Type__c!=null)
                OliListforDigital.add(tempOLI);
        }
        Map<Id,List<Order_Line_Items__c>> tempMap=new Map<Id,List<Order_Line_Items__c>>();
        for(Order_Line_Items__c tempOLI:OliListforPrint){
            if(!MapOfDirectoryEdition.containskey(tempOLI.Directory__c)){
                tempMap=new Map<Id,List<Order_Line_Items__c>>();
                tempMap.put(tempOLI.Account__c,new list<Order_Line_Items__c>{tempOLI});
                MapOfDirectoryEdition.PUT(tempOLI.Directory__c,tempMap);
            }
            else{
                if(!MapOfDirectoryEdition.get(tempOLI.Directory__c).containskey(tempOLI.Account__c))
                    MapOfDirectoryEdition.get(tempOLI.Directory__c).PUT(tempOLI.Account__c,new list<Order_Line_Items__c>{tempOLI});
                else
                    MapOfDirectoryEdition.get(tempOLI.Directory__c).get(tempOLI.Account__c).add(tempOLI);
            }
            
        }
        
        // this logic for digital 
         for(Order_Line_Items__c tempOLIDigital:OliListforDigital){
            Id OwnerIdofAccount=null;
            if(tempOLIDigital.Modification_Order_Line_Items__r.Size()==0){
              /*  if(tempOLIDigital.Account__r.owner.isActive==false)
                    OwnerIdofAccount='005Z0000001g3tX';
                else*/
                    OwnerIdofAccount =tempOLIDigital.Account__r.ownerID;
                 if(tempOLIDigital.Payment_Duration__c==12 &&  tempOLIDigital.Last_Billing_Date__c!=null){
                     if(tempOLIDigital.Days_For_Task_Notification__c==180 || tempOLIDigital.Days_For_Task_Notification__c==45){                
                         Task t=new Task(Subject='Digital Renewal Notification - '+tempOLIDigital.Account__r.Name,ActivityDate=tempOLIDigital.Last_Billing_Date__c,Status='Not Started',Priority='Normal',WhatId=tempOLIDigital.Account__c,OwnerId=OwnerIdofAccount,Description=tempOLIDigital.ProductCode__c);
                         taskListForInsert.add(t);
                     }
                 }
                 if(tempOLIDigital.Payment_Duration__c==6 &&  tempOLIDigital.Last_Billing_Date__c!=null){
                     if(tempOLIDigital.Days_For_Task_Notification__c==45){                
                         Task t=new Task(Subject='Digital Renewal Notification - '+tempOLIDigital.Account__r.Name,ActivityDate=tempOLIDigital.Last_Billing_Date__c,Status='Not Started',Priority='Normal',WhatId=tempOLIDigital.Account__c,OwnerId=OwnerIdofAccount,Description=tempOLIDigital.ProductCode__c);
                         taskListForInsert.add(t);
                     }
                 }
            }             
         }
    }
    
    Global void finish(Database.BatchableContext bc)
    {
    
        System.debug('############ '+MapOfDirectoryEdition.keySet());
        
        Map<Id,Directory_Edition__c> deMap =new Map<Id,Directory_Edition__c>([select id,Name,Sales_Lockout__c from Directory_Edition__c where id In :MapOfDirectoryEdition.keySet() AND Book_Status__c='NI']);        
        System.debug('#####%%%%%#### '+deMap.Size());
        Map<id,string> AccountMap=new Map<id,string>();
        for(Id tempId:MapOfDirectoryEdition.KeySet())
        {
            for(Id accid:MapOfDirectoryEdition.get(tempId).KeySet())
            {
              for(Order_Line_Items__c tempOLI:MapOfDirectoryEdition.get(tempId).get(accid))
              {
                  AccountMap.put(accid,tempOLI.Account__r.Name);
              }
            }
        }
        string OLIName;
        for(Id tempId:MapOfDirectoryEdition.KeySet())
        {
            MAP<ID,ID> AccountOwnerId=new MAP<ID,ID> ();
            Directory_Edition__c directoryEditionWithNI=MapforDirectoryWithDirectoryEdition.get(tempId);
            integer days=Date.today().daysBetween(directoryEditionWithNI.Sales_Lockout__c);
            if(days==5 || days==30 || days==60)
            {
                System.debug('################ Directory  '+tempId);
                OLIName='';
                Map<id,map<id,string>> AccountOLIsMap=new Map<id,map<id,string>>();
                Map<id,string> AccOliMap=new map<id,string>();            
                for(Id accid:MapOfDirectoryEdition.get(tempId).KeySet())
                {
                    System.debug('%%%%%%%%%% Account '+accid);
                    OLIName='\r\n' + AccountMap.get(accid)+':- ';
                    integer icount=0;
                    integer count=0;
                    integer OliSize=MapOfDirectoryEdition.get(tempId).get(accid).Size();
                    Id accountowner;
                    for(Order_Line_Items__c tempOLI:MapOfDirectoryEdition.get(tempId).get(accid))
                    {
                    
                       icount++;
                       if(icount<OliSize)
                       {
                            if(tempOLI.Modification_Order_Line_Items__r.Size()==0)
                            {
                                    count++;
                                    OLIName=OLIName+tempOLI.Name +',';
                                    if(tempOLI.Account__r.owner.isActive!=false)
                                    {
                                        AccountOwnerId.put(tempOLI.Account__r.ownerID,accid);
                                        accountowner=tempOLI.Account__r.ownerID;
                                    }
                                    else
                                    {
                                        System.debug('########## ownerId inActive '+tempOLI.Account__r.ownerID);
                                        System.debug('########## OLI--ID '+tempOLI.ID);
                                        
                                    }                            
                            }
                        }
                        else
                        {
                           if(tempOLI.Modification_Order_Line_Items__r.Size()==0)
                            {
                                count++;
                                OLIName=OLIName+tempOLI.Name;
                                if(tempOLI.Account__r.owner.isActive!=false)
                                {
                                    AccountOwnerId.put(tempOLI.Account__r.ownerID,accid);
                                    accountowner=tempOLI.Account__r.ownerID;
                                }
                                else
                                {
                                    System.debug('########## ownerId inActive '+tempOLI.Account__r.ownerID);
                                    System.debug('########## OLI--ID '+tempOLI.ID);
                                }
                                 
                            }
                        }                
                    }
                    if(count>0)
                    {
                        if(!AccOliMap.containsKey(accountowner))
                            AccOliMap.put(accountowner,OLIName);
                        else
                        {
                            string temp=AccOliMap.get(accountowner);
                            AccOliMap.put(accountowner,OLIName+temp);
                        }
                       // AccountOLIsMap.put(tempId,AccOliMap);
                    }
                }

                System.debug('######## AccountOwnerId '+AccountOwnerId);
                System.debug('######## AccountOwnerId.size() '+AccountOwnerId.size());
                for(Id accountId:AccountOwnerId.keySet())
                {
                    String buildStr='';
                   /* for(String strIterator :AccOliMap.get(accountId))
                    { 
                      buildStr=buildStr+strIterator;
                    }*/
                    Task t=new Task(Subject='Print Renewal Notification - '+directoryEditionWithNI.name,ActivityDate=directoryEditionWithNI.Sales_Lockout__c,Status='Not Started',Priority='Normal',WhatId=directoryEditionWithNI.id,OwnerId=accountId,Description=AccOliMap.get(accountId));
                    taskListForInsert.add(t);
                }
                       
            }  
        }   
  
        //Task List for Insert
       System.debug('@@@@@@@@@@@vikas '+taskListForInsert); 
       System.debug('###########size '+taskListForInsert.size());  
       if(taskListForInsert.size()>0)
           insert taskListForInsert;

    }
}