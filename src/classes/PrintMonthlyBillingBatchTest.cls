@isTest(SeeAllData=true)
public class PrintMonthlyBillingBatchTest {
    static testMethod void PrintMonthlyBillingTest() {
        Test.startTest();        
        Account acct = TestMethodsUtility.createAccount('cmr');
        Account childAcct = TestMethodsUtility.generateAccount();
        childAcct.ParentId = acct.Id;
        insert childAcct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
                
        PrintMonthlyBilling obj = new PrintMonthlyBilling(oln.Billing_End_Date__c);
        ID batchprocessid = Database.executeBatch(obj);
        Test.stopTest();
    }
}