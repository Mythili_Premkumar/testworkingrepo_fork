public with sharing class LocalBillingCreateTelco{

  public String strSelectedDirectoryEdition {get;set;}
  public list<SelectOption> lstDirectoryEdition {get;set;}
public Dummy_Object__c objDummy {get;set;}
public String strSelectedMediaType{get;set;}
public String strSelectedBillingEntity {get;set;}
public list<SelectOption> lstBillingEntity {get;set;}
Map<String,Digital_Telco_Scheduler__c> mpTelcoScheBE;
public Digital_Telco_Scheduler__c objDTS {get;set;}
public boolean bShowCreateTelco {get;set;}
public Decimal SILITotal {get;set;}
public String strNoofAccountAndInvoice {get;set;}
public String strNoofAccountAndSCN{get;set;}
public boolean bInvoicePresent {get;set;}
public String strBatchStatusTelco {get;set;}
public String strSCNBatchStatus{get;set;}
public String strInvBatchStatus{get;set;}
public boolean bEnabledPollerForTelco {get;set;}
Map<id,c2g__codaInvoice__c> MapSalesinvoice;
Map<id,c2g__codaCreditNote__c> MapofSalesCreditnote;
    Set<id> setSIId;
     Set<id> SetSalesCreditnote;
     


public List<SelectOption> getMediaTypes()
{ 
   List<SelectOption> options = new List<SelectOption>();
   options.add(new SelectOption('Digital','Digital'));
   options.add(new SelectOption('Print','Print'));
   return options;
}

public LocalBillingCreateTelco(){
  mpTelcoScheBE = new Map<String,Digital_Telco_Scheduler__c>();
  bShowCreateTelco =false;
}

 public void fetchDirectoryEdition() {
        lstBillingEntity = new list<SelectOption>();
        if(objDummy.Date__c != null) {
            list<Digital_Telco_Scheduler__c> lstTelcoScheduler = [Select id,Name,Telco__r.Name,Billing_Entity__c,Invoice_To_Date__c,Invoice_From_Date__c,Telco__c,Telco_Receives_EFile__c,
                                        XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where Bill_Prep_Date__c =:objDummy.Date__c];
            if(lstTelcoScheduler.size()>0) {
                for(Digital_Telco_Scheduler__c iterator : lstTelcoScheduler) {
          lstBillingEntity.add(new SelectOption(iterator.id, iterator.Billing_Entity__c));
          mpTelcoScheBE.put(iterator.id, iterator);
                }
            }
        }
    }
    
 public void checkInvoicesforDigitalNew() {
        set<Id> setAccountID = new set<Id>();
        set<Id> setAccountSCNID = new set<Id>();
        clearDatas();
        
        if(String.isNotBlank(strSelectedBillingEntity)) {
            SILITotal = 0.0;
            objDTS = mpTelcoScheBE.get(strSelectedBillingEntity);            
            list<c2g__codaInvoice__c> lstSalesInvoice = [SELECT SI_Billing_Partner__c, Customer_Name__c,Reconcile__c,Name,c2g__NetTotal__c 
                                  FROM c2g__codaInvoice__c 
                                  where SI_P4P__c = false AND c2g__InvoiceStatus__c = 'In Progress' AND Billing_Entity__c=:objDTS.Billing_Entity__c 
                                  AND SI_Media_Type__c='Digital' AND  c2g__InvoiceDate__c >=:objDTS.Invoice_From_Date__c AND c2g__InvoiceDate__c <=:objDTS.Invoice_To_Date__c  ];
            
      list<c2g__codaCreditNote__c> lstCreditNote = [Select Name, SC_Billing_Partner__c, c2g__Invoice__r.Name, c2g__NetTotal__c, Customer_Name__c, Id,Reconcile__c 
                             From c2g__codaCreditNote__c where Transaction_Type__c in ('TD - Billing Transfer Invoice','FC - Frequency Change') 
                             AND c2g__CreditNoteStatus__c='In Progress' and SC_P4P__c =false AND SC_Billing_Entity__c=:objDTS.Billing_Entity__c 
                             AND SC_Media_Type__c='Digital' AND  c2g__CreditNoteDate__c >=:objDTS.Invoice_From_Date__c AND c2g__CreditNoteDate__c <=:objDTS.Invoice_To_Date__c];
                        
            for(c2g__codaInvoice__c iterator : lstSalesInvoice) {                    
                if(!MapSalesinvoice.containskey(iterator.id)) {
                    MapSalesinvoice.put(iterator.id,new c2g__codaInvoice__c(id=iterator.Id,Reconcile__c=true));
                }
                if(iterator.SI_Billing_Partner__c != CommonMessages.BerryForDimension) {
                    SILITotal=SILITotal+iterator.c2g__NetTotal__c;
                    if(!setAccountID.contains(iterator.Customer_Name__c)) {                                                 
                        setAccountID.add(iterator.Customer_Name__c);
                    }
                    if(!setSIId.contains(iterator.id)) {
                        setSIId.add(iterator.id);          
                    }
                }
            }
      
            for(c2g__codaCreditNote__c iterator : lstCreditNote) {
        if(!MapofSalesCreditnote.containskey(iterator .id)) {
           MapofSalesCreditnote.put(iterator.id,new c2g__codaCreditNote__c(id=iterator.Id,Reconcile__c=true));
        }                        
                if(iterator.SC_Billing_Partner__c != CommonMessages.BerryForDimension) {
          if(!SetSalesCreditnote.contains(iterator.id)) {
            SetSalesCreditnote.add(iterator.id);
          }
          if(!setAccountSCNID.contains(iterator.Customer_Name__c)) {
                        setAccountSCNID.add(iterator.Customer_Name__c);
                    }
                }
            }
        }
        
        System.debug('Testingg setAccountID '+setAccountID);
        strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
        strNoofAccountAndSCN = concatenateSCNString(setAccountSCNID.size(), SetSalesCreditnote.size());
        if(setSIId.size() > 0 || SetSalesCreditnote.size() > 0) {
            bInvoicePresent = true;
        }
    }
    
    
    private void clearDatas() {
      strNoofAccountAndInvoice = concatenateString(0, 0);
        strNoofAccountAndSCN = concatenateSCNString(0, 0);
        strBatchStatusTelco = '';
        strSCNBatchStatus = '';
        strInvBatchStatus = '';
        
        bEnabledPollerForTelco = false;
        bInvoicePresent = false;


        bShowCreateTelco = true;
       
          MapSalesinvoice=new Map<id,c2g__codaInvoice__c>();
          MapofSalesCreditnote =new Map<id,c2g__codaCreditNote__c>();
    }


  private String concatenateString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
    }
    
    private String concatenateSCNString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Credit Notes for Selections';
    }
    
    public void SendTelcoFile() {
        if(objDTS != null) {
            Digital_Telco_Scheduler__c dts=new Digital_Telco_Scheduler__c(id=objDTS.id,Sent_Telco_File__c=true);
            bEnabledPollerForTelco = true;
            update dts;         
        }
    }
    
     public void CheckTelcoOutput() {
        if(objDTS !=null) {
            strBatchStatusTelco = 'XML Telco Invoice Status : InProgress';
            fetchDTSForReport();
        }
    }
    
    public void fetchDTSForReport() {
        objDTS=[select id,Name,Invoice_From_Date__c ,Billing_Entity__c,Invoice_To_Date__c,Telco__c,Telco_Receives_EFile__c,XML_Output_Total_Amount__c from Digital_Telco_Scheduler__c where id=:strSelectedBillingEntity];
        if(objDTS.XML_Output_Total_Amount__c != null && objDTS.XML_Output_Total_Amount__c > 0) {
            strBatchStatusTelco = 'XML Telco Invoice Status : Completed';
            bEnabledPollerForTelco = false;
        }
    }


}