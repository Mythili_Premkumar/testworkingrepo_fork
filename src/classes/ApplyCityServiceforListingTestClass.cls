@IsTest
private class ApplyCityServiceforListingTestClass {
    static testMethod void ApplyCityServiceforListingTest() {
        Test.StartTest();
        Account objAcc = TestMethodsUtility.generateAccount();
        insert objAcc;
        Telco__c objTelco = TestMethodsUtility.createTelco(objAcc.Id);
        Directory__c ObjDir = TestMethodsUtility.createDirectory(); 
        Directory_Section__c ObjDirsec = TestMethodsUtility.createDirectorySection(ObjDir);
        Listing__c objListing = TestMethodsUtility.generateMainListing();
        insert objListing; 
        Directory_Listing__c objDirLst = TestMethodsUtility.generateDirectoryListing();
    	objDirLst.Listing__c =  objListing.Id;
        objDirLst.Directory_section__c=ObjDirsec.Id;
        insert objDirLst;
        Directory_Pagination__c objDp = new Directory_Pagination__c();
        objDp.Reference_Listing__c=objDirLst.Id;
        objDp.Directory_section__c=ObjDirsec.Id;
        insert objDp;
        
        ApexPages.StandardController sc = new ApexPages.standardController(new Listing__c());    
        ApexPages.currentPage().getParameters().put('Id',objListing.Id);
        
        ApplyCityforListing applyCityLst = new ApplyCityforListing(sc);
        ApplyCityforListing.WrapobjDP wrap = new ApplyCityforListing.WrapobjDP();
        wrap.ischecked = true;
        wrap.objDP = objDp;
        wrap.objDP.Manual_City_Display_Rule__c = 'Abbreviate';
        wrap.manualAbbreviate = 'Abbreviate';
        List<ApplyCityforListing.WrapobjDP> lstW = new List<ApplyCityforListing.WrapobjDP>();
        lstW.add(wrap);
        applyCityLst.wrapobjDPLst = new List<ApplyCityforListing.WrapobjDP>();
        applyCityLst.wrapobjDPLst.addall(lstW);
        applyCityLst.listing = new Listing__c ();
        applyCityLst.listing = objListing;
        applyCityLst.listingDPLst();
        try{
            applyCityLst.ApplyRule();
        }
        catch(Exception e){}
        applyCityLst.redirect();
        
        Test.StopTest();    
    }
}