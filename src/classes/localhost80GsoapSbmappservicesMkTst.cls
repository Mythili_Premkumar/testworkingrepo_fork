@IsTest()
public class localhost80GsoapSbmappservicesMkTst implements WebServiceMock {

    public void doInvoke(Object stub, Object request, Map < string, object > response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {

        localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();
        sbmappservices72.GetAvailableTransitionsResponse_element tElmnts = new sbmappservices72.GetAvailableTransitionsResponse_element();
        sbmappservices72.GetSolutionsResponse_element slRsp = new sbmappservices72.GetSolutionsResponse_element();
        sbmappservices72.GetReportsResponse_element gtRptRsp = new sbmappservices72.GetReportsResponse_element();
        sbmappservices72.GetItemsByQueryResponse_element ttitmlst = new sbmappservices72.GetItemsByQueryResponse_element();
        sbmappservices72.TransitionItemResponse_element trnsItmRsp = new sbmappservices72.TransitionItemResponse_element();
        sbmappservices72.GetFileAttachmentResponse_element gtFlAtchRsp = new sbmappservices72.GetFileAttachmentResponse_element();
        sbmappservices72.CreateAuxItemResponse_element crtAxItmRsp = new sbmappservices72.CreateAuxItemResponse_element();
        sbmappservices72.UpdateFileAttachmentResponse_element updtFlAtchRsp = new sbmappservices72.UpdateFileAttachmentResponse_element();
        sbmappservices72.GetItemResponse_element gtItmRsp = new sbmappservices72.GetItemResponse_element();
        sbmappservices72.DeleteItemsResponse_element dltItmRsp = new sbmappservices72.DeleteItemsResponse_element();
        sbmappservices72.GetUsersResponse_element gtUsrRsp = new sbmappservices72.GetUsersResponse_element();
        sbmappservices72.RunReportXmlResponse_element rnRptXmlRsp = new sbmappservices72.RunReportXmlResponse_element();
        sbmappservices72.GetItemsResponse_element gtItmsRsp = new sbmappservices72.GetItemsResponse_element();
        sbmappservices72.GetStateChangeHistoryResponse_element gtStChngHstRsp = new sbmappservices72.GetStateChangeHistoryResponse_element();
        sbmappservices72.GetApplicationsResponse_element gtAplRsp = new sbmappservices72.GetApplicationsResponse_element();
        sbmappservices72.CreatePrimaryItemResponse_element crtPrmyItmRsp = new sbmappservices72.CreatePrimaryItemResponse_element();
        sbmappservices72.CreateAuxItemsResponse_element crtAxItmsRsp = new sbmappservices72.CreateAuxItemsResponse_element();
        sbmappservices72.RunReportResponse_element rnRptRsp = new sbmappservices72.RunReportResponse_element();
        sbmappservices72.GetSubmitProjectsResponse_element gtSbtPrjtRsp = new sbmappservices72.GetSubmitProjectsResponse_element();
        sbmappservices72.CreateFileAttachmentResponse_element crtFlAtchRsp = new sbmappservices72.CreateFileAttachmentResponse_element();
        sbmappservices72.TransitionItemsResponse_element trnsItmsRsp = new sbmappservices72.TransitionItemsResponse_element();
        sbmappservices72.CreatePrimaryItemsResponse_element crtPrmyItmsRsp = new sbmappservices72.CreatePrimaryItemsResponse_element();
        sbmappservices72.GetAvailableSubmitTransitionsResponse_element gtAvlSbtTrnsRsp = new sbmappservices72.GetAvailableSubmitTransitionsResponse_element();
        sbmappservices72.GetNoteLoggerInfoResponse_element gtNtLgrInrRsp = new sbmappservices72.GetNoteLoggerInfoResponse_element();
        sbmappservices72.GetTablesResponse_element gtTblRsp = new sbmappservices72.GetTablesResponse_element();
        sbmappservices72.isUserValidResponse_element usrVldRsp = new sbmappservices72.isUserValidResponse_element();
        sbmappservices72.CreateNoteAttachmentResponse_element crtNtAttchRsp = new sbmappservices72.CreateNoteAttachmentResponse_element();
        sbmappservices72.GetVersionResponse_element gtVrsnRsp = new sbmappservices72.GetVersionResponse_element();

        system.debug('************STUB************' + requestName);
        if (requestName == 'GetAvailableTransitions') {
            response.put('response_x', tElmnts);
        }
        if (requestName == 'GetSolutions') {
            response.put('response_x', slRsp);
        }
        if (requestName == 'GetReports') {
            response.put('response_x', gtRptRsp);
        }
        if (requestName == 'GetItemsByQuery') {
            response.put('response_x', ttitmlst);
        }
        if (requestName == 'TransitionItem') {
            response.put('response_x', trnsItmRsp);
        }
        if (requestName == 'GetFileAttachment') {
            response.put('response_x', gtFlAtchRsp);
        }
        if (requestName == 'CreateAuxItem') {
            response.put('response_x', crtAxItmRsp);
        }
        if (requestName == 'UpdateFileAttachment') {
            response.put('response_x', updtFlAtchRsp);
        }
        if (requestName == 'GetItem') {
            response.put('response_x', gtItmRsp);
        }
        if (requestName == 'DeleteItems') {
            response.put('response_x', dltItmRsp);
        }
        if (requestName == 'GetUsers') {
            response.put('response_x', gtUsrRsp);
        }
        if (requestName == 'RunReportXml') {
            response.put('response_x', rnRptXmlRsp);
        }
        if (requestName == 'GetItems') {
            response.put('response_x', gtItmsRsp);
        }
        if (requestName == 'GetStateChangeHistory') {
            response.put('response_x', gtStChngHstRsp);
        }
        if (requestName == 'GetApplications') {
            response.put('response_x', gtAplRsp);
        }
        if (requestName == 'CreatePrimaryItem') {
            response.put('response_x', crtPrmyItmRsp);
        }
        if (requestName == 'CreateAuxItems') {
            response.put('response_x', crtAxItmsRsp);
        }
        if (requestName == 'CreateNoteAttachment') {
            response.put('response_x', crtNtAttchRsp);
        }
        if (requestName == 'GetVersion') {
            response.put('response_x', gtVrsnRsp);
        }
        if (requestName == 'IsUserValid') {
            response.put('response_x', usrVldRsp);
        }
        if (requestName == 'RunReport') {
            response.put('response_x', rnRptRsp);
        }
        if (requestName == 'GetSubmitProjects') {
            response.put('response_x', gtSbtPrjtRsp);
        }
        if (requestName == 'CreateFileAttachment') {
            response.put('response_x', crtFlAtchRsp);
        }
        if (requestName == 'TransitionItems') {
            response.put('response_x', trnsItmsRsp);
        }
        if (requestName == 'CreatePrimaryItems') {
            response.put('response_x', crtPrmyItmsRsp);
        }
        if (requestName == 'GetAvailableSubmitTransitions') {
            response.put('response_x', gtAvlSbtTrnsRsp);
        }
        if (requestName == 'GetNoteLoggerInfo') {
            response.put('response_x', gtNtLgrInrRsp);
        }
        if (requestName == 'GetTables') {
            response.put('response_x', gtTblRsp);
        }
    }

}