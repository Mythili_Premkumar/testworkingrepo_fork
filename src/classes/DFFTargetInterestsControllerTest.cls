@isTest(seeAllData = true)
public Class DFFTargetInterestsControllerTest{

    public static testmethod void dffTITest(){
    
    Test.StartTest();
    
    Digital_Product_Requirement__c dff = CommonUtility.createDffTest();
    insert dff;
    
    ApexPages.StandardController sc = new ApexPages.StandardController(dff);
    DFFTargetInterestsController dffTI = new DFFTargetInterestsController(sc);
    
    dffTI.selectclick();
    dffTI.unselectclick();
    dffTI.getunSelectedValues();
    dffTI.getSelectedValues();
    dffTI.dffTrgtIntrst();
    
    Test.StopTest();
    
    }

}