public class ListingsLeadAdrUpdate{
    public static void leadAdrUpdate(List<Listing__c> objLstng,map<Id,Listing__c> lstOldmap){
        map<Id,Lead> mapobjLdLst = new map<Id,Lead>();
        map<Id,Account> mapobjActLst = new map<Id,Account>();
        
            for(Listing__c objLst : objLstng){
            system.debug('******Listing List FOR LOOP Entered ********'+objLst);
                if(lstOldmap != null) {
                    listing__c oldLst = lstOldmap.get(objLst.Id);
                    if(objLst.Account__c != null){
                        Account objAct = new Account(Id = objLst.Account__c);
                        if(oldLst.Telco_Provider_Account_ID__c != objLst.Telco_Provider_Account_ID__c && objLst.Telco_Provider_Account_ID__c != null){
                            objAct.Telco_Partner__c = objLst.Telco_Provider_Account_ID__c;
                        }
                        //objActLst.add(objAct);
                        if(!mapobjActLst.containsKey(objAct.Id)){
                            mapobjActLst.put(objAct.Id,objAct);
                        }
                    }
                    else if(objLst.Lead__c != Null) {
                        Lead objLd = new Lead(Id = objLst.Lead__c);
                        System.debug('*******Lead Record *******'+objLd);
                        if(objLd.IsConverted == false){ 
                        System.debug('*******Listing Record  *******'+objLst.Listing_Street__c);
                        objLd.Phone = objLst.Phone__c;               
                        objLd.Street = objLst.Listing_Street__c;
                        objLd.City = objLst.Listing_City__c;
                        objLd.State = objLst.Listing_State__c;
                        objLd.PostalCode = objLst.Listing_Postal_Code__c;
                        objLd.Country = objLst.Listing_Country__c;
                        if(objLst.Telco_Provider_Account_ID__c != null){
                            objLd.Telco_Partner_Account__c = objLst.Telco_Provider_Account_ID__c;
                        }
                        //objLdLst.add(objLd);
                        if(!mapobjLdLst.containsKey(objLd.Id)){
                            mapobjLdLst.put(objLd.Id,objLd);
                        }
                        }
                    }
                }
            }
            if(mapobjLdLst.size()>0 && CommonVariables.LeadListingAdrRecursive==false){
                    CommonVariables.LeadListingAdrRecursive=true;
                    update mapobjLdLst.values();                   
            }
            if(mapobjActLst.size()>0){
                update mapobjActLst.values();
            }
    }
    public static void listingUpdatewithAccount(set<Id> ldconvertId, String AccountId){

        List<Listing__c> objLstng = new List<Listing__c>();
        map<Id, Listing__c> mapLstUpdate = new map<Id, Listing__c>();
            objLstng =[Select Id,Name,Account__c from Listing__c where Lead__c IN : ldconvertId AND Account__c = NULL];
            system.debug('***********Listing LIST*********'+objLstng); 
                for(Listing__c objL : objLstng){
                   Listing__c objLstUpdte = new Listing__c(Id = objL.ID);
                   objLstUpdte.Account__c = AccountId;
                   //LstUpdate.add(objLstUpdte);
                   if(!mapLstUpdate.containskey(objLstUpdte.Id)){
                       mapLstUpdate.put(objLstUpdte.Id,objLstUpdte);
                   }
                }
                if(mapLstUpdate.size()>0 && CommonVariables.LeadListingAdrRecursive==false){
                    CommonVariables.LeadListingAdrRecursive=true;
                    update mapLstUpdate.values();
                }
    }
}