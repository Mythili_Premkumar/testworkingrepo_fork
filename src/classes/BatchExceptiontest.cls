global class BatchExceptiontest implements Database.Batchable<sObject>, Database.Stateful{
    global final String Query;
    public list<Exception_Error__c> logList = new list<Exception_Error__c> ();
    public String FailedRecords;
    global BatchExceptiontest(String q)
    {
        Query=q;
        FailedRecords='';
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC,List <sObject> scope)
    {
        Savepoint sp = Database.setSavepoint();
        try{
            List <Account> lstAccount = new list<Account>();
            for(Sobject s : scope)
            {
                 Account a = (Account)s;
                lstAccount.add(a);
            }
            update lstAccount;
        }
        catch(DMLException e)
        {
            for (Integer index = 0; index < e.getNumDml(); index++){
                logList.add(new Exception_Error__c(Exception_Message__c=e.getDmlMessage(index),Record_Id__c=e.getDmlId(index),StackTraceString__c=e.getStackTraceString()));
                FailedRecords+= '<br>Record Id :'+e.getDmlId(index)+'  Exception Msg '+e.getDmlMessage(index)+'<br>';
            }
            Database.rollback(sp);
            throw e;
        }
    }
    global void finish(Database.BatchableContext BC)
    {
        if(logList.size() > 0)
        {
            insert logList;
        }
        String[] toAddresses = new String[] {'bpallan@csc.com'};
        CommonEmailUtils.sendHTMLEmail(toAddresses,'FailedRecords','LogSize'+logList.size()+'<br></br>'+FailedRecords);
    }
    

}