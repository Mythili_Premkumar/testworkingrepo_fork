global class MonthlyCronWeeklyScheduler Implements Schedulable {
    global void execute(SchedulableContext sc) {
        MonthlyCronCreationBatch obj = new MonthlyCronCreationBatch(system.today() + 7, null);
        Database.executeBatch(obj, 2000);
    }
}