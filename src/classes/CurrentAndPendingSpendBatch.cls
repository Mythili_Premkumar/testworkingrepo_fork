global class CurrentAndPendingSpendBatch implements Database.Batchable<sobject>{
    global Database.Querylocator start(Database.BatchableContext bc){
        String SOQL = 'SELECT ID, Current_Spend__c, Pending_Spend__c, Total_Spend__c, ParentId FROM Account';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accountList){
        Set<Id> accountIds = new Set<Id>();
        List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c> (); 
        List<Modification_Order_Line_Item__c> modifiedOLIList = new List<Modification_Order_Line_Item__c>();
        Map<Id, List<Order_Line_Items__c>> accountOLIListMap = new Map<Id, List<Order_Line_Items__c>>();
        Map<Id, Modification_Order_Line_Item__c> OLIIdModifiedOLIMap = new Map<Id, Modification_Order_Line_Item__c>();
        List<Account> accountUpdateList = new List<Account>();    
        List<Account> childAccountList = new List<Account>();   
        List<Account> childAccountUpdateList = new List<Account>();   
        Map<Id, List<Account>> parentIdAcctListMap = new Map<Id, List<Account>>();    
        
        for(Account acct : accountList){
            accountIds.add(acct.Id);
        }                                
        
        OLIList = OrderLineItemSOQLMethods.getOrderLineItemByAccountIDs(accountIds);
        
        modifiedOLIList = ModificationOrderLineItemSOQLMethods.getMOLIBYAccountIds(accountIds);
        
        if(modifiedOLIList.size() > 0){
            for(Modification_Order_Line_Item__c MOLI : modifiedOLIList){
                OLIIdModifiedOLIMap.put(MOLI.Order_Line_Item__c, MOLI);
            }
        }
        
        for(Order_Line_Items__c OLI : OLIList){
            if(!accountOLIListMap.containsKey(OLI.Account__c)){
                accountOLIListMap.put(OLI.Account__c, new List<Order_Line_Items__c>());
            }
            accountOLIListMap.get(OLI.Account__c).add(OLI);
        }                 
        
        for(Account acct : accountList){
            Double currentSpend = 0;
            Double pendingSpend = 0;
            List<Order_Line_Items__c> tempOLIList = new List<Order_Line_Items__c> ();
            if(accountOLIListMap.containsKey(acct.Id)){
                tempOLIList = accountOLIListMap.get(acct.Id);
            }
            if(!tempOLIList.isEmpty()){
                for(Order_Line_Items__c OLI : tempOLIList){
                    if(!OLI.isCanceled__c) {
                        if(OLI.Billing_Partner__c == 'THE BERRY COMPANY' && OLI.Talus_Go_Live_Date__c != null){
                            if(OLI.Billing_Frequency__c != null){
                                if(OLI.Billing_Frequency__c == CommonMessages.singlePayment){
                                    if(OLI.Order_Line_Total__c != null && OLI.Payment_Duration__c != null) {
                                        currentSpend = currentSpend + (OLI.Order_Line_Total__c / OLI.Payment_Duration__c);
                                    }
                                } else {
                                    if(OLI.UnitPrice__c != null){
                                        currentSpend = currentSpend + OLI.UnitPrice__c;
                                    }
                                }
                            }
                        }               
                        if(OLI.UnitPrice__c != null){
                            if(OLI.Talus_Go_Live_Date__c > System.Today() || OLI.Talus_Go_Live_Date__c == null) {
                                if(OLIIdModifiedOLIMap.containsKey(OLI.Id) && OLIIdModifiedOLIMap.get(OLI.Id).Effective_Date__c > commonMessages.systemDate){
                                    pendingSpend = pendingSpend + OLIIdModifiedOLIMap.get(OLI.Id).UnitPrice__c;
                                } else {
                                    pendingSpend = pendingSpend + OLI.UnitPrice__c;
                                }
                            }
                        }
                    }
                }
                acct.Pending_Spend__c = pendingSpend;
                acct.Current_Spend__c = currentSpend;                
                accountUpdateList.add(acct);                
            }
        }   
        
        if(!accountUpdateList.isEmpty()){
            update accountUpdateList;
        }
        
        childAccountList = AccountSOQLMethods.getAccountByParentId(accountIds);
        
        if(childAccountList.size() > 0){
            for(Account acc : childAccountList){
                if(!parentIdAcctListMap.containsKey(acc.ParentId)){
                    parentIdAcctListMap.put(acc.ParentId, new List<Account>());
                }
                parentIdAcctListMap.get(acc.ParentId).add(acc);
            }
        }
        /* Calculating Total Spend after the calculation of Current Spend and Pending Spend */
        if(!accountUpdateList.isEmpty()){
	        Double totalSpend = 0;
	        for(Account acct : accountUpdateList){
	            if(parentIdAcctListMap.containsKey(acct.Id)){
	                totalSpend = acct.Current_Spend__c;
	                for(Account acc : parentIdAcctListMap.get(acct.Id)){
	                    if(acc.Current_Spend__c != null){
	                        totalSpend += acc.Current_Spend__c;
	                    }
	                }
	            } else {
	                totalSpend = acct.Current_Spend__c;
	            }
	            acct.Total_Spend__c = totalSpend;
	        }
	        if(!accountUpdateList.isEmpty()){
	            update accountUpdateList;
	        }
        }              
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
}