public class PrintLOBReport {
public String[] selectedtelco{get;set;}
public String datename{get; set;}        
public String startdatename{get; set;}
public String enddatename{get; set;}
public List<Order_Line_Items__c> OrderLineItem{get;set;}
public string emailaddr{get;set;}
public boolean showexport{get;set;}
public boolean showback{get;set;}
public String telcoProduct1{get;set;}
public String content{get;set;}
public boolean exporthide {get;set;}
public List<FinalWrapper> finalList{get;set;}
public List<BillSummaryWrapper> display_list_1{get;set;}
public List<UDACWrapper>  display_list_2{get;set;}
public boolean IsNational{get;set;}
public boolean ShowReport{get;set;}
public boolean ShowError{get;set;}
public String DirectoryID{get;set;}
public String DirectoryName{get;set;}
public String DirectoryCode{get;set;}
public String TelcoName{get;set;}
public List<Order_Line_Items__c> OLIs{get;set;}
public List<c2g__codaInvoiceLineItem__c> ListILI{get;set;}
public Double OrderTotal{get;set;}
public Decimal MonthlyOrderTotal{get;set;}
public Integer InvoiceTotal{get;set;}
public Decimal UDACMonthlyOrderTotal{get;set;}
public Decimal UDACOrderTotal{get;set;}
public Integer UDACItemCount{get;set;}
public Date PublicationDate{get;set;}
public String contenttype{get;set;}
public String DirectoryEdition{get;set;}
public map<string,BillSummaryWrapper>BillingsummeryMap{get;set;}

public PrintLOBReport(){
BillingsummeryMap=new map<string,BillSummaryWrapper>();
if(Apexpages.currentpage().getparameters().get('telcoProduct')!=null)
{
  selectedtelco=String.valueof(Apexpages.currentpage().getparameters().get('telcoProduct')).split(',');
}
DirectoryID=Apexpages.currentpage().getparameters().get('directoryId');
DirectoryEdition=Apexpages.currentpage().getparameters().get('Edition');
    if(Apexpages.currentpage().getparameters().get('IsNational')!=null)
    {
     IsNational=Boolean.valueof(Apexpages.currentpage().getparameters().get('IsNational'));
    }
    else
    {
     IsNational=false;   
    }   
contenttype='text/html';
showreport=true;
if(selectedtelco!=null)
{
    if(selectedtelco.size()>0)
    {
        TelcoName ='';
        for(Telco__c tl :[Select Name from Telco__c where id in:selectedtelco])
        {
            TelcoName +=','+tl.Name;
        }
        TelcoName =TelcoName.substring (1, TelcoName.length());
        doGenerateCSV_email(selectedtelco,DirectoryID,IsNational,DirectoryEdition);
    }
}
}

public void doGenerateCSV_email(String[] telcoProduct,String Direcotryid,Boolean IsNational,String DirectoryEdition)
{
finalList=new List<FinalWrapper>();
display_list_1=new List<BillSummaryWrapper>();
display_list_2=new List<UDACWrapper>();
ListILI=new List<c2g__codaInvoiceLineItem__c>();
List<c2g__codaInvoiceLineItem__c> IterativeILI=new List<c2g__codaInvoiceLineItem__c>();
List<c2g__codaInvoiceLineItem__c> NationalIterativeILI=new List<c2g__codaInvoiceLineItem__c>();
Set<ID> InvoiceLineItemIds=new Set<ID>();
Map<ID,String> Dir_Code_map=new Map<ID,String>();
List<ID> DirectoryIDs=new List<ID>();
Set<Id> InvoiceIDS=new Set<ID>();
Set<Id> NationalInvoiceIDS=new Set<ID>();
Set<String> BillingPartnerSet=new Set<String>();
String TelcoName;
RecordType  objRT = CommonMethods.getRecordTypeDetailsByName('Order_Line_Items__c','National Order Line Item');



List<Directory_Edition__c> dirMap=[Select id,Telco__r.name,Pub_Date__c,Directory_Code__c,Directory__c,Directory__r.Name from Directory_Edition__c where id=:DirectoryEdition];
if(dirMap.size()>0)
{
DirectoryCode=dirMap[0].Directory_Code__c;
DirectoryName=dirMap[0].Directory__r.Name;
//TelcoName=dirMap[0].Telco__r.name;
PublicationDate=dirMap[0].Pub_Date__c;
}
if(DirectoryCode==null || DirectoryName==null || TelcoName==null || PublicationDate==null)
{
 Directory_Edition__c dir=[Select id,Telco__r.name,Pub_Date__c,Directory_Code__c,Directory__c,Directory__r.Name from Directory_Edition__c where id=:DirectoryEdition];
 DirectoryName=dir.Directory__r.Name;
 DirectoryCode=dir.Directory_Code__c;
 //TelcoName=dir.Telco_Provider__r.Name;
 PublicationDate=dir.Pub_Date__c ;
}

Set<ID> AccountIds=new Set<ID>();

IterativeILI=[SELECT c2g__Invoice__r.Transaction_Type__c,c2g__Invoice__r.c2g__Account__c,Order_Line_Item__r.Telco__r.Name,Order_Line_Item__r.Listing_Name__c,Order_Line_Item__r.Directory__c,Order_Line_Item__r.Billing_Partner__c,Order_Line_Item__r.RecordtypeId,c2g__Invoice__c,Id,Name,Order_Line_Item__c,c2g__Invoice__r.c2g__NetTotal__c FROM c2g__codaInvoiceLineItem__c where Order_Line_Item__r.RecordtypeId !=: objRT.Id AND Order_Line_Item__r.Product2__r.Media_Type__c='Print' AND Order_Line_Item__r.Telco__c in :telcoProduct  AND Order_Line_Item__r.Directory__c=: Direcotryid AND Order_Line_Item__r.Directory_Edition__c=:DirectoryEdition]; 
for(c2g__codaInvoiceLineItem__c ILI: IterativeILI)
{
       DirectoryIDs.add(ILI.Order_Line_Item__r.Directory__c);  
       InvoiceIDS.add(ILI.id);
       AccountIds.add(ILI.c2g__Invoice__r.c2g__Account__c);
}
ListILI.addall(IterativeILI);
System.debug('##################IterativeILI==='+IterativeILI);
if(ListILI.size()>0)
{
showback=true;
 //For Non National OLI Records
Map<String,AggregateResult> MapAccountNameGroup=new Map<String,AggregateResult>();
for(AggregateResult agrInvoice:[SELECT c2g__Invoice__r.Customer_Name__r.Name CustomerName,SUM(c2g__NetValue__c) TotalAmount,SUM(Order_Line_Item__r.UnitPrice__c) MonthlyAmount,Order_Line_Item__r.Billing_Partner__c BillingPartner from c2g__codaInvoiceLineItem__c where id in : InvoiceIDS Group By c2g__Invoice__r.Customer_Name__r.Name,Order_Line_Item__r.Billing_Partner__c])
{
  MapAccountNameGroup.put(String.Valueof(agrInvoice.get('CustomerName')),agrInvoice);
}
 set<string>InvoiceRecipientName=new set<string>();
 

for(c2g__codaInvoiceLineItem__c InvoieLI: [SELECT Order_Line_Item__r.Account__r.Account_Number__c,Order_Line_Item__r.Account__r.Parent.Name,c2g__Invoice__r.Transaction_Type__c,Order_Line_Item__r.Payment_Method__c,Order_Line_Item__r.Listing__r.Name,c2g__Invoice__r.c2g__Account__r.Name,Order_Line_Item__r.Billing_Contact__r.Phone,c2g__Invoice__r.c2g__Account__r.Berry_ID__c,Order_Line_Item__r.Billing_Frequency__c,Order_Line_Item__r.RecordTypeID,c2g__Invoice__r.Name,Order_Line_Item__r.Billing_Partner__c,c2g__NetValue__c,c2g__Invoice__r.c2g__Account__r.Phone,c2g__Product__r.name,c2g__Quantity__c,c2g__StartDate__c,c2g__Invoice__r.c2g__InvoiceDate__c,c2g__UnitPrice__c,Name,Order_Line_Item__r.name,c2g__Invoice__r.Customer_Name__r.Name from c2g__codaInvoiceLineItem__c where id in : InvoiceIDS ])
{
         
          String RecortTypeName;
          Double TotalMonthlyAmount=0.0;
          String CustAccName;
          String MapKeyName=InvoieLI.c2g__Invoice__r.Customer_Name__r.Name;
         // String MapKeyName =InvoieLI.c2g__Invoice__r.c2g__Account__r.Name+InvoieLI.Order_Line_Item__r.Listing__r.Name;
          RecortTypeName=InvoieLI.Order_Line_Item__r.Billing_Partner__c;
          
          if(MapAccountNameGroup.containskey(MapKeyName))
          {
             TotalMonthlyAmount=Double.valueof(MapAccountNameGroup.get(MapKeyName).get('MonthlyAmount'));
             
          }
          if(!InvoiceRecipientName.contains(MapKeyName))
          {
              if(InvoieLI.Order_Line_Item__r.Payment_Method__c=='Telco Billing')
              {
                CustAccName=InvoieLI.c2g__Invoice__r.Customer_Name__r.Name;
              }
              else
              {
                CustAccName=InvoieLI.c2g__Invoice__r.Customer_Name__r.Name;
                //CustAccName=InvoieLI.c2g__Invoice__r.c2g__Account__r.Name;
              }
              
              finallist.add(new FinalWrapper(CustAccName,RecortTypeName,InvoieLI.Order_Line_Item__r.Billing_Contact__r.Phone,InvoieLI.Order_Line_Item__r.Account__r.Account_Number__c,TotalMonthlyAmount,null,InvoieLI.Order_Line_Item__r.Billing_Frequency__c,InvoieLI.Order_Line_Item__r.Billing_Partner__c,InvoieLI.c2g__Invoice__r.c2g__Account__r.Phone,'12',InvoieLI.Order_Line_Item__r.Billing_Partner__c,'null',InvoieLI.Order_Line_Item__r.Listing__r.Name,CustAccName,String.valueof(InvoieLI.Order_Line_Item__r.Account__r.Parent.Name),String.valueof(InvoieLI.c2g__Invoice__r.Transaction_Type__c)));
              InvoiceRecipientName.add(MapKeyName);
          }
         
}  

//For National Order Contact Phone
Map<String,String> AccName_ConPhoneMap=new Map<String,String>();
System.debug('#$#$#$#$#$#$#$#$#$#$#'+AccountIds);
for(Account accIterator:[Select id,Name,Phone, (Select id,Phone from Contacts where Primary_Contact__c=true AND IsActive__c=true Limit 1) from Account where Id in: AccountIds])
{
   if(accIterator.Contacts.size()>0)
   {
     if(accIterator.Contacts[0].Phone!=null)
     {
        AccName_ConPhoneMap.put(accIterator.Name,accIterator.Contacts[0].Phone);
     }
     else
     {
       AccName_ConPhoneMap.put(accIterator.Name,String.valueof(accIterator.Phone));
     }
   }
   else
   {
       AccName_ConPhoneMap.put(accIterator.Name,String.valueof(accIterator.Phone));
   }
     
}

if(finallist.size()>0)
{
  
  map<String,Double> mapBPAmount=new map<String,Double>();
  for(AggregateResult agr:[SELECT Order_Line_Item__r.Billing_Partner__c BillingPartner,SUM(Order_Line_Item__r.UnitPrice__c) MonthlyTotal from c2g__codaInvoiceLineItem__c where id in :InvoiceIDS Group By Order_Line_Item__r.Billing_Partner__c])
  {
     mapBPAmount.put(String.Valueof(agr.get('BillingPartner')),Double.Valueof(agr.get('MonthlyTotal')));
  }
  
  for(FinalWrapper frm: finallist)
  {
      if(!BillingsummeryMap.containskey(frm.BillingPartner))
      {
          BillingsummeryMap.put(frm.BillingPartner,new BillSummaryWrapper(frm.BillingPartner,mapBPAmount.get(frm.BillingPartner),'null',1));
      }
      else if(BillingsummeryMap.containskey(frm.BillingPartner))
      {
            BillingsummeryMap.get(frm.BillingPartner).CountInvoice=BillingsummeryMap.get(frm.BillingPartner).CountInvoice+1;
      }
      
  }
  if(BillingsummeryMap.size()>0)
  {
       MonthlyOrderTotal=0.00;
       InvoiceTotal=0;
      for(BillSummaryWrapper bswrp: BillingsummeryMap.values())
      {
          MonthlyOrderTotal=MonthlyOrderTotal+Decimal.valueof(bswrp.MonthlyBilling);
          InvoiceTotal=InvoiceTotal+bswrp.CountInvoice;
      }
  }
}
else
{
showback=true;
ShowReport=false;
ShowError=true;
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No Record Found. Kindly go back & refine your search filter');
ApexPages.addMessage(myMsg);
}

if(InvoiceIDS.size()>0)
{
// For UDAC Summary

for(AggregateResult agr1:[SELECT Order_Line_Item__r.Billing_Partner__c BillingPartner,Order_Line_Item__r.Recordtype.Name RecordType,Order_Line_Item__r.Product2__r.ProductCode UDAC,SUM(Order_Line_Item__r.Order_Line_Total__c) OrderTotal,SUM(Order_Line_Item__r.UnitPrice__c) MonthlyPayment,SUM(c2g__Invoice__r.c2g__NetTotal__c) InvoiceTotal,Count(ID) TotalItems FROM c2g__codaInvoiceLineItem__c where id in :InvoiceIDS Group By Order_Line_Item__r.Recordtype.Name,Order_Line_Item__r.Product2__r.ProductCode,Order_Line_Item__r.Billing_Partner__c])
{
      Double TotalMonthlyAmount=0.0;
      Double TotalAmount=0.0;
      String BillingPartner=null;
      String UDAC=null;
      Integer ItemCount=0;
      if(agr1.get('MonthlyPayment')!=null)
      {
      TotalMonthlyAmount=Double.valueof(agr1.get('MonthlyPayment'));
      }
      if(agr1.get('InvoiceTotal')!=null)
      {
      TotalAmount=Double.valueof(agr1.get('InvoiceTotal'));
      }
      if(agr1.get('BillingPartner') != null)
      {
      BillingPartner=String.valueof(agr1.get('BillingPartner'));
      }
      if(agr1.get('UDAC') !=null)
      {
      UDAC=String.valueof(agr1.get('UDAC'));
      }
      if(agr1.get('TotalItems')!=null)
      {
      ItemCount=Integer.valueof(agr1.get('TotalItems'));
      }
      if(agr1.get('RecordType') != null)
      {
         if(agr1.get('RecordType') == 'National Order Line Item')
         {
          BillingPartner='National Billed';
         }
         else
         {
         BillingPartner=String.Valueof(agr1.get('BillingPartner'));
         }
      }
      System.debug('@@@@@@@@@@@'+TotalMonthlyAmount+'###############'+TotalAmount+'#############'+BillingPartner);
      
      display_list_2.add(new UDACWrapper(BillingPartner,UDAC,TotalMonthlyAmount,TotalAmount,ItemCount));
}
}


if(NationalInvoiceIDS.size()>0)
{
// For National UDAC Summary

for(AggregateResult agrNational:[SELECT ISS_Line_Item__r.Product__r.ProductCode UDAC,SUM(ISS_Line_Item__r.Gross_Amount__c) MonthlyPayment,SUM(c2g__Invoice__r.c2g__NetTotal__c) InvoiceTotal,Count(ID) TotalItems FROM c2g__codaInvoiceLineItem__c where id in :NationalInvoiceIDS Group By ISS_Line_Item__r.Product__r.ProductCode])
{
      Double TotalMonthlyAmount=0.0;
      Double TotalAmount=0.0;
      String BillingPartner=null;
      String UDAC=null;
      Integer ItemCount=0;
      if(agrNational.get('MonthlyPayment')!=null)
      {
      TotalMonthlyAmount=Double.valueof(agrNational.get('MonthlyPayment'));
      }
      if(agrNational.get('InvoiceTotal')!=null)
      {
      TotalAmount=Double.valueof(agrNational.get('InvoiceTotal'));
      }
      
      BillingPartner='National Billed';
      
      if(agrNational.get('UDAC') !=null)
      {
      UDAC=String.valueof(agrNational.get('UDAC'));
      }
      if(agrNational.get('TotalItems')!=null)
      {
      ItemCount=Integer.valueof(agrNational.get('TotalItems'));
      }
      display_list_2.add(new UDACWrapper(BillingPartner,UDAC,TotalMonthlyAmount,TotalAmount,ItemCount));
}
}

if(display_list_2.size()>0)
{
   UDACOrderTotal=0.00;
   UDACMonthlyOrderTotal=0.00;
   UDACItemCount=0;
    for(UDACWrapper u:display_list_2)
    {
      UDACOrderTotal=UDACOrderTotal+Decimal.valueof(u.TotalBilling);
      UDACMonthlyOrderTotal=UDACMonthlyOrderTotal+Decimal.valueof(u.MonthlyBilling);
      UDACItemCount=UDACItemCount+u.ItemCount;
    }
}
}
else
{
ShowReport=false;
ShowError=true;
showback=true;
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Error: No Record Found. Kindly go back & refine your search filter');
ApexPages.addMessage(myMsg);
}

}

public void ExportReport()
{
  showreport=false;
  showback=false;
  contenttype='application/vnd.ms-excel#PrintLOBReport.xls';
  
}

public PageReference cancel()
{
 PageReference pg=new PageReference('/apex/TelcoLOBReport');
 pg.setredirect(true);
 return pg;
 
}
public class BillSummaryWrapper
{
        
        public String  Billing_Partner{get; set;} 
        public Decimal MBilling{get;set;}
        public Double MonthlyBilling{get;set;}
        public String PI{get;set;}
        public Integer CountInvoice{get;set;}
        public BillSummaryWrapper(String  Billing_Partner,Double MonthlyBilling,String PI,Integer CountInvoice) 
        {
           
           this.Billing_Partner=Billing_Partner;
           this.MonthlyBilling=MonthlyBilling;
           this.MBilling=Decimal.valueof(MonthlyBilling).setscale(2);
           this.PI=PI;
           this.CountInvoice=CountInvoice;
        }
}

public class UDACWrapper{
        
        public String  Billing_Partner{get; set;} 
        public String UDAC{get; set;} 
        public Double MonthlyBilling{get;set;}
        public Double TotalBilling{get;set;}
        public Integer ItemCount {get;set;}
        public Decimal TBilling{get;set;}
        public Decimal MBilling{get;set;}
        public UDACWrapper(String  Billing_Partner,String UDAC,Double MonthlyBilling,Double TotalBilling,Integer ItemCount) 
        {
           
           this.Billing_Partner=Billing_Partner;
           this.TotalBilling=TotalBilling;
           this.TBilling=Decimal.valueof(TotalBilling).setscale(2);
           
         //  this.TotalBilling=ItemCount*MonthlyBilling;
           this.MonthlyBilling=MonthlyBilling;
           this.MBilling=Decimal.valueof(MonthlyBilling).setscale(2);
           
           this.UDAC=UDAC;
           this.ItemCount=ItemCount;
          
        }
    }
      
   
public class FinalWrapper{
        public String InvoiceRecipientName{get; set;}
        public String BTN{get; set;} 
        public Double MonthlyDirectoryTotal{get; set;}
        public Decimal MDTotal{get;set;}
        public String PI{get; set;}  
        public String BillingFrequency{get; set;} 
        public String BillingPartner{get;set;}
        public String ListedPhone{get; set;} 
        public String BookLength{get;set;}
        public String BilledBy{get;set;} 
        public String Operator{get;set;} 
        public String ListingName{get;set;} 
        public String EnterpriseId {get;set;}
      //  public Double TotalBilling{get;set;}
        public String RecortTypeName{get;set;}
        public String CustomerAccountName{get;set;}
        public String ParentAccountName{get;set;}
        public String TransactionType{get;set;}
        public FinalWrapper(String InvoiceRecipientName,String RecortTypeName,String BTN,String EnterpriseId,Double MonthlyDirectoryTotal,String PI,String BillingFrequency,String BillingPartner,String ListedPhone,String BookLength,String BilledBy,String Operator,String ListingName,String CustomerAccountName,String ParentAccountName,String TransactionType)
        {
          this.TransactionType=TransactionType;
          this.InvoiceRecipientName=InvoiceRecipientName;
          this.BTN=BTN;
          this.RecortTypeName=RecortTypeName;
          this.EnterpriseId =EnterpriseId ;
          this.MonthlyDirectoryTotal=MonthlyDirectoryTotal;
          this.MDTotal=Decimal.valueof(MonthlyDirectoryTotal).setscale(2);
          this.PI=PI;  
          this.BillingFrequency=BillingFrequency;
          this.BillingPartner=BillingPartner;
          this.ListedPhone=ListedPhone;
          this.BookLength=BookLength;
          this.BilledBy=BilledBy;
          this.Operator=Operator;
          this.ListingName=ListingName;
          this.ParentAccountName=ParentAccountName;
          this.CustomerAccountName=CustomerAccountName;
         // this.TotalBilling=TotalBilling;
        
        }    
}
}