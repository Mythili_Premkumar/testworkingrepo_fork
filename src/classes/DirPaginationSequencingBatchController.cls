public class DirPaginationSequencingBatchController {
    public static void assignSequence(List<Directory_Pagination__c> listdirPagination, Decimal startNum) {
        List<Directory_Pagination__c> dirPaginationList = new List<Directory_Pagination__c>();
        Decimal initial = startNum;
        //List<DirPaginationWrapper> dirPaginationWrapperList = new List<DirPaginationWrapper>();
        
        dirPaginationList = listdirPagination;
        
        /*for(Directory_Pagination__c DP : dirPaginationList) {
            dirPaginationWrapperList.add(new DirPaginationWrapper(DP));
        }
        
        dirPaginationWrapperList.sort();
        dirPaginationList.clear();
        
        for(DirPaginationWrapper DPW : dirPaginationWrapperList) {
            dirPaginationList.add(DPW.dirPagination);
        }*/ 

        for(Directory_Pagination__c DP : dirPaginationList) {                   
            DP.Sequence_in_Section__c = initial;
            //added by Mythili - for ATP-367
            if(DP.Sequence_in_Section__c!=null){
            	DP.Sequence_Padded__c=String.valueOf(DP.Sequence_in_Section__c).leftpad(5,'0');
            }else{
            	DP.Sequence_Padded__c='00000';
            }
            
            initial = initial + 10;
        }        
                
        update dirPaginationList;
    }
    
    /*public class DirPaginationWrapper implements Comparable {

        public Directory_Pagination__c dirPagination = new Directory_Pagination__c();
        
        // Constructor
        public DirPaginationWrapper(Directory_Pagination__c dirPaginationRecord) {
            dirPagination = dirPaginationRecord;
        }
        
        // Compare dirPagination based on the Directory_Pagination__c Directory_Heading__c.
        public Integer compareTo(Object compareTo) {
            // Cast argument to DirPaginationWrapper
            DirPaginationWrapper compareTodirPagination = (DirPaginationWrapper)compareTo;
            
            // The return value of 0 indicates that both elements are equal.
            Integer returnValue = 0;
            if (dirPagination.Directory_Heading__c > compareTodirPagination.dirPagination.Directory_Heading__c) {
                // Set return value to a positive value.
                returnValue = 1;
            } else if (dirPagination.Directory_Heading__c < compareTodirPagination.dirPagination.Directory_Heading__c) {
                // Set return value to a negative value.
                returnValue = -1;
            }
            
            return returnValue;       
        }
    }*/
    
    /* Populating Achor Ref */
    public static void populateAnchorRef(List<Directory_Pagination__c> dirPaginationList) {
        List<Directory_Pagination__c> dirPaginationUpdateList = new List<Directory_Pagination__c>();
        List<Directory_Pagination__c> dirPaginationRetrieveList = new List<Directory_Pagination__c>();
        Map<Id, Directory_Pagination__c> dirPaginationMap = new Map<Id, Directory_Pagination__c>();
        Map<Id, List<Directory_Pagination__c>> underCaptionDirPaginationListMap = new Map<Id, List<Directory_Pagination__c>>();
        Set<String> setUnderCaption = new Set<String>();
        
        for(Directory_Pagination__c DP : dirPaginationList) {
            if(DP.Under_Caption__c!= null){                
                setUnderCaption.add(DP.Under_Caption__c);
                DP.Anchor_Ref__c = ''; 
                dirPaginationMap.put(DP.Id, DP);
            }
        }
        
        update dirPaginationList;
        
        dirPaginationRetrieveList = DirectoryPaginationSOQLMethods.getDirPaginationByUnderCaption(setUnderCaption);
        
        if(dirPaginationRetrieveList.size() > 0) {
            for(Directory_Pagination__c DP : dirPaginationRetrieveList) {
                if(!underCaptionDirPaginationListMap.containsKey(DP.DP_OLI_Anchor_Caption_Header__c)){
                    underCaptionDirPaginationListMap.put(DP.DP_OLI_Anchor_Caption_Header__c, new List<Directory_Pagination__c>());
                }
                underCaptionDirPaginationListMap.get(DP.DP_OLI_Anchor_Caption_Header__c).add(DP);
            }
            if(underCaptionDirPaginationListMap.size() > 0) {
                for(Id underCaption : dirPaginationMap.keySet()) {
                    Directory_Pagination__c dirPagination = new Directory_Pagination__c();
                    dirPagination = dirPaginationMap.get(underCaption);         
                    List<Directory_Pagination__c> tempDirPaginationList = new List<Directory_Pagination__c>();
                    String anchorRef = '';
                    
                    tempDirPaginationList = underCaptionDirPaginationListMap.get(dirPagination.DP_OLI_Anchor_Caption_Header__c);
                                
                    if(tempDirPaginationList.size() > 0) {
                        for(Directory_Pagination__c DP : tempDirPaginationList) {
                            if(DP.Sequence_in_Section__c != null) {
                                anchorRef += String.valueOf(DP.Sequence_in_Section__c) + ' ';
                            }
                        }    
                        dirPagination.Anchor_Ref__c = String.isBlank(dirPagination.Anchor_Ref__c) ? anchorRef.trim() : dirPagination.Anchor_Ref__c + anchorRef.trim();
                        dirPaginationUpdateList.add(dirPagination);     
                    }
                }
                
                if(dirPaginationUpdateList.size() > 0) {
                    update dirPaginationUpdateList;
                }
            }
        }
    }
}