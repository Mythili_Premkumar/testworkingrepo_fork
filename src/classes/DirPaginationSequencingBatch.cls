global class DirPaginationSequencingBatch implements Database.Batchable<sobject>, Database.Stateful{
    Integer SequenceNumberStart = 10;
    Id idDirSec;
    String DPtype;
    string dirEditnYear;
    List<Pagination_Job__c> paginationJobs = new  List<Pagination_Job__c>();   
    DirPaginationSequencingBatch nextBatch;
    
    global DirPaginationSequencingBatch(Id idDirSection, String typ, String year,  List<Pagination_Job__c> PJs){
        idDirSec = idDirSection;
        DPtype = typ;
        dirEditnYear = year;
        if(PJs != null){
            paginationJobs = PJs;
        }
    }
    
    global DirPaginationSequencingBatch(Id idDirSection, String typ, String year,  List<Pagination_Job__c> PJs, DirPaginationSequencingBatch DPSB){
        idDirSec = idDirSection;
        DPtype = typ;
        dirEditnYear = year;
        if(PJs != null){
            paginationJobs = PJs;
        }
        nextBatch = DPSB;
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Display_Ad__c, Banner__c, Billboard__c, DP_OLI_Anchor_Caption_Header__c, Sequence_in_Section__c, Directory_Heading__c, UDAC_Group__c, Seniority_Date__c, Directory_Section__c, Id, Name, Anchor_Ref__c, Under_Caption__c, Order_Line_Item__c FROM Directory_Pagination__c WHERE Directory_Section__c = : idDirSec AND Year__c = : dirEditnYear';
        if(DPtype == 'YP') {
            SOQL += ' AND Display_Ad__c = true ORDER BY Directory_Heading_Normalized_Sort_Name__c, Leader_Ad__c DESC, Display_Ad__c DESC, Coupon_Ad__c, UDAC_Group__c, Seniority_Date__c';
        } else if(DPtype == 'WP'){
            //SOQL += ' AND (Display_Ad__c = true OR Banner__c = true OR Billboard__c = true) ORDER BY Directory_Edition_Name__c ASC, Banner__c DESC, Billboard__c DESC, Display_Ad__c DESC, Sort_As__c ASC, Name ASC';
            SOQL += ' ORDER BY Directory_Edition_Name__c ASC, Banner__c DESC, Billboard__c DESC, Display_Ad__c DESC, Sort_As__c ASC, Name ASC';
        }
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> listDirPagination) {
        try{
            if(paginationJobs.size() > 0){ 
                for(Pagination_Job__c PJ : paginationJobs) {
                    PJ.Sequencing_Status__c = 'In Progress';
                }
                update paginationJobs;
            }
            list<Directory_Pagination__c> lstFinalDirectoryPagination = new list<Directory_Pagination__c>();
            for(Directory_Pagination__c DP : listDirPagination) {                   
                if(DPtype == 'WP'){
                    if(DP.Display_Ad__c|| DP.Banner__c || DP.Billboard__c) {
                        lstFinalDirectoryPagination.add(DP);
                    }
                }
                else {
                    lstFinalDirectoryPagination.add(DP);
                }
            }
            DirPaginationSequencingBatchController.assignSequence(lstFinalDirectoryPagination, SequenceNumberStart);
            DirPaginationSequencingBatchController.populateAnchorRef(lstFinalDirectoryPagination);
            SequenceNumberStart = SequenceNumberStart + lstFinalDirectoryPagination.size() * 10;
        }catch(Exception e){
            system.debug('*****The exception is****'+e);
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //Boolean bolMailFlag = true;
        /*String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com', 'mduraipandia@csc.com'};*/
        /*if(a.JobItemsProcessed > 0 && paginationJobs.size() > 0) {            
            if(a.NumberOfErrors != a.JobItemsProcessed) {
                //insert paginationJobs;
            }
        }*/
        if(a.NumberOfErrors > 0) {
            futureCreateErrorLog.createErrorRecordBatch(a.ExtendedStatus, 'Directory Pagination Sequencing batch process status :'  + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.', 'Directory Pagination Sequencing batch process');
            CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Directory Pagination Sequencing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            //bolMailFlag = false;
        }
        else if(a.JobItemsProcessed != a.TotalJobItems) {
            futureCreateErrorLog.createErrorRecordBatch('Either the batch job was aborted or Job failed to process due to internal error', 'Directory Pagination Sequencing batch process status : ' + a.Status+'. The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
                ' batches with '+ a.NumberOfErrors + ' failures.', 'Directory Pagination Sequencing batch process');
            
            CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Directory Pagination Sequencing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception/Errors record.');
            
        }
        
        if(paginationJobs.size() > 0) {
        
            if(a.JobItemsProcessed == a.TotalJobItems && a.NumberOfErrors == 0) { 
                for(Pagination_Job__c PJ : paginationJobs) {
                    PJ.Sequencing_Status__c = a.Status;
                }
                update paginationJobs;
            } else {
                for(Pagination_Job__c PJ : paginationJobs) {                    
                    PJ.PJ_Sequencing_Status_Detail__c = a.ExtendedStatus;
                    if(a.Status != 'Completed') {
                        PJ.Sequencing_Status__c = a.Status;
                    }
                    else {
                        PJ.Sequencing_Status__c = 'Failed';
                    }
                }
                update paginationJobs;
            }
        }
        
        if(nextBatch != null){
            Database.executeBatch(nextBatch);
        }
    }
}