public with sharing class UserSOQLMethods {	
	public static list<User> getUserDetailsByID(set<Id> setUserIds) {
		return [Select ManagerId, Id, Email From User where Id IN:setUserIds];
	}
}