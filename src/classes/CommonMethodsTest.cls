//common methods Test class
@isTest(seeAllData=true)
public class CommonMethodsTest{

    static testmethod void commettest(){        
        Account newAccount = TestMethodsUtility.createAccount('customer');      
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);  
        Product2 newProduct = TestMethodsUtility.generateproduct();
        Pricebook2 pricebook = TestMethodsUtility.createpricebook();
        newProduct.Family = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = pricebook.Id;
        newOpportunity.Billing_Contact__c = newContact.Id;
        newOpportunity.Signing_Method__c = CommonMessages.opportunitySigningMethodDocuSign;
        insert newOpportunity;  
        PriceBookEntry PBE = TestMethodsUtility.createpricebookentry(newProduct.Id, pricebook.Id);
        Directory_Heading__c directoryHeading = TestMethodsUtility.createDirectoryHeading();
        Directory__c Directory = TestMethodsUtility.createDirectory();
        OpportunityLineItem OLI = TestMethodsUtility.generateOpportunityLineItem();
        OLI.PricebookEntryId = PBE.Id;
        OLI.OpportunityId = newOpportunity.Id;
        OLI.Heading__c = directoryHeading.Name;
        OLI.Package_ID__c = 'TestPackage';
        OLI.Directory__c = Directory.Id;
        insert OLI;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        string error1,message;
        list<String> lstStrErrors = new list<String>();   
        lstStrErrors.add(error1);
        List<Directory_Product_Mapping__c> dirProdMappingList = new List<Directory_Product_Mapping__c>();
        List<Directory_Section__c> dirSecList = new List<Directory_Section__c>();
        List<Section_Heading_Mapping__c> secHeadMapList = new List<Section_Heading_Mapping__c>();
        
        Digital_Product_Requirement__c DFF = TestMethodsUtility.createDataFulfillmentForm('Print Graphic');
        Fulfillment_Profile__c FP = TestMethodsUtility.createFulfillmentProfile('Initial Fulfillment Profile', newAccount);
        
        String myString = 'StringToBlob';
        Blob myBlob = Blob.valueof(myString);
    
        Test.startTest();
        //Call all the methods.
        system.assertnotequals(null,CommonMethods.getRedordTypeIdByName('Customer Account','Account'));
        CommonMethods.getCurrentPeriod();
        //edited
        CommonMethods.getsObjectRecordTypeIdKeyset('Account');
        CommonMethods.getsObjectRecordTypeNameKeyset('Account');
        CommonMethods.getDescribSObject('Account');
        CommonMethods.getRecordTypeDetailsByName('Account','Customer Account');
        CommonMethods.getRecordTypeName();
        CommonMethods.addError('error');
        CommonMethods.addINFO('error');
        CommonMethods.checkOpportunityHasClosedWon(newOpportunity);
        Opportunity objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(new set<Id>{newOpportunity.ID})[0];
        CommonMethods.checkOpportunityandBMIQuoteBeforeCreateOrder(objOpportunity,lstStrErrors);
        CommonMethods.checkQuantityBeforeCreateOrder(newOpportunity);
        CommonMethods.validateQuantity(newOpportunity,dirProdMappingList,dirSecList, secHeadMapList, lstStrErrors);
        CommonMethods.checkBillingInfo(newOpportunity,lstStrErrors);
        CommonMethods.addBatchUpdateList(lstStrErrors,'message');
        CommonMethods.checkSigningMethods(newOpportunity,lstStrErrors);
        CommonMethods.newAttachment(myBlob,'attachmentname',newAccount.Id);
        //CommonMethods.dff 370
        //CommonMethods.getPageContent('http://www.google.com/');
        //728
        set < Id > groupId = new set < Id >();
        map < String, Id > mapGroup = new map < String, Id >();
        map < String, String > mapDivistion = new map < String, String >();
        CommonMethods.mapQueueSObjectType(groupId );
        CommonMethods.updateDivisionWithOwnerID(mapGroup,mapDivistion);
        Division__c dd=new Division__c();
        String JSONString = JSON.serialize(dd);
        CommonMethods.updateDivisionWithOwnerID(JSONString );
        list < Id > nextApproverId = new  list < Id >();
        nextApproverId.add(UserInfo.getUserId());
        //CommonMethods.submitApprovalProcess(newAccount.Id,nextApproverId);
        //1079
        CommonMethods.createTask('subject','status','priority', newAccount.Id, UserInfo.getUserId()); 
        CommonMethods.copyValuesFromProfileToDFF(DFF, FP, newOrLI);  
        //CommonMethods.copyValuesFromDFFToFProfile(DFF, FP, newOrLI);
        //CommonMethods.oliHistoryByOliId(newOrLI.Id);     
        Test.stopTest();
    }
}