public class PortalBalancePayController {
    public decimal invoiceSum { get; set; }
    public decimal payAmount { get; set; }
    private List<c2g__codaInvoice__c> invList;
    private User u;
    private pymt__PaymentX__c payment;
    public pymt__Processor_Connection__c processor {get; set;}
    private string baseURL = string.valueOf(System.Url.getSalesforceBaseUrl().toExternalForm()) + '/berry360';
    private String finishURL;
    private String cancelURL;
    public boolean autoPayEnrolled { get; set; }
    public boolean optIn { get; set; }
    public boolean noDefaultPMFound { get; set; }
    public boolean showOptInConfirmationMsg { get; set; }
   
    public PortalBalancePayController() {
        try {
            finishURL = baseURL + ApexPages.currentPage().getUrl();
            finishURL = finishURL.substringBefore('?');
            cancelURL = baseURL + ApexPages.currentPage().getUrl();
            cancelURL = cancelURL.substringBefore('?');
        
             u = [SELECT id, email, usertype, firstname, lastname, phone, 
                    street, city, country, postalcode, state, contact.Agreed_To_AutoBill__c, contactId, contact.accountid FROM User
                    WHERE id = :UserInfo.getUserId()];
           
 
           
             if (u.usertype == 'GUEST') {
                    ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You are Not Logged in'));
                     return;
                    }  
           
           this.autoPayEnrolled = u.contact.Agreed_To_AutoBill__c;
           this.optIn = this.autoPayEnrolled;
           
             invList = [select c2g__account__c, id, c2g__OutstandingValue__c from c2g__codaInvoice__c 
                    where Payment_Type__c != 'ECheck' 
                    AND  c2g__PaymentStatus__c !='Paid'AND c2g__Opportunity__r.Billing_Partner__c <> 'THE BERRY COMPANY'
                    AND c2g__Account__c =: u.contact.accountid ];
        
              this.invoiceSum = 0;
              for (c2g__codaInvoice__c  tmpInv : invList) {
                  this.invoiceSum = this.invoiceSum + tmpInv.c2g__OutstandingValue__c;  
                }
        }
        catch(exception ex) {
             ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Portal Generated Error:  ' + ex.getMessage() + ' Line Number: ' + ex.getLineNumber()));
        }
    }

    private boolean setupPaymentRecord() {
        try {
            if (this.payment == null) {
                this.payment = new pymt__PaymentX__c();
                this.payment.pymt__amount__c = this.payAmount;
                this.payment.pymt__contact__c = this.u.contactid;
                this.payment.name = 'Portal Payment';
                  this.payment.pymt__Invoice_Number__c = String.valueOf(Math.Random()).substring(2,9);
                 this.payment.name += ' ' + this.payment.pymt__Invoice_Number__c;
                this.payment.pymt__status__c = 'In Process';
                this.payment.pymt__date__c = System.today();
                this.payment.pymt__Currency_ISO_Code__c = this.processor.pymt__Default_Currency__c;
                this.payment.pymt__payment_processor__c = this.processor.pymt__processor_id__c;
                this.payment.pymt__processor_connection__c = this.processor.id;
                this.payment.pymt__account__c = u.contact.accountid;
                this.payment.pymt__log__c = ' ++++ Payment for Portal User'+
                     this.payment.pymt__Amount__c +  this.payment.pymt__Currency_ISO_Code__c;
            }
        if (this.payment.id == null) {
            insert this.payment;
        } else {
            update this.payment;
        }
    }
    catch (Exception ex) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error setting up purchase records:' + ex.getMessage()));
        return false;
    }
        return true;
}
    
    public PageReference acceptTermsandSubmit() {
    User[] assigneeUsers;
    string assigneeUserName = ApexPages.currentPage().getParameters().get('assigneeUserName');
      
    string assigneeUserId;
    
    
    try {
        //does user have a default payment method?
         List<pymt__Payment_Method__c> defaultPM = [ select id FROM pymt__Payment_Method__c  
                WHERE pymt__Contact__c =: u.ContactId
                AND Expiration_Date__c >=: system.today()
                AND pymt__Default__c = True];
        if (defaultPM.size() == 0) {
            this.noDefaultPMFound = true;
            return null;
        }
        
        if (!pymt.Util.isNullOrEmpty(assigneeUserName)) {
       
            assigneeUsers = [ select id from User where username =: assigneeUserName and isActive = true ];
            if (assigneeUsers.size() > 0) {
                assigneeUserId = assigneeUsers[0].id;
            }
        }
        if (pymt.Util.isNullOrEmpty(assigneeUserId)) {
            assigneeUserId = Site.getAdminId();
        }
          if (!pymt.Util.isNullOrempty(assigneeUserID)) {
            Task t = new Task();
                t.whoId = u.Contactid;
                t.whatId = u.Contact.AccountId;
                t.ownerId = assigneeUserId;
                t.subject = 'Set up Portal User for Auto-Bill';
                t.Status = 'Not Started';
                t.Priority = 'High';
                t.ActivityDate = System.today();
                t.CallType = 'Internal';
                t.Description = 'Portal User accepted auto bill pay terms from this IP address: ' +  this.getCurrentIPAddress();
                insert t;
            }
        }
    catch(exception ex) {
         ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error preparing Auto-Bill task request' + ex.getMessage() + ' Line Number:  ' + ex.getLineNumber()));
    }    
    this.autoPayEnrolled = true;
    
    try {
        Contact updateContact = new Contact(id = u.contactId, agreed_to_autobill__c = true);
        update updateContact;
        this.showOptInConfirmationMsg = true;
    }
    catch(exception ex) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Error Updating Portal Contact' + ex.getMessage()));
    }
    return null;
    }
    
     public String getCurrentIPAddress(){
        string ipAddress;
        if (pymt.Util.IsNullOrEmpty(ipAddress)){
            ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
            if (pymt.Util.IsNullOrEmpty(ipAddress)) ipAddress='255.255.255.0';
        }
       return ipAddress;
   }   
    
    public PageReference submitAmount() {
     PageReference nextPage;
     
     if (!(this.payAmount > 0) && (this.payAmount != null)) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING,'Pay Amount must be greater than 0.  Please verify'));
        return null;
     }
     
        if (this.processor == null) {
            String connectionId = pymt.PaymentX.getSiteDefaultProcessorConnection(Site.getName());
        if (connectionId <> null) {
            this.processor = [Select id, pymt__default_connection__c, pymt__processor_id__c, pymt__test_mode__c, pymt__Default_Currency__c,
                            pymt__PP_Merchant_Id__c from pymt__Processor_Connection__c where isDeleted = false and id = :connectionId
                          ];
        }
        
        if (this.processor == null ) {
              ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Payment Processor is not defined on this Site Configuration'));
                return null;
        }
      }
     
        if (setupPaymentRecord() == true) {
            nextPage = Page.pymt__ptlCheckout;   
            nextPage.getParameters().put('pid',this.payment.Id);  // pass payment record id to checkout 
              if (!pymt.Util.isNullOrEmpty(this.cancelURL)) nextPage.getParameters().put('cancel_url', EncodingUtil.urlEncode(this.cancelURL,'UTF-8'));
              if (!pymt.Util.isNullOrEmpty(this.finishURL)) nextPage.getParameters().put('finish_url', EncodingUtil.urlEncode(this.finishURL,'UTF-8'));   
            return nextPage;
        }
        return null;
    }
}