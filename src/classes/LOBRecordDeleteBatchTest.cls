@istest
public with sharing class LOBRecordDeleteBatchTest {
    static testmethod void testSample() {
        Test.startTest();
        List_Of_Business__c LOB = new List_Of_Business__c();
        insert LOB;
        Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
        String jobId = System.schedule('LOBCleanUpScheduler', CRON_EXP, new LOBCleanUpScheduler() );   
        Test.stopTest(); 
    }

}