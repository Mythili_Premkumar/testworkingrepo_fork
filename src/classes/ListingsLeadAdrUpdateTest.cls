@isTest(seeAllData=True)
public class ListingsLeadAdrUpdateTest{
    public static testMethod void ListingsLeadAdrUpdateTest01(){
    
    Canvass__c objCanvass = TestMethodsUtility.createCanvass();
   
    Lead objLead = new Lead(FirstName = 'Test', LastName = 'Test Unit20', Company = 'Test Berry', Primary_Canvass__c = objCanvass.Id, Phone = '9999993276', Status = 'Open');
    insert objLead;
     
    List<Listing__c> objLstngLst= new List<Listing__c>();
    String ts = objLead.id;
    set<Id> ldconvertId = new set<Id>(); 
    ldconvertId.add(ts); 
    Listing__c objListing = TestMethodsUtility.generateListing();
        objListing.Lead__c = objLead.Id;
        objLstngLst.add(objListing);
    insert  objLstngLst;
    Account Acc= [select id from account limit 1];
    
    map<Id,Listing__c> oldMap = new map<Id,Listing__c>();
    oldMap.put(objListing.Id,objListing);
    Test.startTest();
    ListingsLeadAdrUpdate.leadAdrUpdate(objLstngLst,oldMap);
    ListingsLeadAdrUpdate.listingUpdatewithAccount(ldconvertId, Acc.id );
    Test.stopTest();
    }
}