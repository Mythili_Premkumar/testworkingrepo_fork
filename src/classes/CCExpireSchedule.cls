global class CCExpireSchedule implements Schedulable {
	global void execute(SchedulableContext sc) {
		CCExpireEmailBatch obj = new CCExpireEmailBatch();
		Database.executeBatch(obj); 
	}
}