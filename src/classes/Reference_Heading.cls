public with sharing class Reference_Heading{

    public string dirHeadingId {get;set;}
    public string dirHeadingname {get;set;}
    public String delID {get;set;}
    public List<Cross_Reference_Headings__c> lstCrossRef {get;set;}
    public Directory_Heading__c objDirHeading;

    public Reference_Heading (ApexPages.StandardController controller) {
        dirHeadingId = Apexpages.currentPage().getParameters().get('id');
    }
        
    public Pagereference onLoad() {
        
        List<Directory_Heading__c> lstDirHeading = [SELECT Id, name, Directory_Heading_Name__c FROM Directory_Heading__c where id=:dirHeadingId];
        if(lstDirHeading != null && lstDirHeading.size() > 0){
            objDirHeading = lstDirHeading[0];
            dirHeadingname = objDirHeading.Directory_Heading_Name__c;
        }
        
        lstCrossRef = [select id, Cross_Reference_Heading__c, Source_Heading__c 
        from Cross_Reference_Headings__c where Source_Heading__c = :dirHeadingId or Cross_Reference_Heading__c = :dirHeadingId order by Cross_Reference_Heading__r.Directory_Heading_Name__c, Source_Heading__r.Name asc];
        return null;
    }
    
    public Pagereference deleteCross() {
        Cross_Reference_Headings__c objReference = new Cross_Reference_Headings__c(id=delID);
        delete objReference;
        PageReference ref = new PageReference('/apex/Reference_Heading?id='+dirHeadingId);
        ref.setredirect(True);
        return ref;
    }
}