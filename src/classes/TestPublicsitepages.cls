/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestPublicsitepages {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Directory_Heading__c cobj_DH=new Directory_Heading__c();
            cobj_DH.Spanish_Heading__c='spanishheading';
            cobj_DH.Related_Headings__c='RelatedHeading';
        insert cobj_DH;
         Directory_Heading__c cobj_DH1=new Directory_Heading__c();
            cobj_DH1.Spanish_Heading__c='spanishheading1';
            cobj_DH1.Related_Headings__c='RelatedHeading1';
        insert cobj_DH1;
        Cross_Reference_Headings__c cobj_CRH=new Cross_Reference_Headings__c();
            cobj_CRH.Cross_Reference_Heading__c=cobj_DH.id;
            cobj_CRH.Source_Heading__c=cobj_DH1.id;
            insert cobj_CRH;
        Directory__c cobj_DT=TestMethodsUtility.createDirectory();
            //cobj_DT.Directory_Code__c='016600';
           // cobj_DT.Publication_Company_Name__c='LOC-557';
            //cobj_DT.Name='Oahu Yellow, HI';
        //insert cobj_DT; 
            
        apexpages.currentpage().getparameters().put('DirName',cobj_DH.id);
        publicsitepages obj_Pspgs=new publicsitepages();
        obj_Pspgs.DirectoryName='weding guide';
        obj_Pspgs.Search_byPhd();
        
    }
     static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        Directory_Heading__c cobj_DH=new Directory_Heading__c();
            cobj_DH.name='weding guide';
            cobj_DH.Spanish_Heading__c='spanishheading';
            cobj_DH.Related_Headings__c='RelatedHeading';
        insert cobj_DH;
         Directory_Heading__c cobj_DH1=new Directory_Heading__c();
            cobj_DH1.Spanish_Heading__c='spanishheading1';
            cobj_DH1.Related_Headings__c='RelatedHeading1';
        insert cobj_DH1;
        Cross_Reference_Headings__c cobj_CRH=new Cross_Reference_Headings__c();
            cobj_CRH.Cross_Reference_Heading__c=cobj_DH.id;
            cobj_CRH.Source_Heading__c=cobj_DH1.id;
            insert cobj_CRH;
        Directory__c cobj_DT=TestMethodsUtility.createDirectory();
            //cobj_DT.Directory_Code__c='016600';
           // cobj_DT.Publication_Company_Name__c='LOC-557';
           // cobj_DT.Name='Oahu Yellow, HI';
       // insert cobj_DT; 
            
        apexpages.currentpage().getparameters().put('DirName',cobj_DH.id);
        publicsitepages obj_Pspgs=new publicsitepages();
        obj_Pspgs.HeadingName='weding guide';
        obj_Pspgs.Search_byPhd();
        
        
    }
     static testMethod void myUnitTest2() {
        // TO DO: implement unit test
       Document dot=new Document();
                             dot.Name='Berry Logo';
                             dot.AuthorId = UserInfo.getUserId();
                             dot.FolderId = UserInfo.getUserId();
                             dot.isPublic=true;
                             dot.body=blob.valueOf('testswtaste');
                         insert dot;
        Directory_Heading__c cobj_DH=new Directory_Heading__c();
            cobj_DH.name='weding guide';
            cobj_DH.Spanish_Heading__c='spanishheading';
            cobj_DH.Related_Headings__c='RelatedHeading';
        insert cobj_DH;
         Directory_Heading__c cobj_DH1=new Directory_Heading__c();
            cobj_DH1.Spanish_Heading__c='spanishheading1';
            cobj_DH1.Related_Headings__c='RelatedHeading1';
        insert cobj_DH1;
        Cross_Reference_Headings__c cobj_CRH=new Cross_Reference_Headings__c();
            cobj_CRH.Cross_Reference_Heading__c=cobj_DH.id;
            cobj_CRH.Source_Heading__c=cobj_DH1.id;
            insert cobj_CRH;
        Account acc=new Account();
        acc.Name='test';
        acc.Publication_Code__c='LOC-557';
        insert acc; 
        Directory__c cobj_DT=TestMethodsUtility.createDirectory();
            //cobj_DT.Directory_Code__c='016600';
            //cobj_DT.Publication_Company__c=acc.id;
            //cobj_DT.Name='Oahu Yellow, HI';
        //insert cobj_DT;
        apexpages.currentpage().getparameters().put('DirName',cobj_DH.id);
        apexpages.currentpage().getparameters().put('alpha','v');
        publicsitepages obj_Pspgs=new publicsitepages();
        obj_Pspgs.Discription='test';
        obj_Pspgs.Selpubstvalues='weding guide';
        obj_Pspgs.Search_byPhd();
        obj_Pspgs.sortExpression='name';
        obj_Pspgs.sorttype='heading';
        obj_Pspgs.sortdata();
        obj_Pspgs.Requestnewheading();
        obj_Pspgs.DirectoryNames(); 
        obj_Pspgs.DirHnames();
        obj_Pspgs.HeadingNames();
        //obj_Pspgs.Publicsitevalues();
        obj_Pspgs.backtohome();
        obj_Pspgs.UploadPhoto();
        obj_Pspgs.advsring();
        obj_Pspgs.HRCancel();
        obj_Pspgs.getSortDirection();
        obj_Pspgs.previousBtnClick();
        obj_Pspgs.getPageNumber();
        obj_Pspgs.getPageSize();
        obj_Pspgs.getPreviousButtonEnabled();
        obj_Pspgs.getNextButtonDisabled();
        obj_Pspgs.getTotalPageNumber();
        obj_Pspgs.LastbtnClick();
        obj_Pspgs.boldalpha();
        obj_Pspgs.HeadingNames_1();
        obj_Pspgs.Insertmehtod();
        obj_Pspgs.SearchByValue();

        obj_Pspgs.document=dot;
        obj_Pspgs.HeadinginDirectory();
        obj_Pspgs.FirstbtnClick();
        obj_Pspgs.nextBtnClick();

         ApexPages.StandardController sc = new ApexPages.standardController(cobj_DH);
        // publicsitepages obj_Pspgs1=new publicsitepages(sc);
      
    }
}