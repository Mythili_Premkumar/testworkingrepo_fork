<apex:page standardController="Case" extensions="SelectOrderSetForDiscountController_v2" standardStylesheets="true" id="page">
    <apex:form id="frm">
        <script>
        window.onload = setFocusOnLoad();
        </script>
        
        <apex:includeScript value="/support/console/22.0/integration.js"/>
        <script type="text/javascript">
            function OpenOLISubTab(oliID){
                if (sforce.console.isInConsole()) {
                    sforce.console.getEnclosingPrimaryTabId(function(result){
                        sforce.console.openSubtab(result.id, '/' + oliID, true, '', null);
                    });
                }
                else{
                    window.open('/' + oliId);
                }
            };
        </script>
        
        <apex:pageMessages ></apex:pageMessages>
        <apex:actionstatus id="LoadStatus">
            <apex:facet name="start">
                <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb;
                    height:100%;opacity:0.65;width:100%;"> 
                    <div class="waitingHolder" style="width: 91px;">
                        <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                        <span class="waitingDescription">Loading...</span>
                    </div>
                </div>
            </apex:facet>
       </apex:actionstatus>
        <apex:outputPanel >
            <apex:pageMessage summary="Instructions:" severity="info" strength="3">
            <div><br/>
             The two steps for selecting Order Line items to Discount/Credit are:<br/>
             1. Check the Order Set(s) below to show the line items and click <b><i>Show Line Items</i></b><br/>
             2. Check in the List of Order Line Items that are assocated with the selected Order Set(s) which you wish to work with and click <b><i>Discount Selected Line Items</i></b>
             <br/><br/>To return to the Case you opened without saving your selections click <b><i>Return to Case</i></b>
            </div>
            </apex:pageMessage>
        </apex:outputPanel>
        <br/>        
        <apex:pageBlock title="Select an Order Set for Items to Discount/Credit" id="ordersetblock" >      
            <apex:pageBlockButtons id="pageblockbuts" location="top">
                <apex:commandButton immediate="true" title="Return to Case" value="Return to Case" action="/{!objCase.id}"/>&nbsp;&nbsp;                
                <apex:commandButton immediate="true" title="Issue BCC/Credit for Non-Order Line Items" value="Issue BCC/Credit for Non-Order Line Items" action="/apex/SelectNonOLIForDiscountPage?id={!objCase.id}&mode=true"/>
            </apex:pageBlockButtons>         
            <apex:pageBlockSection collapsible="True" title="Account and Contact" columns="2"  id="accountpbsection" >
                <apex:pageBlockSectionItem >
                    <apex:outputLabel styleClass="labelCol first last " >Account:&nbsp;</apex:outputLabel>
                    <apex:OutputField value="{!objCase.AccountId}"  />
                </apex:pageBlockSectionItem>

                <apex:pageBlockSectionItem >
                    <apex:outputLabel styleClass="labelCol first last " >Contact: </apex:outputLabel>
                    <apex:OutputField value="{!objCase.ContactId}"  />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel styleClass="labelCol first last ">Account #:&nbsp;</apex:outputLabel>
                    <apex:OutputField value="{!objCase.account.Account_Number__c}"  />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >&nbsp;</apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel styleClass="labelCol first last ">Main Listed Phone:&nbsp;</apex:outputLabel>
                    <apex:OutputField value="{!objCase.account.phone}"  />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >&nbsp;</apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel styleClass="labelCol first last ">Primary Canvass:&nbsp;</apex:outputLabel>
                    <apex:OutputField value="{!objCase.account.Primary_Canvass__c}"  />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >&nbsp;</apex:pageBlockSectionItem>
                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection title="Order Set Filter" columns="6" collapsible="false" id="filterPgBlckSec">
                <apex:pageBlockSectionItem dataStyle="float:left;">
                    <apex:outputLabel styleClass="labelCol first last" id="filter"><b>Select Filter</b></apex:outputLabel>
                        <apex:selectList size="1"  id="status" multiselect="false" value="{!OSoptions}">
                            <apex:selectOption itemValue="None" itemLabel="--None--"/>
                            <apex:selectOption itemValue="ByDates" itemLabel="By Date"/>
                            <apex:selectOption itemValue="ByCanvass" itemLabel="By Canvass"/>
                            <apex:actionSupport event="onchange" rerender="frm,sec1,sec1,sec3,filterPgBlckSec,fitlerdate" status="LoadStatus"/>
                        </apex:selectList> 
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="sec1" dataStyle="float:left;" rendered="{!IF(OSoptions == 'ByDates', true , false)}" >
                    <apex:outputLabel styleClass="labelCol first last" id="fromdatefilter"><b>From </b></apex:outputLabel>
                        <apex:inputField value="{!tempOLI.Billing_End_Date__c}" id="fromdate" /> 
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem id="sec2" dataStyle="float:left;" rendered="{!IF(OSoptions == 'ByDates', true , false)}"> 
                    <apex:outputLabel styleClass="labelCol first last" id="todatefilter"><b>To</b></apex:outputLabel>
                        <apex:inputField value="{!tempOLI.Billing_Frequency_Change_Date__c}" id="todate" />
                </apex:pageBlockSectionItem> 

                <apex:pageBlockSectionItem id="sec3" dataStyle="float:left;" rendered="{!IF(OSoptions == 'ByCanvass', true , false)}"> 
                    <apex:outputLabel styleClass="labelCol first last" id="canvassfil"><b>Canvass</b></apex:outputLabel>
                        <apex:outputPanel >
                            <apex:selectList value="{!canvassId}" size="1" label="Canvass">
                                <apex:selectOptions value="{!primaryCanvassList}"/>
                            </apex:selectList>
                        </apex:outputPanel>
                </apex:pageBlockSectionItem> 
                                
                <apex:pageBlockSectionItem >    
                    <apex:outputText id="fitlerdate"> 
                        <apex:commandbutton value="Filter Order Sets" action="{!applyFilter}" rerender="frm,filterPgBlckSec,fitlerdate,ossection,ospbitem,OSDT,orderlineitemsblock,thePanel" status="LoadStatus" />
                        <apex:commandbutton value="Clear Filter" action="{!clearFilter}" rerender="frm,filterPgBlckSec,fitlerdate,ossection,OSDT,orderlineitemsblock" status="LoadStatus" />
                    </apex:outputText> 
                </apex:pageBlockSectionItem>                
            </apex:pageBlockSection>
            
            <apex:pageBlockSection collapsible="true" title="Current Order Sets for Account {!objCase.Account.Name}" columns="1" id="ossection">

                <apex:pageBlockSectionItem id="ospbitem">
                    <apex:pageBlockTable width="100%" value="{!lstWOS}" var="os" id="OSDT" rules="rows">
                        <apex:column width="5%" style="text-align:center">
                        <apex:facet name="header"><center>Select</center></apex:facet>
                        &nbsp;&nbsp;<apex:inputField value="{!os.objOS.selected__c}" onchange="countordersets(this);"  id="osselect"/>      
                        </apex:column>
                        
                        <apex:column width="15%" style="text-align:center">
                        <apex:facet name="header"><center>Closed Date</center></apex:facet>
                        <apex:outputField value="{!os.objOS.CreatedDate}"/>
                        </apex:column>
                        
                        <apex:column width="10%" style="text-align:center">
                        <apex:facet name="header"><center># of Line Items</center></apex:facet>
                        <apex:outputField value="{!os.objOS.oli_count__c}"/>
                        </apex:column>
                        
                        <apex:column width="10%" style="text-align:center">
                        <apex:facet name="header"><center>Client #</center></apex:facet>
                        <apex:outputField value="{!os.objOS.Order_Account__r.Client_Number__c}"/>
                        </apex:column>
                        
                        <apex:column width="20%" style="text-align:center">
                        <apex:facet name="header"><center>Client Name</center></apex:facet>
                        <apex:outputField value="{!os.objOS.Order_Account__r.Name}"/>
                        </apex:column>
                        
                        <apex:column width="20%" style="text-align:center" >
                        <apex:facet name="header"><center>Opportunity</center></apex:facet>
                        <apex:outputField value="{!os.objOS.Opportunity__c}"/>
                        </apex:column>
                        
                        <apex:column width="20%" style="text-align:center">
                            <apex:facet name="header"><center>Edition/Canvass</center></apex:facet>
                                <apex:outputPanel >
                                    <apex:repeat value="{!os.setValues}" var="picklistvalue">
                                        <apex:outputText value="{!picklistvalue}" ></apex:outputText>
                                        <br/> 
                                    </apex:repeat>                
                                </apex:outputPanel> 
                        </apex:column>

                    </apex:pageBlockTable>

                </apex:pageBlockSectionItem>
                <apex:commandButton title="Show Line Items" value="Show Line Items" action="{!ShowSelectOLI}" status="LoadStatus" rerender="frm,orderlineitemsblock,repeatsection,osloop"/>
            </apex:pageBlockSection>
            
            <apex:pageBlockSection id="repeatsection" columns="1">
                <apex:actionFunction action="{!SelectAllByOS}" name="selectDeselectMethod" rerender="frm" status="LoadStatus">
                                <apex:param name="oschecked" value="" />
                                <apex:param name="osid" value="" />
                            </apex:actionFunction>
                <apex:repeat value="{!lstWShowOLIFinal}" var="OSH" id="osloop">
                        
                    <apex:pageBlockSection title="Selected Order Lines for {!OSH.orderSetName}" columns="1" collapsible="false" id="orderlineitemsblock">
                        <apex:pageBlockTable width="100%" value="{!OSH.lstWOLI}" var="oli" id="olioutdt"  rules="rows"  columns="15" columnsWidth="4%,4%,4%,6%,5%,12%,16%,15%,5%,7%,5%,5%,5%,7%">
                            <apex:column >
                                <apex:facet name="header"><apex:inputCheckbox value="{!OSH.bFlagCheck}" onchange="selectDeselectMethod(this.checked, '{!OSH.orderSetId}')">
                                                                </apex:inputCheckbox></apex:facet>
                                <apex:inputField value="{!oli.objOLI.checked__c}" id="OLItemCheck"/>
                            </apex:column> 
                            
                            <apex:column >
                                <apex:facet name="header"><center>Line Item<br/>Total Value</center></apex:facet>
                                $<apex:outputText value="{!(oli.objOLI.UnitPrice__c * oli.objOLI.Payment_Duration__c)}"/> 
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>Product<br/>Type</center></apex:facet>
                                <apex:outputText value="{!IF(oli.objOLI.product2__r.media_type__c == 'Print','Print','Digital')}"/>                  
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>DFF</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Digital_Product_Requirement__c}" />             
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>Order Line<br/>Item Name</center></apex:facet>
                                <a  href="/{!oli.objOLI.Id}" id="lookup{!oli.objOLI.id}opp4"
                                    onblur="LookupHoverDetail.getHover('lookup{!oli.objOLI.id}opp4').hide();"
                                    onfocus="LookupHoverDetail.getHover('lookup{!oli.objOLI.id}opp4', '/{!oli.objOLI.id}/m?retURL=/{!oli.objOLI.id}&isAjaxRequest=1').show();"
                                    onmouseout="LookupHoverDetail.getHover('lookup{!oli.objOLI.id}opp4').hide();"
                                    onmouseover="LookupHoverDetail.getHover('lookup{!oli.objOLI.id}opp4', '/{!oli.objOLI.id}/m?retURL=/{!oli.objOLI.id}&isAjaxRequest=1').show();" onClick="OpenOLISubTab('{!oli.objOLI.Id}');return false">
                                    {!oli.objOLI.name}</a>
                                <!--<apex:outputLink value="/{!oli.objOLI.Id}">{!oli.objOLI.name}</apex:outputLink>-->
                                <!--<apex:outputField value="{!oli.objOLI.name}"/>-->
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>Heading<br/>Column</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Directory_Heading__r.Name}"/>
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>UDAC/<br/>Description</center></apex:facet>
                                <apex:outputText ><apex:outputField value="{!oli.objOLI.ProductCode__c}"/>/<br/>
                                    <apex:outputField value="{!oli.objOLI.product2__r.name}"/>
                                </apex:outputText>
                            </apex:column>   
                            
                            <apex:column >
                                <apex:facet name="header"><center>Canvass</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Canvass__r.name}"/>
                            </apex:column>      
                            
                            <apex:column >
                                <apex:facet name="header"><center>Sale<br/>Price</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.UnitPrice__c}"/>
                            </apex:column> 
                            
                            <apex:column style="text-align:center;">
                                <apex:facet name="header"><center>Billed/<br/>Remaining</center></apex:facet>
                                    <apex:outputText ><apex:outputField value="{!oli.objOLI.Successful_Payments__c}"/>/<apex:outputField value="{!oli.objOLI.Payments_Remaining__c}"/>
                                    </apex:outputText> 
                            </apex:column>

                            <apex:column style="text-align:center;">
                                <apex:facet name="header"><center>Evergreen<br/>Months</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Evergreen_Payments__c}" rendered="{!IF(oli.objOLI.Evergreen_Payments__c > -1,true,false)}"/>
                            </apex:column>

                            <apex:column style="text-align:center;">
                                <apex:facet name="header"><center>Billing<br/>Status</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Status__c}"/>
                            </apex:column> 
                            
                            <apex:column style="text-align:center;">
                                <apex:facet name="header"><center>Billing<br/>Duration</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Payment_Duration__c}"/>
                            </apex:column>
                            
                            <apex:column >
                                <apex:facet name="header"><center>Talus Go<br/>Live Date</center></apex:facet>
                                <apex:outputField value="{!oli.objOLI.Talus_Go_Live_Date__c}"/>
                            </apex:column>
                        </apex:pageBlockTable>
                    </apex:pageBlockSection>
                </apex:repeat>
            </apex:pageBlockSection>
            <apex:outputPanel rendered="{!IF(AND(lstWShowOLIFinal != null , lstWShowOLIFinal.size > 0), true,false)}" id="thePanel">
                <apex:commandButton title="Credit Selected Line Items" value="Credit Selected Line Items"  id="cmdCreditSelectedLineItems" action="{!enterSelectOSDPage2}"/>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
</apex:page>