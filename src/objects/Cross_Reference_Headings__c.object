<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>This object manages the cross-ref headings associated with any one heading. It drives the &quot;See Also&quot; entries in pagination</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Cross_Reference_Heading_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Cross_Reference_Heading__r.code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cross Reference Heading Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cross_Reference_Heading_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Cross_Reference_Heading__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cross Reference Heading Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cross_Reference_Heading__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The target heading for this cross reference</description>
        <externalId>false</externalId>
        <label>Cross Reference Heading</label>
        <referenceTo>Directory_Heading__c</referenceTo>
        <relationshipLabel>Cross Reference Headings (Referenced to this Heading)</relationshipLabel>
        <relationshipName>Cross_Reference_Headings1</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <externalId>true</externalId>
        <label>ExtId</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Cross_Reference_Id_s__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>false</externalId>
        <label>Source and Cross Reference Id&apos;s</label>
        <length>40</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>Source_Heading_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Source_Heading__r.code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Source Heading Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Heading_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Source_Heading__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Source Heading Name</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Source_Heading__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The originating heading for this cross reference</description>
        <externalId>false</externalId>
        <label>Source Heading</label>
        <referenceTo>Directory_Heading__c</referenceTo>
        <relationshipLabel>Cross Reference Headings</relationshipLabel>
        <relationshipName>Cross_Reference_Headings</relationshipName>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Cross Reference Heading</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Cross Reference Headings Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Cross Reference Headings</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Cross_Reference_Heading__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Source_Heading__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Cross_Reference_Heading__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Source_Heading__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATED_DATE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Cross_Reference_Heading__c</searchFilterFields>
        <searchFilterFields>Source_Heading__c</searchFilterFields>
        <searchFilterFields>CREATED_DATE</searchFilterFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <validationRules>
        <fullName>Source_Cross_Reference_Heading</fullName>
        <active>true</active>
        <description>Source and Cross Reference Heading cannot be the same</description>
        <errorConditionFormula>Cross_Reference_Heading__c ==  Source_Heading__c</errorConditionFormula>
        <errorMessage>Source and Cross Reference Heading cannot be the same</errorMessage>
    </validationRules>
</CustomObject>
