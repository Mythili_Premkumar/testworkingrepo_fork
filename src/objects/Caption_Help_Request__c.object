<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>For Sales Reps to request help with a caption creation or change from a Back Office team</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Caption_Creation_Update_Details__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Please provide details on what needs to be done.  You can write text, copy and paste a mock-up, include an image, or include text for instructions regarding any attachments you add to this request.  Please be as specific as possible.</inlineHelpText>
        <label>Caption Creation/Update Details</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Html</type>
        <visibleLines>25</visibleLines>
    </fields>
    <fields>
        <fullName>Data_Fulfillment_Form__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Data Fulfillment Form Initiated From</label>
        <referenceTo>Digital_Product_Requirement__c</referenceTo>
        <relationshipLabel>Caption Help Requests</relationshipLabel>
        <relationshipName>Caption_Help_Requests</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Request_Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Request Completed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Scoped_Listing__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Scoped Listing Initiated From</label>
        <referenceTo>Directory_Listing__c</referenceTo>
        <relationshipLabel>Caption Help Requests</relationshipLabel>
        <relationshipName>Caption_Help_Requests</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <label>Caption Help Request</label>
    <listViews>
        <fullName>Caption_Queue_Completed</fullName>
        <columns>NAME</columns>
        <columns>Data_Fulfillment_Form__c</columns>
        <columns>Request_Completed__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Request_Completed__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Caption Queue - Completed</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>Caption_Queue_Pending</fullName>
        <columns>NAME</columns>
        <columns>Data_Fulfillment_Form__c</columns>
        <columns>Request_Completed__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Queue</filterScope>
        <label>Caption Queue - Pending</label>
        <language>en_US</language>
        <queue>Caption_Queue</queue>
    </listViews>
    <listViews>
        <fullName>My_Caption_Queue_Pending</fullName>
        <columns>NAME</columns>
        <columns>Data_Fulfillment_Form__c</columns>
        <columns>Request_Completed__c</columns>
        <columns>OWNER.FIRST_NAME</columns>
        <columns>OWNER.LAST_NAME</columns>
        <columns>CREATEDBY_USER</columns>
        <filterScope>Mine</filterScope>
        <filters>
            <field>Request_Completed__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>My Caption Queue - Pending</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>CHR-{0000}</displayFormat>
        <label>Caption Help Request Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Caption Help Requests</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
