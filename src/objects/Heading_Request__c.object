<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <content>HeadingRequestDetail</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>For Sales or CMR&apos;s to request a new heading for a directory or directories</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Business_Name__c</fullName>
        <externalId>false</externalId>
        <label>Business Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Business_Phone__c</fullName>
        <externalId>false</externalId>
        <label>Business Phone #</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Describe_Product_or_Service__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>How is it used?</inlineHelpText>
        <label>Describe Product or Service</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Directory_Name_s__c</fullName>
        <externalId>false</externalId>
        <label>Directory Name(s)</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Heading_restricted_by_legal_or_prof_assc__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Is this heading restricted by legal, or professional associations? Other? If so, explain.</inlineHelpText>
        <label>Heading restricted by legal or prof assc</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Heading_s_Requested__c</fullName>
        <externalId>false</externalId>
        <label>Heading(s) Requested</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Headings_used_in_surrounding_directories__c</fullName>
        <externalId>false</externalId>
        <label>Headings used in surrounding directories</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Is_the_requested_product_service_used_by__c</fullName>
        <externalId>false</externalId>
        <label>Is the requested product/service used by</label>
        <picklist>
            <picklistValues>
                <fullName>Consumers</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Businesses</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Is_the_terminology_well_know_to__c</fullName>
        <externalId>false</externalId>
        <label>Is the terminology well known to</label>
        <picklist>
            <picklistValues>
                <fullName>Users(Consumers)</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Industry</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Is_this_a_new_Business_Product_Service__c</fullName>
        <externalId>false</externalId>
        <label>Is this a new Business/Product/Service</label>
        <picklist>
            <picklistValues>
                <fullName>Yes</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>No</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>National_association_related_to_heading__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>If there is a national association(s) relating to the requested product/service, please list the name(s).</inlineHelpText>
        <label>National association related to heading</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Primary_Heading_s_Currently_Used__c</fullName>
        <externalId>false</externalId>
        <label>Primary Heading(s) Currently Used</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Pubsite_Desc__c</fullName>
        <externalId>false</externalId>
        <label>Pubsite_Desc</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Pubsite_Desc_vis__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Pubsite_Desc_vis</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Request_Type__c</fullName>
        <externalId>false</externalId>
        <label>Request Type</label>
        <picklist>
            <picklistValues>
                <fullName>New</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Change</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Delete</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cross Reference</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Requester_Email_Address__c</fullName>
        <defaultValue>IF( ISPICKVAL($User.UserType, &quot;Standard&quot;), $User.Email , &quot;&quot;)</defaultValue>
        <externalId>false</externalId>
        <label>Requester Email Address</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Email</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Requester_Name__c</fullName>
        <defaultValue>IF(  ISPICKVAL($User.UserType, &quot;Standard&quot;), $User.FirstName &amp; &quot; &quot; &amp;  $User.LastName, &quot;&quot;)</defaultValue>
        <externalId>false</externalId>
        <label>Requester Name</label>
        <length>120</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Requester_Phone__c</fullName>
        <defaultValue>IF( ISPICKVAL($User.UserType, &quot;Standard&quot;), $User.Phone, &quot;&quot;)</defaultValue>
        <externalId>false</externalId>
        <label>Requester Phone #</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Suggested_Cross_Reference_s__c</fullName>
        <externalId>false</externalId>
        <label>Suggested Cross Reference(s)</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Suggested_Related_Heading_s__c</fullName>
        <externalId>false</externalId>
        <label>Suggested Related Heading(s)</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>What_advertising_is_planned_for_headings__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>What item of advertising does your customer plan to place at the requested heading?</inlineHelpText>
        <label>What advertising is planned for headings</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Why_would_the_new_heading_be_beneficial__c</fullName>
        <externalId>false</externalId>
        <inlineHelpText>Why would the new heading be more beneficial to the advertiser than the one currently being utilized? (Please explain)</inlineHelpText>
        <label>Why would the new heading be beneficial?</label>
        <length>2000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <label>Heading Request</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <displayFormat>HeadingRequest-{0000}</displayFormat>
        <label>Heading Request Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Heading Requests</pluralLabel>
    <searchLayouts>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>Back_to_Home</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Back to Home</masterLabel>
        <openType>sidebar</openType>
        <page>publicsitepages</page>
        <protected>false</protected>
    </webLinks>
</CustomObject>
