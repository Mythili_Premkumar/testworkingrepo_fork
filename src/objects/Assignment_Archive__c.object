<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>For SF1 to BF data migration of Assignments (Opportunities)</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Manager__c</fullName>
        <externalId>false</externalId>
        <label>Performance Advisor</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Name__c</fullName>
        <description>Text field that houses the SFDC1 Assignment&apos;s Account Name</description>
        <externalId>false</externalId>
        <inlineHelpText>Text field that houses the SFDC1 Assignment&apos;s Account Name</inlineHelpText>
        <label>Account Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Assignment Archives</relationshipLabel>
        <relationshipName>Assignment_Archives</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Additional_Website2__c</fullName>
        <externalId>false</externalId>
        <label>Additional Website</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Advance_Pay_Required__c</fullName>
        <externalId>false</externalId>
        <label>Advance Pay Required?</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <externalId>false</externalId>
        <label>Forecast NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>AssignmentID__c</fullName>
        <externalId>false</externalId>
        <label>AssignmentID</label>
        <length>99</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Back_Office_Close_Status__c</fullName>
        <externalId>false</externalId>
        <label>Back Office Query Status</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_Client_Number_for_NYPS__c</fullName>
        <externalId>false</externalId>
        <label>CMR/Client Number for NYPS</label>
        <length>20</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CORE_Contract_Closed_Date__c</fullName>
        <externalId>false</externalId>
        <label>CORE Contract Closed Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CORE_Contract_Status__c</fullName>
        <externalId>false</externalId>
        <label>CORE Contract Status</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Canvass_3L_Code__c</fullName>
        <externalId>false</externalId>
        <label>Canvass 3L Code</label>
        <length>60</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Canvass_Group__c</fullName>
        <externalId>false</externalId>
        <label>Canvass Group</label>
        <length>3</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Canvass_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>Assignment Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Claim_Is_Or_Was_Open_This_Contract__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Claim Is Or Was Open This Contract</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Claim_Open_Now_This_Contract__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Claim Open Now This Contract</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Client_Persona__c</fullName>
        <externalId>false</externalId>
        <label>Client Persona</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Close_Date__c</fullName>
        <externalId>false</externalId>
        <label>Assignment End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Close_Sub_Types__c</fullName>
        <externalId>false</externalId>
        <label>Close Sub-Types</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Closed_Subscription_NI__c</fullName>
        <externalId>false</externalId>
        <label>Closed Subscription NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Complaint_Open_On_This_Canvass_Edition__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Open Inquiry On This Account</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_Canvass_Billing_Parties__c</fullName>
        <externalId>false</externalId>
        <label>Current Canvass Billing Parties</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Customer_Issue_Resolved__c</fullName>
        <externalId>false</externalId>
        <label>Customer Issue Resolved?</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_Tier__c</fullName>
        <externalId>false</externalId>
        <label>Customer Tier</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_id__c</fullName>
        <externalId>false</externalId>
        <label>Account 3LID</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSM_Activity__c</fullName>
        <externalId>false</externalId>
        <label>DSM Activity</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>DSM_Last_Activity_Date__c</fullName>
        <externalId>false</externalId>
        <label>DSM Last Activity Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>DSM_Notes__c</fullName>
        <externalId>false</externalId>
        <label>DSM Notes</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Delinquency_Indicator__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Delinquency Indicator</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Delinquency_Override__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Delinquency Override</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Delinquent__c</fullName>
        <externalId>false</externalId>
        <label>Delinquent</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <length>32768</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Digital_Sales_Manager_Text__c</fullName>
        <externalId>false</externalId>
        <label>Digital Sales Manager</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>EACD_Date__c</fullName>
        <externalId>false</externalId>
        <label>EACD Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Fax__c</fullName>
        <externalId>false</externalId>
        <label>Fax</label>
        <length>80</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Forecast_vs_Total_Monthly_NI_Over_1_Off__c</fullName>
        <externalId>false</externalId>
        <label>Forecast vs Total Monthly NI Over $1 Off</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Growth_Potentials__c</fullName>
        <externalId>false</externalId>
        <label>Growth Potentials</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Historical_Assignment__c</fullName>
        <externalId>false</externalId>
        <label>Historical Assignment?</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Homebase_Directory__c</fullName>
        <externalId>false</externalId>
        <label>Homebase Directory</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Inquiry_Indicators__c</fullName>
        <externalId>false</externalId>
        <label>Inquiry Indicators</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Last_Rep_Activity_Date__c</fullName>
        <externalId>false</externalId>
        <label>Last Rep Activity Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Lead_Source__c</fullName>
        <externalId>false</externalId>
        <label>Lead Source</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Limited_Inventory_Indicator__c</fullName>
        <externalId>false</externalId>
        <label>Limited Inventory Indicator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Limited_Inventory__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Limited Inventory</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Main_Listed_Address2__c</fullName>
        <externalId>false</externalId>
        <label>Main Listed Address</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Main_Listed_Number2__c</fullName>
        <externalId>false</externalId>
        <label>Main Listed Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Migration_ID__c</fullName>
        <description>For data migration of SF1 to BF.  Holds the previous SF1 ID.</description>
        <externalId>false</externalId>
        <label>Migration ID</label>
        <length>30</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monthly_Closed_Subscription_NI__c</fullName>
        <externalId>false</externalId>
        <label>Monthly Closed Subscription NI</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monthly_NI__c</fullName>
        <externalId>false</externalId>
        <label>Monthly Current Subscription NI</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monthly_PI_Stage_Del_Canvass__c</fullName>
        <externalId>false</externalId>
        <label>Monthly PI, Stage, Del, Canvass</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monthly_PI__c</fullName>
        <externalId>false</externalId>
        <label>Monthly Subscription PI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Monthly_Projected_NI__c</fullName>
        <externalId>false</externalId>
        <label>Monthly Projected NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Monthly_Projected_PI__c</fullName>
        <externalId>false</externalId>
        <label>Monthly Projected PI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Monthly_Dollars__c</fullName>
        <externalId>false</externalId>
        <label>Net Monthly Dollars</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Net_Percent__c</fullName>
        <externalId>false</externalId>
        <label>Net Percent</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>New_Connect_Aging__c</fullName>
        <externalId>false</externalId>
        <label>New Install Aging</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>New_Connect_Start_Date__c</fullName>
        <externalId>false</externalId>
        <label>New Install Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>New_Install__c</fullName>
        <externalId>false</externalId>
        <label>New Install?</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Next_Step__c</fullName>
        <externalId>false</externalId>
        <label>Next Step</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OPCO__c</fullName>
        <externalId>false</externalId>
        <label>OPCO</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Open_Inquiry_1_Or_0_For_Account_Rollup__c</fullName>
        <externalId>false</externalId>
        <label>Open Inquiry 1 Or 0 For Account Rollup</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Open_Inquiry_On_This_Account_Indicator__c</fullName>
        <externalId>false</externalId>
        <label>Open Inquiry On This Account Indicator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Owner__c</fullName>
        <externalId>false</externalId>
        <label>Owner</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>P4P_Assignment__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>P4P Assignment</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>P4P_or_Call_Tracking_Indicator__c</fullName>
        <externalId>false</externalId>
        <label>P4P Assignment Indicator</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>P4P_or_Call_Tracking__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>P4P Assignment?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>PI_Directory_Cycle__c</fullName>
        <externalId>false</externalId>
        <label>PI Directory Cycle</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent_Account__c</fullName>
        <externalId>false</externalId>
        <label>Parent Account</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Payment_Status__c</fullName>
        <externalId>false</externalId>
        <label>Payment Status</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Probability__c</fullName>
        <externalId>false</externalId>
        <label>Probability</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Projected_NI__c</fullName>
        <externalId>false</externalId>
        <label>Current Projected NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Projected_PI__c</fullName>
        <externalId>false</externalId>
        <label>Projected PI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Related_Contact__c</fullName>
        <externalId>false</externalId>
        <label>Related Contact</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Released__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Released</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>SFDC1_Account_ID__c</fullName>
        <description>Text field that houses the SFDC1 Assignment&apos;s Account ID</description>
        <externalId>false</externalId>
        <inlineHelpText>Text field that houses the SFDC1 Assignment&apos;s Account ID</inlineHelpText>
        <label>SFDC1 Account ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Segment__c</fullName>
        <externalId>false</externalId>
        <label>Segment</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Spec_Instr_on_Quote_Summary__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Spec Instr on Quote Summary</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Stage__c</fullName>
        <externalId>false</externalId>
        <label>Stage</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Times_Called__c</fullName>
        <externalId>false</externalId>
        <label># Times Called</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Monthly_NI__c</fullName>
        <externalId>false</externalId>
        <label>Total Monthly NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Total_Monthly_PI__c</fullName>
        <externalId>false</externalId>
        <label>Total Monthly PI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Twentyten_NI__c</fullName>
        <externalId>false</externalId>
        <label>Current Subscription NI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Twentyten_PI__c</fullName>
        <externalId>false</externalId>
        <label>Current PI</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Website__c</fullName>
        <externalId>false</externalId>
        <label>Website</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Years_as_Advertisor__c</fullName>
        <externalId>false</externalId>
        <label>Years as Advertiser</label>
        <precision>16</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>of_Campaign_Completed__c</fullName>
        <externalId>false</externalId>
        <label>% of Campaign Completed</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>of_Months_Delinquent__c</fullName>
        <externalId>false</externalId>
        <label># of Months Delinquent</label>
        <precision>10</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Assignment Archive</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Account_Name__c</columns>
        <columns>Customer_id__c</columns>
        <columns>Main_Listed_Number2__c</columns>
        <columns>SFDC1_Account_ID__c</columns>
        <columns>Owner__c</columns>
        <columns>Canvass_3L_Code__c</columns>
        <columns>Total_Monthly_PI__c</columns>
        <columns>Total_Monthly_NI__c</columns>
        <columns>Stage__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Assignment Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Assignment Archives</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Owner__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Monthly_PI__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Monthly_NI__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Stage__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
