<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>stores the data on a telco</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Accepts_Cancellation_Fee__c</fullName>
        <defaultValue>true</defaultValue>
        <description>Leave this checkbox checked if the Telco accepts Cancellation Fees. If they do not accept the Cancellation Fees, uncheck it.</description>
        <externalId>false</externalId>
        <inlineHelpText>Leave this checkbox checked if the Telco accepts Cancellation Fees. If they do not accept the Cancellation Fees, uncheck it.</inlineHelpText>
        <label>Accepts Cancellation Fee</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Accepts_Installation_Fees__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check this checkbox if the Telco accepts Installation Fees.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this checkbox if the Telco accepts Installation Fees.</inlineHelpText>
        <label>Accepts Installation Fees</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Telco Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Telcos</relationshipLabel>
        <relationshipName>Telcos</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CLEC_Provider__c</fullName>
        <externalId>false</externalId>
        <formula>IF( RecordType.DeveloperName = &quot;CLEC_Provider&quot;, TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CLEC Provider</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Contractual_Revenue_Loss_Percentage__c</fullName>
        <description>Percentage field requested by Financial Force</description>
        <externalId>false</externalId>
        <label>Contractual Revenue Loss Percentage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Customer_ID__c</fullName>
        <description>Customer ID needed for the CLEC Providers that may not have an account in the system - John M 2/14/2014</description>
        <externalId>false</externalId>
        <label>Customer ID</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Document_Id__c</fullName>
        <externalId>false</externalId>
        <label>Document Id</label>
        <length>50</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Pub_Right_Percentage__c</fullName>
        <description>Requested by FF for custom development.</description>
        <externalId>false</externalId>
        <label>Pub Right Percentage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Revenue_loss_percentage__c</fullName>
        <description>Rev Loss Percent will be reduced from the Billing Amount owed from Telco if any.</description>
        <externalId>false</externalId>
        <label>Revenue loss percentage</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Sales_Comm_Indicator__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Requested by FF for custom development</description>
        <externalId>false</externalId>
        <label>Sales Comm Indicator</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Settlement_agreement_Type__c</fullName>
        <description>Settlement Calculation Method - as outlined in contract with Telco</description>
        <externalId>false</externalId>
        <label>Settlement agreement Type</label>
        <picklist>
            <picklistValues>
                <fullName>NET COLLECTED</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>GROSS REVENUE</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BILLED REVENUE</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Settlement_frequency_Berry__c</fullName>
        <description>When Telco Sends Paybacks to Berry</description>
        <externalId>false</externalId>
        <label>Settlement frequency - Berry</label>
        <picklist>
            <picklistValues>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Settlement_frequency_TBC__c</fullName>
        <description>When Telco Settlement is run</description>
        <externalId>false</externalId>
        <label>Settlement frequency - TBC</label>
        <picklist>
            <picklistValues>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Sold_Letter_Logos__c</fullName>
        <description>This pick list identifies the logo that will be used on the sold letter.</description>
        <externalId>false</externalId>
        <label>Sold Letter Logo</label>
        <picklist>
            <picklistValues>
                <fullName>Berry</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>CBD</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Century Link</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Frontier Berry</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Hawaiian Telcom</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Windstream</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Telco_Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Telco Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Telco_Code__c</fullName>
        <description>contains the code # for this Telco</description>
        <externalId>false</externalId>
        <label>Telco Code</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Telco_Drives_WP_Sorting__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check this checkbox if the Telco provides their own WP Sort Order values for their WP Sections</description>
        <externalId>false</externalId>
        <inlineHelpText>Check this checkbox if the Telco provides their own WP Sort Order values for their WP Sections</inlineHelpText>
        <label>Telco Drives WP Sorting</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Use_As_Account_Telco_Name_and_Code__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Use As Account Telco Name and Code</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Telco</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Telco_Code__c</columns>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Accepts_Cancellation_Fee__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <columns>NAME</columns>
        <columns>Telco_Code__c</columns>
        <columns>Account__c</columns>
        <columns>RECORDTYPE</columns>
        <columns>Telco_Account_Name__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>CLEC_Providers</fullName>
        <columns>RECORDTYPE</columns>
        <columns>NAME</columns>
        <columns>Telco_Code__c</columns>
        <columns>Account__c</columns>
        <columns>Telco_Account_Name__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Telco__c.CLEC_Provider</value>
        </filters>
        <label>CLEC Providers</label>
    </listViews>
    <listViews>
        <fullName>Telco_Providers</fullName>
        <columns>RECORDTYPE</columns>
        <columns>NAME</columns>
        <columns>Telco_Code__c</columns>
        <columns>Account__c</columns>
        <columns>Telco_Account_Name__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>RECORDTYPE</field>
            <operation>equals</operation>
            <value>Telco__c.Telco_Provider</value>
        </filters>
        <label>Telco Providers</label>
    </listViews>
    <nameField>
        <label>Telco Name</label>
        <trackHistory>true</trackHistory>
        <type>Text</type>
    </nameField>
    <pluralLabel>Telcos</pluralLabel>
    <recordTypeTrackHistory>false</recordTypeTrackHistory>
    <recordTypes>
        <fullName>CLEC_Provider</fullName>
        <active>true</active>
        <description>CLEC Providers are an entity that provides Service Orders to Berry from the Telcos</description>
        <label>CLEC Provider</label>
        <picklistValues>
            <picklist>Settlement_agreement_Type__c</picklist>
            <values>
                <fullName>BILLED REVENUE</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>GROSS REVENUE</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NET COLLECTED</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Settlement_frequency_Berry__c</picklist>
            <values>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Settlement_frequency_TBC__c</picklist>
            <values>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Sold_Letter_Logos__c</picklist>
            <values>
                <fullName>Berry</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CBD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Century Link</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Frontier Berry</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hawaiian Telcom</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Windstream</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <recordTypes>
        <fullName>Telco_Provider</fullName>
        <active>true</active>
        <description>Telco Providers</description>
        <label>Telco Provider</label>
        <picklistValues>
            <picklist>Settlement_agreement_Type__c</picklist>
            <values>
                <fullName>BILLED REVENUE</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>GROSS REVENUE</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>NET COLLECTED</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Settlement_frequency_Berry__c</picklist>
            <values>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Settlement_frequency_TBC__c</picklist>
            <values>
                <fullName>ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>MONTHLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>QUARTERLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>SEMI-ANNUALLY</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>TRI-ANNUALLY</fullName>
                <default>false</default>
            </values>
        </picklistValues>
        <picklistValues>
            <picklist>Sold_Letter_Logos__c</picklist>
            <values>
                <fullName>Berry</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>CBD</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Century Link</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Frontier Berry</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Hawaiian Telcom</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Windstream</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>Telco_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Telco_Account_Name__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Accepts_Cancellation_Fee__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Accepts_Installation_Fees__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>RECORDTYPE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Telco_Code__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>RECORDTYPE</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Telco_Code__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>RECORDTYPE</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Telco_Code__c</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>RECORDTYPE</searchFilterFields>
        <searchResultsAdditionalFields>Telco_Code__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>RECORDTYPE</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
