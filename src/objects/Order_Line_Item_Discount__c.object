<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <content>DeleteOverrideOLID</content>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Visualforce</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>contains all order line item discounts used in the creation of Sales credit notes</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Additional_BCC__c</fullName>
        <externalId>false</externalId>
        <label>Additional BCC</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>BCC_Amount__c</fullName>
        <defaultValue>0.00</defaultValue>
        <externalId>false</externalId>
        <label>BCC Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>BCC_Duraton__c</fullName>
        <defaultValue>1</defaultValue>
        <externalId>false</externalId>
        <label>BCC Duraton</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Canvass_Primary_Telco__c</fullName>
        <externalId>false</externalId>
        <formula>Order_Line_Item__r.Canvass__r.Primary_Telco__r.Telco_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Canvass Primary Telco</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CaseReason_CaseSource__c</fullName>
        <externalId>false</externalId>
        <formula>reason__c &amp; &quot; / &quot; &amp;  Source__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CaseReason/CaseSource</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Order Line Item Credits</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client_Tier__c</fullName>
        <externalId>false</externalId>
        <formula>TEXT(Case__r.Account.Client_Tier__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Client Tier</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contract_Credit_For_Ind_Telco_Report__c</fullName>
        <externalId>false</externalId>
        <formula>IF( claim_reversed__c = FALSE, TEXT( ROUND(Discount_Per_period__c, 2) ), &quot;-&quot;&amp;TEXT( ROUND(Discount_Per_period__c, 2) ) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Contract Credit For Ind Telco Report</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Amount_Reversed__c</fullName>
        <description>If the Claim was reversed, this is the amount that was posted to AR to reverse the credit amount that has already been posted to AR to balance it out.</description>
        <externalId>false</externalId>
        <inlineHelpText>If the Claim was reversed, this is the amount that was posted to AR to reverse the credit amount that has already been posted to AR to balance it out.</inlineHelpText>
        <label>Credit Amount Reversed</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Credit_Frequency__c</fullName>
        <externalId>false</externalId>
        <formula>IF(AND(Issue_as_Single_Credit__c = true, Order_Line_Item__r.Billing_Frequency__c ==&apos;Single Payment&apos;), &quot;One Time&quot;, 
IF(AND(Issue_as_Single_Credit__c = false, Order_Line_Item__r.Billing_Frequency__c ==&apos;Monthly&apos;), &quot;Recurring&quot;,
IF(AND(Issue_as_Single_Credit__c = false, Order_Line_Item__r.Billing_Frequency__c ==&apos;Single Payment&apos;), &quot;Recurring&quot;, 
IF(AND(Issue_as_Single_Credit__c = true, Order_Line_Item__r.Billing_Frequency__c ==&apos;Monthly&apos;), &quot;One Time&quot;, null))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Credit Frequency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Credit_Issued_To1__c</fullName>
        <externalId>false</externalId>
        <label>Credit Issued To</label>
        <picklist>
            <picklistValues>
                <fullName>Berry</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Telco</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Credit_Issued_To__c</fullName>
        <externalId>false</externalId>
        <label>Credit Issued To</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date_Created_Reversed__c</fullName>
        <externalId>false</externalId>
        <formula>IF(  claim_reversed__c = TRUE,  Date_of_Reversal__c ,  DATEVALUE( CreatedDate ) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Date Created/Reversed</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Date_of_Reversal__c</fullName>
        <description>The date that the Claim was reversed. Populated by Workflows</description>
        <externalId>false</externalId>
        <inlineHelpText>The date that the Claim was reversed</inlineHelpText>
        <label>Date of Reversal</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Dimension_1__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 1</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Product_Non_OLI__c</field>
                <operation>equals</operation>
                <valueField>c2g__codaDimension1__c.Type__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>c2g__codaDimension1__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_2__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 2</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>$Source.Product_Non_OLI__c</field>
                <operation>equals</operation>
                <valueField>c2g__codaDimension2__c.Type__c</valueField>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>c2g__codaDimension2__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_3__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 3</label>
        <referenceTo>c2g__codaDimension3__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Dimension_4__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Dimension 4</label>
        <referenceTo>c2g__codaDimension4__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory_Edition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>The Edition that held the error which resulted in the Claim to be opened</description>
        <externalId>false</externalId>
        <inlineHelpText>The Edition that held the error which resulted in the Claim to be opened</inlineHelpText>
        <label>Directory Edition</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Discount_Per_period__c</fullName>
        <defaultValue>0.00</defaultValue>
        <externalId>false</externalId>
        <label>Credit Per period</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Discount_Percent__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label>Credit Percent</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Discount_Type__c</fullName>
        <externalId>false</externalId>
        <label>Credit Type</label>
        <picklist>
            <picklistValues>
                <fullName>Fixed Amount</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Percent Amount</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Effective_Date__c</fullName>
        <externalId>false</externalId>
        <label>Effective Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Issue_Month__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Edition__r.Pub_Month__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Issue Month</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Issue_SCN__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Issue SCN</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Issue_as_Single_Credit__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Issue as Single Credit</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Line_Item_History__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Line Item History</label>
        <referenceTo>Line_Item_History__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Line_item_total_value__c</fullName>
        <description>used to determine the contractual value of an order line item</description>
        <externalId>false</externalId>
        <label>Line item total value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Max_BCC__c</fullName>
        <externalId>false</externalId>
        <label>Max BCC</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Max_Credit__c</fullName>
        <defaultValue>0.00</defaultValue>
        <externalId>false</externalId>
        <label>Max Credit</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Maxbcc_Num__c</fullName>
        <description>this converts the text to a number should never be seen by the user</description>
        <externalId>false</externalId>
        <formula>IF(ISNUMBER(Total_BCC__c), VALUE(Total_BCC__c), 0.0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Maxbcc Num</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Commission__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Net Commission</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Number_of_Billing_Periods_to_discount__c</fullName>
        <defaultValue>0</defaultValue>
        <externalId>false</externalId>
        <label># of Credited Billing Periods</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OLI_Billing_Frequency__c</fullName>
        <externalId>false</externalId>
        <formula>Order_Line_Item__r.Billing_Frequency__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>OLI Billing Frequency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OLI_Billing_Partner_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Order_Line_Item__r.Billing_Partner_Account__r.Telco_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>OLI Billing Partner Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Group__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>ties back to the order set where this oli came from</description>
        <externalId>false</externalId>
        <label>Order Set</label>
        <referenceTo>Order_Group__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Order_Line_Item_Canvass__c</fullName>
        <externalId>false</externalId>
        <formula>Order_Line_Item__r.Canvass__r.Canvass_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order Line Item Canvass</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Order_Line_Item__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order Line Item</label>
        <referenceTo>Order_Line_Items__c</referenceTo>
        <relationshipLabel>Order Line Item Credits</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Overcompensated__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Discount_Per_period__c &gt;  Order_Line_Item__r.UnitPrice__c , TRUE, FALSE)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Overcompensated</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_Non_OLI__c</fullName>
        <externalId>false</externalId>
        <label>Product (Non OLI)</label>
        <picklist>
            <picklistValues>
                <fullName>Print</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Digital</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pub_Year__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Edition__r.Year__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Pub Year</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SCNTelco__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Telco</label>
        <referenceTo>Telco__c</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Source__c</fullName>
        <externalId>false</externalId>
        <label>Case Source</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Telco_Partner_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Account.Telco_Partner__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Telco Partner Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Telco__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Telco</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Telco Partner</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Order Line Item Discounts</relationshipLabel>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Total_Adjustment_For_Ind_Telco_Report__c</fullName>
        <externalId>false</externalId>
        <formula>IF(  claim_reversed__c = FALSE,  total_Credit_amount__c , &quot;-&quot;&amp;total_Credit_amount__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Adjustment For Ind Telco Report</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_BCC__c</fullName>
        <defaultValue>&quot;0.00&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Total BCC</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Credit_num__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISNUMBER(total_Credit_amount__c), VALUE(total_Credit_amount__c), 0.0)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Credit num</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Total_Net_Commission__c</fullName>
        <description>This is used for national claim. Will determine what is the net commission value.</description>
        <externalId>false</externalId>
        <label>Total Net Commission</label>
        <precision>8</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>User_Responsible__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>User Responsible</label>
        <lookupFilter>
            <active>true</active>
            <errorMessage>Value does not exist or does not match filter criteria. ( only standard user can be selected)</errorMessage>
            <filterItems>
                <field>User.UserType</field>
                <operation>notEqual</operation>
                <value>Partner, Customer Portal Manager, Customer Portal User, Guest, High Volume Portal, CSN Only, Self Service</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>User</referenceTo>
        <relationshipName>Order_Line_Item_Discounts</relationshipName>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>checked__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>checked</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>claim_reversed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>true if this llid is reversed</description>
        <externalId>false</externalId>
        <label>claim reversed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>date_of_issuance__c</fullName>
        <description>this holds the date the discount was issued ( create date in most cases)</description>
        <externalId>false</externalId>
        <label>date of issuance</label>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>isvalid__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>isvalid</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>reason__c</fullName>
        <externalId>false</externalId>
        <label>Case Reason</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>total_Credit_amount__c</fullName>
        <defaultValue>&quot;0.00&quot;</defaultValue>
        <externalId>false</externalId>
        <label>Total Credit Amount</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>true</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Order Line Item Discount</label>
    <nameField>
        <displayFormat>OLID-{00000000}</displayFormat>
        <label>Order Line Item Credit Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Order Line Item Discounts</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
</CustomObject>
