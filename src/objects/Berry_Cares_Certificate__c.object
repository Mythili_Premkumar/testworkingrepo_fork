<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Custom object to house issued BCCs.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableEnhancedLookup>false</enableEnhancedLookup>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account_Telco_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Telco_Partner__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Account Telco Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The Account that owns the BCC</description>
        <externalId>false</externalId>
        <inlineHelpText>The Account that owns the BCC</inlineHelpText>
        <label>Account</label>
        <lookupFilter>
            <active>true</active>
            <filterItems>
                <field>Account.RecordTypeId</field>
                <operation>equals</operation>
                <value>Customer Account</value>
            </filterItems>
            <isOptional>false</isOptional>
        </lookupFilter>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Berry Cares Certificates</relationshipLabel>
        <relationshipName>Berry_Cares_Certificates</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Additional_BCC_Amount__c</fullName>
        <description>The amount if an additional amount was added onto the BCC upon issuance</description>
        <externalId>false</externalId>
        <inlineHelpText>The amount if an additional amount was added onto the BCC upon issuance</inlineHelpText>
        <label>Additional BCC Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>BCC_Sub_Amount__c</fullName>
        <externalId>false</externalId>
        <label>BCC Sub Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Branding__c</fullName>
        <description>The branding of the BCC</description>
        <externalId>false</externalId>
        <inlineHelpText>The branding of the BCC</inlineHelpText>
        <label>Branding</label>
        <picklist>
            <picklistValues>
                <fullName>Berry Cares</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Berry Cares Elite</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Frontier Cares</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Frontier Cares Elite</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>CSR_Alias__c</fullName>
        <externalId>false</externalId>
        <formula>CreatedBy.Alias</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CSR Alias</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CSR_First_Name__c</fullName>
        <externalId>false</externalId>
        <formula>CreatedBy.FirstName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>CSR First Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <description>The Case where the BCC originated from</description>
        <externalId>false</externalId>
        <inlineHelpText>The Case where the BCC originated from</inlineHelpText>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>Berry Cares Certificates</relationshipLabel>
        <relationshipName>Berry_Cares_Certificates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Certificate_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.CaseNumber &amp;&quot;-&quot;&amp;  Case__r.Case_Closed_By_Alias__c &amp;&quot;-&quot;&amp; TEXT(DAY(DATEVALUE(CreatedDate))) &amp; &quot;-&quot; &amp; TEXT(MONTH(DATEVALUE(CreatedDate))) &amp; &quot;-&quot; &amp; RIGHT(TEXT(YEAR(DATEVALUE(CreatedDate))) , 2)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Certificate Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CheckBCCValue__c</fullName>
        <externalId>false</externalId>
        <label>CheckBCCValue</label>
        <picklist>
            <picklistValues>
                <fullName>BCC_Checked</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BCC_Unchecked</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>CheckValues__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>CheckValues</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Client_Contact_Email__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Contact.Email</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Client Contact Email</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Contact_First_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Contact.FirstName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Client Contact First Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Contact_Last_Name__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Contact.LastName</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Client Contact Last Name</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Tier__c</fullName>
        <externalId>false</externalId>
        <formula>Case__r.Client_Tier__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Client Tier</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Customer_ID__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Account_Number__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Customer ID</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Date_of_Issue__c</fullName>
        <externalId>false</externalId>
        <formula>TRIM(LEFT(TEXT(CreatedDate),10))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Date of Issue</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Duration__c</fullName>
        <description>The duration the BCC is worth</description>
        <externalId>false</externalId>
        <inlineHelpText>The duration the BCC is worth</inlineHelpText>
        <label>Duration</label>
        <precision>2</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Expiration_Date__c</fullName>
        <externalId>false</externalId>
        <label>Expiration Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>External_Id__c</fullName>
        <externalId>false</externalId>
        <label>External Id</label>
        <length>255</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Is_Redeemed__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If checked, the BCC has been redeemed</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, the BCC has been redeemed</inlineHelpText>
        <label>Is Redeemed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Order_Line_Item_Discount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Order Line Item Discount</label>
        <referenceTo>Order_Line_Item_Discount__c</referenceTo>
        <relationshipLabel>Berry Cares Certificates</relationshipLabel>
        <relationshipName>Berry_Cares_Certificates</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Primary_Telephone_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Account__r.Phone</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Primary Telephone Number</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Redeemable__c</fullName>
        <defaultValue>false</defaultValue>
        <description>If this is checked the BCC can be redeemed. It will not be checked until the Case it originated from is Closed and will be unchecked once it is redeemed in BMI.</description>
        <externalId>false</externalId>
        <label>Redeemable</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>BCC_Sub_Amount__c * Duration__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>UnCheckBCCValue__c</fullName>
        <externalId>false</externalId>
        <label>UnCheckBCCValue</label>
        <picklist>
            <picklistValues>
                <fullName>BCC_Checked</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>BCC_Unchecked</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Berry Cares Certificate</label>
    <listViews>
        <fullName>All</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>BCC-{0000000}</displayFormat>
        <label>BCC Number</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Berry Cares Certificates</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Case__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Certificate_Number__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Date_of_Issue__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Expiration_Date__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Is_Redeemed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total_Amount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATEDBY_USER.ALIAS</customTabListAdditionalFields>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <excludedStandardButtons>New</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Case__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Certificate_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Date_of_Issue__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Expiration_Date__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Is_Redeemed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Total_Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATEDBY_USER.ALIAS</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Primary_Telephone_Number__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Case__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Certificate_Number__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Date_of_Issue__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Expiration_Date__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Is_Redeemed__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Total_Amount__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>CREATEDBY_USER.ALIAS</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>Case__c</searchFilterFields>
        <searchFilterFields>Certificate_Number__c</searchFilterFields>
        <searchFilterFields>Primary_Telephone_Number__c</searchFilterFields>
        <searchFilterFields>Client_Contact_Email__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Case__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Certificate_Number__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Date_of_Issue__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Expiration_Date__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Is_Redeemed__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Total_Amount__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>CREATEDBY_USER.ALIAS</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
