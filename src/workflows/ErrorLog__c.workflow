<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Fulfillment_Integration_Error_Log_EA</fullName>
        <description>Fulfillment Integration Error Log EA</description>
        <protected>false</protected>
        <recipients>
            <recipient>reddy.yellanki@theberrycompany.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Fulfillment_ErrorLog</template>
    </alerts>
    <rules>
        <fullName>Fulfillment Integration Error Log</fullName>
        <actions>
            <name>Fulfillment_Integration_Error_Log_EA</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>ErrorLog__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
