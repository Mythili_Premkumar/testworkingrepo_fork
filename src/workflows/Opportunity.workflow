<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Late_Print_Order_Approved_Email_Alert</fullName>
        <description>Late Print Order Approved Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Late_Print_Order_Approved</template>
    </alerts>
    <alerts>
        <fullName>Late_Print_Order_Rejected_Email_Alert</fullName>
        <description>Late Print Order Rejected Email Alert</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Opportunity_Templates/Late_Print_Order_Rejected</template>
    </alerts>
    <fieldUpdates>
        <fullName>Close_Date_withTime</fullName>
        <field>Close_Date__c</field>
        <formula>LastModifiedDate</formula>
        <name>Close Date withTime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Close</fullName>
        <description>Opportunity status will change to Closed Lost when</description>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Opportunity Close</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Opportunity_Name_Field_Update</fullName>
        <field>Name</field>
        <formula>IF( LEN(Account.Name )&gt; 105 , LEFT(Account.Name, 105)+ &quot; &quot; + TEXT ( Today() ) , Account.Name + &quot; &quot; + TEXT ( Today() ))</formula>
        <name>Opportunity Name Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Oppty_Website</fullName>
        <description>Website should feed from Account 
Should only edit at Account page</description>
        <field>Website__c</field>
        <formula>Account.Website</formula>
        <name>Oppty Website</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Late_Print_Order_Approved_to_TRUE</fullName>
        <description>Once approved, set Late Print Order Approved to TRUE</description>
        <field>Late_Print_Order_Approved__c</field>
        <literalValue>1</literalValue>
        <name>Set Late Print Order Approved to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Late_Print_Order_Rejected_to_TRUE</fullName>
        <description>If rejected, set Late Order Rejected to TRUE</description>
        <field>Late_Print_Order_Rejected__c</field>
        <literalValue>1</literalValue>
        <name>Set Late Print Order Rejected to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Close Date withTime</fullName>
        <actions>
            <name>Close_Date_withTime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Name Default</fullName>
        <actions>
            <name>Opportunity_Name_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populates the Opportunity Name with the Account Name and date of creation.</description>
        <formula>ISBLANK(CORE_Migration_ID__c) &amp;&amp; NOT(CONTAINS( Name, (Account.Name + TEXT(Today())) ))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity%3A Expired Close Date</fullName>
        <actions>
            <name>Open_Expired_Opportunity_Please_close</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.IsClosed</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>No Contact,Attempting to contact,Contacted,Proposal Created,Signature Pending</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.CloseDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Opportunities with Close Date that is expiring</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Open_Expired_Opportunity_Please_close_Account_Owner</name>
                <type>Task</type>
            </actions>
            <actions>
                <name>X2nd_Notice_Opportunity_Expired_Date</name>
                <type>Task</type>
            </actions>
            <timeLength>10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Website</fullName>
        <actions>
            <name>Oppty_Website</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Website should feed from Account 
Should only edit at Account page</description>
        <formula>NOT(ISBLANK( Account.Website ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <tasks>
        <fullName>Open_Expired_Opportunity_Please_close</fullName>
        <assignedToType>owner</assignedToType>
        <description>Your Opportunity&apos;s Close Date has an expired date.  Please review the Opportunity and change the Close Date to a future date or change the Opportunity Stage to Closed Lost.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Open Expired Opportunity: Please close</subject>
    </tasks>
    <tasks>
        <fullName>Open_Expired_Opportunity_Please_close_Account_Owner</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>Please review this expired quote.  If it is still being worked, please update the close date. If this Opportunity is dead, please change the stage to Closed - Lost.</description>
        <dueDateOffset>15</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.CloseDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Open Expired Opportunity: Please close</subject>
    </tasks>
    <tasks>
        <fullName>X2nd_Notice_Opportunity_Expired_Date</fullName>
        <assignedToType>accountOwner</assignedToType>
        <description>The close date that has expired on this Opportunity. Please update this opportunity with a new close date if you are continuing to work this opportunity or if the deal is considered dead, please change the Opportunity stage to Closed - Lost.</description>
        <dueDateOffset>5</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>2nd Notice: Opportunity with Expired Close Date</subject>
    </tasks>
</Workflow>
