<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_Rep_Inactive_Notification</fullName>
        <description>Notify Branch Manager when Sales Rep is set to Inactive</description>
        <protected>false</protected>
        <recipients>
            <field>Branch_Manager_s_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Sales_Rep_Inactive_Notification_VF</template>
    </alerts>
    <fieldUpdates>
        <fullName>Branch_Manager_s_Email</fullName>
        <field>Branch_Manager_s_Email__c</field>
        <formula>Branch_Manager__r.Email</formula>
        <name>Branch Manager&apos;s Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Discontinued_Date</fullName>
        <description>Used to update miles when user is deactivvated</description>
        <field>Discontinued_Date__c</field>
        <formula>Now()</formula>
        <name>Update Discontinued Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Branch Manager%27s Email</fullName>
        <actions>
            <name>Branch_Manager_s_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.Branch_Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.UserType</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Sales Rep Inactive Notification</fullName>
        <actions>
            <name>Sales_Rep_Inactive_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Send the Branch Manager an email notification when a Sales Rep is set to Inactive</description>
        <formula>AND (NOT ( ISNEW()),ISCHANGED( IsActive ), IsActive = FALSE,   Profile.Name = &quot;Sales Rep&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Discontinued Date</fullName>
        <actions>
            <name>Update_Discontinued_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.IsActive</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Use to set the discontinued date when the user is deactivated in SFDC</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
