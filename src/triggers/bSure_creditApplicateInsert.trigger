trigger bSure_creditApplicateInsert on Vertex_Berry__BsureCreditApplication__c (before insert, after insert, after update) {
    String caobjId ='';
    list<BsureCreditApplication__c> lstca = new list<BsureCreditApplication__c>();
    list<BsureCreditApplication__c> lstExistingca = new list<BsureCreditApplication__c>();
    list<Vertex_Berry__BSureC_Customer_Basic_Info__c> lstcust = new list<Vertex_Berry__BSureC_Customer_Basic_Info__c>();
    
    if(Trigger.isBefore)
    { 
        for(Vertex_Berry__BsureCreditApplication__c caobj: Trigger.new)
        {
            if(caobj.Vertex_Berry__ExternalId__c != null)
            {
                list<Opportunity> listoppt = new list<Opportunity>([Select Id,Name from Opportunity where Id =: caobj.Vertex_Berry__ExternalId__c]); 
                if(listoppt != null && listoppt.size() > 0)
                {
                    caobj.Vertex_Berry__Opportunity_Name__c = listoppt.get(0).Name;
                }
            }        
        }   
    }
    if(Trigger.isAfter && Trigger.isInsert)
    {
        for(Vertex_Berry__BsureCreditApplication__c caobj: Trigger.new)
        { 
            String strOppId = caobj.Vertex_Berry__ExternalId__c;
            caobjId = strOppId.substring(0,15);
            if(caobjId != null && caobjId != '')
            {
                    lstExistingca = [SELECT Id,ExternalId__c,Status__c FROM BsureCreditApplication__c WHERE ExternalId__c =: caobjId and Status__c != 'Completed'];
                    if(lstExistingca !=null && lstExistingca.size()>0)
                    {
                        for(BsureCreditApplication__c objCreditInfo : lstExistingca)
                        {
                            objCreditInfo.Status__c='Cancelled';
                        }
                        update lstExistingca;
                    } 
                   
                    BsureCreditApplication__c ca = new BsureCreditApplication__c();
                    if(caobj.Id != null)
                    {
                        String strcaId = caobj.Id;
                        ca.Bsure_CA_Id__c = strcaId.substring(0,15);
                    }
                    if(caobj.Vertex_Berry__Buisness_Name__c != null)
                    {
                        ca.Buisness_Name__c = caobj.Vertex_Berry__Buisness_Name__c;
                    }
                    if(caobj.Vertex_Berry__Number_of_Employees__c != null)
                    {   
                        ca.Number_of_Employees__c = caobj.Vertex_Berry__Number_of_Employees__c;
                    }
                    
                    //Billing Address
                    if(caobj.Vertex_Berry__Billing_City__c != null)
                    {   
                        ca.Billing_City__c = caobj.Vertex_Berry__Billing_City__c;
                    }
                    if(caobj.Vertex_Berry__Billing_State__c != null)
                    {   
                        ca.Billing_State__c = caobj.Vertex_Berry__Billing_State__c;
                    }
                    if(caobj.Vertex_Berry__Billing_Street__c != null)
                    {   
                        ca.Billing_Street__c = caobj.Vertex_Berry__Billing_Street__c;
                    }   
                    if(caobj.Vertex_Berry__Billing1_Zip__c != null)
                    {
                        //ca.Billing_Zip__c = caobj.Vertex_Berry__Billing_Zip__c;
                        ca.Billing_Zip__c = caobj.Vertex_Berry__Billing1_Zip__c;
                    }  
                    
                    //Temporary Address
                    if(caobj.Vertex_Berry__Temporary_City__c != null)
                    {   
                        ca.Temporary_City__c = caobj.Vertex_Berry__Temporary_City__c;
                    }
                    if(caobj.Vertex_Berry__Temporary_State__c != null)
                    {   
                        ca.Temporary_State__c = caobj.Vertex_Berry__Temporary_State__c;
                    }
                    if(caobj.Vertex_Berry__Temporary_Street__c != null)
                    {   
                        ca.Temporary_Street__c = caobj.Vertex_Berry__Temporary_Street__c;
                    }   
                    if(caobj.Vertex_Berry__Temporary_Zip__c != null)
                    {
                        ca.Temporary_Zip__c = caobj.Vertex_Berry__Temporary_Zip__c;
                    }  
                    
                    //Permanent Address
                    if(caobj.Vertex_Berry__Permanent_City__c != null)
                    {   
                        ca.Permanent_City__c = caobj.Vertex_Berry__Permanent_City__c;
                    }
                    if(caobj.Vertex_Berry__Permanent_State__c != null)
                    {   
                        ca.Permanent_State__c = caobj.Vertex_Berry__Permanent_State__c;
                    }
                    if(caobj.Vertex_Berry__Permanent_Street__c != null)
                    {   
                        ca.Permanent_Street__c = caobj.Vertex_Berry__Permanent_Street__c;
                    }   
                    if(caobj.Vertex_Berry__Permanent_Zip__c != null)
                    {
                        ca.Permanent_Zip__c = caobj.Vertex_Berry__Permanent_Zip__c;
                    }  
                    
                     
                    if(caobj.Vertex_Berry__ExternalId__c != null)
                    {
                        String strOppId1 = '';
                        strOppId1 = caobj.Vertex_Berry__ExternalId__c;
                        if(strOppId1 != null && strOppId1 != '')
                        {
                            ca.Opportunity__c = strOppId1.substring(0,15);
                            ca.ExternalId__c = strOppId1.substring(0,15);
                        }    
                        ca.Status__c = 'Pending';
                    }
                    
                    //new
                    if(caobj.Vertex_Berry__Customer_Account_Number__c   != null)
                    {
                        ca.Customer_Account_Number__c    = caobj.Vertex_Berry__Customer_Account_Number__c   ;
                    }
                    if(caobj.Vertex_Berry__Contact_Number__c != null)
                    {
                        ca.Contact_Number__c = caobj.Vertex_Berry__Contact_Number__c;
                    }
                    if(caobj.Vertex_Berry__Legal_Business_Name__c != null)
                    {
                        ca.Legal_Business_Name__c = caobj.Vertex_Berry__Legal_Business_Name__c;
                    }
                    if(caobj.Vertex_Berry__Listed_Business_Name__c != null)
                    {
                        ca.Listed_Business_Name__c = caobj.Vertex_Berry__Listed_Business_Name__c;
                    }
                    if(caobj.Vertex_Berry__Telephone_Numbers_New__c != null)
                    {
                        ca.Telephone_Numbers_New__c = caobj.Vertex_Berry__Telephone_Numbers_New__c;
                    }
                    if(caobj.Vertex_Berry__Annual_Estimated_Income__c != null)
                    {
                        ca.Annual_Estimated_Income__c = caobj.Vertex_Berry__Annual_Estimated_Income__c;
                    }
                    //Date
                    if(caobj.Vertex_Berry__Year_Business_Established__c != null)
                    {
                        ca.Year_Business_Established__c = caobj.Vertex_Berry__Year_Business_Established__c;
                    }
                    //Text
                    if(caobj.Vertex_Berry__Year_Business_Established1__c != null)
                    {
                        ca.Year_Business_Established1__c = caobj.Vertex_Berry__Year_Business_Established1__c;
                    }
                    if(caobj.Vertex_Berry__Physical_Address__c != null)
                    {
                        ca.Physical_Address__c = caobj.Vertex_Berry__Physical_Address__c ;
                    }
                    if(Caobj.Vertex_Berry__Physical_City__c != null)
                    {
                        ca.Physical_City__c = caobj.Vertex_Berry__Physical_City__c ;
                    }
                    if(caobj.Vertex_Berry__Physical1_State__c != null)
                    {
                        ca.Physical_State__c = caobj.Vertex_Berry__Physical1_State__c ;
                    }
                    if(caobj.Vertex_Berry__Physical_Street__c != null) 
                    {
                        ca.Physical_Street__c = caobj.Vertex_Berry__Physical_Street__c ;
                    }
                     if(caobj.Vertex_Berry__Physical1_Zip__c != null)
                    {
                        ca.Physical_Zip__c = caobj.Vertex_Berry__Physical1_Zip__c ;
                    }
                    if(caobj.Vertex_Berry__Owner_Officer_information__c != null)
                    {
                        ca.Owner_Officer_information__c = caobj.Vertex_Berry__Owner_Officer_information__c ;
                    }
                    if(caobj.Vertex_Berry__SSN1_New__c != null)
                    {
                        ca.SSN1_New__c = caobj.Vertex_Berry__SSN1_New__c ;
                    }
                    if(caobj.Vertex_Berry__Title1__c != null) 
                    {
                        ca.Title1__c = caobj.Vertex_Berry__Title1__c ;
                    }
                    if(caobj.Vertex_Berry__Telephone_Number1__c != null)
                    {
                        ca.Telephone_Number1__c = caobj.Vertex_Berry__Telephone_Number1__c ;
                    }
                    if(caobj.Vertex_Berry__Residence_Address1__c != null )
                    {
                        ca.Residence_Address1__c = caobj.Vertex_Berry__Residence_Address1__c ;
                    }
                    if(caobj.Vertex_Berry__Sales_Representative_Name__c != null)
                    {
                        ca.Sales_Representative_Name__c = caobj.Vertex_Berry__Sales_Representative_Name__c ;
                    }
                    if(caobj.Vertex_Berry__Owner_Officer_information2__c != null)
                    {
                        ca.Owner_Officer_information2__c = caobj.Vertex_Berry__Owner_Officer_information2__c;
                    }
                    if(caobj.Vertex_Berry__Title2__c != null)
                    {
                        ca.Title2__c = caobj.Vertex_Berry__Title2__c;
                    }
                    if(caobj.Vertex_Berry__SSN2_New__c != null)
                    {
                        ca.SSN2_New__c = caobj.Vertex_Berry__SSN2_New__c;
                    }
                    if(caobj.Vertex_Berry__Residence_Address2__c != null)
                    {
                        ca.Residence_Address2__c = caobj.Vertex_Berry__Residence_Address2__c;
                    }
                    if(caobj.Vertex_Berry__Telephone_Number2__c != null)
                    {
                        ca.Telephone_Number2__c = caobj.Vertex_Berry__Telephone_Number2__c;
                    }
                    if(caobj.Vertex_Berry__Salesperson__c != null)
                    {
                        ca.Salesperson__c = caobj.Vertex_Berry__Salesperson__c;
                    }  
                    if(caobj.Vertex_Berry__Sales_Manager__c != null)
                    {
                        ca.Sales_Manager__c = caobj.Vertex_Berry__Sales_Manager__c;
                    }          
                    //Equifax data insert -- Start
                    if(caobj.Vertex_Berry__BCS__c != null)
                    {
                        ca.BCS__c = caobj.Vertex_Berry__BCS__c;
                    }   
                    if(caobj.Vertex_Berry__PCS__c != null)
                    {
                        ca.PCS__c = caobj.Vertex_Berry__PCS__c;
                    } 
                    if(caobj.Vertex_Berry__EquifaxId__c != null)
                    {
                        ca.EquifaxId__c = caobj.Vertex_Berry__EquifaxId__c;
                    } 
                    if(caobj.Vertex_Berry__SIC_Code__c != null)
                    {
                        ca.SIC_Code__c = caobj.Vertex_Berry__SIC_Code__c;
                    } 
                    if(caobj.Vertex_Berry__NumberOfBankruptcies__c != null)
                    {
                        ca.NumberOfBankruptcies__c = caobj.Vertex_Berry__NumberOfBankruptcies__c;
                    }   
                    if(caobj.Vertex_Berry__Number_Of_Liens__c != null)
                    {
                        ca.Number_Of_Liens__c = caobj.Vertex_Berry__Number_Of_Liens__c;
                    } 
                    if(caobj.Vertex_Berry__Available_Credit__c != null)
                    {
                        ca.Available_Credit__c = caobj.Vertex_Berry__Available_Credit__c;
                    } 
                    if(caobj.Vertex_Berry__Payment_Index_Industry__c != null)
                    {
                        ca.Payment_Index_Industry__c = caobj.Vertex_Berry__Payment_Index_Industry__c;
                    } 
                    if(caobj.Vertex_Berry__Payment_Index_Business__c != null)
                    {
                        ca.Payment_Index_Business__c = caobj.Vertex_Berry__Payment_Index_Business__c;
                    } 
                    if(caobj.Vertex_Berry__Other_Business_Names__c != null)
                    {
                        ca.Other_Business_Names__c = caobj.Vertex_Berry__Other_Business_Names__c ;
                    }
                     if(caobj.Vertex_Berry__Business_Structure__c != null)
                     {
                        ca.Business_Structure__c = caobj.Vertex_Berry__Business_Structure__c ;
                     }
                     if(caobj.Vertex_Berry__Date_of_Incorporation__c != null)
                     {
                        ca.Date_of_Incorporation__c = caobj.Vertex_Berry__Date_of_Incorporation__c ;
                     }
                     if(caobj.Vertex_Berry__State_of_Incorporation__c != null)
                     {
                        ca.State_of_Incorporation__c = caobj.Vertex_Berry__State_of_Incorporation__c ;
                     }
                     if(caobj.Vertex_Berry__Federal_Tax_ID_Number__c != null)
                     {
                        ca.Federal_Tax_ID_Number__c = caobj.Vertex_Berry__Federal_Tax_ID_Number__c ;
                     }
                     if(caobj.Vertex_Berry__Cell_Phone_New__c != null)
                     {
                        ca.Cell_Phone_New__c = caobj.Vertex_Berry__Cell_Phone_New__c ;
                     }
                     if(caobj.Vertex_Berry__PIA__c != null)
                     {
                        ca.PIA__c = caobj.Vertex_Berry__PIA__c ;
                     }  
                     if(caobj.Vertex_Berry__Annual_Sales__c != null)
                     {
                        ca.Annual_Sales__c = caobj.Vertex_Berry__Annual_Sales__c ;
                     } 
                     if(caobj.Vertex_Berry__AvailableCredit__c != null)
                     {
                        ca.AvailableCredit__c = caobj.Vertex_Berry__AvailableCredit__c ;
                     } 
                     if(caobj.Vertex_Berry__TotalBalance__c != null)
                     { 
                        ca.TotalBalance__c = caobj.Vertex_Berry__TotalBalance__c ;
                     }   
                     if(caobj.Vertex_Berry__Business_Failure_National_Percentage__c != null)
                     { 
                        ca.Business_Failure_National_Percentage__c = caobj.Vertex_Berry__Business_Failure_National_Percentage__c ;
                     }   
                     if(caobj.Vertex_Berry__Business_Failure_Score__c != null)
                     { 
                        ca.Business_Failure_Score__c = caobj.Vertex_Berry__Business_Failure_Score__c;
                     }    
                     if(caobj.Vertex_Berry__Failure_Rate_National_Average__c != null)
                     { 
                        ca.Failure_Rate_National_Average__c = caobj.Vertex_Berry__Failure_Rate_National_Average__c;
                     }   
                     if(caobj.Vertex_Berry__Failure_Rate_Within_Business_Failure_Ris__c != null)
                     { 
                        ca.Failure_Rate_Within_Business_Failure_Ris__c = caobj.Vertex_Berry__Failure_Rate_Within_Business_Failure_Ris__c;
                     }   
                     if(caobj.Vertex_Berry__Business_Failure_Risk_Class_and_Desc__c != null)
                     { 
                        ca.Business_Failure_Risk_Class_and_Desc__c = caobj.Vertex_Berry__Business_Failure_Risk_Class_and_Desc__c;
                     }   
                     if(caobj.Vertex_Berry__Judgments_number__c != null)
                     { 
                        ca.Judgments_number__c = caobj.Vertex_Berry__Judgments_number__c;
                     } 
                    //Equifax data insert -- End
                    
                    lstca.add(ca);
                    System.debug('lstca=====size====='+lstca.size());   
                    if(lstca != null && lstca.size() > 0)
                    {
                        upsert lstca;
                    }
              
            }      
        }
        for(Vertex_Berry__BsureCreditApplication__c caobj2: Trigger.new)
        {
            System.debug('caobj2.Id========='+caobj2.Id);
            System.debug('caobj2.Vertex_Berry__ExternalId__c========'+caobj2.Vertex_Berry__ExternalId__c);
            list<Vertex_Berry__BsureCreditApplication__c> lstCA2 = new list<Vertex_Berry__BsureCreditApplication__c>([Select Id,Vertex_Berry__ExternalId__c,Vertex_Berry__Status__c from Vertex_Berry__BsureCreditApplication__c 
                                                                 where Vertex_Berry__Status__c ='Pending' and Vertex_Berry__ExternalId__c =: caobj2.Vertex_Berry__ExternalId__c and Id !=: caobj2.Id]);
            System.debug('lstCA2========'+lstCA2);
            if(lstCA2 != null && lstCA2.size() > 0)
            {
                for(Vertex_Berry__BsureCreditApplication__c caobj3 : lstCA2)
                {
                    caobj3.Vertex_Berry__Status__c = 'Cancelled';
                }
                update lstCA2;
            }                                                     
        }
    }
    if(Trigger.isUpdate && Trigger.isAfter)
    {
        for(Vertex_Berry__BsureCreditApplication__c caobj: Trigger.new)
        {
            String strOppId2 = '';
            strOppId2 = caobj.Vertex_Berry__ExternalId__c;
            if(strOppId2 != null && strOppId2 != '')
            {
                caobjId = strOppId2.substring(0,15);
            }   
            if(caobj.Vertex_Berry__Status__c == 'Completed')
            {
                if(caobjId != null && caobjId != '')
                {
                        lstExistingca = [SELECT Id,ExternalId__c,Status__c FROM BsureCreditApplication__c WHERE ExternalId__c =: caobjId and Status__c = 'Pending'];
                        if(lstExistingca !=null && lstExistingca.size()>0)
                        {
                            for(BsureCreditApplication__c objCreditInfo : lstExistingca)
                            {
                                objCreditInfo.Status__c='Completed';
                                if(caobj.Id != null)
                                {
                                    String strcaId = caobj.Id;
                                    objCreditInfo.Bsure_CA_Id__c = strcaId.substring(0,15);
                                } 
                                if(caobj.Vertex_Berry__Buisness_Name__c != null)
                                {
                                    objCreditInfo.Buisness_Name__c = caobj.Vertex_Berry__Buisness_Name__c;
                                }
                                if(caobj.Vertex_Berry__Number_of_Employees__c != null)
                                {   
                                    objCreditInfo.Number_of_Employees__c = caobj.Vertex_Berry__Number_of_Employees__c;
                                }
                                
                                //Billing Address
                                if(caobj.Vertex_Berry__Billing_City__c != null)
                                {   
                                    objCreditInfo.Billing_City__c = caobj.Vertex_Berry__Billing_City__c;
                                }
                                if(caobj.Vertex_Berry__Billing_State__c != null)
                                {   
                                    objCreditInfo.Billing_State__c = caobj.Vertex_Berry__Billing_State__c;
                                }
                                if(caobj.Vertex_Berry__Billing_Street__c != null)
                                {   
                                    objCreditInfo.Billing_Street__c = caobj.Vertex_Berry__Billing_Street__c;
                                }   
                                if(caobj.Vertex_Berry__Billing1_Zip__c != null)
                                { 
                                    //objCreditInfo.Billing_Zip__c = caobj.Vertex_Berry__Billing_Zip__c;
                                    objCreditInfo.Billing_Zip__c = caobj.Vertex_Berry__Billing1_Zip__c;
                                }  
                                
                                //Temporary Address
                                if(caobj.Vertex_Berry__Temporary_City__c != null)
                                {   
                                    objCreditInfo.Temporary_City__c = caobj.Vertex_Berry__Temporary_City__c;
                                }
                                if(caobj.Vertex_Berry__Temporary_State__c != null)
                                {   
                                    objCreditInfo.Temporary_State__c = caobj.Vertex_Berry__Temporary_State__c;
                                }
                                if(caobj.Vertex_Berry__Temporary_Street__c != null)
                                {   
                                    objCreditInfo.Temporary_Street__c = caobj.Vertex_Berry__Temporary_Street__c;
                                }   
                                if(caobj.Vertex_Berry__Temporary_Zip__c != null)
                                {
                                    objCreditInfo.Temporary_Zip__c = caobj.Vertex_Berry__Temporary_Zip__c;
                                }  
                                
                                //Permanent Address
                                if(caobj.Vertex_Berry__Permanent_City__c != null)
                                {   
                                    objCreditInfo.Permanent_City__c = caobj.Vertex_Berry__Permanent_City__c;
                                }
                                if(caobj.Vertex_Berry__Permanent_State__c != null)
                                {   
                                    objCreditInfo.Permanent_State__c = caobj.Vertex_Berry__Permanent_State__c;
                                }
                                if(caobj.Vertex_Berry__Permanent_Street__c != null)
                                {   
                                    objCreditInfo.Permanent_Street__c = caobj.Vertex_Berry__Permanent_Street__c;
                                }   
                                if(caobj.Vertex_Berry__Permanent_Zip__c != null)
                                {
                                    objCreditInfo.Permanent_Zip__c = caobj.Vertex_Berry__Permanent_Zip__c;
                                }  
                    
                    
                                   //new
                                if(caobj.Vertex_Berry__Customer_Account_Number__c   != null)
                                {
                                    objCreditInfo.Customer_Account_Number__c     = caobj.Vertex_Berry__Customer_Account_Number__c   ;
                                }
                                if(caobj.Vertex_Berry__Contact_Number__c != null)
                                {
                                    objCreditInfo.Contact_Number__c = caobj.Vertex_Berry__Contact_Number__c;
                                }
                                if(caobj.Vertex_Berry__Legal_Business_Name__c != null)
                                {
                                    objCreditInfo.Legal_Business_Name__c = caobj.Vertex_Berry__Legal_Business_Name__c;
                                }
                                if(caobj.Vertex_Berry__Listed_Business_Name__c != null)
                                {
                                    objCreditInfo.Listed_Business_Name__c = caobj.Vertex_Berry__Listed_Business_Name__c;
                                }
                                if(caobj.Vertex_Berry__Telephone_Numbers_New__c != null)
                                {
                                    objCreditInfo.Telephone_Numbers_New__c = caobj.Vertex_Berry__Telephone_Numbers_New__c;
                                }
                                if(caobj.Vertex_Berry__Annual_Estimated_Income__c != null)
                                {
                                    objCreditInfo.Annual_Estimated_Income__c = caobj.Vertex_Berry__Annual_Estimated_Income__c;
                                }
                                //Date
                                if(caobj.Vertex_Berry__Year_Business_Established__c != null)
                                {
                                    objCreditInfo.Year_Business_Established__c = caobj.Vertex_Berry__Year_Business_Established__c;
                                }
                                //Text
                                if(caobj.Vertex_Berry__Year_Business_Established1__c != null)
                                {
                                    objCreditInfo.Year_Business_Established1__c = caobj.Vertex_Berry__Year_Business_Established1__c;
                                }
                                if(caobj.Vertex_Berry__Physical_Address__c != null)
                                {
                                    objCreditInfo.Physical_Address__c = caobj.Vertex_Berry__Physical_Address__c ;
                                }
                                if(Caobj.Vertex_Berry__Physical_City__c != null)
                                {
                                    objCreditInfo.Physical_City__c = caobj.Vertex_Berry__Physical_City__c ;
                                }
                                if(caobj.Vertex_Berry__Physical1_State__c != null)
                                {
                                    objCreditInfo.Physical_State__c = caobj.Vertex_Berry__Physical1_State__c ;
                                }
                                if(caobj.Vertex_Berry__Physical_Street__c != null)
                                {
                                    objCreditInfo.Physical_Street__c = caobj.Vertex_Berry__Physical_Street__c ;
                                }
                                if(caobj.Vertex_Berry__Physical1_Zip__c != null)
                                {
                                    objCreditInfo.Physical_Zip__c = caobj.Vertex_Berry__Physical1_Zip__c;
                                }
                                if(caobj.Vertex_Berry__Owner_Officer_information__c != null)
                                {
                                    objCreditInfo.Owner_Officer_information__c = caobj.Vertex_Berry__Owner_Officer_information__c ;
                                }
                                if(caobj.Vertex_Berry__SSN1_New__c != null)
                                {
                                    objCreditInfo.SSN1_New__c = caobj.Vertex_Berry__SSN1_New__c ;
                                }
                                if(caobj.Vertex_Berry__Title1__c != null) 
                                {
                                    objCreditInfo.Title1__c = caobj.Vertex_Berry__Title1__c ;
                                }
                                if(caobj.Vertex_Berry__Telephone_Number1__c != null)
                                {
                                    objCreditInfo.Telephone_Number1__c = caobj.Vertex_Berry__Telephone_Number1__c ;
                                }
                                if(caobj.Vertex_Berry__Residence_Address1__c != null )
                                {
                                    objCreditInfo.Residence_Address1__c = caobj.Vertex_Berry__Residence_Address1__c ;
                                }
                                if(caobj.Vertex_Berry__Sales_Representative_Name__c != null)
                                {
                                    objCreditInfo.Sales_Representative_Name__c = caobj.Vertex_Berry__Sales_Representative_Name__c ;
                                }
                                if(caobj.Vertex_Berry__Owner_Officer_information2__c != null)
                                {
                                    objCreditInfo.Owner_Officer_information2__c = caobj.Vertex_Berry__Owner_Officer_information2__c;
                                }
                                if(caobj.Vertex_Berry__Title2__c != null)
                                {
                                    objCreditInfo.Title2__c = caobj.Vertex_Berry__Title2__c;
                                }
                                if(caobj.Vertex_Berry__SSN2_New__c != null)
                                {
                                    objCreditInfo.SSN2_New__c = caobj.Vertex_Berry__SSN2_New__c;
                                }
                                if(caobj.Vertex_Berry__Residence_Address2__c != null)
                                {
                                    objCreditInfo.Residence_Address2__c = caobj.Vertex_Berry__Residence_Address2__c;
                                }
                                if(caobj.Vertex_Berry__Telephone_Number2__c != null)
                                {
                                    objCreditInfo.Telephone_Number2__c = caobj.Vertex_Berry__Telephone_Number2__c;
                                }
                                 if(caobj.Vertex_Berry__Salesperson__c != null)
                                {
                                    objCreditInfo.Salesperson__c = caobj.Vertex_Berry__Salesperson__c;
                                }  
                                if(caobj.Vertex_Berry__Sales_Manager__c != null)
                                {
                                    objCreditInfo.Sales_Manager__c = caobj.Vertex_Berry__Sales_Manager__c;
                                }    
                                //Equifax data insert -- Start
                                if(caobj.Vertex_Berry__BCS__c != null)
                                {
                                    objCreditInfo.BCS__c = caobj.Vertex_Berry__BCS__c;
                                }   
                                if(caobj.Vertex_Berry__PCS__c != null)
                                {
                                    objCreditInfo.PCS__c = caobj.Vertex_Berry__PCS__c;
                                } 
                                if(caobj.Vertex_Berry__EquifaxId__c != null)
                                {
                                    objCreditInfo.EquifaxId__c = caobj.Vertex_Berry__EquifaxId__c;
                                } 
                                if(caobj.Vertex_Berry__SIC_Code__c != null)
                                {
                                    objCreditInfo.SIC_Code__c = caobj.Vertex_Berry__SIC_Code__c;
                                } 
                                if(caobj.Vertex_Berry__NumberOfBankruptcies__c != null)
                                {
                                    objCreditInfo.NumberOfBankruptcies__c = caobj.Vertex_Berry__NumberOfBankruptcies__c;
                                }   
                                if(caobj.Vertex_Berry__Number_Of_Liens__c != null)
                                {
                                    objCreditInfo.Number_Of_Liens__c = caobj.Vertex_Berry__Number_Of_Liens__c;
                                } 
                                if(caobj.Vertex_Berry__Available_Credit__c != null)
                                {
                                    objCreditInfo.Available_Credit__c = caobj.Vertex_Berry__Available_Credit__c;
                                } 
                                if(caobj.Vertex_Berry__Payment_Index_Industry__c != null)
                                {
                                    objCreditInfo.Payment_Index_Industry__c = caobj.Vertex_Berry__Payment_Index_Industry__c;
                                } 
                                if(caobj.Vertex_Berry__Payment_Index_Business__c != null)
                                {
                                    objCreditInfo.Payment_Index_Business__c = caobj.Vertex_Berry__Payment_Index_Business__c;
                                } 
                                //Equifax data insert -- End
                                if(caobj.Vertex_Berry__Other_Business_Names__c != null)
                                {
                                    objCreditInfo.Other_Business_Names__c = caobj.Vertex_Berry__Other_Business_Names__c ;
                                }
                                 if(caobj.Vertex_Berry__Business_Structure__c != null)
                                 {
                                    objCreditInfo.Business_Structure__c = caobj.Vertex_Berry__Business_Structure__c ;
                                 }
                                 if(caobj.Vertex_Berry__Date_of_Incorporation__c != null)
                                 {
                                    objCreditInfo.Date_of_Incorporation__c = caobj.Vertex_Berry__Date_of_Incorporation__c ;
                                 }
                                 if(caobj.Vertex_Berry__State_of_Incorporation__c != null)
                                 {
                                    objCreditInfo.State_of_Incorporation__c = caobj.Vertex_Berry__State_of_Incorporation__c ;
                                 } 
                                 if(caobj.Vertex_Berry__Federal_Tax_ID_Number__c != null)
                                 {
                                    objCreditInfo.Federal_Tax_ID_Number__c = caobj.Vertex_Berry__Federal_Tax_ID_Number__c ;
                                 }
                                 if(caobj.Vertex_Berry__Cell_Phone_New__c != null)
                                 {
                                    objCreditInfo.Cell_Phone_New__c = caobj.Vertex_Berry__Cell_Phone_New__c ;
                                 }
                                 if(caobj.Vertex_Berry__PIA__c != null)
                                 {
                                    objCreditInfo.PIA__c = caobj.Vertex_Berry__PIA__c ;
                                 }
                                 
                                 if(caobj.Vertex_Berry__Annual_Sales__c != null)
			                     {
			                        objCreditInfo.Annual_Sales__c = caobj.Vertex_Berry__Annual_Sales__c ;
			                     } 
			                     if(caobj.Vertex_Berry__AvailableCredit__c != null)
			                     {
			                        objCreditInfo.AvailableCredit__c = caobj.Vertex_Berry__AvailableCredit__c ;
			                     } 
			                     if(caobj.Vertex_Berry__TotalBalance__c != null)
			                     {
			                        objCreditInfo.TotalBalance__c = caobj.Vertex_Berry__TotalBalance__c ;
			                     } 
			                     if(caobj.Vertex_Berry__Business_Failure_National_Percentage__c != null)
			                     { 
			                        objCreditInfo.Business_Failure_National_Percentage__c = caobj.Vertex_Berry__Business_Failure_National_Percentage__c ;
			                     }
			                     if(caobj.Vertex_Berry__Business_Failure_Score__c != null)
			                     { 
			                        objCreditInfo.Business_Failure_Score__c = caobj.Vertex_Berry__Business_Failure_Score__c;
			                     } 
			                     if(caobj.Vertex_Berry__Failure_Rate_National_Average__c != null)
			                     { 
			                        objCreditInfo.Failure_Rate_National_Average__c = caobj.Vertex_Berry__Failure_Rate_National_Average__c;
			                     }
			                     if(caobj.Vertex_Berry__Failure_Rate_Within_Business_Failure_Ris__c != null)
			                     { 
			                        objCreditInfo.Failure_Rate_Within_Business_Failure_Ris__c = caobj.Vertex_Berry__Failure_Rate_Within_Business_Failure_Ris__c;
			                     }
			                     if(caobj.Vertex_Berry__Business_Failure_Risk_Class_and_Desc__c != null)
			                     { 
			                        objCreditInfo.Business_Failure_Risk_Class_and_Desc__c = caobj.Vertex_Berry__Business_Failure_Risk_Class_and_Desc__c;
			                     }
			                     if(caobj.Vertex_Berry__Judgments_number__c != null)
			                     { 
			                        objCreditInfo.Judgments_number__c = caobj.Vertex_Berry__Judgments_number__c;
			                     }  
                            }
                            update lstExistingca;
                        } 
                        /*if(caobj.Vertex_Berry__PIA__c != null)
                        {
                            if(caobj.Vertex_Berry__Customer_Information__c != null )
                            {
                                lstcust = [Select Id from Vertex_Berry__BSureC_Customer_Basic_Info__c where Id =: caobj.Vertex_Berry__Customer_Information__c];
                            }   
                            if(lstcust != null && lstcust.size() > 0)
                            {
                                for(Vertex_Berry__BSureC_Customer_Basic_Info__c custobj : lstcust)
                                {
                                    custobj.Vertex_Berry__PIA__c = caobj.Vertex_Berry__PIA__c;
                                }
                                update lstcust;
                            }
                            
                        }  */  
                    }
            }   
        }
    }
}