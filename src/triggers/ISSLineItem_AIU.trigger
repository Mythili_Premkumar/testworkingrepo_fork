trigger ISSLineItem_AIU on ISS_Line_Item__c (after Insert, after update) {
	if(trigger.isInsert) { 
		ISSLineItemHandlerController.onAfterInsert(trigger.new);
	} else if(trigger.isUpdate) {
		ISSLineItemHandlerController.onAfterUpdate(trigger.new);
	} 
}