trigger checkforAutoPayPymtMethod on pymt__Payment_Method__c (before delete) {
// assumption to only check for 
try {
	user u=[Select contactid, isPortalEnabled, name from user where id=:userinfo.getUserid()];

	if (u.isPortalEnabled) {
	
		contact c = [select id, agreed_to_autobill__c from Contact where id =: u.ContactId ];
		
		if (c.agreed_to_autobill__c) {
			for (pymt__Payment_Method__c tmpPM : Trigger.old) {
				if (tmpPM.pymt__Contact__c == c.id && tmpPM.pymt__Default__c) {
					tmpPM.addError('The system indicates you are on AutoBill.  You cannot delete a Default Payment Method');
					return;
				}
			}
		}
	}
	}
	catch(system.exception ex) {
		system.debug('Linvio Error Message:  ' + ex.getMessage() + ' Line: '+ex.getLineNumber());
	}
}