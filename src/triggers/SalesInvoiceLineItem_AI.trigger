trigger SalesInvoiceLineItem_AI on c2g__codaInvoiceLineItem__c (after insert)
{
     if (trigger.isAfter) 
     {
        if (trigger.isInsert) 
        {
               SalesInvoiceLineItemHandlerController.onAfterInsert(trigger.new);
        }
     }
}