trigger Contact_BIUAIU on Contact (before insert, before update, after insert,after update) {    
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.contactObjectName)) {
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                ContactHandlerController.onBeforeInsert(trigger.new);
            } else if(trigger.isUpdate) {
                ContactHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
        
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                ContactHandlerController.onAfterInsert(trigger.new);
            } else if (trigger.isUpdate) {
                ContactHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
            }
        }
    }
}