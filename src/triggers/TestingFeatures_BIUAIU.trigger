trigger TestingFeatures_BIUAIU on Testing_Features__c (before update, after update) {

    for(Testing_Features__c ite : trigger.new) {
        if(trigger.isbefore) {
            system.debug('Before : '+ ite.DFF_Account__c);
            ite.Data_Fulfillment_Form__c = 'a23K0000000X74k';
            system.debug('Before 1 : '+ ite.DFF_Account__c);
        }
        else {
            system.debug('After: '+ ite.DFF_Account__c);
        }
    }

}