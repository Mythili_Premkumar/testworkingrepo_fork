trigger SmartStreetLeadVerification on lead(before insert,before update,after insert,after update) {
 
  
  if(trigger.isBefore){
      if(trigger.isupdate){
      for(lead lead_obj:Trigger.New){
          if(updateHelper.inFutureContext==False){
          if(lead_obj.state!=trigger.oldmap.get(lead_obj.id).state || lead_obj.city!=trigger.oldmap.get(lead_obj.id).city || lead_obj.Street!=trigger.oldmap.get(lead_obj.id).Street || lead_obj.postalcode!=trigger.oldmap.get(lead_obj.id).postalcode ){
               
               lead_obj.street_status__c='';
              
                lead_obj.Latitude__latitude__s=Double.ValueOf(00.000);
                lead_obj.Latitude__longitude__s=Double.ValueOf(00.000);
                lead_obj.County_FIPS_Code__c= '';
                lead_obj.County_Name__c ='';
                lead_obj.Geolocation_Precision__c ='';
                lead_obj.Daylight_Savings_Observed__c ='' ;
                lead_obj.Delivery_Point_Validation_Code__c = '';
                lead_obj.Type_of_Zip_Code__c='';
                lead_obj.Delivery_Point_Vacant__c='';
                lead_obj.Address_Changes_Made_Codes__c ='';
                lead_obj.Residential_Delivery_Indicator__c=''; 
                lead_obj.LACSLink_Code__c='';
                lead_obj.LACSLink_Match_Indicator__c='';
                lead_obj.SuiteLink_Match__c='';
                lead_obj.Building_Default_Indicator__c='';
                lead_obj.POSTNET_Barcode__c='';
                lead_obj.Private_Mailbox_Unit_Designator__c='';
                lead_obj.Private_Mailbox_Number__c='';
                lead_obj.Uses_Commercial_Mail_Receiving_Agency__c='';
                lead_obj.DPV_Reason_Code_s__c='';
                lead_obj.Type_of_Address__c='';
                lead_obj.Address_is_Active__c ='';
                lead_obj.Address_Not_Ready_for_Mail__c='';
                lead_obj.UTC_Offset__c ='';
                lead_obj.Carrier_Route__c = ''; 
                lead_obj.Addressee__c='';
                lead_obj.Time_Zone__c='';
              System.debug('Vertex-SmartyStreets**************isUpdate1*******************'+Trigger.New);
          }
          else
          {
           if(trigger.oldmap.get(lead_obj.id).Do_Not_Validate_Address__c==true){
             
             } 
             else{         
               
                if(lead_obj.Do_Not_Validate_Address__c==true)
                {
                lead_obj.street_status__c='';
                
                lead_obj.Latitude__latitude__s=Double.ValueOf(0.00);
                lead_obj.Latitude__longitude__s=Double.ValueOf(0.00);
                lead_obj.County_FIPS_Code__c= '';
                lead_obj.County_Name__c ='';
                lead_obj.Geolocation_Precision__c ='';
                lead_obj.Daylight_Savings_Observed__c ='' ;
                lead_obj.Delivery_Point_Validation_Code__c = '';
                lead_obj.Type_of_Zip_Code__c='';
                lead_obj.Delivery_Point_Vacant__c='';
                lead_obj.Address_Changes_Made_Codes__c ='';
                lead_obj.Residential_Delivery_Indicator__c=''; 
                lead_obj.LACSLink_Code__c='';
                lead_obj.LACSLink_Match_Indicator__c='';
                lead_obj.SuiteLink_Match__c='';
                lead_obj.Building_Default_Indicator__c='';
                lead_obj.POSTNET_Barcode__c='';
                lead_obj.Private_Mailbox_Unit_Designator__c='';
                lead_obj.Private_Mailbox_Number__c='';
                lead_obj.Uses_Commercial_Mail_Receiving_Agency__c='';
                lead_obj.DPV_Reason_Code_s__c='';
                lead_obj.Type_of_Address__c='';
                lead_obj.Address_is_Active__c ='';
                lead_obj.Address_Not_Ready_for_Mail__c='';
                lead_obj.UTC_Offset__c ='';
                lead_obj.Carrier_Route__c = ''; 
                lead_obj.Addressee__c='';
                lead_obj.Time_Zone__c='';
                }
            }
              System.debug('Vertex-SmartyStreets**************isUpdate2--********************'+Trigger.New);
          }
          }
        }
      }
  }
  
   if(trigger.isAfter){
    if(trigger.isInsert){
     if(updateHelper.inFutureContext==False){
        for(lead lead_obj: Trigger.New){
            System.debug('Vertex-SmartyStreets***************Dont validdate trigger********************'+lead_obj.Do_Not_Validate_Address__c);
            if(lead_obj.Do_Not_Validate_Address__c==false){
             SmartStreetLeadVerification.updateLead(lead_obj.Id);
            System.debug('Vertex-SmartyStrees***************Dont validdate trigger********************'+lead_obj.Do_Not_Validate_Address__c);
            }
        }
        System.debug('Vertex-SmartyStreets***************isAfter1-isInsert********************'+Trigger.New);
        }
    }
    if(trigger.isUpdate){
     if(updateHelper.inFutureContext==False){
        for(lead lead_obj: Trigger.New){
                if(lead_obj.Do_Not_Validate_Address__c==false){
                    System.debug('Vertex-SmartyStreets****************trigger.isUpdate**************************');
                      if(lead_obj.state!=trigger.oldmap.get(lead_obj.id).state || lead_obj.city!=trigger.oldmap.get(lead_obj.id).city || lead_obj.Street!=trigger.oldmap.get(lead_obj.id).Street || lead_obj.postalcode!=trigger.oldmap.get(lead_obj.id).postalcode ){
                             SmartStreetLeadVerification.updateLead(lead_obj.Id);
                              System.debug('Vertex-SmartyStreets*************trigger.isUpdate-if******************');
                       }
                       else
                       {
                              System.debug('Vertex-SmartyStreets*************trigger.isUpdate-else******************'+trigger.oldmap.get(lead_obj.id).Do_Not_Validate_Address__c);
                             if(trigger.oldmap.get(lead_obj.id).Do_Not_Validate_Address__c==true){
                             SmartStreetLeadVerification.updateLead(lead_obj.Id);
                       }
                   }
           }
        }
        System.debug('Vertex-SmartyStreets***************isAfter2-isUpdate********************'+Trigger.New);
        }
      }
  }
  
  
}