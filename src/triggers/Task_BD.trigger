/****************************
Created By : Sathishkumar Periyasamy (speriyasamy2@csc.com)
Created Date : 11/22/2012 1:59 AM
Last Modified By : Christopher W. Roberts Ph.D (croberts24@csc.com)
Last Modified Date : 11/27/2012 4:12 PM
Trigger Status : Active
Use of this Trigger : To prevent the deletion from the Activity History & Task object.
***************************/
trigger Task_BD on Task (before delete) {
  for(Task objTask : Trigger.old) {
     objTask.adderror('Task '+objTask.Subject+' Cannot be Deleted');
  }
}