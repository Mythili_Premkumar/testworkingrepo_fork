trigger PotentialDupeListingLookup on CRMfusionDBR101__Potential_Duplicate__c (before insert, before update) {
    for (CRMfusionDBR101__Potential_Duplicate__c pd : Trigger.new){
        if (pd.CRMfusionDBR101__Scenario_Type__c == 'Listing__c'){
            pd.Listing__c = pd.CRMfusionDBR101__Generic_Object__c;
        }
    }
}