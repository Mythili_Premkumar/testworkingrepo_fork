trigger NationalStagingOrderSet_BAIUD on National_Staging_Order_Set__c (after insert, after update, before insert, before update) { 
    if (trigger.isInsert) {
    	if(trigger.isBefore) {
    		NationalStagingOrderSetHandlerController.onBeforeInsert(trigger.new);
    	}
    }
}