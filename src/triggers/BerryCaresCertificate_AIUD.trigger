trigger BerryCaresCertificate_AIUD on Berry_Cares_Certificate__c (after delete, after insert, after update) {
	if(trigger.isInsert || trigger.isUpdate) {
		BerryCaresCertificate_AIUDTrigger.createCertificate(trigger.new);
	}
}