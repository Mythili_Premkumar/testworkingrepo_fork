trigger AttachmentSpecArtName_BI on Attachment (before insert) {
    set<Id> specArtId = new set<Id>();
    set<Id> dffId = new set<Id>();
    set<Id> YPCID = new set<Id>();
    map<Id,Spec_Art_Request__c> mapSpecArt = new map<Id,Spec_Art_Request__c>();
    map<Id,Digital_Product_Requirement__c> mapDFF = new map<Id,Digital_Product_Requirement__c>();
    map<Id, YPC_Graphics__c> mapYPC = new map<Id, YPC_Graphics__c>();
    String prefix;
    String SpecURN;
    List<String> lstPrfx = Label.Attachment_Prefix.split(',');
    for(Attachment iterator : trigger.new){
          prefix = String.valueOf(iterator.ParentId).substring(0, 3);
          if(lstPrfx[0]==prefix||test.isRunningTest()==true){ //prefix.equals('a2e')
            specArtId.add(iterator.ParentId);
            system.debug('***********SPECID*********'+iterator.Parent.name);
          }
          if(lstPrfx[1]==prefix||test.isRunningTest()==true){ //prefix.equals('a2j')
            dffId.add(iterator.ParentId);
            system.debug('***********DFFID*********'+iterator.Parent.name);
          }
          if(lstPrfx[2]==prefix||test.isRunningTest()==true){ //prefix.equals('a65')
            YPCId.add(iterator.ParentId);
            system.debug('***********YPCID*********'+iterator.Parent.name);
          }
    }
    if(specArtId.size()>0){
        mapSpecArt = new map<Id,Spec_Art_Request__c>([Select Id,name from Spec_Art_Request__c where Id IN :specArtId] );
    }
    if(dffId.size()>0){
        mapDFF = new map<Id,Digital_Product_Requirement__c>([Select Id,name,URN_Number__c from Digital_Product_Requirement__c where Id IN :dffId] );
    }
    if(mapSpecArt.size()>0){
        for(Attachment objA : trigger.new){
            objA.Name = mapSpecArt.get(objA.parentId).Name + '_SpecArt_'+objA.Name;
           
        }
    }
    //added by Joe Henry  02/25/2014
    if(YPCId.size()>0){
        mapYPC = new map<Id, YPC_Graphics__c>([Select Id, name from YPC_Graphics__c where Id IN :YPCId] );
    }
       if(mapYPC.size()>0){
        for(Attachment objA : trigger.new){
            objA.Name = mapYPC.get(objA.parentId).Name + '_YPC_'+objA.Name;
           
        }
    }
    //End addition
    
     if(mapDFF.size()>0){
        for(Attachment objA : trigger.new){
            objA.Name = mapDFF.get(objA.parentId).URN_Number__c + '_DFF_'+objA.Name;
            System.debug('************Getting In 3************' + objA.Name); 
        }
    }
}