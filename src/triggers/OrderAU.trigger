trigger OrderAU on Order__c (after update) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.orderObjectName)) {
        if(trigger.isUpdate && trigger.isAfter) {
            OrderHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
}