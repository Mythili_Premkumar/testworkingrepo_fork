trigger UpdateOLI_P4PTransactionLog_AIU on P4P_Transaction__c (after insert,after update) 
{
 Set<ID> OLIIDs=new Set<ID>();
 Set<Order_Line_Items__c> FinalOLI_for_Update=new Set<Order_Line_Items__c> ();
List<Order_Line_Items__c> FinalOLI=new List<Order_Line_Items__c> ();
 for(P4P_Transaction__c P4Ps: trigger.new)
 {
  OLIIDs.add(P4Ps.Order_Line_Item__c);
 }
 
 if(OLIIDs.size()>0)
{
for(Order_Line_Items__c olis:[Select id,P4P_Billing__c,P4P_Current_Billing_Clicks_Leads__c,P4P_Current_Months_Clicks_Leads__c,UnitPrice__c,Service_End_Date__c,Service_Start_Date__c,P4P_Price_Per_Click_Lead__c,(Select id,Total_Billable_Calls__c,Order_Line_Item__c,Date_of_Click_Call_Lead__c from P4P_Transactions__r) from Order_Line_Items__c where id in: OLIIDs])
{
Double countTotalBillCalls=0.0;   
Double countThisMonthlyCalls=0.0;
Integer CurrentMonth=Date.Today().month(); 
for(P4P_Transaction__c ps:olis.P4P_Transactions__r)
   {

     if(ps.Date_of_Click_Call_Lead__c>=olis.Service_Start_Date__c && ps.Date_of_Click_Call_Lead__c<=olis.Service_End_Date__c)
     {
     countTotalBillCalls=countTotalBillCalls+ ps.Total_Billable_Calls__c;     
     }
     if(ps.Date_of_Click_Call_Lead__c.month()==CurrentMonth)
     {
       countThisMonthlyCalls=countThisMonthlyCalls+ps.Total_Billable_Calls__c;
     }
     
     
   }

olis.P4P_Current_Billing_Clicks_Leads__c=countTotalBillCalls;
olis.P4P_Current_Months_Clicks_Leads__c=countThisMonthlyCalls;
//updating the Sales Price in the Order Line Item
if(olis.P4P_Billing__c == True && olis.P4P_Current_Billing_Clicks_Leads__c != null && olis.P4P_Price_Per_Click_Lead__c != null){
   // olis.UnitPrice__c = olis.P4P_Current_Billing_Clicks_Leads__c * olis.P4P_Price_Per_Click_Lead__c;
  //  olis.Full_Rate__c= olis.P4P_Current_Billing_Clicks_Leads__c * olis.P4P_Price_Per_Click_Lead__c;
}
FinalOLI_for_Update.add(olis);
}

if(FinalOLI_for_Update.size()>0)
{
        FinalOLI.addall(FinalOLI_for_Update);
        update FinalOLI;
}        
} 
 
}