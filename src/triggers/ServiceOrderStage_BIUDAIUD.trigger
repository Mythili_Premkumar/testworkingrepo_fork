trigger ServiceOrderStage_BIUDAIUD on Service_Order_Stage__c (before Insert, before Update) {
      if(trigger.isBefore) {
            if(trigger.isInsert) {
                ServiceOrderstageHandlerController.onBeforeInsert(trigger.new);
            }
            else if(trigger.isUpdate) {
                ServiceOrderstageHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
}